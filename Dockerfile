FROM openjdk:8-jre-alpine
RUN apk add --update tzdata
VOLUME /tmp
EXPOSE 8443 8080
ARG JAR_FILE
ENV TZ=Europe/Kiev
ENV API_JAR=${JAR_FILE}
COPY target/${API_JAR} ${API_JAR}
ENTRYPOINT java -jar /${API_JAR}
