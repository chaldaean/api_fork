== User favorite restaurants

=== Add restaurant

.Request
include::{snippets}/user-flow/favourite-restaurant-page-test/add-restaurant-to-user-favorites/http-request.adoc[]

.Request headers
include::{snippets}/user-flow/favourite-restaurant-page-test/add-restaurant-to-user-favorites/request-headers.adoc[]

.Response
include::{snippets}/user-flow/favourite-restaurant-page-test/add-restaurant-to-user-favorites/http-response.adoc[]

=== Delete restaurant
.Request
include::{snippets}/user-flow/favourite-restaurant-page-test/delete-liked-restaurant-to-user/http-request.adoc[]

.Request headers
include::{snippets}/user-flow/favourite-restaurant-page-test/delete-liked-restaurant-to-user/request-headers.adoc[]

.Response
include::{snippets}/user-flow/favourite-restaurant-page-test/delete-liked-restaurant-to-user/http-response.adoc[]
