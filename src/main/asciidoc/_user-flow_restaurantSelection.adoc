== Restaurants selections and articles

=== Get restaurant selections
*Get all restaurant selections*

.Request
include::{snippets}/user-flow/restaurant-selection-page-test/all-restaurants-from-selections/http-request.adoc[]

.Request headers
include::{snippets}/user-flow/restaurant-selection-page-test/all-restaurants-from-selections/request-headers.adoc[]

.Request parameters
include::{snippets}/user-flow/restaurant-selection-page-test/all-restaurants-from-selections/request-parameters.adoc[]

.Response
include::{snippets}/user-flow/restaurant-selection-page-test/all-restaurants-from-selections/http-response.adoc[]

.Response payload
include::{snippets}/user-flow/restaurant-selection-page-test/all-restaurants-from-selections/response-fields.adoc[]

*Sorting and paging*
Sorting can be done by multiple fields in descending or ascending order. To get the results page by page specify the page size
and page number.

*Filtration*
Filtration can be done by multiple fields and nested properties paths (logical OR is assumed).

=== Get restaurants from selection

.Request
include::{snippets}/user-flow/restaurant-selection-page-test/restaurant-selections-feed/http-request.adoc[]

.Request headers
include::{snippets}/user-flow/restaurant-selection-page-test/restaurant-selections-feed/request-headers.adoc[]

.Request parameters
include::{snippets}/user-flow/restaurant-selection-page-test/restaurant-selections-feed/request-parameters.adoc[]

.Response
include::{snippets}/user-flow/restaurant-selection-page-test/restaurant-selections-feed/http-response.adoc[]

.Response payload
include::{snippets}/user-flow/restaurant-selection-page-test/restaurant-selections-feed/response-fields.adoc[]


=== Get articles

.Request
include::{snippets}/user-flow/restaurant-selection-page-test/articles-feed/http-request.adoc[]

.Request headers
include::{snippets}/user-flow/restaurant-selection-page-test/articles-feed/request-headers.adoc[]

.Request parameters
include::{snippets}/user-flow/restaurant-selection-page-test/articles-feed/request-parameters.adoc[]

.Response
include::{snippets}/user-flow/restaurant-selection-page-test/articles-feed/http-response.adoc[]

.Response payload
include::{snippets}/user-flow/restaurant-selection-page-test/articles-feed/response-fields.adoc[]

*Sorting and paging*
Sorting can be done by multiple fields in descending or ascending order. To get the results page by page specify the page size
and page number.

*Filtration*
Filtration can be done by multiple fields and nested properties paths (logical OR is assumed).
