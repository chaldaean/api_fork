== Register user
https://www.evernote.com/shard/s249/sh/076a6237-4b37-44e8-aa09-9d0f3bf77f51/95bef33507e4e8ec

*All required fields are valid*

.Request
include::{snippets}/user-flow/registration-page-test/register-new-user-in-application/http-request.adoc[]

.Request payload
include::{snippets}/user-flow/registration-page-test/register-new-user-in-application/request-fields.adoc[]

.Response
include::{snippets}/user-flow/registration-page-test/register-new-user-in-application/http-response.adoc[]

*Error. Required fields are empty*

.Request
include::{snippets}/user-flow/registration-page-test/register-new-user-empty-fields/http-request.adoc[]

.Response
include::{snippets}/user-flow/registration-page-test/register-new-user-empty-fields/http-response.adoc[]

.Response payload
include::{snippets}/user-flow/registration-page-test/register-new-user-empty-fields/response-fields.adoc[]

*Error. User already exists*

.Request
include::{snippets}/user-flow/registration-page-test/register-new-user-login-already-exist/http-request.adoc[]

.Response
include::{snippets}/user-flow/registration-page-test/register-new-user-login-already-exist/http-response.adoc[]

.Response payload
include::{snippets}/user-flow/registration-page-test/register-new-user-login-already-exist/response-fields.adoc[]
