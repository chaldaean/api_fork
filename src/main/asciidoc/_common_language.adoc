== Language

=== Get available languages

.Request
include::{snippets}/common/user-language-test/get-languages/http-request.adoc[]

.Response
include::{snippets}/common/user-language-test/get-languages/http-response.adoc[]

.Response payload
include::{snippets}/common/user-language-test/get-languages/response-fields.adoc[]

=== Get the language of the current user

.Request
include::{snippets}/common/user-language-test/get-language-of-current-user-if-set/http-request.adoc[]

.Request headers
include::{snippets}/common/user-language-test/get-language-of-current-user-if-set/request-headers.adoc[]

.Response
include::{snippets}/common/user-language-test/get-language-of-current-user-if-set/http-response.adoc[]

.Response payload
include::{snippets}/common/user-language-test/get-language-of-current-user-if-set/response-fields.adoc[]

=== Get the language of the current user (language is not set)

.Request
include::{snippets}/common/user-language-test/get-language-of-current-user-if-not-set/http-request.adoc[]

.Request headers
include::{snippets}/common/user-language-test/get-language-of-current-user-if-not-set/request-headers.adoc[]

.Response
include::{snippets}/common/user-language-test/get-language-of-current-user-if-not-set/http-response.adoc[]

.Response payload
include::{snippets}/common/user-language-test/get-language-of-current-user-if-not-set/response-fields.adoc[]

=== Set the language of the current user
.Request
include::{snippets}/common/user-language-test/set-language-for-current-user/http-request.adoc[]

.Request headers
include::{snippets}/common/user-language-test/set-language-for-current-user/request-headers.adoc[]

.Request payload
include::{snippets}/common/user-language-test/set-language-for-current-user/request-body.adoc[]



