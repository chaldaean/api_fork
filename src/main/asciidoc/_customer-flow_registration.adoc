== Register customer

*All required fields are valid*

.Request
include::{snippets}/customer-flow/registration-page-test/register-new-customer-in-application/http-request.adoc[]

.Request payload
include::{snippets}/customer-flow/registration-page-test/register-new-customer-in-application/request-fields.adoc[]

.Response
include::{snippets}/customer-flow/registration-page-test/register-new-customer-in-application/http-response.adoc[]

*Error. Required fields are empty*

.Request
include::{snippets}/customer-flow/registration-page-test/register-new-customer-empty-fields/http-request.adoc[]

.Response
include::{snippets}/customer-flow/registration-page-test/register-new-customer-empty-fields/http-response.adoc[]

.Response payload
include::{snippets}/customer-flow/registration-page-test/register-new-customer-empty-fields/response-fields.adoc[]


*Error. Customer already exists*

.Request
include::{snippets}/customer-flow/registration-page-test/register-new-customer-login-already-exist/http-request.adoc[]

.Response
include::{snippets}/customer-flow/registration-page-test/register-new-customer-login-already-exist/http-response.adoc[]

.Response payload
include::{snippets}/customer-flow/registration-page-test/register-new-customer-login-already-exist/response-fields.adoc[]
