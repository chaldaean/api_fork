package com.stol.service.statistic;

import com.stol.model.statistic.Screen;
import com.stol.model.user.User;
import com.stol.repository.ReservationRepository;
import com.stol.repository.ReviewRepository;
import com.stol.repository.UserSystemInformationRepository;
import com.stol.repository.statistic.ScreenRepository;
import com.stol.service.user.UserService;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class StatisticService {
    private final UserService userService;
    private final ReservationRepository reservationRepository;
    private final ReviewRepository reviewRepository;
    private final UserSystemInformationRepository userSystemInformationRepository;
    private final ScreenRepository screenRepository;

    public StatisticService(UserService userService, ReservationRepository reservationRepository, ReviewRepository reviewRepository,
                            UserSystemInformationRepository userSystemInformationRepository, ScreenRepository screenRepository) {
        Objects.requireNonNull(userService);
        Objects.requireNonNull(reservationRepository);
        Objects.requireNonNull(reviewRepository);
        Objects.requireNonNull(userSystemInformationRepository);
        Objects.requireNonNull(screenRepository);
        this.userService = userService;
        this.reservationRepository = reservationRepository;
        this.reviewRepository = reviewRepository;
        this.userSystemInformationRepository = userSystemInformationRepository;
        this.screenRepository = screenRepository;
    }

    public long numberOfReservations(LocalDate date) {
        return reservationRepository.numberOfReservations(date.atStartOfDay());
    }

    public long numberOfReviews(LocalDate date) {
        return reviewRepository.numberOfReviews(date.atStartOfDay());
    }

    public long numberOfActiveUsers(LocalDate date) {
        return userSystemInformationRepository.numberOfActiveUsers(date.atStartOfDay());
    }

    public void updateScreenNumber() {
        User user = userService.getLoggedInUser();
        if (Objects.nonNull(user)) {
            Optional<Screen> screens = screenRepository.findByUserIdAndDate(user.getId(), LocalDate.now());
            if (screens.isPresent()) {
                Screen screen = screens.get();
                screen.setScreens(screen.getScreens() + 1);
                screenRepository.save(screen);
            } else {
                Screen screen = new Screen();
                screen.setUserId(user.getId());
                screenRepository.save(screen);
            }
        }
    }

    public long averageNumberOfScreens(LocalDate date) {
        List<Integer> screens = screenRepository.numberOfScreensByUsers(date);

        if(screens.isEmpty()) {
            return 0;
        }

        int allScreens = screens.stream().mapToInt(s -> s).sum();
        return allScreens/screens.size();
    }
}
