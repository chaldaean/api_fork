package com.stol.service.telegram;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.SendMessage;
import com.stol.model.Reservation;
import org.springframework.beans.factory.annotation.Value;

public class TelegramNotifier {
    private String token = "884765601:AAF8IXkG8PCIfecEPHfDdWXMjp6ZRXN5iOs";
    private long chatId = -373650105;
    private TelegramBot bot = new TelegramBot(token);

    @Value("${stol.telegram.enable}")
    public boolean IS_TELEGRAM_ENABLE;

    public void sendNewReservationNotification(Reservation reservation) {
        if (IS_TELEGRAM_ENABLE) {
            String text = "Ресторан: " + reservation.getRestaurant().getName() + "\n" +
                    "Время: " + reservation.getDateTime().toLocalDate() + " " + reservation.getDateTime().toLocalTime() + "\n" +
                    "Количество людей: " + reservation.getGuestNumber();
            bot.execute(new SendMessage(chatId, text));
        }
    }
}
