package com.stol.service;

import com.stol.exception.CreateDirectoryException;
import com.stol.exception.SaveFileException;
import com.stol.model.MediaFile;
import com.stol.repository.MediaFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

public class FileService {
    @Value("${stol.file.upload-path}")
    private String uploadPath;
    @Value("${stol.file.url-path}")
    private String urlPath;

    Logger logger = LoggerFactory.getLogger(FileService.class);

    private final MediaFileRepository fileRepository;

    public FileService(MediaFileRepository fileRepository) {
        Objects.requireNonNull(fileRepository);
        this.fileRepository = fileRepository;
    }

    @Transactional
    public MediaFile createAndSaveNewFile(MultipartFile file, String folder) {
        MediaFile f = createNewFileEntity(file);
        f = fileRepository.save(f);
        Path path = Paths.get(uploadPath).resolve(folder).resolve(String.format("%s.%s", f.getName(), f.getExtension()));
        if(Objects.nonNull(path.getParent())) {
            createDirectoryIfNotExist(path.getParent());
        }
        saveFileOnServer(path, file);
        addUrl(folder, f);
        return fileRepository.save(f);
    }

    public void deleteFile(MediaFile mediaFile) {
        String url = mediaFile.getUrl();
        Path path = Paths.get(uploadPath).resolve(url.substring(url.lastIndexOf('/') + 1));
        if(Files.exists(path)) {
            try {
                Files.delete(path);
            } catch (IOException e) {
                logger.error("Error deleting file:", e);
            }
        }
    }

    private MediaFile createNewFileEntity(MultipartFile file) {
        MediaFile entity = new MediaFile();

        String fileName = file.getOriginalFilename();
        if(Objects.nonNull(fileName)) {
            int idx = fileName.lastIndexOf('.');
            if(idx != -1 && idx < fileName.length() - 1) {
                entity.setExtension(fileName.substring(idx + 1));
            } else {
                entity.setExtension("");
            }

        }
        entity.setName(UUID.randomUUID().toString());
        entity.setMediaType(file.getContentType());
        return entity;
    }

    private void createDirectoryIfNotExist(Path path) {
        if (!path.toFile().exists()) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                throw new CreateDirectoryException(path, e);
            }
        }
    }

    private void saveFileOnServer(Path path, MultipartFile file) {
        try {
            Files.write(path, file.getBytes());
        } catch (IOException e) {
            throw new SaveFileException(file.getOriginalFilename(), e);
        }
    }

    private void addUrl(String folder, MediaFile f) {
        if (folder.isEmpty()) {
            f.setUrl(String.format("%s%s.%s", urlPath, f.getName(), f.getExtension()));
        } else {
            f.setUrl(String.format("%s/%s/%s.%s", urlPath, folder, f.getName(), f.getExtension()));
        }
    }
}
