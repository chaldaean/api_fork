package com.stol.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

import java.util.Date;
import java.util.Objects;

public class AuthService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final DefaultTokenServices tokenServices;
    private final TokenStore tokenStore;

    @Value("${stol.oauth2.client-ids}")
    private String[] oauthClientIds;


    public AuthService(DefaultTokenServices tokenServices, TokenStore tokenStore) {
        Objects.requireNonNull(tokenServices);
        Objects.requireNonNull(tokenStore);

        this.tokenServices = tokenServices;
        this.tokenStore = tokenStore;
    }

    public String getAccessToken() {
        if(SecurityContextHolder.getContext().getAuthentication() != null &&
                SecurityContextHolder.getContext().getAuthentication() instanceof OAuth2Authentication) {
            Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
            if(details instanceof OAuth2AuthenticationDetails) {
                return ((OAuth2AuthenticationDetails) details).getTokenValue();
            }
        }

        return null;
    }

    public void logout() {
        String token = getAccessToken();
        if(token != null) {
            tokenServices.revokeToken(token);
        }
    }

    @Scheduled(cron = "0 0 3 1 * ?")
    private void cleanupStaleTokens() {
        logger.debug("Start checking for expired access/refresh tokens");

        for(int i = 0; i < oauthClientIds.length; i++) {
            String clientId = oauthClientIds[i];

            tokenStore.findTokensByClientId(clientId).forEach(token -> {
                if(token.isExpired()) {
                    OAuth2RefreshToken refreshToken = token.getRefreshToken();
                    if (refreshToken != null && refreshToken instanceof DefaultExpiringOAuth2RefreshToken) {
                        DefaultExpiringOAuth2RefreshToken expiringRefreshToken = (DefaultExpiringOAuth2RefreshToken) refreshToken;
                        if (expiringRefreshToken.getExpiration() != null && expiringRefreshToken.getExpiration().before(new Date())) {
                            logger.info("Access token %s and its refresh token %s have expired and will be revoked", token.getValue(), expiringRefreshToken.getValue());
                            tokenServices.revokeToken(token.getValue());
                        }
                    }
                }
            });
        }




    }
}
