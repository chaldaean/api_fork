package com.stol.service.user;

import com.stol.model.user.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import static com.stol.model.property.UserStatus.ENABLE;

public class CurrentUser extends org.springframework.security.core.userdetails.User {
    private Long id;

    public CurrentUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public CurrentUser(User user, boolean canBeLoggedIn) {
        super(user.getCredential().getLogin(), user.getCredential().getPassword(), Objects.equals(user.getStatus(), ENABLE), true, true,
                canBeLoggedIn, new ArrayList<>(user.getRoles()));
        this.id = user.getId();
    }

    public Long getId() {
        return id;
    }
}
