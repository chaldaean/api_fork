package com.stol.service.user;

import com.stol.model.user.User;

import java.util.Objects;

import static com.stol.model.property.UserRole.ROLE_RESTAURANT_ADMIN;
import static com.stol.model.property.UserRole.ROLE_SYSTEM_ADMIN;

public class LoginStaticService {
    public static boolean isSystemAdmin(User user) {
        return Objects.nonNull(user) && user.getRoles().stream().anyMatch(role -> role.getRole().equals(ROLE_SYSTEM_ADMIN));
    }

    public static boolean isRestaurantAdmin(User user) {
        return Objects.nonNull(user) && user.getRoles().stream().anyMatch(role -> role.getRole().equals(ROLE_RESTAURANT_ADMIN));
    }
}
