package com.stol.service.user;

import com.stol.exception.UserAlreadyExist;
import com.stol.exception.UserNotFoundException;
import com.stol.model.AddRestaurantRequest;
import com.stol.model.Language;
import com.stol.model.Restaurant;
import com.stol.model.application.Registration;
import com.stol.model.projection.UserManager;
import com.stol.model.projection.UserPage;
import com.stol.model.projection.UserRoles;
import com.stol.model.user.Credential;
import com.stol.model.user.User;
import com.stol.model.user.UserSystemInformation;
import com.stol.repository.LanguageRepository;
import com.stol.repository.UserRepository;
import com.stol.repository.UserSystemInformationRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import static com.stol.configuration.constant.Messages.*;

public class UserService {
    @Value("${stol.login.max-attempts}")
    private int MAX_LOGIN_ATTEMPTS;
    @Value("${stol.login.cooldown.expiration-time}")
    private long EXPIRATION_TIME_SECONDS;

    private final UserRepository userRepository;
    private final UserSystemInformationRepository userSystemInformationRepository;
    private final LanguageRepository languageRepository;


    public UserService(UserRepository userRepository, UserSystemInformationRepository userSystemInformationRepository,
                       LanguageRepository languageRepository) {
        Objects.requireNonNull(userRepository);
        Objects.requireNonNull(userSystemInformationRepository);
        Objects.requireNonNull(languageRepository);
        this.userRepository = userRepository;
        this.userSystemInformationRepository = userSystemInformationRepository;
        this.languageRepository = languageRepository;
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User createUser(Registration registration) {
        checkIfLoginAlreadyExist(registration);
        User user = new User();
        Credential credential = new Credential();
        credential.setLogin(registration.getLogin());
        credential.setPassword(registration.getPassword());
        user.setCredential(credential);
        user.setPhoneNumber(registration.getLogin());
        user.setName(registration.getName());
        user.setSurname(registration.getSurname());
        user.setEmail(registration.getEmail());

        Optional<Language> language = languageRepository.findByCode(registration.getLanguage());
        if(language.isPresent()) {
            user.setLanguage(language.get());
        }

        if(registration.isCustomer() && StringUtils.isNotBlank(registration.getRestaurantName())) {
            AddRestaurantRequest addRestaurantRequest = new AddRestaurantRequest();
            addRestaurantRequest.setRestaurantName(registration.getRestaurantName());
            user.setAddRestaurantRequest(addRestaurantRequest);
            addRestaurantRequest.setUser(user);
        }

        return user;
    }

    public void saveUserInformation(UserSystemInformation userSystemInformation) {
        userSystemInformationRepository.save(userSystemInformation);
    }

    public User findUserByLogin(String login) {
        User user = userRepository.findUserByCredentialLogin(login);
        if (Objects.isNull(user)) {
            throw new UserNotFoundException(NO_USER_WITH_LOGIN, login);
        }
        return user;
    }

    private User findById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new UserNotFoundException(NO_USER_WITH_ID, id.toString());
        }
        return user.get();
    }

    public UserPage findUserPageByLogin(String login) {
        UserPage user = userRepository.findUserPageByCredentialLogin(login);
        if (Objects.isNull(user)) {
            throw new UserNotFoundException(NO_USER_WITH_LOGIN, login);
        }
        return user;
    }

    public UserRoles findUserRolesByLogin(String login) {
        UserRoles user = userRepository.findUserRolesByCredentialLogin(login);
        if (Objects.isNull(user)) {
            throw new UserNotFoundException(NO_USER_WITH_LOGIN, login);
        }
        return user;
    }

    public UserManager findUserRestaurantManagerByLogin(String login) {
        UserManager user = userRepository.findUserManagerByCredentialLogin(login);
        if (Objects.isNull(user)) {
            throw new UserNotFoundException(NO_USER_WITH_LOGIN, login);
        }
        return user;
    }

    public UserSystemInformation findUserInformationByPhoneNumber(String phoneNumber) throws UsernameNotFoundException {
        UserSystemInformation info = userSystemInformationRepository.findByPhoneNumber(phoneNumber);
        if (Objects.isNull(info)) {
            throw new UserNotFoundException(NO_USER_WITH_PHONE_NUMBER, phoneNumber);
        }
        return info;
    }

    public UserSystemInformation createUserActivationInformation(User user) {
        UserSystemInformation info = new UserSystemInformation();
        info.setPhoneNumber(user.getPhoneNumber());
        info.setActivationCode(ActivationStaticService.createActivationCode());
        info.setActivationAttempts(0);
        info.setActivationCodeCreation(LocalDateTime.now());
        return userSystemInformationRepository.save(info);
    }

    public Optional<UserSystemInformation> findUserSystemInformation(Object principal) {
        try {
            String username = (principal instanceof CurrentUser) ? ((CurrentUser) principal).getUsername() : principal.toString();
            return Optional.of(findUserInformationByPhoneNumber(username));
        } catch (Exception ignore) {
        }
        return Optional.empty();
    }

    public void updateUserSystemInformation(UserSystemInformation info) {
        info.setLastLoginTime(LocalDateTime.now());
        saveUserInformation(info);
    }

    public boolean hasLoginAttempts(UserSystemInformation userSystemInformation) {
        return userSystemInformation.getLoginAttempts() < MAX_LOGIN_ATTEMPTS;
    }

    public boolean isLoginTryCooldownExpired(UserSystemInformation userSystemInformation) {
        long now = Instant.now().getEpochSecond();
        long lastLoginTime = userSystemInformation.getLastLoginTime().toEpochSecond(ZoneOffset.UTC);
        return now - lastLoginTime >= EXPIRATION_TIME_SECONDS;
    }

    public boolean canBeLoggedIn(UserSystemInformation userSystemInformation) {
        return hasLoginAttempts(userSystemInformation) || isLoginTryCooldownExpired(userSystemInformation);
    }

    public int numberOfActivatedAccountsByDate(LocalDate date) {
        return userRepository.numberOfActivatedAccountsByDate(date.atStartOfDay());
    }

    public int numberOfAccountsByDate(LocalDate date) {
        return userRepository.numberOfAccountsByDate(date.atStartOfDay());
    }

    public User getLoggedInUser() {
        if (Objects.nonNull(SecurityContextHolder.getContext().getAuthentication())) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof CurrentUser) {
                return findById(((CurrentUser) principal).getId());
            }
        }
        return null;
    }

    public boolean isRestaurantIsSavedForUser(Restaurant restaurant) {
        final User loggedInUser = getLoggedInUser();
        return Objects.nonNull(loggedInUser) && userRepository.existsByIdAndSavedRestaurants(loggedInUser.getId(), restaurant);
    }

    public void setLanguage(User user, String languageCode) {
        Optional<Language> language = languageRepository.findByCode(languageCode);
        if(language.isPresent()) {
            user.setLanguage(language.get());
        }
    }

    private void checkIfLoginAlreadyExist(Registration registration) {
        String login = registration.getLogin();
        User userInDataBase = userRepository.findUserByCredentialLogin(login);
        if (!Objects.isNull(userInDataBase)) {
            throw new UserAlreadyExist(login);
        }
    }
}
