package com.stol.service.user;

import com.stol.controller.UserActivationController;
import com.stol.controller.model.UserActivation;
import com.stol.model.property.UserRole;
import com.stol.model.user.User;
import com.stol.model.user.UserSystemInformation;
import com.stol.service.sms.SmsSenderService;
import org.springframework.beans.factory.annotation.Value;

import java.util.Objects;

import static com.stol.model.property.UserStatus.NOT_CONFIRMED;

public class UserSmsEventService {
    @Value("${stol.user.activation_via_sms.disable}")
    private boolean DISABLE_USER_ACTIVATION_VIA_SMS;

    private final UserService userService;
    private final SmsSenderService smsSenderService;
    private final UserActivationController userActivationController;

    public UserSmsEventService(UserService userService, SmsSenderService smsSenderService, UserActivationController userActivationController) {
        Objects.requireNonNull(userService);
        Objects.requireNonNull(smsSenderService);
        Objects.requireNonNull(userActivationController);
        this.userService = userService;
        this.smsSenderService = smsSenderService;
        this.userActivationController = userActivationController;
    }

    public void sendActivationCode(User u) {
        if (Objects.equals(u.getStatus(), NOT_CONFIRMED)) {
            UserSystemInformation info = userService.createUserActivationInformation(u);

            boolean suppressActivation = u.getRoles().stream().anyMatch(r -> r.getRole() != UserRole.ROLE_USER);

            if(!suppressActivation) {
                if(!DISABLE_USER_ACTIVATION_VIA_SMS) {
                    smsSenderService.sendSms(u.getPhoneNumber(), info.getActivationCode());
                } else {
                    userActivationController.activateUser(new UserActivation(info.getPhoneNumber(), info.getActivationCode()));
                }
            }
        }
    }
}
