package com.stol.service.user;

import com.stol.model.user.UserSystemInformation;
import org.apache.commons.lang3.RandomUtils;

import java.time.Instant;
import java.time.ZoneOffset;

public class ActivationStaticService {
    private static final long EXPIRATION_TIME_SECONDS = 86400;
    private static final int MAX_RESEND_ATTEMPTS = 2;

    public static String createActivationCode() {
        return String.valueOf(RandomUtils.nextInt(1000, 10000));
    }

    public static boolean isActivationCooldownExpired(UserSystemInformation userInfo) {
        long now = Instant.now().getEpochSecond();
        long activationCodeCreationTime = userInfo.getActivationCodeCreation().toEpochSecond(ZoneOffset.UTC);
        return now - activationCodeCreationTime >= EXPIRATION_TIME_SECONDS;
    }

    public static boolean isResetPasswordCooldownExpired(UserSystemInformation userInfo) {
        long now = Instant.now().getEpochSecond();
        long passwordResetCreationTime = userInfo.getPasswordResetCreation().toEpochSecond(ZoneOffset.UTC);
        return now - passwordResetCreationTime >= EXPIRATION_TIME_SECONDS;
    }

    public static boolean hasResendAttempts(int activationTry) {
        return activationTry < MAX_RESEND_ATTEMPTS;
    }
}
