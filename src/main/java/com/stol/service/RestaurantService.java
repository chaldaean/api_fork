package com.stol.service;

import com.stol.configuration.constant.Localization;
import com.stol.controller.model.MinMaxRestaurantsAverageCheck;
import com.stol.model.HappyHour;
import com.stol.model.Restaurant;
import com.stol.model.Review;
import com.stol.repository.HappyHourRepository;
import com.stol.repository.RestaurantRepository;
import com.stol.repository.ReviewRepository;
import com.stol.service.localization.LocalizationService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Identifiable;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.data.domain.PageRequest.of;

public class RestaurantService {
    private final RestaurantRepository restaurantRepository;
    private final ReviewRepository reviewRepository;
    private final HappyHourRepository happyHourRepository;
    private final LocalizationService localizationService;

    public RestaurantService(RestaurantRepository restaurantRepository, ReviewRepository reviewRepository,
                             HappyHourRepository happyHourRepository, LocalizationService localizationService) {
        Objects.requireNonNull(restaurantRepository);
        Objects.requireNonNull(reviewRepository);
        Objects.requireNonNull(happyHourRepository);
        Objects.requireNonNull(localizationService);

        this.restaurantRepository = restaurantRepository;
        this.reviewRepository = reviewRepository;
        this.happyHourRepository = happyHourRepository;
        this.localizationService = localizationService;
    }

    public Restaurant findById(long restaurantId) {
        Optional<Restaurant> restaurant = restaurantRepository.findById(restaurantId);
        if (restaurant.isPresent()) {
            return restaurant.get();
        }
        throw new ObjectNotFoundException(restaurantId, Restaurant.class.getCanonicalName());
    }

    public List<String> findRestaurantNamesByOwner() {
        List<Restaurant> restaurants = restaurantRepository.findAllForLoggedInUser();
        return Objects.nonNull(restaurants) ? restaurants.stream().map(r -> localizationService.getRestaurant().getName(r))
                .collect(Collectors.toList()) : Collections.emptyList();
    }

    public List<Long> findRestaurantIdsByOwner() {
        List<Restaurant> restaurants = restaurantRepository.findAllForLoggedInUser();
        return Objects.nonNull(restaurants) ? restaurants.stream().map(Restaurant::getId).collect(Collectors.toList()) : Collections.emptyList();
    }

    public MinMaxRestaurantsAverageCheck minMaxRestaurantsAverageCheck() {
        Double[][] averageCheck = restaurantRepository.findMinMaxRestaurantsAverageCheck();
        return new MinMaxRestaurantsAverageCheck(averageCheck[0][0], averageCheck[0][1]);
    }

    public long numberOfAllRestaurantReviews(Restaurant restaurant) {
        return reviewRepository.numberOfAllRestaurantReviews(restaurant);
    }

    public Review latestRestaurantReview(Restaurant restaurant) {
        return reviewRepository.findAllByRestaurant(restaurant, of(0, 1)).stream().findFirst().orElse(null);
    }

    public <T extends Identifiable<Long>> Page<T> findRestaurants(Double avgTicketFrom, Double avgTicketTo, List<Long> cuisines, List<Long> tags,
                                                                  BigDecimal latitude, BigDecimal longitude, String sorting, Pageable pageable, Class<T> clazz) {

        Page<Long> restaurantIds = restaurantRepository.findRestaurants(avgTicketFrom, avgTicketTo, cuisines, tags, latitude,
                longitude, sorting, localizationService.getLocale().getLanguage(), pageable);
        List<T> content = restaurantRepository.findByIdIn(restaurantIds.getContent(), clazz);
        content.sort(Comparator.comparingInt(o -> restaurantIds.getContent().indexOf(o.getId())));
        return new PageImpl<>(content, pageable, restaurantIds.getTotalElements());
    }

    public List<HappyHour> findActiveHappyHours(Restaurant restaurant) {
        return happyHourRepository.findActive(restaurant, LocalDate.now());
    }
}
