package com.stol.service;

import com.stol.model.Reservation;
import com.stol.model.Review;
import com.stol.repository.ReviewRepository;

import java.util.Objects;

public class ReviewService {
    private final ReviewRepository reviewRepository;

    public ReviewService(ReviewRepository reviewRepository) {
        Objects.requireNonNull(reviewRepository);
        this.reviewRepository = reviewRepository;
    }

    public Review findReviewByReservation(Reservation reservation) {
        return reviewRepository.findByReservation(reservation).orElse(null);
    }
}
