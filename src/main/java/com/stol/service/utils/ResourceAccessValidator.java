package com.stol.service.utils;

import com.stol.service.user.CurrentUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Objects;

import static com.stol.model.property.UserRole.ROLE_RESTAURANT_ADMIN;

public class ResourceAccessValidator {
    private static final String LOGIN_PARAMETER = "restaurant.owners.credential.login";

    public boolean isSameLoggedInUser(Authentication authentication, long userId) {
        if (authentication != null && (authentication.getPrincipal() instanceof User)) {
            return Objects.equals(((CurrentUser) authentication.getPrincipal()).getId(), userId);
        }
        return false;
    }

    public boolean isCorrectRestaurantAdmin(Authentication authentication, HttpServletRequest request) {
        if (authentication != null && (authentication.getPrincipal() instanceof User)) {
            CurrentUser principal = (CurrentUser) authentication.getPrincipal();
            Collection<GrantedAuthority> authorities = principal.getAuthorities();
            if (isRestaurantAdmin(authorities)) {
                if (request.getParameterMap().containsKey(LOGIN_PARAMETER)) {
                    String login = request.getParameter(LOGIN_PARAMETER);
                    return Objects.equals(login, principal.getUsername());
                }
            }
        }
        return false;
    }

    private boolean isRestaurantAdmin(Collection<GrantedAuthority> authorities) {
        return Objects.nonNull(authorities) && authorities.stream().anyMatch(role -> role.getAuthority().equals(ROLE_RESTAURANT_ADMIN.name()));
    }
}
