package com.stol.service.utils;

import com.stol.model.Restaurant;
import com.stol.service.RestaurantService;
import com.stol.service.user.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;

import java.util.Objects;

public class RestaurantAccessValidator {
    private final UserService userService;
    private final RestaurantService restaurantService;

    public RestaurantAccessValidator(UserService userService, RestaurantService restaurantService) {
        Objects.requireNonNull(userService);
        Objects.requireNonNull(restaurantService);
        this.userService = userService;
        this.restaurantService = restaurantService;
    }

    public boolean isRestaurantOwner(Authentication authentication, long restaurantId) {
        if (authentication != null && (authentication.getPrincipal() instanceof User)) {
            String username = ((User) authentication.getPrincipal()).getUsername();
            com.stol.model.user.User user = userService.findUserByLogin(username);
            Restaurant restaurant = restaurantService.findById(restaurantId);
            return restaurant.getOwners().contains(user);
        }
        return false;
    }
}
