package com.stol.service.notification;

import com.stol.configuration.constant.Localization;
import com.stol.model.Restaurant;
import com.stol.model.property.ApplicationType;
import com.stol.model.user.User;
import com.stol.service.localization.LocalizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class NotificationService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final List<Notifier> notifiers;
    private final LocalizationService localizationService;
    

    public NotificationService(List<Notifier> notifiers, LocalizationService localizationService) {
        Objects.requireNonNull(notifiers);
        Objects.requireNonNull(localizationService);

        this.notifiers = notifiers;
        this.localizationService = localizationService;
    }

    public void notifyClientAboutNewReservation(User user, Restaurant restaurant) {
        NotificationContext context = new NotificationContext(user, restaurant, ApplicationType.CLIENT_APP);
        MessageContent messageContent = localizeMessage(context,  MessageContent.builder().newReservationForClient());

        sendNotification(context, messageContent);
    }

    public void notifyClientAboutCanceledReservation(User user, Restaurant restaurant) {
        NotificationContext context = new NotificationContext(user, restaurant, ApplicationType.CLIENT_APP);
        MessageContent messageContent = localizeMessage(context, MessageContent.builder().canceledReservationForClient());

        sendNotification(context, messageContent);
    }

    public void notifyUserAboutConfirmedReservation(User user, Restaurant restaurant) {
        NotificationContext context = new NotificationContext(user, restaurant, ApplicationType.USER_APP);
        MessageContent messageContent = localizeMessage(context, MessageContent.builder().confirmedReservationForUser());

        sendNotification(context, messageContent);
    }

    public void notifyUserAboutRejectedReservation(User user, Restaurant restaurant) {
        NotificationContext context = new NotificationContext(user, restaurant, ApplicationType.USER_APP);
        MessageContent messageContent = localizeMessage(context, MessageContent.builder().rejectedReservationForUser());

        sendNotification(context, messageContent);
    }

    public void notifyUserAboutReviewNeeded(User user, Restaurant restaurant) {
        NotificationContext context = new NotificationContext(user, restaurant, ApplicationType.USER_APP, true);
        MessageContent messageContent = localizeMessage(context,  MessageContent.builder().reviewNeededFromUser());

        sendNotification(context, messageContent);
    }

    private void sendNotification(NotificationContext context, MessageContent content) {
        logger.debug("Trying to send notification. {}. {}.", context, content);

        for (Notifier n: notifiers) {
            try {
                n.sendNotification(context,  content);
                if(context.isProcessingStopped()) {
                    return;
                }
            } catch (Exception e) {
                logger.error("Error sending notification. {}. {}.", context, content);
            }
        }
    }

    private MessageContent localizeMessage(NotificationContext context, MessageContent content) {
        Locale locale = Localization.DEFAULT_LOCALE;

        if(context.getUser() != null && context.getUser().getLanguage() != null) {
            locale = context.getUser().getLanguage().getLocale();
        }

        return new MessageContent(localizationService.translate(locale, content.getTitle()), localizationService.translate(locale, content.getBody()));
    }
}
