package com.stol.service.notification.sms;

import com.stol.model.property.ApplicationType;
import com.stol.service.notification.MessageContent;
import com.stol.service.notification.NotificationContext;
import com.stol.service.notification.Notifier;
import com.stol.service.sms.SmsSenderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;


public class SmsNotifier implements Notifier {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final SmsSenderService smsSenderService;

    public SmsNotifier(SmsSenderService smsSenderService) {
        Objects.requireNonNull(smsSenderService);

        this.smsSenderService = smsSenderService;
    }

    @Override
    public void sendNotification(NotificationContext context, MessageContent content) {
        if(!smsSenderService.IS_SMS_ENABLE || context.isSkipSms()) {
            logger.debug("SMS notifications disabled. Skip sending sms notification.");
            return;
        }

        String phoneNumber;
        if(context.getApplicationType() == ApplicationType.CLIENT_APP) {
            phoneNumber = context.getRestaurant().getPhoneNumber();
        } else if(context.getApplicationType() == ApplicationType.USER_APP) {
            phoneNumber = context.getUser().getPhoneNumber();
        } else {
            logger.debug("Not supported application type. {}. Skip sending sms notification.", context);
            return;
        }

        if(StringUtils.isEmpty(phoneNumber)) {
            logger.info("Cannot find phone number. {}. Skip sending sms notification.", context);
            return;
        }

        smsSenderService.sendSms(phoneNumber, content.getBody());

        logger.info("SMS sent. {}. {}.", context, content);
    }
}
