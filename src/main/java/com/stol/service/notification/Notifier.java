package com.stol.service.notification;

public interface Notifier {
    void sendNotification(NotificationContext context, MessageContent content);
}
