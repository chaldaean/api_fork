package com.stol.service.notification.push.message;

import com.google.firebase.messaging.Message;
import com.stol.model.MobileDevice;
import com.stol.service.notification.MessageContent;

import java.util.HashMap;
import java.util.Map;

public class WebPushMessage implements PushMessage {
    @Override
    public Message createPushMessage(MobileDevice mobileDevice, MessageContent content) {
        Map<String, String> data = new HashMap();
        data.put("title", content.getTitle());
        data.put("body", content.getBody());

        return Message.builder().putAllData(data).setToken(mobileDevice.getDeviceToken()).build();
    }
}
