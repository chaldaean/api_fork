package com.stol.service.notification.push;

import org.springframework.beans.factory.annotation.Value;

public class FirebaseProperties {
    @Value("${stol.firebase.account_key}")
    private String firebaseKey;
    @Value("${stol.firebase.database_url}")
    private String databaseUrl;
    @Value("${stol.firebase.project_id}")
    private String projectId;
    @Value("${stol.firebase.enable}")
    private boolean isNotificationEnabled;

    public String getFirebaseKey() {
        return firebaseKey;
    }

    public String getDatabaseUrl() {
        return databaseUrl;
    }

    public String getProjectId() {
        return projectId;
    }

    public boolean isNotificationEnabled() {
        return isNotificationEnabled;
    }
}
