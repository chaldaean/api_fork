package com.stol.service.notification.push.message;

import com.google.firebase.messaging.Message;
import com.stol.model.MobileDevice;
import com.stol.service.notification.MessageContent;

public interface PushMessage {
    Message createPushMessage(MobileDevice mobileDevice, MessageContent content);
}
