package com.stol.service.notification.push;

import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.stol.model.MobileDevice;
import com.stol.service.MobileDeviceService;
import com.stol.service.notification.*;
import com.stol.service.notification.push.message.AndroidPushMessage;
import com.stol.service.notification.push.message.IOSPushMessage;
import com.stol.service.notification.push.message.PushMessage;
import com.stol.service.notification.push.message.WebPushMessage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Optional;


public class PushNotifier implements Notifier {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final MobileDeviceService mobileDeviceService;
    private final PushMessage iOSPushMessage = new IOSPushMessage();
    private final PushMessage androidPushMessage = new AndroidPushMessage();
    private final PushMessage webPushMessage = new WebPushMessage();
    private final FirebaseService firebaseService;


    public PushNotifier(MobileDeviceService mobileDeviceService, FirebaseService firebaseService) {
        Objects.requireNonNull(mobileDeviceService);
        Objects.requireNonNull(firebaseService);
        this.mobileDeviceService = mobileDeviceService;
        this.firebaseService = firebaseService;
    }

    @Override
    public void sendNotification(NotificationContext context, MessageContent content) {
        if(!firebaseService.isNotificationEnabled()) {
            logger.debug("Firebase notifications disabled. Skip sending push notification.");
            return;
        }

        Optional<MobileDevice> mobileDevice = mobileDeviceService.mobileDeviceByUserAndApplicationType(context.getUser(), context.getApplicationType());

        if(!mobileDevice.isPresent()) {
            logger.debug("No information about user device found. {}.  Skip sending push notification.", context);
            return;
        }

        String device = StringUtils.defaultString(mobileDevice.get().getDevice(), "").toLowerCase();

        Message message;
        if(device.contains("iphone") || device.contains("ipod")) {
            message = iOSPushMessage.createPushMessage(mobileDevice.get(), content);
        } else if(device.equals("web")) {
            message = webPushMessage.createPushMessage(mobileDevice.get(), content);
        } else {
            message = androidPushMessage.createPushMessage(mobileDevice.get(), content);
        }

        try {
            firebaseService.sendPushNotificationToDevice(message);
            logger.info("Push notification sent. {}. {}.", context, content);

            if(!device.equals("web")) {
                context.stopProcessing();
            }
        } catch (FirebaseMessagingException e) {
            logger.error("Error sending push notification. {}. {}.  Firebase error: {} {}",
                    context, content, e.getMessage(), e.getErrorCode());
        }
    }
}
