package com.stol.service.notification.push.message;

import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.stol.model.MobileDevice;
import com.stol.service.notification.MessageContent;

public class AndroidPushMessage implements PushMessage {
    @Override
    public Message createPushMessage(MobileDevice mobileDevice, MessageContent content) {
        return Message.builder().setNotification(new Notification(content.getTitle(), content.getBody()))
                .setToken(mobileDevice.getDeviceToken()).build();
    }
}
