package com.stol.service.notification.push;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.Objects;

public class FirebaseService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final FirebaseProperties firebaseProperties;

    public FirebaseService(FirebaseProperties firebaseProperties) {
        Objects.requireNonNull(firebaseProperties);
        this.firebaseProperties = firebaseProperties;

        if (firebaseProperties.isNotificationEnabled()) {
            initFirebase();
        }
    }

    public void sendPushNotificationToDevice(Message message) throws FirebaseMessagingException {
        if (isNotificationEnabled()) {
            String response = FirebaseMessaging.getInstance().send(message);
            logger.info("Firebase response: {}", response);
        } else {
            logger.debug("Firebase notifications disabled. Message will not be sent.");
        }
    }

    boolean isNotificationEnabled() {
        return firebaseProperties.isNotificationEnabled();
    }

    private void initFirebase() {
        try {
            FirebaseApp.initializeApp(new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(new ClassPathResource(firebaseProperties.getFirebaseKey()).getInputStream()))
                    .setDatabaseUrl(firebaseProperties.getDatabaseUrl())
                    .setProjectId(firebaseProperties.getProjectId())
                    .build());
        } catch (IOException e) {
            logger.info("Firebase error: {}", e.getMessage());
        }
    }
}
