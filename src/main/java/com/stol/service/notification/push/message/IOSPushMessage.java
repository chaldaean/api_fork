package com.stol.service.notification.push.message;

import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.ApsAlert;
import com.google.firebase.messaging.Message;
import com.stol.model.MobileDevice;
import com.stol.service.notification.MessageContent;

public class IOSPushMessage implements PushMessage {
    private final String sound = "bell.caf";

    @Override
    public Message createPushMessage(MobileDevice mobileDevice, MessageContent content) {
        ApnsConfig apnsConfig = createIOSConfig(content);
        return Message.builder().setApnsConfig(apnsConfig).setToken(mobileDevice.getDeviceToken()).build();
    }

    private ApnsConfig createIOSConfig(MessageContent content) {
        ApsAlert apsAlert = ApsAlert.builder().setTitle(content.getTitle()).setBody(content.getBody()).build();
        Aps aps = Aps.builder().setAlert(apsAlert).setBadge(1).setSound(sound).build();
        return ApnsConfig.builder().setAps(aps).build();
    }
}
