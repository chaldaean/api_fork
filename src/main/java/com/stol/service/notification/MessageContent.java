package com.stol.service.notification;

public class MessageContent {
    private final String title;
    private final String body;

    public MessageContent(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    @Override
    public String toString() {
        return String.format("Message (title: %s, body: %s)", title, body);
    }

    public static MessageContentBuilder builder() {
        return new MessageContentBuilder();
    }

    public static class MessageContentBuilder {

        public MessageContent newReservationForClient() {
            return new MessageContent("Новый разерв!", "У Вас новая заявка на бронирование.");
        }

        public MessageContent canceledReservationForClient() {
            return new MessageContent("Бронирование отменено!", "Клиент отменил свое бронирование.");
        }

        public MessageContent rejectedReservationForUser() {
            return new MessageContent("Ваше бронирование отклонено!", "К сожалению, у нас нету свободных столиков :(");
        }

        public MessageContent confirmedReservationForUser() {
            return new MessageContent("Готово! \uD83D\uDECE", "Ваше бронирование подтвержденно.");
        }

        public MessageContent reviewNeededFromUser() {
            return new MessageContent("Оставьте отзыв!", "Поделитесь впечатлениями, оставьте свой отзыв о заведении!");
        }

    }
}
