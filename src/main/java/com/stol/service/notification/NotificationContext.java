package com.stol.service.notification;

import com.stol.model.Restaurant;
import com.stol.model.property.ApplicationType;
import com.stol.model.user.User;

import java.util.Objects;

public class NotificationContext {
    private final User user;
    private final Restaurant restaurant;
    private final ApplicationType applicationType;
    private boolean processingStopped;
    private boolean skipSms;

    public NotificationContext(User user, Restaurant restaurant, ApplicationType applicationType) {
        this(user, restaurant, applicationType, false);
    }

    public NotificationContext(User user, Restaurant restaurant, ApplicationType applicationType, boolean skipSms) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(restaurant);
        Objects.requireNonNull(applicationType);

        this.user = user;
        this.restaurant = restaurant;
        this.applicationType = applicationType;
        this.skipSms = skipSms;
    }

    public User getUser() {
        return user;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public ApplicationType getApplicationType() {
        return applicationType;
    }

    public boolean isProcessingStopped() {
        return processingStopped;
    }

    public boolean isSkipSms() {
        return skipSms;
    }

    public void stopProcessing() {
        this.processingStopped = true;
    }

    @Override
    public String toString() {
        return String.format("Notification context (user: %s, restaurant: %s, application type: %s, processingStopped: %s)",
                getUser().getId(), getRestaurant().getId(), getApplicationType(), processingStopped);
    }
}
