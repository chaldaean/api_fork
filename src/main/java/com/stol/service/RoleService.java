package com.stol.service;

import com.stol.model.property.UserRole;
import com.stol.model.user.Role;
import com.stol.repository.RoleRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Objects;

import static com.stol.configuration.constant.Messages.NO_ROLE_WITH_NAME;

public class RoleService {
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        Objects.requireNonNull(roleRepository);
        this.roleRepository = roleRepository;
    }

    public Role findRoleByName(UserRole userRole) {
        Role role = roleRepository.findByRole(userRole);
        if (Objects.isNull(role)) {
            /* TODO: refactore */
            throw new EntityNotFoundException(NO_ROLE_WITH_NAME + userRole);
        }
        return role;
    }
}
