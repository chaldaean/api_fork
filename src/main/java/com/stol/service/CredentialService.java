package com.stol.service;

import com.stol.model.user.Credential;
import com.stol.model.user.Role;
import com.stol.model.user.User;
import com.stol.repository.CredentialRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.stol.model.property.UserRole.ROLE_RESTAURANT_ADMIN;
import static com.stol.model.property.UserRole.ROLE_USER;

public class CredentialService {
    private final CredentialRepository credentialRepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    public CredentialService(CredentialRepository credentialRepository, RoleService roleService, PasswordEncoder passwordEncoder) {
        Objects.requireNonNull(credentialRepository);
        Objects.requireNonNull(roleService);
        Objects.requireNonNull(passwordEncoder);
        this.credentialRepository = credentialRepository;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }

    public Credential findCredentialByLogin(String login) {
        return credentialRepository.findByLogin(login);
    }

    public Credential saveCredential(Credential credential) {
        return credentialRepository.save(credential);
    }

    public void updateUsersFields(User u, boolean isCustomer) {
        setUserDefaultRole(u, isCustomer);
        userPasswordEncode(u.getCredential());
    }

    private void userPasswordEncode(Credential c) {
        if (StringUtils.isNotBlank(c.getPassword())) {
            c.setPassword(passwordEncoder.encode(c.getPassword()));
        }
    }

    private void setUserDefaultRole(User u, boolean isCustomer) {
        if (Objects.isNull(u.getRoles()) || u.getRoles().isEmpty()) {
            Role defaultRole = isCustomer ? roleService.findRoleByName(ROLE_RESTAURANT_ADMIN) : roleService.findRoleByName(ROLE_USER);
            Set<Role> roles = new HashSet<>();
            roles.add(defaultRole);
            u.setRoles(roles);
        }
    }
}
