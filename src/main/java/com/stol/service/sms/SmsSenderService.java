package com.stol.service.sms;

import com.stol.service.sms.wsdl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import java.util.List;

public class SmsSenderService extends WebServiceGatewaySupport {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Value("${stol.turbosms.sender-name}")
    public String SENDER_NAME;
    @Value("${stol.turbosms.enable}")
    public boolean IS_SMS_ENABLE;
    static String SMS_TOKEN;

    private final Auth auth;
    private final WebServiceTemplate webServiceTemplate;

    public SmsSenderService(WebServiceTemplate webServiceTemplate, Auth auth) {
        this.webServiceTemplate = webServiceTemplate;
        this.auth = auth;
    }

    //	TODO what to do when we have error here?
    public String sendSms(String phoneNumber, String text) {
        if (IS_SMS_ENABLE) {
            logger.info(String.format("Sending sms to number %s", phoneNumber));
            authorization();
            SendSMS request = createSmsRequest(phoneNumber, text);
            SendSMSResponse response = (SendSMSResponse) webServiceTemplate.marshalSendAndReceive(request);
            return sendResult(response);
        }
        logger.info(String.format("Sending sms to number %s is disabled", phoneNumber));
        return null;
    }

    public String creditBalance() {
        if (IS_SMS_ENABLE) {
            authorization();
            GetCreditBalanceResponse b = (GetCreditBalanceResponse) webServiceTemplate.marshalSendAndReceive(new GetCreditBalance());
            return b.getGetCreditBalanceResult();
        }
        return null;
    }

    private void authorization() {
        SMS_TOKEN = null;
        webServiceTemplate.marshalSendAndReceive(auth);
    }

    private SendSMS createSmsRequest(String phoneNumber, String text) {
        SendSMS request = new SendSMS();
        request.setSender(SENDER_NAME);
        request.setDestination("+" + phoneNumber);
        request.setText(text);
        return request;
    }

    private String sendResult(SendSMSResponse response) {
        List<String> result = response.getSendSMSResult().getResultArray();
        return result.get(1);
    }
}
