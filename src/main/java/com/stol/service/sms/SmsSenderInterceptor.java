package com.stol.service.sms;

import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.support.interceptor.AbstractValidatingInterceptor;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import javax.xml.transform.Source;
import java.util.Objects;

import static com.stol.service.sms.SmsSenderService.SMS_TOKEN;
import static org.springframework.http.HttpHeaders.COOKIE;
import static org.springframework.http.HttpHeaders.SET_COOKIE;

public class SmsSenderInterceptor extends AbstractValidatingInterceptor {

    public SmsSenderInterceptor() {
        setValidateResponse(true);
    }

    @Override
    protected Source getValidationRequestSource(WebServiceMessage request) {
        if (Objects.nonNull(SMS_TOKEN)) {
            ((SaajSoapMessage) request).getSaajMessage().getMimeHeaders().addHeader(COOKIE, SMS_TOKEN);
        }
        return null;
    }

    @Override
    protected Source getValidationResponseSource(WebServiceMessage response) {
        if (Objects.isNull(SMS_TOKEN)) {
            SMS_TOKEN = ((SaajSoapMessage) response).getSaajMessage().getMimeHeaders().getHeader(SET_COOKIE)[0];
        }
        return null;
    }
}
