package com.stol.service;

import com.stol.model.MobileDevice;
import com.stol.model.property.ApplicationType;
import com.stol.model.user.User;
import com.stol.repository.MobileDeviceRepository;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class MobileDeviceService {
    private final MobileDeviceRepository mobileDeviceRepository;

    public MobileDeviceService(MobileDeviceRepository mobileDeviceRepository) {
        Objects.requireNonNull(mobileDeviceRepository);
        this.mobileDeviceRepository = mobileDeviceRepository;
    }

    public Optional<MobileDevice> mobileDeviceByUserAndApplicationType(User user, ApplicationType applicationType) {
        return mobileDeviceRepository.findByUserAndApplicationType(user, applicationType);
    }

    public Set<MobileDevice> mobileDeviceByUsersAndApplicationType(Set<User> users, ApplicationType applicationType) {
        Set<MobileDevice> mobileDevices = new HashSet<>();
        users.forEach(u -> {
            Optional<MobileDevice> mobileDevice = mobileDeviceRepository.findByUserAndApplicationType(u, applicationType);
            if(mobileDevice.isPresent()) {
                mobileDevices.add(mobileDevice.get());
            }
        });

        return mobileDevices;
    }

    public void deleteByUserAndApplicationType(User user, ApplicationType applicationType) {
        mobileDeviceRepository.deleteByUserAndApplicationType(user, applicationType);
    }
}
