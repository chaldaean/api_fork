package com.stol.service;

import com.stol.model.Reservation;
import com.stol.repository.ReservationRepository;

import java.util.List;
import java.util.Objects;

public class ReservationService {
    private final ReservationRepository reservationRepository;

    public ReservationService(ReservationRepository reservationRepository) {
        Objects.requireNonNull(reservationRepository);
        this.reservationRepository = reservationRepository;
    }

    public void save(Reservation reservation) {
        reservationRepository.save(reservation);
    }

    public List<Reservation> findAllNewOutdated() {
        return reservationRepository.findAllNewOutdated();
    }

    public long numberOfUserReservationsInRestaurant(Reservation reservation) {
        return reservationRepository.numberOfUserReservationsInRestaurant(reservation.getClient(), reservation.getRestaurant());
    }
}
