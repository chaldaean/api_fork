package com.stol.service;

import com.stol.model.Promotion;
import com.stol.repository.PromotionRepository;

import java.util.Objects;

public class PromotionService {
    private final PromotionRepository promotionRepository;

    public PromotionService(PromotionRepository promotionRepository) {
        Objects.requireNonNull(promotionRepository);
        this.promotionRepository = promotionRepository;
    }

    public Promotion savePromotion(Promotion promotion) {
        return promotionRepository.save(promotion);
    }
}
