package com.stol.service;

import com.stol.controller.model.UserPassword;
import com.stol.model.user.Credential;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.stol.configuration.constant.Messages.NEW_PASSWORD_IS_EMPTY;
import static com.stol.configuration.constant.Messages.OLD_PASSWORD_IS_INCORRECT;

public class PasswordEncoderService extends BCryptPasswordEncoder {

    public String verifyAndEncodeNewPassword(UserPassword userPassword, Credential credential) {
        if (!matches(userPassword.getOldPassword(), credential.getPassword())) {
            throw new AccessDeniedException(OLD_PASSWORD_IS_INCORRECT);
        }
        if (StringUtils.isEmpty(userPassword.getNewPassword())) {
            throw new AccessDeniedException(NEW_PASSWORD_IS_EMPTY);
        }
        return encode(userPassword.getNewPassword());
    }
}
