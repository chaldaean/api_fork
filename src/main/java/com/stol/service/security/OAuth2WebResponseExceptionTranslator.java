package com.stol.service.security;

import com.stol.service.localization.LocalizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;

import java.util.Objects;

public class OAuth2WebResponseExceptionTranslator implements WebResponseExceptionTranslator<OAuth2Exception> {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final LocalizationService localizationService;

    public OAuth2WebResponseExceptionTranslator(LocalizationService localizationService) {
        Objects.requireNonNull(localizationService);
        this.localizationService = localizationService;
    }

    @Override
    public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
        String msg = localizationService.translate(e.getMessage());

        String code = "";
        if(OAuth2Exception.class.isAssignableFrom(e.getClass())) {
            code = ((OAuth2Exception)e).getOAuth2ErrorCode();
        }

        logger.error(msg, e);

        OAuth2Exception ex = OAuth2Exception.create(code, msg);

        return ResponseEntity.status(ex.getHttpErrorCode()).body(ex);
    }
}
