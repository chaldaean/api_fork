package com.stol.service.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;
import java.util.Objects;

import static com.stol.configuration.CacheConfig.ACCESS_TOKEN_STORE;
import static com.stol.configuration.CacheConfig.AUTHENTICATION_STORE;

public class CashingJdbcTokenStore extends JdbcTokenStore {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Cache accessTokenCache;
    private final Cache authenticationCache;

    public CashingJdbcTokenStore(DataSource dataSource, CacheManager cacheManager) {
        super(dataSource);
        accessTokenCache = Objects.requireNonNull(cacheManager.getCache(ACCESS_TOKEN_STORE));
        authenticationCache = Objects.requireNonNull(cacheManager.getCache(AUTHENTICATION_STORE));
    }

    @Override
    public OAuth2AccessToken readAccessToken(String tokenValue) {
        OAuth2AccessToken oAuth2AccessToken = accessTokenCache.get(tokenValue, OAuth2AccessToken.class);
        if (Objects.isNull(oAuth2AccessToken)) {
            oAuth2AccessToken = super.readAccessToken(tokenValue);
            if (Objects.nonNull(oAuth2AccessToken)) {
                logger.info(String.format("Putting user's token into cache. Token: %s", tokenValue));
                accessTokenCache.put(tokenValue, oAuth2AccessToken);
            }
        } else {
            logger.debug(String.format("Get user's token from cache. Token: %s", tokenValue));
        }
        clearCachesIfTokenExpired(tokenValue, oAuth2AccessToken);
        return oAuth2AccessToken;
    }


    @Override
    public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
        OAuth2Authentication oAuth2Authentication = authenticationCache.get(token.getValue(), OAuth2Authentication.class);
        if (Objects.isNull(oAuth2Authentication)) {
            oAuth2Authentication = super.readAuthentication(token);
            if (Objects.nonNull(oAuth2Authentication)) {
                logger.info(String.format("Putting user's authentication into cache. Token: %s", token.getValue()));
                authenticationCache.put(token.getValue(), oAuth2Authentication);
            }
        } else {
            logger.debug(String.format("Get user's authentication from cache. Token: %s", token.getValue()));
        }
        return super.readAuthentication(token);
    }

    @Override
    public OAuth2RefreshToken readRefreshToken(String token) {
        clearCaches(token);
        return super.readRefreshToken(token);
    }

    @Override
    public void removeAccessToken(String tokenValue) {
        clearCaches(tokenValue);
        super.removeAccessToken(tokenValue);
    }

    private void clearCachesIfTokenExpired(String tokenValue, OAuth2AccessToken oAuth2AccessToken) {
        if (Objects.nonNull(oAuth2AccessToken) && oAuth2AccessToken.isExpired()) {
            clearCaches(tokenValue);
        }
    }

    private void clearCaches(String tokenValue) {
        logger.info(String.format("Clearing user's authentication and token from cache. Token: %s", tokenValue));
        accessTokenCache.evict(tokenValue);
        authenticationCache.evict(tokenValue);
    }
}
