package com.stol.service.security;

import com.stol.model.user.User;
import com.stol.model.user.UserSystemInformation;
import com.stol.service.user.CurrentUser;
import com.stol.service.user.UserService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Objects;

public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserService userService;

    public UserDetailsServiceImpl(UserService userService) {
        Objects.requireNonNull(userService);
        this.userService = userService;
    }

    @Override
    @Transactional
    public CurrentUser loadUserByUsername(String login) throws UsernameNotFoundException {
        User u = userService.findUserByLogin(login);
        UserSystemInformation userInfo = userService.findUserInformationByPhoneNumber(login);
        return new CurrentUser(u, userService.canBeLoggedIn(userInfo));
    }

    public static String getAccessTokenFromPrincipal(Principal principal) {
        String tokenValue = null;
        if (principal instanceof OAuth2Authentication) {
            Object details = ((OAuth2Authentication) principal).getDetails();
            if (details instanceof OAuth2AuthenticationDetails) {
                tokenValue = ((OAuth2AuthenticationDetails) details).getTokenValue();
            }
        }
        return tokenValue;
    }
}