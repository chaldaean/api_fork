package com.stol.service.security;

import com.stol.model.user.UserSystemInformation;
import com.stol.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Objects;
import java.util.Optional;

public class CustomDaoAuthenticationProvider extends DaoAuthenticationProvider {
    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public CustomDaoAuthenticationProvider(UserService userService) {
        Objects.requireNonNull(userService);
        this.userService = userService;
    }

    @Override
    protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
        Authentication a = super.createSuccessAuthentication(principal, authentication, user);
        resetLoginAttempts(principal);
        return a;
    }

    private void resetLoginAttempts(Object principal) {
        Optional<UserSystemInformation> optional = userService.findUserSystemInformation(principal);
        if (optional.isPresent()) {
            UserSystemInformation info = optional.get();
            info.setLoginAttempts(0);
            userService.updateUserSystemInformation(info);

            logger.info("User {} logged in", info.getPhoneNumber());
        }
    }
}
