package com.stol.service;

import com.stol.controller.model.UserActivation;
import com.stol.exception.InvalidRequestParameters;
import com.stol.model.user.Credential;
import com.stol.model.user.Role;
import com.stol.model.user.User;
import com.stol.model.user.UserSystemInformation;
import com.stol.service.localization.LocalizationService;
import com.stol.service.sms.SmsSenderService;
import com.stol.service.user.UserService;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.security.access.AccessDeniedException;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.stol.configuration.constant.Messages.*;
import static com.stol.model.property.UserRole.ROLE_USER;
import static com.stol.model.property.UserStatus.ENABLE;
import static com.stol.service.user.ActivationStaticService.*;

public class UserActivationService {
    private final UserService userService;
    private final CredentialService credentialService;
    private final SmsSenderService smsSenderService;
    private final RoleService roleService;
    private final LocalizationService localizationService;

    public UserActivationService(CredentialService credentialService, UserService userService, SmsSenderService smsSenderService,
                                 RoleService roleService, LocalizationService localizationService) {
        Objects.requireNonNull(credentialService);
        Objects.requireNonNull(userService);
        Objects.requireNonNull(smsSenderService);
        Objects.requireNonNull(roleService);
        Objects.requireNonNull(localizationService);
        this.credentialService = credentialService;
        this.userService = userService;
        this.smsSenderService = smsSenderService;
        this.roleService = roleService;
        this.localizationService = localizationService;
    }

    public User findUserByLogin(String login) {
        return userService.findUserByLogin(login);
    }

    public Credential findCredentialByLogin(String login) {
        return credentialService.findCredentialByLogin(login);
    }

    public void saveUser(User user) {
        userService.saveUser(user);
    }

    public void saveCredential(Credential credential) {
        credentialService.saveCredential(credential);
    }

    public UserSystemInformation findUserInformationByPhoneNumber(String phoneNumber) {
        return userService.findUserInformationByPhoneNumber(phoneNumber);
    }

    public UserSystemInformation validateActivationCode(UserActivation activation) {
        UserSystemInformation info = userService.findUserInformationByPhoneNumber(activation.getPhone());
        if (!Objects.equals(info.getActivationCode(), activation.getCode())) {
            throw new InvalidRequestParameters(ACTIVATION_CODE_IS_INVALID);
        }
        return info;
    }

    public String createAndUpdatePasswordResetInformation(UserSystemInformation userInfo, int activationTry) {
        String newPassword = String.valueOf(RandomUtils.nextInt(100000, 1000000));
        userInfo.setPasswordResetCreation(LocalDateTime.now());
        userInfo.setPasswordResetAttempts(activationTry);
        userService.saveUserInformation(userInfo);
        return newPassword;
    }

    public String createNewCodeAndUpdateActivationInformation(UserSystemInformation userInfo, int activationTry) {
        String activationCode = createActivationCode();
        userInfo.setActivationCode(activationCode);
        userInfo.setActivationCodeCreation(LocalDateTime.now());
        userInfo.setActivationAttempts(activationTry);
        userService.saveUserInformation(userInfo);
        return activationCode;
    }

    public boolean hasNoPasswordResendAttempts(UserSystemInformation userInfo) {
        return !hasResendAttempts(userInfo.getPasswordResetAttempts());
    }

    public boolean hasNoActivationAttempts(UserSystemInformation userInfo) {
        return !hasResendAttempts(userInfo.getActivationAttempts());
    }

    public User checkIfUserExistAndActivated(String login) {
        User user = userService.findUserByLogin(login);
        if (!Objects.equals(user.getStatus(), ENABLE)) {
            throw new AccessDeniedException(MUST_BE_ACTIVATED_FIRST);
        }
        return user;
    }

    public void checkIfResetPasswordCooldownExpired(UserSystemInformation userInfo) {
        if (!isResetPasswordCooldownExpired(userInfo)) {
            throw new AccessDeniedException(NUMBER_OF_TRIES_REACHED);
        }
    }

    public void checkIfActivationCooldownExpired(UserSystemInformation userInfo) {
        if (!isActivationCooldownExpired(userInfo)) {
            throw new AccessDeniedException(NUMBER_OF_TRIES_REACHED);
        }
    }

    public void updateUserActivationTime(UserSystemInformation userInfo) {
        userInfo.setActivationTime(LocalDateTime.now());
        userService.saveUserInformation(userInfo);
    }

    public void sendSms(String phoneNumber, String activationCode) {
        smsSenderService.sendSms(phoneNumber, activationCode);
    }

    public void resendSmsWithNewPassword(UserSystemInformation userInfo, String newPassword) {
        smsSenderService.sendSms(userInfo.getPhoneNumber(), localizationService.translate(RESET_PASSWORD_TEXT, newPassword));
    }

    public void setUserRole(User user) {
        Role role = roleService.findRoleByName(ROLE_USER);
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
    }
}
