package com.stol.service.localization;

import com.stol.model.Restaurant;
import com.stol.model.RestaurantI18n;

import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

public class RestaurantLocalization extends LocalizationBase {

    public RestaurantLocalization(Locale locale) {
        super(locale);
    }

    public String getName(Restaurant restaurant) {
        return getI18n(restaurant, locale).map(RestaurantI18n::getName).filter(Objects::nonNull).findFirst()
                .orElse(restaurant.getName());
    }

    public String getAddress(Restaurant restaurant) {
        return getI18n(restaurant, locale).map(RestaurantI18n::getAddress).filter(Objects::nonNull).findFirst()
                .orElse(restaurant.getAddress());
    }

    public String getDescription(Restaurant restaurant) {
        return getI18n(restaurant, locale).map(RestaurantI18n::getDescription).filter(Objects::nonNull).findFirst()
                .orElse(restaurant.getDescription());
    }

    public String getBreakfast(Restaurant restaurant) {
        return getI18n(restaurant, locale).map(RestaurantI18n::getBreakfast).filter(Objects::nonNull).findFirst()
                .orElse(restaurant.getBreakfast());
    }

    public String getParking(Restaurant restaurant) {
        return getI18n(restaurant, locale).map(RestaurantI18n::getParking).filter(Objects::nonNull).findFirst()
                .orElse(restaurant.getParking());
    }

    public String getPaymentMethod(Restaurant restaurant) {
        return getI18n(restaurant, locale).map(RestaurantI18n::getPaymentMethod).filter(Objects::nonNull).findFirst()
                .orElse(restaurant.getPaymentMethod());
    }

    private Stream<RestaurantI18n> getI18n(Restaurant restaurant, Locale locale) {
        return restaurant.getI18ns().stream().filter(i -> i.getLanguage().isActive() && Objects.equals(i.getLanguage().getLocale(), locale));
    }
}
