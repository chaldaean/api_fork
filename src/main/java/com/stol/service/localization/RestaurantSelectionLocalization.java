package com.stol.service.localization;

import com.stol.model.RestaurantSelection;
import com.stol.model.RestaurantSelectionI18n;

import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

public class RestaurantSelectionLocalization extends LocalizationBase {

    public RestaurantSelectionLocalization(Locale locale) {
        super(locale);
    }

    public String getTitle(RestaurantSelection restaurantSelection) {
        return getI18n(restaurantSelection, locale).map(RestaurantSelectionI18n::getTitle).filter(Objects::nonNull).findFirst()
                .orElse(restaurantSelection.getTitle());
    }

    private Stream<RestaurantSelectionI18n> getI18n(RestaurantSelection restaurantSelection, Locale locale) {
        return restaurantSelection.getI18ns().stream().filter(i -> i.getLanguage().isActive() && Objects.equals(i.getLanguage().getLocale(), locale));
    }
}
