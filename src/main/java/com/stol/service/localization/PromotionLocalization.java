package com.stol.service.localization;

import com.stol.model.Promotion;
import com.stol.model.PromotionI18n;

import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

public class PromotionLocalization extends LocalizationBase {

    public PromotionLocalization(Locale locale) {
        super(locale);
    }

    public String getDescription(Promotion promotion) {
        return getI18n(promotion, locale).map(PromotionI18n::getDescription).filter(Objects::nonNull).findFirst()
                .orElse(promotion.getDescription());
    }

    private Stream<PromotionI18n> getI18n(Promotion promotion, Locale locale) {
        return promotion.getI18ns().stream().filter(i -> i.getLanguage().isActive() && Objects.equals(i.getLanguage().getLocale(), locale));
    }
}
