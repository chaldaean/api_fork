package com.stol.service.localization;

import java.util.Locale;
import java.util.Objects;

public abstract class LocalizationBase {

    protected final Locale locale;

    protected LocalizationBase(Locale locale) {
        Objects.requireNonNull(locale);
        this.locale = locale;

    }
}
