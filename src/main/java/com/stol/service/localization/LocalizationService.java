package com.stol.service.localization;

import com.stol.configuration.constant.Localization;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;
import java.util.Objects;

public class LocalizationService {
    @Value("${stol.localization.enabled:true}")
    private boolean enabled;

    private final MessageSource messageSource;

    public LocalizationService(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String translate(Locale locale, String message, Object ...args) {
        if(!enabled) {
            return message;
        }

        return messageSource.getMessage(message, args, message, locale);
    }

    public String translate(String message, Object... args) {
        return translate(LocaleContextHolder.getLocale(), message, args);
    }

    public RestaurantLocalization getRestaurant() {
        return new RestaurantLocalization(getLocale());
    }

    public RestaurantSelectionLocalization getRestaurantSelection() {
        return new RestaurantSelectionLocalization(getLocale());
    }

    public ArticleLocalization getArticle() {
        return new ArticleLocalization(getLocale());
    }

    public PromotionLocalization getPromotion() {
        return new PromotionLocalization(getLocale());
    }

    public TagLocalization getTag() {
        return new TagLocalization(getLocale());
    }

    public CuisineLocalization getCuisine() {
        return new CuisineLocalization(getLocale());
    }

    public Locale getLocale() {
        return enabled ? LocaleContextHolder.getLocale() : Localization.DEFAULT_LOCALE;
    }
}
