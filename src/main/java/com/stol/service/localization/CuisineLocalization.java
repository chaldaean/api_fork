package com.stol.service.localization;

import com.stol.model.Cuisine;
import com.stol.model.CuisineI18n;

import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

public class CuisineLocalization extends LocalizationBase {

    public CuisineLocalization(Locale locale) {
        super(locale);
    }

    public String getName(Cuisine cuisine) {
        return getI18n(cuisine, locale).map(CuisineI18n::getName).filter(Objects::nonNull).findFirst()
                .orElse(cuisine.getName());
    }

    private Stream<CuisineI18n> getI18n(Cuisine cuisine, Locale locale) {
        return cuisine.getI18ns().stream().filter(i -> i.getLanguage().isActive() && Objects.equals(i.getLanguage().getLocale(), locale));
    }
}
