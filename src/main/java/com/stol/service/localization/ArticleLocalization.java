package com.stol.service.localization;

import com.stol.model.Article;
import com.stol.model.ArticleI18n;

import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

public class ArticleLocalization extends LocalizationBase {

    public ArticleLocalization(Locale locale) {
        super(locale);
    }

    public String getTitle(Article article) {
        return getI18n(article, locale).map(ArticleI18n::getTitle).filter(Objects::nonNull).findFirst()
                .orElse(article.getTitle());
    }

    public String getText(Article article) {
        return getI18n(article, locale).map(ArticleI18n::getText).filter(Objects::nonNull).findFirst()
                .orElse(article.getText());
    }

    private Stream<ArticleI18n> getI18n(Article article, Locale locale) {
        return article.getI18ns().stream().filter(i -> i.getLanguage().isActive() && Objects.equals(i.getLanguage().getLocale(), locale));
    }
}
