package com.stol.service.localization;

import com.stol.model.Cuisine;
import com.stol.model.CuisineI18n;
import com.stol.model.Tag;
import com.stol.model.TagI18n;

import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

public class TagLocalization extends LocalizationBase {

    public TagLocalization(Locale locale) {
        super(locale);
    }

    public String getName(Tag tag) {
        return getI18n(tag, locale).map(TagI18n::getName).filter(Objects::nonNull).findFirst()
                .orElse(tag.getName());
    }

    private Stream<TagI18n> getI18n(Tag tag, Locale locale) {
        return tag.getI18ns().stream().filter(i -> i.getLanguage().isActive() && Objects.equals(i.getLanguage().getLocale(), locale));
    }
}
