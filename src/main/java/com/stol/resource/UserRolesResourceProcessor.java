package com.stol.resource;

import com.stol.model.projection.UserRoles;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;

public class UserRolesResourceProcessor implements ResourceProcessor<Resource<UserRoles>> {

    @Override
    public Resource<UserRoles> process(Resource<UserRoles> userResource) {
        UserResourceProcessor.addResetPasswordLink(userResource, userResource.getContent().getPhoneNumber());
        return userResource;
    }
}