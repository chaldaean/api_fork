package com.stol.resource;

import com.stol.controller.UserActivationController;
import com.stol.controller.UserPasswordController;
import com.stol.controller.model.UserActivation;
import com.stol.controller.model.UserLogin;
import com.stol.model.property.UserStatus;
import com.stol.model.user.User;
import com.stol.model.user.UserSystemInformation;
import com.stol.service.user.ActivationStaticService;
import com.stol.service.user.UserService;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;

import java.util.Objects;

import static com.stol.service.user.ActivationStaticService.hasResendAttempts;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class UserResourceProcessor implements ResourceProcessor<Resource<User>> {
    public static final String RESENT_USER_PASSWORD_LINK_NAME = "resent_user_password";
    private static final String ACTIVATION_LINK_NAME = "activation";
    private static final String RESENT_ACTIVATION_CODE_LINK_NAME = "resent_activation_code";

    private final UserService userService;

    public UserResourceProcessor(UserService userService) {
        Objects.requireNonNull(userService);
        this.userService = userService;
    }

    @Override
    public Resource<User> process(Resource<User> userResource) {
        User u = userResource.getContent();
        if (isUserNotConfirmed(u)) {
            addActivationLink(userResource);
            addResentActivationCodeLinkIfNeeded(userResource);
        }
        addResetPasswordLink(userResource, userResource.getContent().getPhoneNumber());
        return userResource;
    }

    private boolean isUserNotConfirmed(User u) {
        return Objects.equals(u.getStatus(), UserStatus.NOT_CONFIRMED);
    }

    private void addActivationLink(Resource<User> userResource) {
        User u = userResource.getContent();
        userResource.add(linkTo(methodOn(UserActivationController.class)
                .activateUser(new UserActivation(u.getPhoneNumber(), getActivationCode(u))))
                .withRel(ACTIVATION_LINK_NAME));
    }

    private void addResentActivationCodeLinkIfNeeded(Resource<User> userResource) {
        User u = userResource.getContent();
        if (hasActivationAttempts(u) || isActivationCooldownExpired(u)) {
            userResource.add(linkTo(methodOn(UserActivationController.class)
                    .resendUserActivationCode(new UserLogin(u.getPhoneNumber())))
                    .withRel(RESENT_ACTIVATION_CODE_LINK_NAME));
        }
    }

    public static void addResetPasswordLink(Resource<?> userResource, String phoneNumber) {
        userResource.add(linkTo(methodOn(UserPasswordController.class)
                .resetUserPassword(new UserLogin(phoneNumber)))
                .withRel(RESENT_USER_PASSWORD_LINK_NAME));
    }

    private boolean hasActivationAttempts(User u) {
        UserSystemInformation userInfo = userService.findUserInformationByPhoneNumber(u.getPhoneNumber());
        return hasResendAttempts(userInfo.getActivationAttempts());
    }

    private String getActivationCode(User user) {
        UserSystemInformation info = userService.findUserInformationByPhoneNumber(user.getPhoneNumber());
        return info.getActivationCode();
    }

    private boolean isActivationCooldownExpired(User user) {
        UserSystemInformation info = userService.findUserInformationByPhoneNumber(user.getPhoneNumber());
        return ActivationStaticService.isActivationCooldownExpired(info);
    }
}
