package com.stol.resolver;

import com.stol.model.user.User;
import com.stol.service.user.UserService;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Objects;

public class UserAwareLocaleResolver extends AcceptHeaderLocaleResolver {

    private final UserService userService;

    public UserAwareLocaleResolver(UserService userService) {
        Objects.requireNonNull(userService);
        this.userService = userService;
    }

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        Locale locale = getUserLocale();

        if(locale != null && getSupportedLocales().contains(locale)) {
            return locale;
        }

        return super.resolveLocale(request);
    }

    private Locale getUserLocale() {
        User user = userService.getLoggedInUser();
        if(user != null && user.getLanguage() != null) {
            return user.getLanguage().getLocale();
        }

        return null;
    }
}
