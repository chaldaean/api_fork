package com.stol.controller;

import com.stol.controller.model.UserLogin;
import com.stol.controller.model.UserPassword;
import com.stol.model.user.Credential;
import com.stol.model.user.UserSystemInformation;
import com.stol.service.PasswordEncoderService;
import com.stol.service.UserActivationService;
import com.stol.service.security.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Objects;

import static com.stol.controller.UserController.isUserLoggedIn;

@RestController
@RequestMapping(value = "/api/user/password")
public class UserPasswordController {
    private final UserActivationService userActivationService;
    private final PasswordEncoderService passwordEncoderService;
    private final DefaultTokenServices tokenServices;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public UserPasswordController(UserActivationService userActivationService, PasswordEncoderService passwordEncoderService, DefaultTokenServices tokenServices) {
        Objects.requireNonNull(userActivationService);
        Objects.requireNonNull(passwordEncoderService);
        Objects.requireNonNull(tokenServices);
        this.userActivationService = userActivationService;
        this.passwordEncoderService = passwordEncoderService;
        this.tokenServices = tokenServices;
    }

    @PostMapping(value = "/reset")
    public ResponseEntity resetUserPassword(@RequestBody UserLogin userLogin) {
        String login = userLogin.getLogin();
        userActivationService.checkIfUserExistAndActivated(login);
        UserSystemInformation userInfo = userActivationService.findUserInformationByPhoneNumber(login);
        int resetTry = calculatePasswordResetTryNumber(userInfo);
        String newPassword = userActivationService.createAndUpdatePasswordResetInformation(userInfo, resetTry);
        saveNewPassword(login, passwordEncoderService.encode(newPassword));
        userActivationService.resendSmsWithNewPassword(userInfo, newPassword);

        logger.info("Password reset for user {}", userLogin.getLogin());

        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/change")
    public ResponseEntity changeUserPassword(@RequestBody UserPassword password, Principal principal) {
        isUserLoggedIn(principal);
        String login = principal.getName();
        Credential credential = userActivationService.findCredentialByLogin(login);
        String newEncodedPassword = passwordEncoderService.verifyAndEncodeNewPassword(password, credential);
        saveNewPassword(login, newEncodedPassword);

        logger.info("Password changed for user {}", login);

        tokenServices.revokeToken(UserDetailsServiceImpl.getAccessTokenFromPrincipal(principal));
        
        return ResponseEntity.noContent().build();
    }

    private int calculatePasswordResetTryNumber(UserSystemInformation userInfo) {
        int resetTry = userInfo.getPasswordResetAttempts() + 1;
        if (userActivationService.hasNoPasswordResendAttempts(userInfo)) {
            userActivationService.checkIfResetPasswordCooldownExpired(userInfo);
            resetTry = 1;
        }
        return resetTry;
    }

    private void saveNewPassword(String login, String newPassword) {
        Credential credential = userActivationService.findCredentialByLogin(login);
        credential.setPassword(newPassword);
        userActivationService.saveCredential(credential);
    }
}
