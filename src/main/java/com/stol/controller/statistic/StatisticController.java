package com.stol.controller.statistic;

import com.stol.service.statistic.StatisticService;
import com.stol.service.user.UserService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api/statistic")
public class StatisticController {
    private final StatisticService statisticService;
    private final UserService userService;

    public StatisticController(StatisticService statisticService, UserService userService) {
        Objects.requireNonNull(statisticService);
        Objects.requireNonNull(userService);
        this.statisticService = statisticService;
        this.userService = userService;
    }

    @GetMapping("/accounts")
    public ResponseEntity<NewAccounts> numberOfAccounts(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        date = validateDate(date);
        NewAccounts acc = new NewAccounts(userService.numberOfAccountsByDate(date), userService.numberOfActivatedAccountsByDate(date));
        return ResponseEntity.ok(acc);
    }

    @GetMapping("/accounts/active")
    public ResponseEntity<Map> numberOfActiveUsers(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        date = validateDate(date);
        Map<String, Long> active = Collections.singletonMap("active_users", statisticService.numberOfActiveUsers(date));
        return ResponseEntity.ok(active);
    }

    @GetMapping("/reservations")
    public ResponseEntity<Map> numberOfReservations(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        date = validateDate(date);
        Map<String, Long> reservations = Collections.singletonMap("total", statisticService.numberOfReservations(date));
        return ResponseEntity.ok(reservations);
    }

    @GetMapping("/reviews")
    public ResponseEntity<Map> numberOfReviews(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        date = validateDate(date);
        Map<String, Long> reviews = Collections.singletonMap("total", statisticService.numberOfReviews(date));
        return ResponseEntity.ok(reviews);
    }

    @PostMapping("/screens")
    public void usersNumberOfScreensCount() {
        statisticService.updateScreenNumber();
    }

    @GetMapping("/screens")
    public ResponseEntity<Map> averageNumberOfScreens(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        date = validateDate(date);
        Map<String, Long> reviews = Collections.singletonMap("average", statisticService.averageNumberOfScreens(date));
        return ResponseEntity.ok(reviews);
    }

    private LocalDate validateDate(LocalDate date) {
        if (Objects.isNull(date)) {
            date = LocalDate.now();
        }
        return date;
    }

    private static final class NewAccounts {
        private final int total;
        private final int activated;

        NewAccounts(int total, int activated) {
            this.total = total;
            this.activated = activated;
        }

        public int getTotal() {
            return total;
        }

        public int getActivated() {
            return activated;
        }
    }
}
