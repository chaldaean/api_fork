package com.stol.controller;

import com.stol.controller.model.UserLogoutRequest;
import com.stol.model.user.User;
import com.stol.service.AuthService;
import com.stol.service.MobileDeviceService;
import com.stol.service.user.UserService;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Objects;

import static com.stol.configuration.constant.Messages.MUST_BE_LOGGED_IN;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {
    private final UserService userService;
    private final MobileDeviceService mobileDeviceService;
    private final AuthService authService;

    public UserController(UserService userService, MobileDeviceService mobileDeviceService, AuthService authService) {
        Objects.requireNonNull(userService);
        Objects.requireNonNull(mobileDeviceService);
        Objects.requireNonNull(authService);

        this.userService = userService;
        this.mobileDeviceService = mobileDeviceService;
        this.authService = authService;
    }

    @GetMapping("/current")
    public ResponseEntity<Resource> loggedInUser(Principal principal, @RequestParam(required = false) String projection) {
        isUserLoggedIn(principal);
        return ResponseEntity.ok(createUserRolesResource(projection, principal.getName()));
    }

    private Resource<?> createUserRolesResource(String projection, String login) {
        if (Objects.equals(projection, "user_page")) {
            return new Resource<>(userService.findUserPageByLogin(login));
        } if(Objects.equals(projection, "manager")) {
            return new Resource<>(userService.findUserRestaurantManagerByLogin(login));
        } else {
            return new Resource<>(userService.findUserRolesByLogin(login));
        }
    }

    static void isUserLoggedIn(Principal principal) {
        if (Objects.isNull(principal)) {
            throw new AccessDeniedException(MUST_BE_LOGGED_IN);
        }
    }

    @PostMapping("/current/logout")
    public ResponseEntity<?> track(@RequestBody UserLogoutRequest req) {
        User user = userService.getLoggedInUser();
        if(user != null) {
            mobileDeviceService.deleteByUserAndApplicationType(user, req.getApplicationType());
            authService.logout();
        }

        return ResponseEntity.noContent().build();
    }

}
