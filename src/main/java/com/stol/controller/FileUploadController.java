package com.stol.controller;

import com.stol.exception.InvalidRequestParameters;
import com.stol.model.MediaFile;
import com.stol.repository.MediaFileRepository;
import com.stol.service.FileService;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Objects;

import static com.stol.configuration.constant.Messages.NO_FILES_TO_STORE;
import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(value = "/api")
public class FileUploadController {
    private final FileService fileService;
    private final RepositoryEntityLinks entityLinks;

    public FileUploadController(FileService fileService, RepositoryEntityLinks entityLinks) {
        Objects.requireNonNull(fileService);
        Objects.requireNonNull(entityLinks);
        this.fileService = fileService;
        this.entityLinks = entityLinks;
    }

    @PostMapping(value = "/upload")
    public Resources<Resource<MediaFile>> uploadFile(@RequestParam(required = false, defaultValue = "") String folder,
                                                     @RequestParam(value = "data") List<MultipartFile> files) {
        checkIfRequestContainFiles(files);
        List<Resource<MediaFile>> fileList = files.stream().map(f -> createMediaFileResource(fileService.createAndSaveNewFile(f, folder)))
                .collect(toList());
        return new Resources<>(fileList);
    }

    private Resource<MediaFile> createMediaFileResource(MediaFile mediaFile) {
        Link link = entityLinks.linkToSingleResource(MediaFileRepository.class, mediaFile.getId());
        Link selfLink = new Link(link.getHref(), Link.REL_SELF);
        return new Resource<>(mediaFile, selfLink);
    }

    private void checkIfRequestContainFiles(List<MultipartFile> files) {
        if (files.isEmpty()) {
            throw new InvalidRequestParameters(NO_FILES_TO_STORE);
        }
    }
}