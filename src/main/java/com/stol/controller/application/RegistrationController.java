package com.stol.controller.application;

import com.stol.model.application.Registration;
import com.stol.model.user.User;
import com.stol.service.CredentialService;
import com.stol.service.user.UserService;
import com.stol.service.user.UserSmsEventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.support.SelfLinkProvider;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api")
public class RegistrationController {
    private final UserService userService;
    private final SelfLinkProvider selfLinkProvider;
    private final UserSmsEventService userSmsEventHandler;
    private final CredentialService credentialService;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public RegistrationController(UserService userService, SelfLinkProvider selfLinkProvider, UserSmsEventService userSmsEventHandler, CredentialService credentialService) {
        Objects.requireNonNull(userService);
        Objects.requireNonNull(selfLinkProvider);
        Objects.requireNonNull(userSmsEventHandler);
        Objects.requireNonNull(credentialService);
        this.userService = userService;
        this.selfLinkProvider = selfLinkProvider;
        this.userSmsEventHandler = userSmsEventHandler;
        this.credentialService = credentialService;
    }

    @PostMapping("/user/registration")
    public ResponseEntity userRegistration(@RequestBody @Valid Registration registration) throws URISyntaxException {
        User user = userService.createUser(registration);
        credentialService.updateUsersFields(user, registration.isCustomer());
        user = userService.saveUser(user);

        userSmsEventHandler.sendActivationCode(user);

        Link selfLinkFor = selfLinkProvider.createSelfLinkFor(user);

        logger.info("New user {} registered", user.getPhoneNumber());

        return ResponseEntity.created(new URI(selfLinkFor.getHref().replace("{?projection}", ""))).build();
    }
}
