package com.stol.controller.application;

import com.stol.controller.model.MinMaxRestaurantsAverageCheck;
import com.stol.service.RestaurantService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class RestaurantsAverageCheckController {
    private RestaurantService restaurantService;

    public RestaurantsAverageCheckController(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @GetMapping("/restaurants/average_check")
    public ResponseEntity<MinMaxRestaurantsAverageCheck> minMaxRestaurantsAverageCheck() {
        return ResponseEntity.ok(restaurantService.minMaxRestaurantsAverageCheck());
    }
}
