package com.stol.controller.model;

import com.stol.model.property.ApplicationType;

public class UserLogoutRequest {
    private ApplicationType applicationType;

    public ApplicationType getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(ApplicationType applicationType) {
        this.applicationType = applicationType;
    }
}
