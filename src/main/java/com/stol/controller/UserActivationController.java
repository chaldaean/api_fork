package com.stol.controller;

import com.stol.controller.model.UserActivation;
import com.stol.controller.model.UserLogin;
import com.stol.model.user.User;
import com.stol.model.user.UserSystemInformation;
import com.stol.service.UserActivationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

import static com.stol.configuration.constant.Messages.USER_ALREADY_ACTIVATED;
import static com.stol.model.property.UserStatus.ENABLE;
import static com.stol.model.property.UserStatus.NOT_CONFIRMED;

@RestController
@RequestMapping(value = "/api/user/activation")
public class UserActivationController {
    private final UserActivationService userActivationService;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public UserActivationController(UserActivationService userActivationService) {
        Objects.requireNonNull(userActivationService);
        this.userActivationService = userActivationService;
    }

    @PostMapping
    public ResponseEntity activateUser(@RequestBody UserActivation activation) {
        User user = findUserAndCheckIfActivationNotConfirmed(activation.getPhone());
        UserSystemInformation info = userActivationService.validateActivationCode(activation);
        enableUserAndSetRole(user);
        userActivationService.updateUserActivationTime(info);

        logger.info("User {} activated", activation.getPhone());

        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/resend")
    public ResponseEntity resendUserActivationCode(@RequestBody UserLogin login) {
        findUserAndCheckIfActivationNotConfirmed(login.getLogin());
        UserSystemInformation userInfo = userActivationService.findUserInformationByPhoneNumber(login.getLogin());
        int activationTry = calculateActivationTryNumber(userInfo);
        String activationCode = userActivationService.createNewCodeAndUpdateActivationInformation(userInfo, activationTry);
        userActivationService.sendSms(userInfo.getPhoneNumber(), activationCode);

        logger.info("Activation code resent for user {}, attempt {}", login.getLogin(), activationTry);

        return ResponseEntity.noContent().build();
    }

    private User findUserAndCheckIfActivationNotConfirmed(String login) {
        User user = userActivationService.findUserByLogin(login);
        if (!Objects.equals(user.getStatus(), NOT_CONFIRMED)) {
            throw new AccessDeniedException(USER_ALREADY_ACTIVATED);
        }
        return user;
    }

    private void enableUserAndSetRole(User user) {
        user.setStatus(ENABLE);
        userActivationService.saveUser(user);
    }

    private int calculateActivationTryNumber(UserSystemInformation userInfo) {
        int activationTry = userInfo.getActivationAttempts() + 1;
        if (userActivationService.hasNoActivationAttempts(userInfo)) {
            userActivationService.checkIfActivationCooldownExpired(userInfo);
            activationTry = 1;
        }
        return activationTry;
    }
}