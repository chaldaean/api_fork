package com.stol.controller;

import com.stol.model.Restaurant;
import com.stol.service.RestaurantService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.core.config.ProjectionDefinitionConfiguration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.hateoas.Identifiable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.core.EmbeddedWrapper;
import org.springframework.hateoas.core.EmbeddedWrappers;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/restaurants")
public class RestaurantController {
    private final RestaurantService restaurantService;
    private final ProjectionDefinitionConfiguration projectionDefinitionConfiguration;

    @Autowired
    public RestaurantController(RestaurantService restaurantService, RepositoryRestConfiguration repositoryRestConfiguration) {
        Objects.requireNonNull(restaurantService);
        Objects.requireNonNull(repositoryRestConfiguration);
        this.restaurantService = restaurantService;
        this.projectionDefinitionConfiguration = repositoryRestConfiguration.getProjectionConfiguration();
    }

    @RequestMapping("/search")
    PagedResources findRestaurants(@RequestParam(required = false) Double avgTicketFrom,
                                      @RequestParam(required = false) Double avgTicketTo,
                                      @RequestParam(required = false) List<Long> cuisines,
                                      @RequestParam(required = false) List<Long> tags,
                                      @RequestParam(required = false) BigDecimal latitude,
                                      @RequestParam(required = false) BigDecimal longitude,
                                      @RequestParam(required = false, defaultValue = "name") String sorting,
                                      @RequestParam(required = false, defaultValue = "0") Integer page,
                                      @RequestParam(required = false, defaultValue = "10") Integer size,
                                      @RequestParam(required = false) String projection) {

        Class<?> projectionClazz = Restaurant.class;

        if(StringUtils.isNoneBlank(projection)) {
            Class<?> clazz = projectionDefinitionConfiguration.getProjectionType(Restaurant.class, projection);
            if(clazz != null && Identifiable.class.isAssignableFrom(clazz)) {
                projectionClazz = clazz;
            }
        }

        Page<?> resultPage = restaurantService.findRestaurants(avgTicketFrom, avgTicketTo, cuisines, tags, latitude, longitude, sorting,
                PageRequest.of(page, size), projectionClazz.asSubclass(Identifiable.class));

        PagedResources.PageMetadata metadata = new PagedResources.PageMetadata(resultPage.getSize(),
                resultPage.getNumber(), resultPage.getTotalElements(), resultPage.getTotalPages());

        if(resultPage.isEmpty()) {
            EmbeddedWrappers wrappers = new EmbeddedWrappers(false);
            EmbeddedWrapper wrapper = wrappers.emptyCollectionOf(Restaurant.class);
            return new PagedResources<>(Collections.singletonList(wrapper), metadata);
        }

        return PagedResources.wrap(resultPage.getContent(), metadata);
    }
}
