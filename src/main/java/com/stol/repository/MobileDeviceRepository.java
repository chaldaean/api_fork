package com.stol.repository;

import com.stol.model.MobileDevice;
import com.stol.model.property.ApplicationType;
import com.stol.model.user.User;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RepositoryRestResource(path = "mobile_device")
public interface MobileDeviceRepository extends Repository<MobileDevice, Long> {
    MobileDevice save(MobileDevice discount);

    @RestResource(exported = false)
    Optional<MobileDevice> findByUserAndApplicationType(User user, ApplicationType applicationType);

    @Transactional
    void deleteByUserAndApplicationType(User user, ApplicationType applicationType);
}
