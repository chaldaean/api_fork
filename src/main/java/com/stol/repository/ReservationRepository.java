package com.stol.repository;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.StringExpression;
import com.stol.model.QReservation;
import com.stol.model.Reservation;
import com.stol.model.Restaurant;
import com.stol.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "reservations")
public interface ReservationRepository extends Repository<Reservation, Long>, QuerydslPredicateExecutor<Reservation>, QuerydslBinderCustomizer<QReservation> {

    Reservation save(Reservation reservation);

    Optional<Reservation> findById(Long var1);

    Page<Reservation> findAll(Pageable pageable);

    void deleteById(Long id);

    @RestResource(path = "my", rel = "reservations")
    @Query("select distinct r from Reservation r where r.client.credential.login = ?#{principal?.username} order by r.created desc")
    @Transactional(readOnly = true)
    Page<Reservation> findAllForLoggedInUser(Pageable pageable);

    @RestResource(path = "forthcoming", rel = "reservations")
    @Query(value = "select distinct r.* from reservation r inner join user u on r.user_id = u.id inner join user_credential uc on u.user_credential_id = uc.id" +
            " where (r.status = 'NEW' or (r.status = 'CONFIRMED' and TIMESTAMPADD(HOUR, 2, r.date_time) >= CURRENT_TIMESTAMP())" +
            " or (r.status = 'REJECTED' and TIMESTAMPADD(HOUR, 2, r.date_time) >= CURRENT_TIMESTAMP())" +
            " or (r.status = 'CANCELED' and TIMESTAMPADD(HOUR, 2, r.date_time) >= CURRENT_TIMESTAMP())) " +
            " and uc.login = ?#{principal?.username} order by r.created desc", nativeQuery = true)
    @Transactional(readOnly = true)
    List<Reservation> findAllNearestForLoggedInUser();

    @RestResource(path = "past", rel = "reservations")
    @Query(value = "select distinct r.* from reservation r inner join user u on r.user_id = u.id inner join user_credential uc on u.user_credential_id = uc.id" +
            " where ((r.status = 'CONFIRMED' and TIMESTAMPADD(HOUR, 2, r.date_time) < CURRENT_TIMESTAMP())" +
            " or (r.status = 'REJECTED' and TIMESTAMPADD(HOUR, 2, r.date_time) < CURRENT_TIMESTAMP() and TIMESTAMPADD(HOUR, 12, r.date_time) >= CURRENT_TIMESTAMP())" +
            " or (r.status = 'CANCELED' and TIMESTAMPADD(HOUR, 2, r.date_time) < CURRENT_TIMESTAMP())) " +
            " and uc.login = ?#{principal?.username} order by r.created desc",
            countQuery = "select count(distinct r.id) from reservation r inner join user u on r.user_id = u.id inner join user_credential uc on u.user_credential_id = uc.id" +
                    " where ((r.status = 'CONFIRMED' and TIMESTAMPADD(HOUR, 2, r.date_time) < CURRENT_TIMESTAMP())" +
                    " or (r.status = 'REJECTED' and TIMESTAMPADD(HOUR, 2, r.date_time) < CURRENT_TIMESTAMP() and TIMESTAMPADD(HOUR, 12, r.date_time) >= CURRENT_TIMESTAMP())" +
                    " or (r.status = 'CANCELED' and TIMESTAMPADD(HOUR, 2, r.date_time) < CURRENT_TIMESTAMP())) " +
                    " and uc.login = ?#{principal?.username}",
            nativeQuery = true)
    @Transactional(readOnly = true)
    Page<Reservation> findAllPassedForLoggedInUser(Pageable pageable);

    @RestResource(path = "my_restaurants_confirmed", rel = "confirmed_restaurant_reservations")
    @Query(value = "select distinct r from Reservation r inner join r.restaurant rr inner join rr.owners o where r.status = 'CONFIRMED'" +
            " and o.credential.login = ?#{principal?.username} and r.dateTime >= ?#{T(java.time.LocalDateTime).now()} order by r.created desc")
    @Transactional(readOnly = true)
    Page<Reservation> findAllConfirmedBeforeVisitTimeForLoggedInRestaurantsOwner(Pageable pageable);

    // TODO: shall filter out future confirmed?
    @RestResource(path = "my_restaurants", rel = "restaurant_reservations")
    @Query("select distinct r from Reservation r inner join r.restaurant rr inner join rr.owners o where r.status <> 'NEW' " +
            "and o.credential.login = ?#{principal?.username} order by r.created desc")
    @Transactional(readOnly = true)
    Page<Reservation> findAllNotNewForLoggedInRestaurantsOwner(Pageable pageable);

    @RestResource(path = "my_restaurants_new", rel = "new_restaurant_reservations")
    @Query("select distinct r from Reservation r inner join r.restaurant rr inner join rr.owners o where r.status = 'NEW' " +
            "and o.credential.login = ?#{principal?.username} order by r.created desc")
    @Transactional(readOnly = true)
    List<Reservation> findAllNewForLoggedInRestaurantsOwner();

    @RestResource(exported = false)
    @Query("select distinct r from Reservation r where r.status = 'NEW' and (r.created < ?#{T(java.time.LocalDateTime).now().minusHours(18)}" +
            " or r.dateTime < ?#{T(java.time.LocalDateTime).now().plusMinutes(30)})")
    List<Reservation> findAllNewOutdated();

    @RestResource(path = "by_user")
    @Transactional(readOnly = true)
    Page<Reservation> findAllByClient(@Param("id") User userId, Pageable pageable);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query("select COUNT(r) from Reservation r where r.created >= ?1")
    long numberOfReservations(LocalDateTime date);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query("select COUNT(r) from Reservation r where r.client = ?1")
    long numberOfUserReservations(User user);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query("select COUNT(r) from Reservation r where r.client = ?1 and r.restaurant = ?2 and r.status = 'CONFIRMED' and r.dateTime < ?#{T(java.time.LocalDateTime).now()}")
    long numberOfUserReservationsInRestaurant(User user, Restaurant restaurant);

    @RestResource(path = "count_new", rel = "reservations")
    @Transactional(readOnly = true)
    @Query("select count(r) from Reservation r inner join r.restaurant rr inner join rr.owners o where r.status = 'NEW' and o.credential.login = ?#{principal?.username}")
    long numberOfNewReservationsInRestaurant();

    @Override
    default void customize(QuerydslBindings bindings, QReservation root) {
        bindings.bind(root.created).first((path, value) ->
                path.year().eq(value.getYear())
                        .and(path.month().eq(value.getMonthValue()))
                        .and(path.dayOfMonth().eq(value.getDayOfMonth())));

        bindings.bind(root.dateTime).first((path, value) ->
                path.year().eq(value.getYear())
                        .and(path.month().eq(value.getMonthValue()))
                        .and(path.dayOfMonth().eq(value.getDayOfMonth())));

        bindings.bind(root.client.name).first(StringExpression::contains);
        bindings.bind(root.client.surname).first(StringExpression::contains);
        bindings.bind(root.restaurant.name).first(StringExpression::contains);

        bindings.bind(root.discount.happyHour.discount).first(NumberExpression::eq);
        bindings.bind(root.discount.isUsed).first(BooleanExpression::eq);
    }
}
