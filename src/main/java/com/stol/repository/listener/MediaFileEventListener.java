package com.stol.repository.listener;

import com.stol.model.MediaFile;
import com.stol.service.FileService;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.persister.entity.EntityPersister;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import java.util.Objects;

public class MediaFileEventListener implements PostDeleteEventListener {
    private final EntityManagerFactory entityManagerFactory;
    private final FileService fileService;

    public MediaFileEventListener(EntityManagerFactory entityManagerFactory, FileService fileService) {
        Objects.requireNonNull(entityManagerFactory);
        Objects.requireNonNull(fileService);
        this.entityManagerFactory = entityManagerFactory;
        this.fileService = fileService;
    }

    @PostConstruct
    private void init() {
        SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(this);
    }

    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        Object entity = postDeleteEvent.getEntity();
        if(entity instanceof MediaFile) {
            fileService.deleteFile((MediaFile)entity);
        }
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }

    @Override
    public boolean requiresPostCommitHandling(EntityPersister persister) {
        return false;
    }
}
