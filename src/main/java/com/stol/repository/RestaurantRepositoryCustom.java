package com.stol.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

public interface RestaurantRepositoryCustom {

    Page<Long> findRestaurants(Double avgTicketFrom, Double avgTicketTo, List<Long> cuisines, List<Long> tags, BigDecimal latitude, BigDecimal longitude,
                               String sorting, String languageCode, Pageable pageable);
}
