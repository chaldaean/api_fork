package com.stol.repository.statistic;

import com.stol.model.statistic.Screen;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(exported = false)
public interface ScreenRepository extends Repository<Screen, Long> {
    Screen save(Screen screen);

    Optional<Screen> findByUserIdAndDate(Long id, LocalDate date);

    @Transactional(readOnly = true)
    @Query(value = "SELECT screen_number from screen s where s.creation_date = ?1", nativeQuery = true)
    List<Integer> numberOfScreensByUsers(LocalDate date);
}
