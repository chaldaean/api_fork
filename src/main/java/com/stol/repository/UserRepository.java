package com.stol.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.stol.model.Restaurant;
import com.stol.model.projection.UserLanguage;
import com.stol.model.projection.UserManager;
import com.stol.model.projection.UserPage;
import com.stol.model.projection.UserRoles;
import com.stol.model.user.QUser;
import com.stol.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@RepositoryRestResource
public interface UserRepository extends Repository<User, Long>, QuerydslPredicateExecutor<User>, QuerydslBinderCustomizer<QUser> {
    User save(User user);

    Optional<User> findById(Long id);

    Page<User> findAll(Pageable pageable);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    User findUserByCredentialLogin(String login);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @EntityGraph(attributePaths = {"roles", "credential.registrationDate", "photo.url"}, type = EntityGraph.EntityGraphType.LOAD)
    UserManager findUserManagerByCredentialLogin(String login);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @EntityGraph(attributePaths = {"roles", "credential.registrationDate", "photo.url"}, type = EntityGraph.EntityGraphType.LOAD)
    UserRoles findUserRolesByCredentialLogin(String login);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @EntityGraph(attributePaths = {"photo.url", "savedRestaurants"}, type = EntityGraph.EntityGraphType.LOAD)
    UserPage findUserPageByCredentialLogin(String login);

    @RestResource(path = "current_language")
    @Transactional(readOnly = true)
    @EntityGraph(attributePaths = {"language"}, type = EntityGraph.EntityGraphType.LOAD)
    @Query("select u from User u where u.credential.login = ?#{principal?.username}")
    UserLanguage findUserLanguage();

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query("select COUNT(u) from User u where u.credential.registrationDate >= ?1")
    int numberOfAccountsByDate(LocalDateTime date);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query("select COUNT(u) from User u where u.credential.registrationDate >= ?1 and u.status = 'ENABLE'")
    int numberOfActivatedAccountsByDate(LocalDateTime date);

    @Query("select u from User u join u.roles r where r.role = 'ROLE_RESTAURANT_ADMIN'")
    @RestResource(path = "restaurant_admins")
    Page<User> findAllRestaurantAdmins(Pageable pageable);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    boolean existsByIdAndSavedRestaurants(Long id, Restaurant restaurant);

    @Override
    default void customize(QuerydslBindings bindings, QUser root) {
        bindings.bind(root.surname).first(StringExpression::contains);
        bindings.bind(root.name).first(StringExpression::contains);
        bindings.bind(root.email).first(StringExpression::contains);
        bindings.bind(root.phoneNumber).first(StringExpression::contains);
        bindings.bind(root.credential.registrationDate).first((path, value) ->
                path.year().eq(value.getYear())
                .and(path.month().eq(value.getMonthValue()))
                .and(path.dayOfMonth().eq(value.getDayOfMonth())));
    }

}
