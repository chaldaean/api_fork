package com.stol.repository;

import com.stol.model.user.Credential;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(exported = false)
public interface CredentialRepository extends Repository<Credential, Long> {
    Credential save(Credential credential);

    Optional<Credential> findById(Long id);

    Credential findByLogin(String login);
}