package com.stol.repository;

import com.stol.model.Reservation;
import com.stol.model.ReviewNotification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(path = "review_notifications")
public interface ReviewNotificationRepository extends Repository<ReviewNotification, Long> {
    @RestResource(exported = false)
    Optional<ReviewNotification> findById(Long id);

    @RestResource(exported = false)
    ReviewNotification save(ReviewNotification reviewNotification);

    @RestResource(path = "my", rel = "review_notifications")
    @Query("select distinct r from ReviewNotification r where r.user.credential.login = ?#{principal?.username} and r.activationTime <= current_timestamp and r.endTime" +
            " >= current_timestamp")
    @Transactional(readOnly = true)
    Page<ReviewNotification> findAllForLoggedInUser(Pageable pageable);

    @Query("select r from ReviewNotification r where r.activationTime <= current_timestamp and r.endTime >= current_timestamp" +
            " and r.sent = false order by r.activationTime")
    @RestResource(exported = false)
    List<ReviewNotification> findAllActiveUnsent();

    @RestResource(exported = false)
    ReviewNotification findByReservation(Reservation reservation);

    void deleteById(Long id);

    @RestResource(exported = false)
    @Query("select distinct r from ReviewNotification r where r.endTime < current_timestamp")
    List<ReviewNotification> findAllOverdue();
}
