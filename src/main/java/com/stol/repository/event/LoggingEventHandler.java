package com.stol.repository.event;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterDelete;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import java.util.Objects;

@RepositoryEventHandler
public class LoggingEventHandler {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @HandleAfterSave
    public void onSave(Object o) {
        logger.info("Saved object: {}", toString(o));
    }

    @HandleAfterDelete
    public void onDelete(Object o) {
        logger.info("Deleted object: {}", toString(o));
    }

    @HandleAfterCreate
    public void onCreate(Object o) {
        logger.info("Created object: {}", toString(o));
    }

    private String toString(Object o) {
        try {
            return ReflectionToStringBuilder.toString(o);
        } catch (Exception e) {
            return Objects.toString(o);
        }
    }
}
