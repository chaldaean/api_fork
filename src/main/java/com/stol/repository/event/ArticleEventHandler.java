package com.stol.repository.event;

import com.stol.model.Article;
import com.stol.service.user.UserService;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import java.util.Objects;

@RepositoryEventHandler
public class ArticleEventHandler {
    private final UserService userService;

    public ArticleEventHandler(UserService userService) {
        Objects.requireNonNull(userService);
        this.userService = userService;
    }

    @HandleBeforeCreate
    public void setAuthor(Article article) {
        article.setAuthor(userService.getLoggedInUser());
    }
}
