package com.stol.repository.event;

import com.stol.model.MobileDevice;
import com.stol.service.MobileDeviceService;
import com.stol.service.user.UserService;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import java.util.Objects;
import java.util.Optional;

@RepositoryEventHandler
public class MobileDeviceEventHandler {
    private final MobileDeviceService mobileDeviceService;
    private final UserService userService;

    public MobileDeviceEventHandler(MobileDeviceService mobileDeviceService, UserService userService) {
        Objects.requireNonNull(mobileDeviceService);
        Objects.requireNonNull(userService);
        this.mobileDeviceService = mobileDeviceService;
        this.userService = userService;
    }

    @HandleBeforeCreate
    public void setUserAndReSaveIfAlreadyExist(MobileDevice mobileDevice) {
        if (Objects.isNull(mobileDevice.getUser())) {
            mobileDevice.setUser(userService.getLoggedInUser());
        }
        Optional<MobileDevice> device = mobileDeviceService.mobileDeviceByUserAndApplicationType(mobileDevice.getUser(), mobileDevice.getApplicationType());
        if (device.isPresent()) {
            mobileDevice.setId(device.get().getId());
        }
    }
}
