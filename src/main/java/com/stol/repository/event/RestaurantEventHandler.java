package com.stol.repository.event;

import com.stol.exception.InvalidOperationException;
import com.stol.model.AddRestaurantRequest;
import com.stol.model.Restaurant;
import com.stol.model.user.User;
import com.stol.service.user.UserService;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import java.util.Objects;

import static com.stol.configuration.constant.Messages.DO_NOT_ALLOW_REMOVE_YOURSELF;
import static com.stol.service.user.LoginStaticService.isSystemAdmin;

@RepositoryEventHandler
public class RestaurantEventHandler {
    private final UserService userService;

    public RestaurantEventHandler(UserService userService) {
        Objects.requireNonNull(userService);
        this.userService = userService;
    }

    @HandleBeforeSave
    public void updateRestaurantInformationIsRestaurantAdminCheck(Restaurant r) { User user = userService.getLoggedInUser();
        if (!isSystemAdmin(user)) {
            if (!isRestaurantOwner(r, user)) {
                throw new InvalidOperationException(DO_NOT_ALLOW_REMOVE_YOURSELF);
            }
        }
    }

    @HandleBeforeSave
    @HandleBeforeCreate
    public void removeRequestsForAddingRestaurant(Restaurant r) {
        r.getOwners().forEach(o -> {
            AddRestaurantRequest request = o.getAddRestaurantRequest();
            if(request != null) {
                request.setUser(null);
                o.setAddRestaurantRequest(null);
            }
        });
    }

    private boolean isRestaurantOwner(Restaurant r, User u) {
        return r.getOwners().contains(u);
    }
}
