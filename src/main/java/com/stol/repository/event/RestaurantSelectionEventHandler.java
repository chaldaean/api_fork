package com.stol.repository.event;

import com.stol.model.RestaurantSelection;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

@RepositoryEventHandler
public class RestaurantSelectionEventHandler {

    @HandleBeforeCreate
    public void calculateTotalRestaurantsInSelection(RestaurantSelection s) {
        s.setTotal(s.getRestaurants().size());
    }
}