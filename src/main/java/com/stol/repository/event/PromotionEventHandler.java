package com.stol.repository.event;

import com.stol.model.Promotion;
import com.stol.model.user.User;
import com.stol.service.user.UserService;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.access.AccessDeniedException;

import java.util.Objects;

import static com.stol.configuration.constant.Messages.NOT_OWNER;
import static com.stol.service.user.LoginStaticService.isSystemAdmin;

@RepositoryEventHandler
public class PromotionEventHandler {
    private final UserService userService;

    public PromotionEventHandler(UserService userService) {
        Objects.requireNonNull(userService);
        this.userService = userService;
    }

    @HandleBeforeCreate
    public void onCreateCheckRestaurantOwner(Promotion promotion) {
        permissionValidation(promotion);
    }

    @HandleBeforeSave
    public void onUpdateCheckRestaurantOwner(Promotion promotion) {
        permissionValidation(promotion);
    }

    @HandleBeforeDelete
    public void onDeleteCheckRestaurantOwner(Promotion promotion) {
        permissionValidation(promotion);
    }

    private void permissionValidation(Promotion promotion) {
        User user = userService.getLoggedInUser();
        if (!isRestaurantOwner(promotion, user)) {
            if (!isSystemAdmin(user)) {
                throw new AccessDeniedException(NOT_OWNER);
            }
        }
    }

    private boolean isRestaurantOwner(Promotion promotion, User user) {
        return promotion.getRestaurant().getOwners().contains(user);
    }
}
