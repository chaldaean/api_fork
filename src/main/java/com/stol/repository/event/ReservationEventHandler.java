package com.stol.repository.event;

import com.stol.exception.InvalidOperationException;
import com.stol.model.*;
import com.stol.model.property.ReservationStatus;
import com.stol.model.user.User;
import com.stol.repository.HappyHourRepository;
import com.stol.repository.ReviewNotificationRepository;
import com.stol.service.PromotionService;
import com.stol.service.telegram.TelegramNotifier;
import com.stol.service.user.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

import static com.stol.configuration.constant.Messages.DO_NOT_HAVE_ACCESS_TO_THIS_RESOURCE;
import static com.stol.service.user.LoginStaticService.isSystemAdmin;

@RepositoryEventHandler
public class ReservationEventHandler {
    private final PromotionService promotionService;
    private final ReviewNotificationRepository reviewNotificationRepository;
    private final HappyHourRepository happyHourRepository;
    private final UserService userService;
    private final TelegramNotifier telegramNotifier;
    @Value("${stol.review.show-notification-time-daley}")
    private int SHOW_NOTIFICATION_TIME_DALEY;

    public ReservationEventHandler(PromotionService promotionService, ReviewNotificationRepository reviewNotificationRepository,
                                   HappyHourRepository happyHourRepository, UserService userService, TelegramNotifier telegramNotifier) {
        Objects.requireNonNull(promotionService);
        Objects.requireNonNull(reviewNotificationRepository);
        Objects.requireNonNull(happyHourRepository);
        Objects.requireNonNull(userService);
        Objects.requireNonNull(telegramNotifier);
        this.promotionService = promotionService;
        this.reviewNotificationRepository = reviewNotificationRepository;
        this.happyHourRepository = happyHourRepository;
        this.userService = userService;
        this.telegramNotifier = telegramNotifier;
    }

    @HandleBeforeCreate
    public void validateNotesOnCreate(Reservation reservation) {
        User user = userService.getLoggedInUser();

        if (!StringUtils.isEmpty(reservation.getNotes()) && !isSystemAdmin(user)) {
            throw new AccessDeniedException(DO_NOT_HAVE_ACCESS_TO_THIS_RESOURCE);
        }
    }

    @HandleBeforeCreate
    public void setClient(Reservation reservation) {
        if (Objects.isNull(reservation.getClient())) {
            reservation.setClient(userService.getLoggedInUser());
        }

        validateVisitTime(reservation);
    }

    @HandleBeforeCreate
    public void addDiscountIfExist(Reservation reservation) {
        happyHourRepository.findActive(reservation.getRestaurant(), reservation.getDateTime().toLocalDate()).stream()
                .filter(hh -> isContains(reservation, hh))
                .forEach(hh -> {
                    Discount discount = new Discount();
                    discount.setValue(hh.getDiscount());
                    discount.setHappyHour(hh);
                    reservation.setDiscount(discount);
                });
    }

    @HandleAfterCreate
    public void informOwnerAboutNewReservation(Reservation reservation) {
        Restaurant restaurant = reservation.getRestaurant();
        updatePromotionInformation(restaurant);
        updateRestaurantHappyHours(reservation);
    }

    @HandleAfterCreate
    public void sendTelegramMessage(Reservation reservation) {
        telegramNotifier.sendNewReservationNotification(reservation);
    }


    @HandleBeforeSave
    public void validateNotesOnUpdate(Reservation reservation) {
        User user = userService.getLoggedInUser();

        if (!Objects.equals(reservation.getNotes(), reservation.getPreviousNotes())) {
            if (!isSystemAdmin(user)) {
                if (!isRestaurantOwner(reservation, user)) {
                    throw new AccessDeniedException(DO_NOT_HAVE_ACCESS_TO_THIS_RESOURCE);
                }
            }
        }
    }


    @HandleBeforeSave
    public void updateReservationNotOwner(Reservation reservation) {
        setStatusChanged(reservation);

        User user = userService.getLoggedInUser();
        User client = reservation.getClient();
        if (!isReservationOwner(user, client)) {
            if (!isRestaurantOwner(reservation, user)) {
                if (!isSystemAdmin(user)) {
                    throw new AccessDeniedException(DO_NOT_HAVE_ACCESS_TO_THIS_RESOURCE);
                }
            }
        }
    }

    @HandleAfterSave
    public void createReviewNotificationIfReservationApproved(Reservation reservation) {
        if (Objects.equals(reservation.getStatus(), ReservationStatus.CONFIRMED)
                && LocalDateTime.now().isBefore(reservation.getDateTime().plusWeeks(2))) {
            ReviewNotification notification = reviewNotificationRepository.findByReservation(reservation);
            if (Objects.isNull(notification)) {
                notification = new ReviewNotification();
                notification.setActivationTime(reservation.getDateTime().plusHours(SHOW_NOTIFICATION_TIME_DALEY));
                notification.setEndTime(reservation.getDateTime().plusWeeks(2));
                notification.setReservation(reservation);
                notification.setRestaurant(reservation.getRestaurant());
                notification.setUser(reservation.getClient());
                reviewNotificationRepository.save(notification);
            }
        }
    }

    private void updatePromotionInformation(Restaurant restaurant) {
        Set<Promotion> promotions = restaurant.getPromotions();
        promotions.forEach(promotion -> {
            if (Objects.nonNull(promotion)) {
                int total = promotion.getTotal();
                int used = promotion.getUsed();
                if (total > 0 && used < total) {
                    promotion.setUsed(used + 1);
                    if (promotion.getUsed() == total) {
                        promotion.setActive(false);
                    }
                    promotionService.savePromotion(promotion);
                }
            }
        });
    }

    private boolean isReservationOwner(User user, User client) {
        return Objects.equals(client, user);
    }

    private boolean isRestaurantOwner(Reservation reservation, User user) {
        return reservation.getRestaurant().getOwners().contains(user);
    }

    private void updateRestaurantHappyHours(Reservation reservation) {
        happyHourRepository.findActive(reservation.getRestaurant(), reservation.getDateTime().toLocalDate()).stream()
                .filter(hh -> isContains(reservation, hh))
                .forEach(hh -> {
                    hh.setUsedQuantity(hh.getUsedQuantity() + 1);
                    if (hh.getQuantity() != null && hh.getQuantity() <= hh.getUsedQuantity()) {
                        hh.setActive(false);
                    }
                    happyHourRepository.save(hh);
                });
    }

    private boolean isContains(Reservation reservation, HappyHour hh) {
        for (Time t : hh.getTimes()) {
            if (t.getTime().equals(reservation.getDateTime().toLocalTime())) {
                return true;
            }
        }
        return false;
    }

    private void setStatusChanged(Reservation reservation) {
        if (reservation.getPreviousStatus() != reservation.getStatus()) {
            reservation.setStatusChanged(LocalDateTime.now());
        }
    }

    private void validateVisitTime(Reservation reservation) {
        if (reservation.getDateTime().isBefore(LocalDateTime.now())) {
            throw new InvalidOperationException(String.format("Restaurant id %s, user id %s, visit time %s: Reservation visit time cannot be in past",
                    reservation.getRestaurant().getId(), reservation.getClient().getId(), reservation.getDateTime()));
        }
    }
}
