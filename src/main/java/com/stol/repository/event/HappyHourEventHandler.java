package com.stol.repository.event;

import com.stol.exception.InvalidOperationException;
import com.stol.model.HappyHour;
import com.stol.model.Time;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.stol.configuration.constant.Messages.HAPPY_HOURS_NO_OVERLAPPING_TIME_ALLOWED;

@RepositoryEventHandler
public class HappyHourEventHandler {

    @HandleBeforeSave
    public void validateHappyHourOnUpdate(HappyHour happyHour) {
        List<HappyHour> happyHours = new ArrayList<>(happyHour.getRestaurant().getHappyHours());
        checkNoOverlappingTimes(happyHours);
    }

    @HandleBeforeCreate
    public void validateHappyHourOnCreate(HappyHour happyHour) {
        List<HappyHour> happyHours = new ArrayList<>(happyHour.getRestaurant().getHappyHours());
        happyHours.add(happyHour);
        checkNoOverlappingTimes(happyHours);
    }

    private void checkNoOverlappingTimes(List<HappyHour> happyHours) {
        HashSet<Time> times = new HashSet<>();
        happyHours.stream()
                .filter(hh -> hh.getTimes() != null)
                .flatMap(hh -> hh.getTimes().stream())
                .forEach(t -> {
                    if (!times.add(t)) {
                        throw new InvalidOperationException(HAPPY_HOURS_NO_OVERLAPPING_TIME_ALLOWED);
                    }
                });
    }
}
