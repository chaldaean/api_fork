package com.stol.repository.event;

import com.stol.model.*;
import com.stol.model.user.User;
import com.stol.service.FileService;
import org.springframework.data.rest.core.annotation.HandleAfterDelete;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import java.util.Objects;

@RepositoryEventHandler
@Deprecated
public class MediaFileEventHandler {
    private final FileService fileService;

    public MediaFileEventHandler(FileService fileService) {
        Objects.requireNonNull(fileService);
        this.fileService = fileService;
    }

    @HandleAfterDelete
    public void deleteFileFromServer(Tag tag) {
        MediaFile icon = tag.getIcon();
        if (Objects.nonNull(icon)) {
            fileService.deleteFile(icon);
        }
    }

    @HandleAfterDelete
    public void deleteFileFromServer(Cuisine cuisine) {
        MediaFile icon = cuisine.getIcon();
        if (Objects.nonNull(icon)) {
            fileService.deleteFile(icon);
        }
    }

    @HandleAfterDelete
    public void deleteFileFromServer(RestaurantSelection restaurantSelection) {
        MediaFile picture = restaurantSelection.getPicture();
        if (Objects.nonNull(picture)) {
            fileService.deleteFile(picture);
        }
    }

    @HandleAfterDelete
    public void deleteFileFromServer(User user) {
        MediaFile photo = user.getPhoto();
        if (Objects.nonNull(photo)) {
            fileService.deleteFile(photo);
        }
    }

    @HandleAfterDelete
    public void deleteFileFromServer(Restaurant restaurant) {
        MediaFile topPicture = restaurant.getTopPicture();
        if (Objects.nonNull(topPicture)) {
            fileService.deleteFile(topPicture);
        }
        MediaFile menu = restaurant.getMenu();
        if (Objects.nonNull(menu)) {
            fileService.deleteFile(menu);
        }
        for (MediaFile file : restaurant.getPictures()) {
            fileService.deleteFile(file);
        }
    }

    @HandleAfterDelete
    public void deleteFileFromServer(Article article) {
        MediaFile picture = article.getPicture();
        if (Objects.nonNull(picture)) {
            fileService.deleteFile(picture);
        }
    }
}
