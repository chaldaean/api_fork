package com.stol.repository.event;

import com.stol.model.ReviewNotification;
import com.stol.model.user.User;
import com.stol.service.user.UserService;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.access.AccessDeniedException;

import java.util.Objects;

import static com.stol.configuration.constant.Messages.DO_NOT_HAVE_ACCESS_TO_THIS_RESOURCE;

@RepositoryEventHandler
public class ReviewNotificationEventHandler {
    private final UserService userService;

    public ReviewNotificationEventHandler(UserService userService) {
        Objects.requireNonNull(userService);
        this.userService = userService;
    }

    @HandleBeforeDelete
    public void validateOwner(ReviewNotification reviewNotification) {
        User user = userService.getLoggedInUser();
        User client = reviewNotification.getUser();
        if (!Objects.equals(client, user)) {
            throw new AccessDeniedException(DO_NOT_HAVE_ACCESS_TO_THIS_RESOURCE);
        }
    }
}
