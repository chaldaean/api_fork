package com.stol.repository.event;

import com.stol.exception.InvalidOperationException;
import com.stol.model.Reservation;
import com.stol.model.Restaurant;
import com.stol.model.Review;
import com.stol.model.ReviewNotification;
import com.stol.model.user.User;
import com.stol.repository.RestaurantRepository;
import com.stol.repository.ReviewNotificationRepository;
import com.stol.repository.ReviewRepository;
import com.stol.service.user.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static com.stol.configuration.constant.Messages.*;
import static com.stol.model.property.ReservationStatus.CONFIRMED;

@RepositoryEventHandler
public class ReviewEventHandler {
    @Value("${stol.review.days_after_allow_review}")
    private int DAYS_AFTER_ALLOW_REVIEW;

    @Value("${stol.rating.minimum_to_aggregate}")
    private int MINIMUM_RATINGS_TO_AGGREGATE;


    private final ReviewRepository reviewRepository;
    private final RestaurantRepository restaurantRepository;
    private final ReviewNotificationRepository reviewNotificationRepository;
    private final UserService userService;

    public ReviewEventHandler(ReviewRepository reviewRepository, RestaurantRepository restaurantRepository, ReviewNotificationRepository reviewNotificationRepository,
                              UserService userService) {
        Objects.requireNonNull(reviewRepository);
        Objects.requireNonNull(restaurantRepository);
        Objects.requireNonNull(reviewNotificationRepository);
        Objects.requireNonNull(userService);
        this.reviewRepository = reviewRepository;
        this.restaurantRepository = restaurantRepository;
        this.reviewNotificationRepository = reviewNotificationRepository;
        this.userService = userService;
    }

    @HandleBeforeCreate
    public void setUserAndvalidateReview(Review review) {
        if (Objects.isNull(review.getUser())) {
            review.setUser(userService.getLoggedInUser());
        }
        checkIfUserIsCorrect(review);
        onlyOneReviewForReservationCheck(review);
        timeAndReservationValidation(review);
        checkIfReviewEmpty(review);
    }

    @HandleAfterCreate
    public void updateConnectedInformation(Review review) {
        updateRestaurantRating(review);
        deleteReviewNotification(review);
    }

    private void updateRestaurantRating(Review review) {
        Restaurant restaurant = review.getRestaurant();
        List<Integer> allRatingValuesForRestaurant = reviewRepository.findAllRatingValuesForRestaurant(restaurant.getId());

        if(allRatingValuesForRestaurant.size() >= MINIMUM_RATINGS_TO_AGGREGATE) {
            int ratings = allRatingValuesForRestaurant.stream().mapToInt(Integer::intValue).sum();
            double rating = (double) ratings / (double) allRatingValuesForRestaurant.size();
            restaurant.setRating((double) Math.round(rating*10)/10);
            restaurantRepository.save(restaurant);
        }
    }

    private void deleteReviewNotification(Review review) {
        ReviewNotification reviewNotification = reviewNotificationRepository.findByReservation(review.getReservation());
        if(reviewNotification != null) {
            reviewNotificationRepository.deleteById(reviewNotification.getId());
        }
    }

    private void timeAndReservationValidation(Review review) {
        Reservation reservation = review.getReservation();
        LocalDate visitDate = reservation.getDateTime().toLocalDate();
        if (!Objects.equals(reservation.getStatus(), CONFIRMED)) {
            throw new InvalidOperationException(REVIEW_ONLY_FOR_CONFIRMED);
        } else if(visitDate.isBefore(LocalDate.now().minusDays(DAYS_AFTER_ALLOW_REVIEW))) {
            throw new InvalidOperationException(MAKE_REVIEW_TIME_IS_EXPIRED);
        }
    }

    private void checkIfUserIsCorrect(Review review) {
        User user = userService.getLoggedInUser();
        User client = review.getReservation().getClient();
        if (!Objects.equals(user, client)) {
            throw new InvalidOperationException(NO_RESERVATION_FOR_REVIEW);
        }
    }

    private void onlyOneReviewForReservationCheck(Review review) {
        boolean hasReview = reviewRepository.findByReservation(review.getReservation()).isPresent();
        if (hasReview) {
            throw new InvalidOperationException(ONLY_ONE_REVIEW_ALLOWED);
        }
    }

    private void checkIfReviewEmpty(Review review) {
        if (Objects.isNull(review.getRating()) && Objects.isNull(review.getComment())) {
            throw new InvalidOperationException(EMPTY_REVIEW);
        }
    }
}
