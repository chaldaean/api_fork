package com.stol.repository.event;

import com.stol.model.Reservation;
import com.stol.model.property.ReservationStatus;
import com.stol.service.notification.NotificationService;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import java.util.Objects;
import static com.stol.model.property.ReservationStatus.*;

@RepositoryEventHandler
public class ReservationNotificationEventHandler {
    private final NotificationService notificationService;

    public ReservationNotificationEventHandler(NotificationService notificationService) {
        Objects.requireNonNull(notificationService);
        this.notificationService = notificationService;
    }

    @HandleAfterCreate
    public void sendPushNotificationToRestaurantAdmin(Reservation reservation) {
        ReservationStatus status = reservation.getStatus();
        if (Objects.equals(status, NEW)) {
            reservation.getRestaurant().getOwners().forEach(
                    o -> notificationService.notifyClientAboutNewReservation(o, reservation.getRestaurant()));
        }
    }

    @HandleAfterSave
    public void sendPushNotificationIfReservationStatusChanged(Reservation reservation) {
        ReservationStatus status = reservation.getStatus();
        if (Objects.equals(status, CONFIRMED)) {
            notificationService.notifyUserAboutConfirmedReservation(reservation.getClient(), reservation.getRestaurant());
        } else if (Objects.equals(status, REJECTED)) {
            notificationService.notifyUserAboutRejectedReservation(reservation.getClient(), reservation.getRestaurant());
        } else if (Objects.equals(status, CANCELED)) {
            reservation.getRestaurant().getOwners().forEach(
                    o -> notificationService.notifyClientAboutCanceledReservation(o, reservation.getRestaurant()));
        }
    }
}
