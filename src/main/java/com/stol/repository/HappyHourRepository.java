package com.stol.repository;

import com.stol.model.HappyHour;
import com.stol.model.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(path = "happy_hours")
public interface HappyHourRepository extends Repository<HappyHour, Long> {
    HappyHour save(HappyHour happyHour);

    Optional<HappyHour> findById(Long id);

    Page<HappyHour> findAll(Pageable pageable);

    @Query("select hh from HappyHour hh where hh.restaurant = ?1 and hh.active = true and (hh.quantity = null or hh.quantity > hh.usedQuantity) and " +
            "(hh.expirationDate = null or hh.expirationDate >= ?2) and (hh.activationDate = null or hh.activationDate <= ?2) order by hh.discount desc")
    @RestResource(path = "active")
    List<HappyHour> findActive(@Param("restaurant") Restaurant restaurant, @Param("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date);
}
