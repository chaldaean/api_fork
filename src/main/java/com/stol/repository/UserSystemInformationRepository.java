package com.stol.repository;

import com.stol.model.user.UserSystemInformation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@RepositoryRestResource(exported = false)
public interface UserSystemInformationRepository extends Repository<UserSystemInformation, Long> {
    UserSystemInformation save(UserSystemInformation userSystemInformation);

    @Transactional(readOnly = true)
    UserSystemInformation findByPhoneNumber(String phoneNumber);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query(value = "SELECT count(DISTINCT u.id) from user_system_information i \n" +
            "  inner join user u on i.phone = u.phone\n" +
            "  INNER JOIN user_roles ur on u.id = ur.user_id\n" +
            "  inner join user_role r on ur.role_id = r.id\n" +
            "where r.role = 'ROLE_USER' and i.last_login_time >= ?1", nativeQuery = true)
    long numberOfActiveUsers(LocalDateTime date);
}
