package com.stol.repository;

import com.stol.model.Marker;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface MarkerRepository extends Repository<Marker, Long> {
    Marker save(Marker marker);

    Optional<Marker> findById(Long id);
}
