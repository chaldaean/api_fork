package com.stol.repository;

import com.stol.model.MediaFile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(path = "files")
public interface MediaFileRepository extends Repository<MediaFile, Long> {
    @RestResource(exported = false)
    MediaFile save(MediaFile mediaFile);

    Optional<MediaFile> findById(Long id);

    void deleteById(Long id);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "SELECT f.id FROM media_file f\n" +
            "LEFT JOIN article a ON a.picture_id = f.id\n" +
            "LEFT JOIN cuisine c ON c.icon_id = f.id\n" +
            "LEFT JOIN restaurant rm ON rm.menu_id = f.id\n" +
            "LEFT JOIN restaurant rtp ON rtp.top_picture_id = f.id\n" +
            "LEFT JOIN restaurant_pictures rp ON rp.media_file_id = f.id\n" +
            "LEFT JOIN restaurant_selection rs ON rs.picture_id = f.id\n" +
            "LEFT JOIN tag t ON t.icon_id = f.id \n" +
            "LEFT JOIN user u ON u.photo_id = f.id \n" +
            "WHERE a.picture_id IS NULL and c.icon_id IS NULL and rm.menu_id IS NULL and rtp.top_picture_id IS NULL " +
            "and rp.media_file_id IS NULL and rs.picture_id IS NULL and t.icon_id IS NULL and u.photo_id IS NULL")
    List<BigInteger> findDetachedPictures();
}
