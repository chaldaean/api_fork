package com.stol.repository;

import com.stol.model.Cuisine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource
public interface CuisineRepository extends Repository<Cuisine, Long> {
    Cuisine save(Cuisine cuisine);

    Optional<Cuisine> findById(Long id);

    @Query("select c from Cuisine c where c.active=true")
    Page<Cuisine> findAll(Pageable pageable);

    @Query("select c from Cuisine c")
    @RestResource(path = "all")
    Page<Cuisine> findAllWithInactive(Pageable pageable);

    void deleteById(Long id);
}
