package com.stol.repository;

import com.stol.model.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource
public interface ArticleRepository extends Repository<Article, Long> {
    Article save(Article article);

    Optional<Article> findById(Long id);

    @Query("select a from Article a where a.active=true")
    Page<Article> findAll(Pageable pageable);

    @Query("select a from Article a")
    @RestResource(path = "all")
    Page<Article> findAllWithInactive(Pageable pageable);

    void deleteById(Long id);
}
