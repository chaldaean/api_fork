package com.stol.repository;

import com.stol.model.Time;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface TimeRepository extends Repository<Time, Long> {
    Time save(Time tag);

    Optional<Time> findById(Long id);

    List<Time> findAll();

    void deleteById(Long id);
}
