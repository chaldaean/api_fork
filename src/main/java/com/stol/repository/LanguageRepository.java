package com.stol.repository;

import com.stol.model.Language;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource
public interface LanguageRepository extends Repository<Language, Long> {
    Language save(Language user);

    Optional<Language> findById(Long id);

    @Query("select l from Language l where l.active = true")
    Page<Language> findAll(Pageable pageable);

    @Query("select l from Language l")
    @RestResource(path = "all")
    Page<Language> findAllWithInactive(Pageable pageable);

    Optional<Language> findByCode(String code);

    void deleteById(Long id);
}
