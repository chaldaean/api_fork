package com.stol.repository;

import com.stol.model.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource
public interface TagRepository extends Repository<Tag, Long> {
    Tag save(Tag tag);

    Optional<Tag> findById(Long id);

    @Query("select t from Tag t where t.active=true")
    Page<Tag> findAll(Pageable pageable);

    @Query("select t from Tag t")
    @RestResource(path = "all")
    Page<Tag> findAllWithInactive(Pageable pageable);

    void deleteById(Long id);
}
