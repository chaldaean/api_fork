package com.stol.repository;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.StringExpression;
import com.stol.model.QRestaurant;
import com.stol.model.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface RestaurantRepository extends Repository<Restaurant, Long>, QuerydslPredicateExecutor<Restaurant>, QuerydslBinderCustomizer<QRestaurant>, RestaurantRepositoryCustom {
    @EntityGraph(attributePaths = {"pictures", "tags", "cuisines", "happyHours", "menu", "analogous", "promotions"},
            type = EntityGraph.EntityGraphType.LOAD)
    <T> List<T> findByIdIn(List<Long> ids, Class<T> clazz);

    Restaurant save(Restaurant restaurant);

    Optional<Restaurant> findById(Long var1);

    @Query("select distinct r from Restaurant r where r.active = true")
    Page<Restaurant> findAll(Pageable pageable);

    @RestResource(path = "all", rel = "restaurants")
    @Query("select distinct r from Restaurant r")
    Page<Restaurant> findAllRestaurants(Pageable pageable);

    @Transactional(readOnly = true)
    Page<Restaurant> findAllByNameContains(@Param("text") String text, Pageable pageable);

    @RestResource(path = "my", rel = "restaurants")
    @Query("select distinct r from Restaurant r inner join r.owners o where o.credential.login = ?#{principal?.username}")
    @Transactional(readOnly = true)
    List<Restaurant> findAllForLoggedInUser();

    void deleteById(Long id);

    @Query(value = "select min(r.averageTicket), max(r.averageTicket) from Restaurant r where r.active = true")
    @RestResource(exported = false)
    @Transactional(readOnly = true)
    Double[][] findMinMaxRestaurantsAverageCheck();

    @Override
    default public void customize(QuerydslBindings bindings, QRestaurant restaurant) {
        bindings.bind(restaurant.active).first(BooleanExpression::eq);
        bindings.bind(restaurant.longitude).all((path, values) -> {
            final List<BigDecimal> longitudes = new ArrayList<>(values);
            Collections.sort(longitudes);
            if (longitudes.size() == 1) {
                return Optional.of(path.eq(longitudes.get(0)));
            } else {
                return Optional.of(path.between(longitudes.get(0), longitudes.get(1)));
            }
        });
        bindings.bind(restaurant.latitude).all((path, values) -> {
            final List<BigDecimal> latitudes = new ArrayList<>(values);
            Collections.sort(latitudes);
            if (latitudes.size() == 1) {
                return Optional.of(path.eq(latitudes.get(0)));
            } else {
                return Optional.of(path.between(latitudes.get(0), latitudes.get(1)));
            }
        });

        bindings.bind(restaurant.averageTicket).all((path, values) -> {
            final List<Double> tickets = new ArrayList<>(values);
            Collections.sort(tickets);
            if (tickets.size() == 1) {
                return Optional.of(path.eq(tickets.get(0)));
            } else {
                return Optional.of(path.between(tickets.get(0), tickets.get(1)));
            }
        });

        bindings.bind(restaurant.name).first(StringExpression::containsIgnoreCase);
    }
}
