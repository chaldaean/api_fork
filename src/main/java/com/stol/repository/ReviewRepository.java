package com.stol.repository;

import com.querydsl.core.types.dsl.StringPath;
import com.stol.model.QReview;
import com.stol.model.Reservation;
import com.stol.model.Restaurant;
import com.stol.model.Review;
import com.stol.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface ReviewRepository extends Repository<Review, Long>, QuerydslPredicateExecutor<Review>, QuerydslBinderCustomizer<QReview> {
    Review save(Review review);

    Optional<Review> findById(Long id);

    @RestResource(exported = false)
    Optional<Review> findByReservation(Reservation reservation);

    Page<Review> findAll(Pageable pageable);

    @Transactional(readOnly = true)
    Page<Review> findAllByRestaurant(@Param("restaurant") Restaurant restaurant, Pageable pageable);

    @Transactional(readOnly = true)
    Page<Review> findAllByUser(@Param("user") User user, Pageable pageable);

    @RestResource(exported = false)
    @Query("select r.rating from Review r where r.rating IS NOT NULL and r.restaurant.id = ?1")
    @Transactional(readOnly = true)
    List<Integer> findAllRatingValuesForRestaurant(long restaurantId);

    @RestResource(path = "my")
    @Transactional(readOnly = true)
    @Query("select distinct r from Review r where r.user.credential.login = ?#{principal?.username} and r.restaurant = :#{#restaurant}")
    Page<Review> findByUserAndRestaurant(@Param("restaurant") Restaurant restaurant, Pageable pageable);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query("select COUNT(r) from Review r where r.user.credential.login = ?#{principal?.username} and r.restaurant = :#{#restaurant}")
    Integer countByUserAndRestaurant(@Param("restaurant") Restaurant restaurant);

    void deleteById(Long id);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query("select COUNT(r) from Review r where r.creationDate >= ?1")
    long numberOfReviews(LocalDateTime date);

    @RestResource(exported = false)
    @Transactional(readOnly = true)
    @Query("select COUNT(r) from Review r where r.restaurant = ?1")
    long numberOfAllRestaurantReviews(Restaurant restaurant);

    @Override
    default void customize(QuerydslBindings bindings, QReview root) {
        bindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));
        bindings.bind(root.creationDate).first((path, value) ->
                path.year().eq(value.getYear())
                        .and(path.month().eq(value.getMonthValue()))
                        .and(path.dayOfMonth().eq(value.getDayOfMonth())));

    }
}
