package com.stol.repository;

import com.stol.exception.InvalidOperationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

public class RestaurantRepositoryImpl implements RestaurantRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;



    @Override
    @Transactional(readOnly = true)
    public Page<Long> findRestaurants(Double avgTicketFrom, Double avgTicketTo, List<Long> cuisines, List<Long> tags,
                                      BigDecimal latitude, BigDecimal longitude, String sorting, String languageCode, Pageable pageable) {

        if(StringUtils.equals(sorting, "name,asc")) {
            sorting = "name";
        }

        if(!StringUtils.equalsAny(sorting, "name", "distance", "rating")) {
            throw new IllegalArgumentException("\"sorting\" must be one of the following: \"name\", \"distance\", \"rating\"");
        }

        StringBuilder sql = new StringBuilder("select r.id from restaurant r ");

        if (sorting.equals("name")) {
            sql.append(" left join (select i.name, i.restaurant_id from restaurant_i18n i");
            sql.append("  inner join language l on i.language_id = l.id and l.code = :langCode)");
            sql.append(" ii on r.id = ii.restaurant_id");
        }

        sql.append(" where r.active = 1");

        if (avgTicketFrom != null) {
            sql.append(" and r.average_ticket >= :avgTicketFrom");
        }

        if (avgTicketTo != null) {
            sql.append(" and r.average_ticket <= :avgTicketTo");
        }

        if (cuisines != null && !cuisines.isEmpty()) {
            sql.append(" and (select count(rc.cuisine_id) from restaurant_cuisines rc where rc.restaurant_id = r.id and rc.cuisine_id in :cuisines) > 0");
        }

        if (tags != null && !tags.isEmpty()) {
            sql.append(" and (select count(rt.tag_id) from restaurant_tags rt where rt.restaurant_id = r.id and rt.tag_id in :tags) > 0");
        }

        sql.append(" order by");

        switch (sorting) {
            case "distance":
                sql.append(" (6371 * acos(cos(radians(:latitude)) * cos(radians(r.latitude)) * cos(radians(r.longitude) - radians(:longitude)) + sin(radians(:latitude)) * sin(radians(r.latitude)))) asc, name asc");
                break;
            case "rating":
                sql.append(" rating desc, name asc");
                break;
            case "name":
                sql.append(" coalesce(ii.name, r.name) asc");
        }

        Query q = entityManager.createNativeQuery(sql.toString());

        if (avgTicketFrom != null) {
            q.setParameter("avgTicketFrom", avgTicketFrom);
        }

        if (avgTicketTo != null) {
            q.setParameter("avgTicketTo", avgTicketTo);
        }

        if (cuisines != null && !cuisines.isEmpty()) {
            q.setParameter("cuisines", cuisines);
        }

        if (tags != null && !tags.isEmpty()) {
            q.setParameter("tags", tags);
        }

        if (sorting.equals("distance")) {
            q.setParameter("latitude", latitude);
            q.setParameter("longitude", longitude);
        }

        if(sorting.equals("name")) {
            q.setParameter("langCode", languageCode);
        }

        int total = q.getResultList().size();
        q.setFirstResult((int) pageable.getOffset());
        q.setMaxResults(pageable.getPageSize());
        List<Long> ids = q.getResultList().stream().mapToLong(obj -> ((BigInteger) obj).longValue()).boxed().collect(Collectors.toList());

        return new PageImpl<>(ids, pageable, total);
    }
}
