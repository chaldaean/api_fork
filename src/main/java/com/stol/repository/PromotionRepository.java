package com.stol.repository;

import com.stol.model.Promotion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RepositoryRestResource
public interface PromotionRepository extends Repository<Promotion, Long> {
    Promotion save(Promotion promotion);

    Optional<Promotion> findById(Long id);

    Page<Promotion> findAll(Pageable pageable);

    @RestResource(path = "my_restaurants", rel = "restaurant_promotions")
    @Query("select distinct p from Promotion p inner join p.restaurant r inner join r.owners o where o.credential.login = ?#{principal?.username}")
    @Transactional(readOnly = true)
    Page<Promotion> findAllForLoggedInUserRestaurants(Pageable pageable);

    void deleteById(Long id);
}
