package com.stol.repository;

import com.stol.model.property.UserRole;
import com.stol.model.user.Role;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface RoleRepository extends Repository<Role, Long> {
    @Transactional(readOnly = true)
    @RestResource(exported = false)
    Role findByRole(UserRole userRole);

    Optional<Role> findById(Long id);

    List<Role> findAll();
}
