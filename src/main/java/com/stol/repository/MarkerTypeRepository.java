package com.stol.repository;

import com.stol.model.MarkerType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(path = "marker_types")
public interface MarkerTypeRepository extends Repository<MarkerType, Long> {
    MarkerType save(MarkerType markerType);

    Optional<MarkerType> findById(Long id);

    Page<MarkerType> findAll(Pageable pageable);
    
    void deleteById(Long id);
}
