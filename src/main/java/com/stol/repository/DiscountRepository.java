package com.stol.repository;

import com.stol.model.Discount;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(exported = false)
public interface DiscountRepository extends Repository<Discount, Long> {
    Discount save(Discount discount);

    Optional<Discount> findById(Long id);
}
