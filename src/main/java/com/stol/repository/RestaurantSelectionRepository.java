package com.stol.repository;

import com.stol.model.RestaurantSelection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource(path = "restaurant_selections")
public interface RestaurantSelectionRepository extends Repository<RestaurantSelection, Long> {
    RestaurantSelection save(RestaurantSelection restaurantSelection);

    Optional<RestaurantSelection> findById(Long id);

    @Query("select s from RestaurantSelection s where s.active=true")
    Page<RestaurantSelection> findAll(Pageable pageable);

    @Query("select s from RestaurantSelection s")
    @RestResource(path = "all")
    Page<RestaurantSelection> findAllWithInactive(Pageable pageable);

    void deleteById(Long id);
}
