package com.stol.schedule;

import com.stol.model.MediaFile;
import com.stol.model.Reservation;
import com.stol.model.ReviewNotification;
import com.stol.repository.MediaFileRepository;
import com.stol.repository.ReviewNotificationRepository;
import com.stol.service.FileService;
import com.stol.service.ReservationService;
import com.stol.service.notification.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.stol.model.property.ReservationStatus.REJECTED;

public class ScheduledTasks {
    private final MediaFileRepository mediaFileRepository;
    private final FileService fileService;
    private final ReservationService reservationService;
    private final ReviewNotificationRepository reviewNotificationRepository;
    private final NotificationService notificationService;


    private final Logger logger = LoggerFactory.getLogger(getClass());

    public ScheduledTasks(FileService fileService, MediaFileRepository mediaFileRepository, ReservationService reservationService,
                          ReviewNotificationRepository reviewNotificationRepository, NotificationService notificationService) {
        Objects.requireNonNull(fileService);
        Objects.requireNonNull(mediaFileRepository);
        Objects.requireNonNull(reservationService);
        Objects.requireNonNull(reservationService);
        Objects.requireNonNull(notificationService);

        this.mediaFileRepository = mediaFileRepository;
        this.fileService = fileService;
        this.reservationService = reservationService;
        this.reviewNotificationRepository = reviewNotificationRepository;
        this.notificationService = notificationService;
    }

    @Scheduled(cron = "0 0 3 1 * ?")
    @Transactional
    public void deleteDetachedPictures() {
        logger.info("Schedule: check for detached pictures.");

        List<BigInteger> pictures = mediaFileRepository.findDetachedPictures();
        for (BigInteger id : pictures) {
            Optional<MediaFile> picture = mediaFileRepository.findById(id.longValue());
            if (picture.isPresent()) {
                mediaFileRepository.deleteById(id.longValue());
                fileService.deleteFile(picture.get());

                logger.info("Schedule: delete file with id {}.", id.longValue());
            }
        }
    }

    //10 minutes
    @Scheduled(fixedDelay = 600000)
    @Transactional
    public void reviewNotificationPush() {
        logger.info("Schedule: sending review notification pushes.");
        reviewNotificationRepository.findAllActiveUnsent().forEach(n -> {
            notificationService.notifyUserAboutReviewNeeded(n.getUser(), n.getRestaurant());
            n.setSent(true);
            reviewNotificationRepository.save(n);
        });
    }

    //5 minutes
    @Scheduled(fixedDelay = 300000)
    @Transactional
    public void cancelAllNotConfirmedReservations() {
        logger.info("Schedule: check for not confirmed reservations.");

        List<Reservation> reservations = reservationService.findAllNewOutdated();
        reservations.forEach(r -> {
            r.setStatus(REJECTED);

            reservationService.save(r);

            notificationService.notifyUserAboutRejectedReservation(r.getClient(), r.getRestaurant());;

            logger.info("Schedule: reject not confirmed reservation with id {}.", r.getId());
        });
    }

    //One day
    @Scheduled(fixedDelay = 86400000)
    @Transactional
    public void deleteOldReviewNotification() {
        logger.info("Schedule: check for old review notifications.");

        List<ReviewNotification> notification = reviewNotificationRepository.findAllOverdue();
        notification.forEach(r -> {
            reviewNotificationRepository.deleteById(r.getId());

            logger.info("Schedule: delete ReviewNotification with id {}.", r.getId());
        });
    }

}
