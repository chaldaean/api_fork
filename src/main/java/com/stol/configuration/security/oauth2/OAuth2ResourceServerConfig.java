package com.stol.configuration.security.oauth2;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.expression.OAuth2WebSecurityExpressionHandler;

import static com.stol.model.property.UserRole.*;
import static org.springframework.http.HttpMethod.*;

@Configuration
@EnableResourceServer
class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {
    private final OAuth2WebSecurityExpressionHandler expressionHandler;

    public OAuth2ResourceServerConfig(OAuth2WebSecurityExpressionHandler expressionHandler) {
        this.expressionHandler = expressionHandler;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        String[] forUser = {ROLE_USER.toString(), ROLE_RESTAURANT_ADMIN.toString(), ROLE_SYSTEM_ADMIN.toString()};
        String[] forRestaurantAdmin = {ROLE_RESTAURANT_ADMIN.toString(), ROLE_SYSTEM_ADMIN.toString()};

        http
                .authorizeRequests()
                .antMatchers("/actuator/health").permitAll()
                .antMatchers("/actuator/**").access("#oauth2.hasScope('actuator')")

                .antMatchers(GET, "/api/languages").permitAll()
                .antMatchers("/api/languages/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(GET, "/api/articles/**").hasAnyRole(forUser)
                .antMatchers(POST, "/api/articles").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(PATCH, "/api/articles/**").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(DELETE, "/api/articles/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(POST, "/api/cuisines").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(PATCH, "/api/cuisines/**").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(DELETE, "/api/cuisines/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(POST, "/api/tags").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(PATCH, "/api/tags/**").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(DELETE, "/api/tags/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(GET, "/api/restaurant_selections").hasAnyRole(forUser)
                .antMatchers(POST, "/api/restaurant_selections").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(PATCH, "/api/restaurant_selections/**").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(DELETE, "/api/restaurant_selections/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(GET, "/api/restaurants/search/my").hasAnyRole(forRestaurantAdmin)
                .antMatchers(GET, "/api/restaurants/*/menu").permitAll()
                .antMatchers(GET, "/api/restaurants/**").permitAll()
                .antMatchers(GET, "/api/restaurants").permitAll()
                .antMatchers(POST, "/api/restaurants").hasAnyRole(forRestaurantAdmin)
                .antMatchers(PATCH, "/api/restaurants/{restaurantId}").access("hasRole('" + ROLE_SYSTEM_ADMIN.toString() + "') or @restaurantAccessValidator.isRestaurantOwner(authentication, #restaurantId)")
                .antMatchers(DELETE, "/api/restaurants/**").hasAnyRole(forRestaurantAdmin)

                .antMatchers(GET, "/api/reservations").access("hasRole('" + ROLE_SYSTEM_ADMIN.toString() + "') or @resourceAccessValidator.isCorrectRestaurantAdmin(authentication, request)")
                .antMatchers(GET, "/api/reservations/search/my").hasAnyRole(forUser)
                .antMatchers(GET, "/api/reservations/search/my_restaurants").hasAnyRole(forRestaurantAdmin)
                .antMatchers(GET, "/api/reservations/search/my_restaurants_new").hasAnyRole(forRestaurantAdmin)
                .antMatchers(GET, "/api/reservations/search/count_new").hasAnyRole(forRestaurantAdmin)
                .antMatchers(GET, "/api/reservations/search/by_user").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(POST, "/api/reservations").hasAnyRole(forUser)
                .antMatchers(PATCH, "/api/reservations/**").hasAnyRole(forUser)
                .antMatchers(DELETE, "/api/reservations/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(GET, "/api/promotions").hasAnyRole(forUser)
                .antMatchers(GET, "/api/promotions/search/my_restaurants").hasAnyRole(forRestaurantAdmin)
                .antMatchers(POST, "/api/promotions").hasAnyRole(forRestaurantAdmin)
                .antMatchers(PATCH, "/api/promotions/**").hasAnyRole(forRestaurantAdmin)
                .antMatchers(DELETE, "/api/promotions/**").hasAnyRole(forRestaurantAdmin)

                .antMatchers(GET, "/api/users").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(GET, "/api/users/{userId}").access("hasRole('" + ROLE_SYSTEM_ADMIN.toString() + "') or @resourceAccessValidator.isSameLoggedInUser(authentication, #userId)")
                .antMatchers(PATCH, "/api/users/{userId}").access("hasRole('" + ROLE_SYSTEM_ADMIN.toString() + "') or @resourceAccessValidator.isSameLoggedInUser(authentication, #userId)")
                .antMatchers(DELETE, "/api/users/*/saved_restaurants/*").hasAnyRole(forUser)
                .antMatchers(DELETE, "/api/users/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(GET, "/api/roles/**").hasAnyRole(forUser)

                .antMatchers(GET, "/api/reviews/**").hasAnyRole(forUser)
                .antMatchers(POST, "/api/reviews").hasAnyRole(forUser)
                .antMatchers(PATCH, "/api/reviews/**").denyAll()
                .antMatchers(DELETE, "/api/reviews/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(POST, "/api/times").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(PATCH, "/api/times/**").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(DELETE, "/api/times/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(POST, "/api/review_notifications/**").denyAll()
                .antMatchers(PATCH, "/api/review_notifications/**").hasAnyRole(forUser)
                .antMatchers(GET, "/api/review_notifications/search/my").hasAnyRole(forUser)
                .antMatchers(DELETE, "/api/review_notifications/**").hasAnyRole(forUser)

                .antMatchers(GET, "/api/happy_hours/**").hasAnyRole(forRestaurantAdmin)
                .antMatchers(POST, "/api/happy_hours").hasAnyRole(forRestaurantAdmin)
                .antMatchers(PATCH, "/api/happy_hours/**").hasAnyRole(forRestaurantAdmin)
                .antMatchers(DELETE, "/api/happy_hours/**").denyAll()

                .antMatchers(GET, "/api/statistic/**").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(POST, "/api/statistic/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(GET, "/api/marker_types/**").permitAll()
                .antMatchers(POST, "/api/marker_types").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(PATCH, "/api/marker_types/**").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers(DELETE, "/api/marker_types/**").hasRole(ROLE_SYSTEM_ADMIN.toString())

                .antMatchers(GET, "/api/markers/**").permitAll()
                .antMatchers(POST, "/api/markers/**").hasAnyRole(forRestaurantAdmin)
                .antMatchers(PATCH, "/api/markers/**").hasAnyRole(forRestaurantAdmin)
                .antMatchers(DELETE, "/api/markers/**").hasAnyRole(forRestaurantAdmin)

                .antMatchers(DELETE, "/api/**").hasRole(ROLE_SYSTEM_ADMIN.toString())
                .antMatchers("/api/**").hasAnyRole(forUser)
                .antMatchers("/**").authenticated();
    }

    //Needed to fix support of custom expressions like .access("@resourceAccessValidator.isSameLoggedInUser(authentication,#userId)") for OAuth2
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.expressionHandler(expressionHandler);
    }
}
