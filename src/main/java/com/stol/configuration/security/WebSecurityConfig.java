package com.stol.configuration.security;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) {
        RegexRequestMatcher angularFiles = new RegexRequestMatcher(".*\\.(js|css)$", HttpMethod.GET.toString());
        web.
                ignoring()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations(), angularFiles)
                .antMatchers(HttpMethod.POST, "/api/user/registration", "/api/user/activation", "/api/user/activation/resend", "/api/user/password/reset")
                .antMatchers(HttpMethod.GET,"/api/times/**", "/api/cuisines/**", "/api/tags/**", "/api/happy_hours/search/active/**")
                .antMatchers("/api/profile/**", "/api")
                .antMatchers("/files/content/**")
                .antMatchers("/assets/icons/**")
                .antMatchers("/v2/api-docs/**", "/swagger-ui.html/**", "/swagger-resources/**")
                .antMatchers("/docs/api-guide.html");
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
