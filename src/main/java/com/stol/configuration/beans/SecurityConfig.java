package com.stol.configuration.beans;

import com.stol.service.PasswordEncoderService;
import com.stol.service.localization.LocalizationService;
import com.stol.service.security.OAuth2WebResponseExceptionTranslator;
import com.stol.service.security.UserDetailsServiceImpl;
import com.stol.service.security.CustomDaoAuthenticationProvider;
import com.stol.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.oauth2.provider.expression.OAuth2WebSecurityExpressionHandler;

@Configuration
public class SecurityConfig {
    private final ApplicationContext applicationContext;

    @Autowired
    public SecurityConfig(final ApplicationContext applicationContext1) {
        this.applicationContext = applicationContext1;
    }

    @Bean
    public UserDetailsService userDetailsService(UserService userService) {
        return new UserDetailsServiceImpl(userService);
    }

    @Bean
    public AuthenticationProvider daoAuthenticationProvider(UserDetailsService userDetailsService, PasswordEncoderService passwordEncoder, UserService userService) {
        DaoAuthenticationProvider provider = new CustomDaoAuthenticationProvider(userService);
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(passwordEncoder);
        return provider;
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }

    @Bean
    public OAuth2WebSecurityExpressionHandler oAuth2WebSecurityExpressionHandler() {
        OAuth2WebSecurityExpressionHandler expressionHandler = new OAuth2WebSecurityExpressionHandler();
        expressionHandler.setApplicationContext(applicationContext);
        return expressionHandler;
    }

    @Bean
    public OAuth2WebResponseExceptionTranslator oAuth2WebResponseExceptionTranslator(LocalizationService localizationService) {
        return new OAuth2WebResponseExceptionTranslator(localizationService);
    }

}
