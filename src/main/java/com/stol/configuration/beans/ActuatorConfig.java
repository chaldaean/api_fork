package com.stol.configuration.beans;

import com.stol.listener.UserAuthenticationAuditListener;
import org.springframework.boot.actuate.security.AuthenticationAuditListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ActuatorConfig {

    @Bean
    public AuthenticationAuditListener authenticationAuditListener() {
        return new UserAuthenticationAuditListener();
    }

}
