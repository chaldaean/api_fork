package com.stol.configuration.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.stol.repository.*;
import com.stol.repository.event.PromotionEventHandler;
import com.stol.repository.event.ReviewEventHandler;
import com.stol.repository.event.ReviewNotificationEventHandler;
import com.stol.repository.statistic.ScreenRepository;
import com.stol.resource.UserResourceProcessor;
import com.stol.resource.UserRolesResourceProcessor;
import com.stol.schedule.ScheduledTasks;
import com.stol.service.*;
import com.stol.service.localization.LocalizationService;
import com.stol.service.notification.NotificationService;
import com.stol.service.notification.push.FirebaseProperties;
import com.stol.service.notification.push.FirebaseService;
import com.stol.service.notification.push.PushNotifier;
import com.stol.service.notification.sms.SmsNotifier;
import com.stol.service.sms.SmsSenderService;
import com.stol.service.statistic.StatisticService;
import com.stol.service.telegram.TelegramNotifier;
import com.stol.service.user.UserService;
import com.stol.service.utils.ResourceAccessValidator;
import com.stol.service.utils.RestaurantAccessValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class ServicesConfig {

    @Bean
    public UserService userService(UserRepository userRepository, UserSystemInformationRepository userSystemInformationRepository,
                                   LanguageRepository languageRepository) {
        return new UserService(userRepository, userSystemInformationRepository, languageRepository);
    }

    @Bean
    public AuthService authService(DefaultTokenServices tokenServices, TokenStore tokenStore) {
        return new AuthService(tokenServices, tokenStore);
    }

    @Bean
    public CredentialService credentialService(CredentialRepository credentialRepository, RoleService roleService, PasswordEncoder passwordEncoder) {
        return new CredentialService(credentialRepository, roleService, passwordEncoder);
    }

    @Bean
    public SpelAwareProxyProjectionFactory projectionFactory() {
        return new SpelAwareProxyProjectionFactory();
    }

    @Bean
    public UserResourceProcessor userResourceProcessor(UserService userService) {
        return new UserResourceProcessor(userService);
    }

    @Bean
    public UserRolesResourceProcessor userRolesResourceProcessor() {
        return new UserRolesResourceProcessor();
    }

    @Bean
    public FileService fileService(MediaFileRepository fileRepository) {
        return new FileService(fileRepository);
    }

    @Bean
    public RoleService roleService(RoleRepository roleRepository) {
        return new RoleService(roleRepository);
    }

    @Bean
    public PromotionService promotionService(PromotionRepository promotionRepository) {
        return new PromotionService(promotionRepository);
    }

    @Bean
    public UserActivationService userActivationService(CredentialService credentialService, UserService userService, SmsSenderService smsSenderService,
                                                       RoleService roleService, LocalizationService localizationService) {
        return new UserActivationService(credentialService, userService, smsSenderService, roleService, localizationService);
    }

    @Bean
    public PasswordEncoderService passwordEncoder() {
        return new PasswordEncoderService();
    }

    @Bean
    public StatisticService statisticService(UserService userService, ReservationRepository reservationRepository,
                                             ReviewRepository reviewRepository, UserSystemInformationRepository userSystemInformationRepository, ScreenRepository screenRepository) {
        return new StatisticService(userService, reservationRepository, reviewRepository, userSystemInformationRepository, screenRepository);
    }

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();
        b.featuresToDisable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS);
        b.featuresToDisable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        b.mixIn(Object.class, IgnoreHibernatePropertiesInJackson.class);
        return b;
    }

    @Bean
    public PromotionEventHandler promotionEventHandler(UserService userService) {
        return new PromotionEventHandler(userService);
    }

    @Bean
    public ScheduledTasks scheduledTasks(FileService fileService, MediaFileRepository mediaFileRepository, ReservationService reservationService,
                                         ReviewNotificationRepository reviewNotificationRepository, NotificationService notificationService) {
        return new ScheduledTasks(fileService, mediaFileRepository, reservationService, reviewNotificationRepository, notificationService);
    }

    @Bean
    public ReviewEventHandler reviewEventHandler(ReviewRepository reviewRepository, RestaurantRepository restaurantRepository,
                                                 ReviewNotificationRepository reviewNotificationRepository, UserService userService) {
        return new ReviewEventHandler(reviewRepository, restaurantRepository, reviewNotificationRepository, userService);
    }

    @Bean
    public ReviewNotificationEventHandler reviewNotificationEventHandler(UserService userService) {
        return new ReviewNotificationEventHandler(userService);
    }

    @Bean
    public ResourceAccessValidator resourceAccessValidator() {
        return new ResourceAccessValidator();
    }

    @Bean
    public RestaurantAccessValidator restaurantAccessValidator(UserService userService, RestaurantService restaurantService) {
        return new RestaurantAccessValidator(userService, restaurantService);
    }

    @Bean
    public RestaurantService restaurantService(RestaurantRepository restaurantRepository, ReviewRepository reviewRepository,
                                               HappyHourRepository happyHourRepository, LocalizationService localizationService) {
        return new RestaurantService(restaurantRepository, reviewRepository, happyHourRepository, localizationService);
    }

    @Bean
    public FirebaseProperties firebaseProperties() {
        return new FirebaseProperties();
    }

    @Bean
    public FirebaseService firebaseService(FirebaseProperties firebaseProperties) {
        return new FirebaseService(firebaseProperties);
    }

    @Bean
    public PushNotifier pushNotifier(MobileDeviceService mobileDeviceService,  FirebaseService firebaseService) {
        return new PushNotifier(mobileDeviceService, firebaseService);
    }

    @Bean
    public SmsNotifier smsNotifier(SmsSenderService smsSenderService) {
        return new SmsNotifier(smsSenderService);
    }

    @Bean
    public NotificationService notificationService(PushNotifier pushNotifier, SmsNotifier smsNotifier, LocalizationService localizationService) {
        return new NotificationService(Collections.unmodifiableList(Arrays.asList(pushNotifier, smsNotifier)), localizationService);
    }

    @Bean
    public MobileDeviceService mobileDeviceService(MobileDeviceRepository mobileDeviceRepository) {
        return new MobileDeviceService(mobileDeviceRepository);
    }

    @Bean
    public ReviewService reviewService(ReviewRepository reviewRepository) {
        return new ReviewService(reviewRepository);
    }

    @Bean
    public ReservationService reservationService(ReservationRepository reservationRepository) {
        return new ReservationService(reservationRepository);
    }

    @Bean
    public TelegramNotifier telegramNotifier() {
        return new TelegramNotifier();
    }

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private abstract class IgnoreHibernatePropertiesInJackson {
    }
}
