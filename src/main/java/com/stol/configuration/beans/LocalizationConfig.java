package com.stol.configuration.beans;

import com.stol.resolver.UserAwareLocaleResolver;
import com.stol.service.localization.LocalizationService;
import com.stol.service.user.UserService;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;

import static com.stol.configuration.constant.Localization.DEFAULT_LOCALE;
import static com.stol.configuration.constant.Localization.SUPPORTED_LOCALES;

@Configuration
public class LocalizationConfig {

    @Bean
    LocaleResolver localeResolver(UserService userService) {
        UserAwareLocaleResolver resolver = new UserAwareLocaleResolver(userService);
        resolver.setDefaultLocale(DEFAULT_LOCALE);
        resolver.setSupportedLocales(SUPPORTED_LOCALES);

        return resolver;
    }
    
    @Bean
    public LocalizationService localizationService() {
        return new LocalizationService(messageSource());
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setBasename("messages");
        messageSource.setUseCodeAsDefaultMessage(true);

        return messageSource;
    }


}
