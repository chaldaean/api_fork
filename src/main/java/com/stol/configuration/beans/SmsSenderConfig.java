package com.stol.configuration.beans;

import com.stol.service.sms.SmsSenderInterceptor;
import com.stol.service.sms.SmsSenderService;
import com.stol.service.sms.wsdl.Auth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

@Configuration
public class SmsSenderConfig {
    private static final String SMS_WSDL_PATH = "com.stol.service.sms.wsdl";
    @Value("${stol.turbosms.login}")
    public String SMS_LOGIN;
    @Value("${stol.turbosms.password}")
    public String SMS_PASSWORD;
    @Value("${stol.turbosms.soap-url}")
    public String SMS_URL;

    @Bean
    public SmsSenderService smsSenderService(WebServiceTemplate webServiceTemplate, Auth auth) {
        return new SmsSenderService(webServiceTemplate, auth);
    }

    @Bean
    Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setContextPath(SMS_WSDL_PATH);
        return jaxb2Marshaller;
    }

    @Bean
    public Auth smsAuth() {
        Auth auth = new Auth();
        auth.setLogin(SMS_LOGIN);
        auth.setPassword(SMS_PASSWORD);
        return auth;
    }

    @Bean
    public WebServiceTemplate webServiceTemplate() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setMarshaller(jaxb2Marshaller());
        webServiceTemplate.setUnmarshaller(jaxb2Marshaller());
        webServiceTemplate.setDefaultUri(SMS_URL);
        webServiceTemplate.setInterceptors(new SmsSenderInterceptor[]{new SmsSenderInterceptor()});
        return webServiceTemplate;
    }
}
