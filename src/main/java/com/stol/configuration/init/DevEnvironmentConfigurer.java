package com.stol.configuration.init;

import org.apache.commons.lang3.Validate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Profiles;

@Order(Integer.MIN_VALUE)
public class DevEnvironmentConfigurer implements EnvironmentPostProcessor {
    private static final String USE_PRODUCTION_MODE_BY_DEFAULT = "No active profile specified. Use PRODUCTION mode by default";
    private static final String DEV_ENVIRONMENT = "dev";
    private static final String MIXING_PROFILES = "Development profile could not be mixed with other profiles";
    private static final String DEVELOPMENT_MODE = "Application is started in DEVELOPMENT mode";

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        String[] activeProfiles = environment.getActiveProfiles();
        if (activeProfiles.length == 0) {
            System.out.println(USE_PRODUCTION_MODE_BY_DEFAULT);
        }
        if (environment.acceptsProfiles(Profiles.of(DEV_ENVIRONMENT))) {
            Validate.validState(activeProfiles.length == 1, MIXING_PROFILES);
            System.out.println(DEVELOPMENT_MODE);
        }
    }
}
