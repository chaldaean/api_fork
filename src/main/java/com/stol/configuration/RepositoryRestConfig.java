package com.stol.configuration;

import com.stol.converter.UrlParameterStringToDouble;
import com.stol.converter.UrlParameterStringToLocalDate;
import com.stol.converter.UrlParameterStringToLocalDateTime;
import com.stol.converter.UrlParameterStringToLocalTime;
import com.stol.model.user.User;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.ExposureConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

@Configuration
public class RepositoryRestConfig implements RepositoryRestConfigurer {
    @Override
    public void configureConversionService(ConfigurableConversionService conversionService) {
        conversionService.addConverter(String.class, LocalDateTime.class, new UrlParameterStringToLocalDateTime());
        conversionService.addConverter(String.class, LocalDate.class, new UrlParameterStringToLocalDate());
        conversionService.addConverter(String.class, LocalTime.class, new UrlParameterStringToLocalTime());
        conversionService.addConverter(String.class, Double.class, new UrlParameterStringToDouble());
    }

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        ExposureConfiguration configuration = config.getExposureConfiguration();
        configuration.disablePutForCreation();
        configuration.forDomainType(User.class).withAssociationExposure((metadata, httpMethods) -> httpMethods.disable(POST, PUT));
        configuration.forDomainType(User.class).withItemExposure((metadata, httpMethods) -> httpMethods.disable(POST, PUT));
        configuration.forDomainType(User.class).withCollectionExposure((metadata, httpMethods) -> httpMethods.disable(POST, PUT));
    }
}
