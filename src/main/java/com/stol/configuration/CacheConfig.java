package com.stol.configuration;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfig {
    public final static String ACCESS_TOKEN_STORE = "accessTokenStore";
    public final static String AUTHENTICATION_STORE = "authenticationStore";
}
