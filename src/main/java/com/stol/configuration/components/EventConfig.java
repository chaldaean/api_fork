package com.stol.configuration.components;

import com.stol.controller.UserActivationController;
import com.stol.listener.AuthenticationEventListener;
import com.stol.repository.CredentialRepository;
import com.stol.repository.HappyHourRepository;
import com.stol.repository.ReviewNotificationRepository;
import com.stol.repository.event.*;
import com.stol.repository.listener.MediaFileEventListener;
import com.stol.service.*;
import com.stol.service.notification.NotificationService;
import com.stol.service.sms.SmsSenderService;
import com.stol.service.telegram.TelegramNotifier;
import com.stol.service.user.UserService;
import com.stol.service.user.UserSmsEventService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.EntityManagerFactory;

@Configuration
public class EventConfig {

    @Bean
    public UserSmsEventService userSmsEventHandler(UserService userService, SmsSenderService smsSenderService, UserActivationController userActivationController) {
        return new UserSmsEventService(userService, smsSenderService, userActivationController);
    }

    @Bean
    public CredentialService userFieldsUpdateEventHandler(CredentialRepository credentialRepository, RoleService roleService, PasswordEncoder passwordEncoder) {
        return new CredentialService(credentialRepository, roleService, passwordEncoder);
    }

    @Bean
    public RestaurantSelectionEventHandler restaurantSelectionEventHandler() {
        return new RestaurantSelectionEventHandler();
    }

    @Bean
    public ReservationEventHandler reservationEventHandler(PromotionService promotionService, ReviewNotificationRepository reviewNotificationRepository,
                                                           HappyHourRepository happyHourRepository, UserService userService,
                                                           TelegramNotifier telegramNotifier) {
        return new ReservationEventHandler(promotionService, reviewNotificationRepository, happyHourRepository, userService, telegramNotifier);
    }

    @Bean
    public HappyHourEventHandler happyHoursEventHandler() {
        return new HappyHourEventHandler();
    }

    @Bean
    public LoggingEventHandler loggingEventHandler() {
        return new LoggingEventHandler();
    }

    @Bean
    public AuthenticationEventListener authenticationEventListener(UserService userService) {
        return new AuthenticationEventListener(userService);
    }

    @Bean
    public MediaFileEventHandler mediaFileEventHandler(FileService fileService) {
        return new MediaFileEventHandler(fileService);
    }

    @Bean
    public RestaurantEventHandler restaurantEventHandler(UserService userService) {
        return new RestaurantEventHandler(userService);
    }

    @Bean
    public ReservationNotificationEventHandler reservationNotificationEventHandler(NotificationService notificationService) {
        return new ReservationNotificationEventHandler(notificationService);
    }

    @Bean
    public MobileDeviceEventHandler mobileDeviceEventHandler(MobileDeviceService mobileDeviceService, UserService userService) {
        return new MobileDeviceEventHandler(mobileDeviceService, userService);
    }

    @Bean
    public ArticleEventHandler articleEventHandler(UserService userService) {
        return new ArticleEventHandler(userService);
    }

    @Bean
    public MediaFileEventListener mediaFileEventListener(EntityManagerFactory entityManagerFactory, FileService fileService) {
        return new MediaFileEventListener(entityManagerFactory, fileService);
    }
}
