package com.stol.configuration.constant;

public class Messages {
    public static final String RESET_PASSWORD_TEXT = "A new password has been generated for you";
    public static final String OLD_PASSWORD_IS_INCORRECT = "Your old password is incorrect";
    public static final String NEW_PASSWORD_IS_EMPTY = "Your new password is empty";
    public static final String ACTIVATION_CODE_IS_INVALID = "Activation code is invalid";
    public static final String MUST_BE_LOGGED_IN = "You must be logged in";
    public static final String MUST_BE_ACTIVATED_FIRST = "User must be activated first";
    public static final String USER_ALREADY_ACTIVATED = "User already activated";
    public static final String NUMBER_OF_TRIES_REACHED = "You have reached maximum number of tries. Try after 24 hours";
    public static final String NO_FILES_TO_STORE = "There is no files to store";
    public static final String EMPTY_REVIEW = "Rating or comment must be filled in";
    public static final String MAKE_REVIEW_TIME_IS_EXPIRED = "You can leave a review within 14 days after the visit";
    public static final String NO_RESERVATION_FOR_REVIEW = "You need to make a reservation in this restaurant to create a review";
    public static final String REVIEW_ONLY_FOR_CONFIRMED = "You need to visit this restaurant before creating a review";
    public static final String ONLY_ONE_REVIEW_ALLOWED = "You can create only one review for a reservation";
    public static final String HAPPY_HOURS_NO_OVERLAPPING_TIME_ALLOWED = "Happy hours cannot have overlapping times withing the same restaurant";
    public static final String USER_ALREADY_EXISTS ="User already exist";

    public static final String FAILED_TO_CREATE_A_NEW_DIRECTORY = "Failed to create a new directory";
    public static final String NO_USER_WITH_LOGIN = "There is no user with login";
    public static final String NO_USER_WITH_ID = "There is no user with ID";
    public static final String NO_ROLE_WITH_NAME = "There is no role with name";
    public static final String NO_USER_WITH_PHONE_NUMBER = "There is no user with phone number";
    public static final String DO_NOT_HAVE_ACCESS_TO_THIS_RESOURCE = "You are not the owner of this resource";
    public static final String DO_NOT_ALLOW_REMOVE_YOURSELF = "You can't remove yourself from restaurant's owners";
    public static final String NOT_OWNER = "You must be an owner to do this";
}
