package com.stol.configuration.constant;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Localization {

    public static final String DEFAULT_LANGUAGE = "ru";
    public static final String LANGUAGE_UK = "uk";
    public static final String LANGUAGE_EN = "en";

    public static final Locale DEFAULT_LOCALE = Locale.forLanguageTag(DEFAULT_LANGUAGE);

    public static final List<Locale> SUPPORTED_LOCALES = Arrays.asList(
            DEFAULT_LOCALE,
            Locale.forLanguageTag(LANGUAGE_EN),
            Locale.forLanguageTag(LANGUAGE_UK)
    );
}
