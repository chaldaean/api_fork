package com.stol.exception;

import org.apache.catalina.connector.ClientAbortException;
import com.stol.service.localization.LocalizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.stol.exception.UserAlreadyExist.USER_ALREADY_EXIST_LOG_REF;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class ServerExceptionHandler {
    public final static String VND_ERROR = "application/vnd.error+json;charset=UTF-8";
    public final static String DEFAULT_LOG_REF = "0";
    public final static String INVALID_FIELDS_LOG_REF = "1";
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final LocalizationService localizationService;

    public ServerExceptionHandler(LocalizationService localizationService) {
        Objects.requireNonNull(localizationService);
        this.localizationService = localizationService;
    }

    @ExceptionHandler(value = SaveFileException.class)
    public ResponseEntity<VndErrors.VndError> saveFileError(SaveFileException ex) {
        String msg = localizationService.translate(ex.getMessage(), ex.getFilename());

        logger.warn(msg);

        VndErrors.VndError error = new VndErrors.VndError(DEFAULT_LOG_REF, msg);
        return ResponseEntity.status(BAD_REQUEST)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<VndErrors.VndError> noSuchUser(UserNotFoundException ex) {
        String msg = localizationService.translate(ex.getMessage(), ex.getIdentity());

        logger.warn(msg);

        VndErrors.VndError error = new VndErrors.VndError(DEFAULT_LOG_REF, msg);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<VndErrors.VndError> forbidden(AccessDeniedException ex) {
        String msg = localizationService.translate(ex.getMessage());

        logger.warn(msg);

        VndErrors.VndError error = new VndErrors.VndError(DEFAULT_LOG_REF, msg);
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    /* TODO: localize*/
    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<VndErrors> error(DataIntegrityViolationException ex) {
        logger.warn(ex.getMessage());

        VndErrors error = new VndErrors(DEFAULT_LOG_REF, ex.getCause().getCause().getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    /* TODO: localize*/
    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<?> handleBadInput(ConstraintViolationException ex) {
        logger.warn(ex.getMessage());

        List<VndErrors.VndError> errors = new ArrayList<>();
        for (ConstraintViolation v : ex.getConstraintViolations()) {
            String message = v.getPropertyPath().toString() + " : " + v.getMessage();
            errors.add(new VndErrors.VndError(DEFAULT_LOG_REF, message));
        }
        return ResponseEntity.status(BAD_REQUEST)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(new VndErrors(errors));
    }

    @ExceptionHandler(value = InvalidRequestParameters.class)
    public ResponseEntity<VndErrors> invalidRequestParameters(RuntimeException ex) {
        String msg = localizationService.translate(ex.getMessage());

        logger.warn(msg);

        VndErrors error = new VndErrors(DEFAULT_LOG_REF, msg);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    @ExceptionHandler(value = UserAlreadyExist.class)
    public ResponseEntity<VndErrors> userAlreadyExist(UserAlreadyExist ex) {
        String msg = localizationService.translate(ex.getMessage(), ex.getLogin());

        logger.warn(msg);

        VndErrors error = new VndErrors(USER_ALREADY_EXIST_LOG_REF, msg);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    /* TODO: localize*/
    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<VndErrors.VndError> error(EntityNotFoundException ex) {
        logger.warn(ex.getMessage());

        VndErrors.VndError error = new VndErrors.VndError(DEFAULT_LOG_REF, ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    @ExceptionHandler(value = InvalidOperationException.class)
    public ResponseEntity<VndErrors.VndError> emptyReview(InvalidOperationException ex) {
        String msg = localizationService.translate(ex.getMessage());

        logger.warn(msg);

        VndErrors.VndError error = new VndErrors.VndError(DEFAULT_LOG_REF, msg);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    @ExceptionHandler(value = ResourceNotFoundException.class)
    public ResponseEntity<VndErrors.VndError> error(ResourceNotFoundException ex) {
        String msg = localizationService.translate(ex.getMessage());

        logger.warn(msg);

        VndErrors.VndError error = new VndErrors.VndError(DEFAULT_LOG_REF, msg);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    /* TODO: localize*/
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<VndErrors.VndError> accessDenied(Exception e) throws Exception {
        logger.warn(e.getMessage(), e);

        Throwable cause = e.getCause();
        if (cause != null) {
            Throwable nestedCause = Objects.nonNull(cause.getCause()) ? cause.getCause() : cause;
            if (AccessDeniedException.class.isAssignableFrom(nestedCause.getClass())) {
                VndErrors.VndError error = new VndErrors.VndError(DEFAULT_LOG_REF, nestedCause.getMessage());
                return ResponseEntity.status(HttpStatus.FORBIDDEN)
                        .header(CONTENT_TYPE, VND_ERROR)
                        .body(error);
            }
        }
        throw e;
    }

    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<VndErrors.VndError> runtimeError(RuntimeException ex) {
        String msg = localizationService.translate(ex.getMessage());

        logger.warn(msg);

        VndErrors.VndError error = new VndErrors.VndError(DEFAULT_LOG_REF, msg);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }

    /* TODO: localize*/
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<VndErrors> error(MethodArgumentNotValidException ex) {
        logger.warn(ex.getMessage());

        List<VndErrors.VndError> errors = new ArrayList<>();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            String message = "Field \"" + fieldError.getField() + "\": " + fieldError.getDefaultMessage();
            errors.add(new VndErrors.VndError(INVALID_FIELDS_LOG_REF, message));
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(new VndErrors(errors));
    }

    @ExceptionHandler(value = ClientAbortException.class)
    public void clientAbort(ClientAbortException ex) {
        logger.debug(ex.getMessage());
    }


    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<VndErrors.VndError> error(Exception ex) {
        String msg = localizationService.translate(ex.getMessage());

        logger.warn(msg);

        VndErrors.VndError error = new VndErrors.VndError(DEFAULT_LOG_REF, msg);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .header(CONTENT_TYPE, VND_ERROR)
                .body(error);
    }
}
