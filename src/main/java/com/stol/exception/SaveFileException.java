package com.stol.exception;

import java.io.IOException;

import static com.stol.configuration.constant.Messages.FAILED_TO_CREATE_A_NEW_DIRECTORY;

public class SaveFileException extends RuntimeException {
    private String filename;

    public SaveFileException(String filename, IOException e) {
        super(FAILED_TO_CREATE_A_NEW_DIRECTORY, e);
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }
}
