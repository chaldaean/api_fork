package com.stol.exception;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserNotFoundException extends UsernameNotFoundException {

    private final String identity;

    public UserNotFoundException(String msg, String identity) {
        super(msg);
        this.identity = identity;
    }

    public String getIdentity() {
        return identity;
    }
}
