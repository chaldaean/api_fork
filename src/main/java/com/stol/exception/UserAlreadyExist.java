package com.stol.exception;

import static com.stol.configuration.constant.Messages.USER_ALREADY_EXISTS;

public class UserAlreadyExist extends RuntimeException {
    public final static String USER_ALREADY_EXIST_LOG_REF = "2";
    private final String login;

    public UserAlreadyExist(String login) {
        super(USER_ALREADY_EXISTS);
        this.login = login;
    }

    public String getLogin() {
        return login;
    }
}
