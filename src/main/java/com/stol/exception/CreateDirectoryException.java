package com.stol.exception;

import java.io.IOException;
import java.nio.file.Path;

public class CreateDirectoryException extends RuntimeException {
    public CreateDirectoryException(Path path, IOException e) {
        super("Failed to save a new file: " + path, e);
    }
}
