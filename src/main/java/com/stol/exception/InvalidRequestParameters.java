package com.stol.exception;

public class InvalidRequestParameters extends RuntimeException {
    public InvalidRequestParameters(String message) {
        super(message);
    }
}
