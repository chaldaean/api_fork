package com.stol.filter;

import com.stol.service.user.CurrentUser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@Order
public class LoggingFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        String correlationId = StringUtils.defaultString(req.getHeader("X-Correlation-ID"));
        String userId = "";

        if (isCurrentUser()) {
            CurrentUser user = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            userId = user.getId().toString();
        }

        MDC.put("correlationId", correlationId);
        MDC.put("userId", userId);

        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            MDC.remove("correlationId");
            MDC.remove("userId");
        }
    }

    private boolean isCurrentUser() {
        return SecurityContextHolder.getContext() != null
                && SecurityContextHolder.getContext().getAuthentication() != null
                && SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null
                && SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof CurrentUser;
    }
}
