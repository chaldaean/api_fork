package com.stol.model;

import com.stol.model.user.User;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "review", uniqueConstraints = @UniqueConstraint(name = "UK_REVIEW_RESERVATION", columnNames = "reservation_id"))
public class Review implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, foreignKey = @ForeignKey(name = "FK_REVIEW_USER_ID"))
    private User user;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id", nullable = false, foreignKey = @ForeignKey(name = "FK_REVIEW_RESTAURANT_ID"))
    private Restaurant restaurant;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reservation_id", nullable = false, unique = true, foreignKey = @ForeignKey(name = "FK_REVIEW_RESERVATION_ID"))
    private Reservation reservation;

    @Min(0)
    @Max(5)
    @Column(name = "rating")
    private Integer rating;

    @Column(name = "comment")
    private String comment;

    @Column(name = "creation_date", nullable = false)
    private final LocalDateTime creationDate = LocalDateTime.now();

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return rating.equals(review.rating) &&
                Objects.equals(user, review.user) &&
                Objects.equals(restaurant, review.restaurant) &&
                Objects.equals(reservation, review.reservation) &&
                Objects.equals(comment, review.comment) &&
                Objects.equals(creationDate, review.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, restaurant, reservation, rating, comment, creationDate);
    }
}
