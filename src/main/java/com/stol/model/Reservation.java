package com.stol.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stol.model.property.ReservationStatus;
import com.stol.model.user.User;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "reservation")
public class Reservation implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "guest_number", nullable = false)
    private int guestNumber;

    @Size(max = 2000)
    @Column(name = "comment")
    private String comment;

    @Size(max = 2000)
    @Column(name = "notes")
    private String notes;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private ReservationStatus status = ReservationStatus.NEW;

    @JsonIgnore
    @Transient
    private ReservationStatus previousStatus;

    @JsonIgnore
    @Transient
    private String previousNotes;

    @NotNull
    @JsonIgnore
    @Column(name = "status_changed", nullable = false)
    private LocalDateTime statusChanged = LocalDateTime.now();

    @NotNull
    @Column(name = "date_time", nullable = false)
    private LocalDateTime dateTime;

    @Column(name = "created", nullable = false)
    private LocalDateTime created = LocalDateTime.now();

    @NotNull
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, foreignKey = @ForeignKey(name = "FK_RESERVATION_USER_ID"))
    private User client;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id", nullable = false, foreignKey = @ForeignKey(name = "FK_RESERVATION_RESTAURANT_ID"))
    private Restaurant restaurant;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
    @JoinColumn(name = "discount_id", foreignKey = @ForeignKey(name = "FK_RESERVATION_DISCOUNT_ID"))
    private Discount discount;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getGuestNumber() {
        return guestNumber;
    }

    public void setGuestNumber(int guestNumber) {
        this.guestNumber = guestNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ReservationStatus getStatus() {
        return status;
    }

    public void setStatus(ReservationStatus status) {
        this.status = status;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public User getClient() {
        return client;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Discount getDiscount() {
        return discount;
    }

    public Reservation setDiscount(Discount discount) {
        this.discount = discount;
        return this;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public LocalDateTime getStatusChanged() {
        return statusChanged;
    }

    public void setStatusChanged(LocalDateTime statusChanged) {
        this.statusChanged = statusChanged;
    }

    public ReservationStatus getPreviousStatus() {
        return previousStatus;
    }

    public String getPreviousNotes() {
        return previousNotes;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return guestNumber == that.guestNumber &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(notes, that.notes) &&
                status == that.status &&
                Objects.equals(created, that.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, guestNumber, comment, notes, status, created);
    }

    @PostLoad
    private void onPostLoad() {
        previousStatus = status;
        previousNotes = notes;
    }
}
