package com.stol.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stol.model.property.UserRole;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_role", uniqueConstraints = @UniqueConstraint(name = "UK_USER_ROLE_ROLE", columnNames = "role"))
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role r = (Role) o;
        return role == r.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(role);
    }

    @Override
    @JsonIgnore
    public String getAuthority() {
        return role.name();
    }
}