package com.stol.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "user_system_information")
public class UserSystemInformation {
    @Id
    @NotEmpty
    @Pattern(regexp = "^[\\d]{12}")
    @Column(name = "phone", unique = true, nullable = false)
    private String phoneNumber;

    @NotEmpty
    @Size(max = 20)
    @Column(name = "activation_code", nullable = false, length = 20)
    private String activationCode;

    @Column(name = "activation_attempts", nullable = false)
    private int activationAttempts;

    @Column(name = "activation_code_creation_time", nullable = false)
    private LocalDateTime activationCodeCreation;

    @Column(name = "password_reset_attempts", nullable = false)
    private int passwordResetAttempts;

    @Column(name = "password_reset_creation_time")
    private LocalDateTime passwordResetCreation;

    @Column(name = "login_attempts", nullable = false)
    private int loginAttempts;

    @Column(name = "last_login_time")
    private LocalDateTime lastLoginTime;

    @Column(name = "activation_time")
    private LocalDateTime activationTime;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public int getActivationAttempts() {
        return activationAttempts;
    }

    public void setActivationAttempts(int activationAttempts) {
        this.activationAttempts = activationAttempts;
    }

    public LocalDateTime getActivationCodeCreation() {
        return activationCodeCreation;
    }

    public void setActivationCodeCreation(LocalDateTime activationCodeCreation) {
        this.activationCodeCreation = activationCodeCreation;
    }

    public int getPasswordResetAttempts() {
        return passwordResetAttempts;
    }

    public void setPasswordResetAttempts(int passwordResetAttempts) {
        this.passwordResetAttempts = passwordResetAttempts;
    }

    public LocalDateTime getPasswordResetCreation() {
        return passwordResetCreation;
    }

    public void setPasswordResetCreation(LocalDateTime passwordResetCreation) {
        this.passwordResetCreation = passwordResetCreation;
    }

    public int getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(int loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    public LocalDateTime getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(LocalDateTime lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public LocalDateTime getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(LocalDateTime activationTime) {
        this.activationTime = activationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSystemInformation that = (UserSystemInformation) o;
        return activationAttempts == that.activationAttempts &&
                passwordResetAttempts == that.passwordResetAttempts &&
                loginAttempts == that.loginAttempts &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(activationCode, that.activationCode) &&
                Objects.equals(activationCodeCreation, that.activationCodeCreation) &&
                Objects.equals(passwordResetCreation, that.passwordResetCreation) &&
                Objects.equals(lastLoginTime, that.lastLoginTime) &&
                Objects.equals(activationTime, that.activationTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneNumber, activationCode, activationAttempts, activationCodeCreation,
                passwordResetAttempts, passwordResetCreation, loginAttempts, lastLoginTime, activationTime);
    }
}
