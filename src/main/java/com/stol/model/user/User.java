package com.stol.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.stol.model.*;
import com.stol.model.property.UserStatus;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "user", uniqueConstraints = {
        @UniqueConstraint(name = "UK_USER_EMAIL", columnNames = "email"),
        @UniqueConstraint(name = "UK_USER_PHONE", columnNames = "phone"),
        @UniqueConstraint(name = "UK_USER_PHOTO", columnNames = "photo_id")})
public class User implements Identifiable<Long>, Serializable {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotEmpty
    @Size(min = 2, max = 30)
    @Pattern(regexp = "^[\\p{L} ]+$")
    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @NotEmpty
    @Size(min = 2, max = 30)
    @Pattern(regexp = "^[\\p{L} ]+$")
    @Column(name = "surname", nullable = false, length = 30)
    private String surname;

    @Email
    @Size(max = 40)
    @Column(name = "email", unique = true, length = 40)
    private String email;

    @NotEmpty
    @Pattern(regexp = "^[\\d]{12}")
    @Size(max = 15)
    @Column(name = "phone", nullable = false, unique = true, length = 15)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String phoneNumber;

    @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "photo_id", unique = true, foreignKey = @ForeignKey(name = "FK_USER_MEDIA_FILE_ID"))
    private MediaFile photo;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 20)
    private UserStatus status = UserStatus.NOT_CONFIRMED;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_saved_restaurants",
            joinColumns = @JoinColumn(name = "user_id", nullable = false),
            foreignKey = @ForeignKey(name = "FK_USER_RESTAURANT_USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "restaurant_id", nullable = false),
            inverseForeignKey = @ForeignKey(name = "FK_USER_RESTAURANT_RESTAURANT_ID"))
    @RestResource(path = "saved_restaurants")
    private Set<Restaurant> savedRestaurants;

    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    private Set<Reservation> reservations;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_credential_id", nullable = false, foreignKey = @ForeignKey(name = "FK_USER_CREDENTIAL_ID"))
    private Credential credential;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id", nullable = false),
            foreignKey = @ForeignKey(name = "FK_USER_ROLE_USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "role_id", nullable = false),
            inverseForeignKey = @ForeignKey(name = "FK_USER_ROLE_ROLE_ID"))
    private Set<Role> roles;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private AddRestaurantRequest addRestaurantRequest;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "owners")
    @RestResource(path = "own_restaurants")
    private Set<Restaurant> ownRestaurants;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "language_id", foreignKey = @ForeignKey(name = "USER_LANGUAGE_ID_FK"))
    private Language language;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public MediaFile getPhoto() {
        return photo;
    }

    public void setPhoto(MediaFile photo) {
        this.photo = photo;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public Set<Restaurant> getSavedRestaurants() {
        return savedRestaurants;
    }

    public void setSavedRestaurants(Set<Restaurant> savedRestaurants) {
        this.savedRestaurants = savedRestaurants;
    }

    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public AddRestaurantRequest getAddRestaurantRequest() {
        return addRestaurantRequest;
    }

    public void setAddRestaurantRequest(AddRestaurantRequest addRestaurantRequest) {
        this.addRestaurantRequest = addRestaurantRequest;
    }

    public Set<Restaurant> getOwnRestaurants() {
        return ownRestaurants;
    }

    public void setOwnRestaurants(Set<Restaurant> ownRestaurants) {
        this.ownRestaurants = ownRestaurants;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(email, user.email) &&
                Objects.equals(phoneNumber, user.phoneNumber) &&
                Objects.equals(status, user.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, email, phoneNumber, status);
    }
}
