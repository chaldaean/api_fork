package com.stol.model.projection;

import com.stol.model.property.UserStatus;
import com.stol.model.user.Role;
import com.stol.model.user.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;
import java.util.Set;

@Projection(name = "user_roles", types = {User.class})
public interface UserRoles {
    String getEmail();

    String getName();

    String getSurname();

    String getPhoneNumber();

    UserStatus getStatus();

    @Value("#{target.photo != null ? target.photo.url : ''}")
    String getPhoto();

    Set<Role> getRoles();

    @Value("#{target.credential.registrationDate}")
    LocalDateTime getRegistrationDate();

    @Value("#{target.addRestaurantRequest != null ? target.addRestaurantRequest.restaurantName : ''}")
    String getAddRestaurantName();

}
