package com.stol.model.projection;

import com.stol.model.user.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "manager", types = {User.class})
public interface UserManager extends UserRoles {
    Long getId();
    
    @Value("#{@restaurantService.findRestaurantIdsByOwner()}")
    List<Long> getRestaurantIds();

    @Value("#{@restaurantService.findRestaurantNamesByOwner()}")
    List<String> getRestaurantNames();
}
