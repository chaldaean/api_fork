package com.stol.model.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;
import java.util.List;

@Projection(name = "restaurant", types = {com.stol.model.Reservation.class})
public interface ReservationRestaurant extends ReservationTime {
    @Value("#{T(com.stol.model.projection.restaurant.RestaurantCard).getPicturesUrls(target.restaurant)}")
    List<String> getRestaurantPictures();

    @Value("#{target.restaurant.latitude}")
    BigDecimal getRestaurantLatitude();

    @Value("#{target.restaurant.longitude}")
    BigDecimal getRestaurantLongitude();
}
