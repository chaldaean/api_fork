package com.stol.model.projection;

import com.stol.model.Article;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;

@Projection(name = "authorFullName", types = {Article.class})
public interface ArticleWithAuthorFullName {
    LocalDateTime getCreationDate();
    String getTitle();
    String getText();
    @Value("#{target.author.surname} #{target.author.name}")
    String getAuthorName();
    boolean isActive();
}
