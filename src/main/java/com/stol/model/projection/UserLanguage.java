package com.stol.model.projection;

import com.stol.model.user.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "user_language", types = {User.class})
public interface UserLanguage {
    @Value("#{target.language != null ? target.language: null}")
    LanguageSnippet getLanguage();
}
