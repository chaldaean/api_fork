package com.stol.model.projection;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalTime;

@Projection(name = "description", types = {com.stol.model.Reservation.class})
public interface ReservationDescriptionClient {
    @Value("#{target.client.name} #{target.client.surname}")
    String getFullName();

    @Value("#{target.client.photo != null ? target.client.photo.url : ''}")
    String getPhoto();

    @Value("#{target.dateTime.toLocalTime()}")
    @JsonFormat(pattern = "HH:mm")
    LocalTime getTime();

    int getGuestNumber();

    String getComment();

    String getNotes();

    @Value("#{@reservationService.numberOfUserReservationsInRestaurant(target)}")
    long getVisitNumber();

    @Value("#{target.status == T(com.stol.model.property.ReservationStatus).CONFIRMED && target.dateTime.toLocalDate().plusDays(14).isAfter(T(java.time.LocalDate).now()) ? target.client.phoneNumber : ''}")
    String getPhoneNumber();

}
