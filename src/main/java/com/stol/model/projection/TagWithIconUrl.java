package com.stol.model.projection;

import com.stol.model.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "iconUrl", types = {Tag.class})
public interface TagWithIconUrl {
    @Value("#{@localizationService.tag.getName(target)}")
    String getName();
    @Value("#{target.icon.url}")
    String getIconUrl();
    boolean isActive();
}
