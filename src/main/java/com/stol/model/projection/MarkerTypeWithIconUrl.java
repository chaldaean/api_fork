package com.stol.model.projection;

import com.stol.model.MarkerType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "iconUrl", types = {MarkerType.class})
public interface MarkerTypeWithIconUrl {
    String getName();
    @Value("#{target.icon != null ? target.icon.url: ''}")
    String getIconUrl();
    boolean isActive();
}
