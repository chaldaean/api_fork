package com.stol.model.projection;

import com.stol.model.RestaurantSelection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;

@Projection(name = "iconUrl", types = {RestaurantSelection.class})
public interface RestaurantSelectionWithIconUrl {
    @Value("#{@localizationService.restaurantSelection.getTitle(target)}")
    String getTitle();

    LocalDateTime getCreationDate();

    int getTotal();

    boolean isActive();

    @Value("#{target.picture.url}")
    String getIconUrl();
}
