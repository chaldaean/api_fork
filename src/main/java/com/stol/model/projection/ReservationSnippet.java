package com.stol.model.projection;

import com.stol.model.Discount;
import com.stol.model.Reservation;
import com.stol.model.property.ReservationStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;

@Projection(name = "snippet", types = {Reservation.class})
public interface ReservationSnippet {
    Long getId();

    LocalDateTime getCreated();

    LocalDateTime getDateTime();

    int getGuestNumber();

    @Value("#{target.client.name} #{target.client.surname}")
    String getClientName();

    @Value("#{@localizationService.restaurant.getName(target.restaurant)}")
    String getRestaurantName();

    ReservationStatus getStatus();

    Discount getDiscount();


}
