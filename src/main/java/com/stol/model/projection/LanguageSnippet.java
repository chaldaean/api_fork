package com.stol.model.projection;

import com.stol.model.*;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "snippet", types = {Language.class})
public interface LanguageSnippet {
    Long getId();

    String getCode();

    String getName();

}
