package com.stol.model.projection.restaurant;

import com.stol.model.*;
import com.stol.model.projection.restaurant.dto.RestaurantCuisine;
import com.stol.model.projection.restaurant.dto.RestaurantDiscount;
import com.stol.model.projection.restaurant.dto.RestaurantReview;
import com.stol.model.projection.restaurant.dto.RestaurantTag;
import com.stol.service.localization.LocalizationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.hateoas.Identifiable;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Projection(name = "card", types = {Restaurant.class})
public interface RestaurantCard extends Identifiable<Long> {
    @Value("#{target.topPicture != null ? target.topPicture.url: ''}")
    String getTopPictureUrl();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantCard).getPicturesUrls(target)}")
    List<String> getPictures();

    @Value("#{@restaurantService.findActiveHappyHours(target)}")
    List<RestaurantDiscount> getDiscounts();

    @Value("#{@localizationService.restaurant.getName(target)}")
    String getName();

    @Value("#{@localizationService.restaurant.getAddress(target)}")
    String getAddress();

    Double getRating();

    BigDecimal getLatitude();

    BigDecimal getLongitude();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantCard).getActiveTags(@localizationService, target)}")
    List<RestaurantTag> getTags();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantCard).getActiveCuisine(@localizationService, target)}")
    List<RestaurantCuisine> getCuisines();

    @Value("#{@localizationService.restaurant.getDescription(target)}")
    String getDescription();

    @Value("#{target.menu != null ? target.menu.url: ''}")
    String getMenu();

    String getPhoneNumber();

    @Value("#{@localizationService.restaurant.getBreakfast(target)}")
    String getBreakfast();

    @Value("#{@localizationService.restaurant.getParking(target)}")
    String getParking();

    @Value("#{@localizationService.restaurant.getPaymentMethod(target)}")
    String getPaymentMethod();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantCard).userReview(@restaurantService.latestRestaurantReview(target))}")
    RestaurantReview getReview();

    @Value("#{@restaurantService.numberOfAllRestaurantReviews(target)}")
    long getTotalReviews();

    Double getAverageTicket();

    List<OpenHour> getOpenHours();

    @Value("#{@userService.isRestaurantIsSavedForUser(target)}")
    boolean isSaved();

    static List<String> getPicturesUrls(Restaurant restaurant) {
        List<MediaFile> pictures = restaurant.getPictures();
        if (Objects.nonNull(pictures)) {
            return pictures.stream().map(MediaFile::getUrl).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    static List<RestaurantTag> getActiveTags(LocalizationService localizationService, Restaurant restaurant) {
        Set<Tag> tags = restaurant.getTags();
        if (Objects.nonNull(tags)) {
            return tags.stream()
                    .filter(Tag::isActive)
                    .map(tag -> new RestaurantTag(localizationService.getTag().getName(tag), tag.getIcon().getUrl()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static List<RestaurantCuisine> getActiveCuisine(LocalizationService localizationService,  Restaurant restaurant) {
        Set<Cuisine> cuisines = restaurant.getCuisines();
        if (Objects.nonNull(cuisines)) {
            return cuisines.stream()
                    .filter(Cuisine::isActive)
                    .map(cuisine -> new RestaurantCuisine(localizationService.getCuisine().getName(cuisine), cuisine.getIcon().getUrl()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    static RestaurantReview userReview(Review review) {
        if (Objects.nonNull(review)) {
            String userName = review.getUser().getName() + " " + review.getUser().getSurname();
            return new RestaurantReview(review.getRating(), review.getComment(), review.getCreationDate(), userName);
        } else {
            return null;
        }
    }
}
