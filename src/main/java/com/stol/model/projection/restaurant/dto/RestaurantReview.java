package com.stol.model.projection.restaurant.dto;

import java.time.LocalDateTime;

public class RestaurantReview {
    private final Integer rating;
    private final String comment;
    private final LocalDateTime creationDate;
    private final String userName;

    public RestaurantReview(Integer rating, String comment, LocalDateTime creationDate, String userName) {
        this.rating = rating;
        this.comment = comment;
        this.creationDate = creationDate;
        this.userName = userName;
    }

    public Integer getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public String getUserName() {
        return userName;
    }
}
