package com.stol.model.projection.restaurant.dto;

public interface RestaurantDiscount {
    Integer getDiscount();

    Integer getQuantity();
}
