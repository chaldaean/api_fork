package com.stol.model.projection.restaurant;

import com.stol.model.Restaurant;
import com.stol.model.user.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Projection(name = "name", types = {Restaurant.class})
public interface RestaurantNames {
    Long getId();

    @Value("#{@localizationService.restaurant.getName(target)}")
    String getName();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantNames).ownerFullNames(target)}")
    List<String> getOwnerNames();

    boolean isActive();

    @Value("#{target.topPicture != null ? target.topPicture.url: ''}")
    String getTopPictureUrl();

    static List<String> ownerFullNames(Restaurant restaurant) {
        Set<User> owners = restaurant.getOwners();
        if (Objects.nonNull(owners)) {
            return owners.stream().map(u -> u.getName() + " " + u.getSurname()).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }
}
