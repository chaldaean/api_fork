package com.stol.model.projection.restaurant.dto;

public class RestaurantTag {
    private final String name;
    private final String iconUrl;

    public RestaurantTag(String name, String iconUrl) {
        this.name = name;
        this.iconUrl = iconUrl;
    }

    public String getName() {
        return name;
    }

    public String getIconUrl() {
        return iconUrl;
    }
}
