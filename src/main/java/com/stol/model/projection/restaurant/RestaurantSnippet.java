package com.stol.model.projection.restaurant;

import com.stol.model.Cuisine;
import com.stol.model.MediaFile;
import com.stol.model.Restaurant;
import com.stol.model.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Projection(name = "snippet", types = {Restaurant.class})
public interface RestaurantSnippet {
    @Value("#{@localizationService.restaurant.getName(target)}")
    String getName();

    Set<Tag> getTags();

    Set<Cuisine> getCuisines();

    Double getAverageTicket();

    MediaFile getTopPicture();

    Double getRating();
}
