package com.stol.model.projection.restaurant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.stol.model.HappyHour;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Projection(name = "discount", types = {HappyHour.class})
public interface RestaurantHappyHours {
    Integer getQuantity();

    int getDiscount();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantHappyHours).selectTime(target)}")
    @JsonFormat(pattern = "HH:mm")
    List<LocalTime> getTimes();

    static List<LocalTime> selectTime(HappyHour happyHour) {
        List<LocalTime> times = new ArrayList<>();
        happyHour.getTimes().forEach(t -> times.add(t.getTime()));
        return times;
    }
}
