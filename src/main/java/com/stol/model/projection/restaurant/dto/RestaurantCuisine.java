package com.stol.model.projection.restaurant.dto;

public class RestaurantCuisine {
    private final String name;
    private final String iconUrl;

    public RestaurantCuisine(String name, String iconUrl) {
        this.name = name;
        this.iconUrl = iconUrl;
    }

    public String getName() {
        return name;
    }

    public String getIconUrl() {
        return iconUrl;
    }
}
