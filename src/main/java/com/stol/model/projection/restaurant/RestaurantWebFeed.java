package com.stol.model.projection.restaurant;

import com.stol.model.OpenHour;
import com.stol.model.Restaurant;
import com.stol.model.projection.restaurant.dto.RestaurantCuisine;
import com.stol.model.projection.restaurant.dto.RestaurantDiscount;
import com.stol.model.projection.restaurant.dto.RestaurantTag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;
import java.util.List;

@Projection(name = "web_feed", types = {Restaurant.class})
public interface RestaurantWebFeed {

    Long getId();

    @Value("#{target.topPicture != null ? target.topPicture.url: ''}")
    String getTopPictureUrl();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantCard).getPicturesUrls(target)}")
    List<String> getPictures();

    @Value("#{@restaurantService.findActiveHappyHours(target)}")
    List<RestaurantDiscount> getDiscounts();

    @Value("#{@localizationService.restaurant.getName(target)}")
    String getName();

    @Value("#{@localizationService.restaurant.getAddress(target)}")
    String getAddress();

    Double getRating();

    BigDecimal getLatitude();

    BigDecimal getLongitude();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantCard).getActiveTags(@localizationService, target)}")
    List<RestaurantTag> getTags();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantCard).getActiveCuisine(@localizationService, target)}")
    List<RestaurantCuisine> getCuisines();

    @Value("#{@localizationService.restaurant.getDescription(target)}")
    String getDescription();

    Double getAverageTicket();

    String getPhoneNumber();

    @Value("#{target.menu != null ? target.menu.url: ''}")
    String getMenu();

    List<OpenHour> getOpenHours();

}
