package com.stol.model.projection.restaurant;

import com.stol.configuration.constant.Localization;
import com.stol.model.Cuisine;
import com.stol.model.OpenHour;
import com.stol.model.Restaurant;
import com.stol.model.Tag;
import com.stol.model.projection.restaurant.dto.RestaurantCuisine;
import com.stol.model.projection.restaurant.dto.RestaurantDiscount;
import com.stol.model.projection.restaurant.dto.RestaurantTag;
import com.stol.service.localization.LocalizationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.hateoas.Identifiable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Projection(name = "feed", types = {Restaurant.class})
public interface RestaurantFeed extends Identifiable<Long> {

    @Value("#{target.topPicture != null ? target.topPicture.url: ''}")
    String getTopPictureUrl();

    @Value("#{@restaurantService.findActiveHappyHours(target)}")
    List<RestaurantDiscount> getDiscounts();

    @Value("#{@localizationService.restaurant.getName(target)}")
    String getName();

    Double getRating();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantFeed).getFirstTag(@localizationService, target)}")
    RestaurantTag getTag();

    @Value("#{T(com.stol.model.projection.restaurant.RestaurantFeed).getFirstCuisine(@localizationService, target)}")
    RestaurantCuisine getCuisine();

    Double getAverageTicket();

    @Value("#{@restaurantService.numberOfAllRestaurantReviews(target)}")
    long getTotalReviews();

    BigDecimal getLatitude();

    BigDecimal getLongitude();

    @Value("#{target.marker != null &&  T(org.apache.commons.lang3.StringUtils).isNotBlank(target.marker.title) ? target.marker.title : ''}")
    String getMarkerTitle();

    @Value("#{target.marker != null &&  target.marker.type != null && target.marker.type.active ? target.marker.type.name : ''}")
    String getMarkerName();

    @Value("#{target.marker != null && target.marker.type != null && target.marker.type.active && target.marker.type.icon != null ? target.marker.type.icon.url : ''}")
    String getMarkerUrl();

    List<OpenHour> getOpenHours();

    @Value("#{@userService.isRestaurantIsSavedForUser(target)}")
    boolean isSaved();

    static RestaurantTag getFirstTag(LocalizationService localizationService, Restaurant restaurant) {
        Set<Tag> tags = restaurant.getTags();
        if (Objects.nonNull(tags)) {
            Tag tag = tags.stream().findFirst().orElse(null);
            if (Objects.nonNull(tag)) {
                return new RestaurantTag(localizationService.getTag().getName(tag), tag.getIcon().getUrl());
            }
        }
        return null;
    }

    static RestaurantCuisine getFirstCuisine(LocalizationService localizationService, Restaurant restaurant) {
        Set<Cuisine> cuisines = restaurant.getCuisines();
        if (Objects.nonNull(cuisines)) {
            Cuisine cuisine = cuisines.stream().findFirst().orElse(null);
            if (Objects.nonNull(cuisine)) {
                return new RestaurantCuisine(localizationService.getCuisine().getName(cuisine), cuisine.getIcon().getUrl());
            }

        }
        return null;
    }
}
