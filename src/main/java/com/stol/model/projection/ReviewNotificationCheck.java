package com.stol.model.projection;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.stol.model.ReviewNotification;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;
import java.time.LocalTime;

@Projection(name = "check", types = {ReviewNotification.class})
public interface ReviewNotificationCheck {
    Long getId();

    @Value("#{target.reservation.id}")
    Long getReservationId();

    @Value("#{target.restaurant.topPicture != null ? target.restaurant.topPicture.url: ''}")
    String getTopPictureUrl();

    @Value("#{target.restaurant.id}")
    Long getRestaurantId();

    @Value("#{@localizationService.restaurant.getName(target.restaurant)}")
    String getRestaurantName();

    @Value("#{@localizationService.restaurant.getAddress(target.restaurant)}")
    String getRestaurantAddress();

    @Value("#{target.restaurant.rating}")
    Double getRestaurantRating();

    @JsonFormat(pattern = "HH:mm")
    @Value("#{target.reservation.dateTime.toLocalTime()}")
    LocalTime getTime();

    @Value("#{target.reservation.dateTime.toLocalDate()}")
    LocalDate getDate();

    @Value("#{target.reservation.guestNumber}")
    int getGuestNumber();
}
