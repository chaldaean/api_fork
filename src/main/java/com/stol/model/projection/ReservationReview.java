package com.stol.model.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "review", types = {com.stol.model.Reservation.class})
public interface ReservationReview extends ReservationTime {
    @Value("#{target.restaurant.rating}")
    Double getRestaurantRating();

    @Value("#{target.restaurant.id}")
    Long getRestaurantId();
}
