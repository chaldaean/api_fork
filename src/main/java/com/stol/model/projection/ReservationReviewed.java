package com.stol.model.projection;

import com.stol.model.Reservation;
import com.stol.model.Review;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "reviewed", types = {Reservation.class})
public interface ReservationReviewed extends ReservationTime{
    @Value("#{@reviewService.findReviewByReservation(target)}")
    Review getReview();
}
