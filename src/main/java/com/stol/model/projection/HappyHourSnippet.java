package com.stol.model.projection;

import com.stol.model.HappyHour;
import com.stol.model.Time;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;
import java.util.Set;

@Projection(name = "snippet", types = {HappyHour.class})
public interface HappyHourSnippet {
    LocalDate getActivationDate();

    LocalDate getExpirationDate();

    Integer getQuantity();

    int getDiscount();

    boolean isActive();

    @Value("#{target.restaurant.name}")
    String getRestaurantName();

    Set<Time> getTimes();
}
