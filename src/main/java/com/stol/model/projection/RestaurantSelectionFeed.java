package com.stol.model.projection;

import com.stol.model.RestaurantSelection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "feed", types = {RestaurantSelection.class})
public interface RestaurantSelectionFeed {
    @Value("#{@localizationService.restaurantSelection.getTitle(target)}")
    String getTitle();

    @Value("#{target.picture.url != null ? target.picture.url: ''}")
    String getIcon();

    int getTotal();
}
