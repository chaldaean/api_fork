package com.stol.model.projection;

import com.stol.model.Article;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "feed", types = {Article.class})
public interface ArticleFeed {

    @Value("#{@localizationService.article.getTitle(target)}")
    String getTitle();

    @Value("#{target.picture.url != null ? target.picture.url: ''}")
    String getIcon();
}
