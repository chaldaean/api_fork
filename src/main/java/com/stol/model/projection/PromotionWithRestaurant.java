package com.stol.model.projection;

import com.stol.model.Promotion;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Projection(name = "restaurant", types = {Promotion.class})
public interface PromotionWithRestaurant {
    @Value("#{@localizationService.promotion.getDescription(target)}")
    String getDescription();

    LocalDateTime getCreationDate();

    LocalDate getEndDate();

    int getTotal();

    int getUsed();

    boolean isActive();

    @Value("#{@localizationService.restaurant.getName(target.restaurant)}")
    String getRestaurantName();
}
