package com.stol.model.projection;

import com.stol.model.Language;
import com.stol.model.user.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "user_page", types = {User.class})
public interface UserPage {
    Long getId();

    String getName();

    String getSurname();

    @Value("#{@reservationRepository.numberOfUserReservations(target)}")
    int getTotalReservations();

    @Value("#{target.photo != null ? target.photo.url : ''}")
    String getPhoto();

    @Value("#{target.savedRestaurants.size()}")
    int getTotalLikedRestaurants();
}