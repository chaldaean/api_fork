package com.stol.model.projection;

import com.stol.model.Cuisine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "iconUrl", types = {Cuisine.class})
public interface CuisineWithIconUrl {
    @Value("#{@localizationService.cuisine.getName(target)}")
    String getName();
    @Value("#{target.icon.url}")
    String getIconUrl();
    boolean isActive();
}
