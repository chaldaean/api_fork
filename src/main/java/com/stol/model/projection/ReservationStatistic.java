package com.stol.model.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;

@Projection(name = "statistic", types = {com.stol.model.Reservation.class})
public interface ReservationStatistic {
    Long getId();

    @Value("#{target.client.id}")
    Long getUserId();

    @Value("#{target.dateTime.toLocalDate()}")
    LocalDate getDate();

    int getGuestNumber();
}
