package com.stol.model.projection;

import com.stol.model.Cuisine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "id", types = {Cuisine.class})
public interface CuisineWithId {
    Long getId();

    @Value("#{@localizationService.cuisine.getName(target)}")
    String getName();
}
