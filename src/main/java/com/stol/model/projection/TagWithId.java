package com.stol.model.projection;

import com.stol.model.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "id", types = {Tag.class})
public interface TagWithId {
    Long getId();

    @Value("#{@localizationService.tag.getName(target)}")
    String getName();
}
