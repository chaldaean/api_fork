package com.stol.model.projection;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.stol.model.property.ReservationStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;
import java.time.LocalTime;

@Projection(name = "client", types = {com.stol.model.Reservation.class})
public interface ReservationClient {
    Long getId();

    @Value("#{target.dateTime.toLocalDate()}")
    LocalDate getDate();

    @Value("#{target.dateTime.toLocalTime()}")
    @JsonFormat(pattern = "HH:mm")
    LocalTime getTime();

    int getGuestNumber();

    String getComment();

    ReservationStatus getStatus();

    @Value("#{@localizationService.restaurant.getName(target.restaurant)}")
    String getRestaurantName();

    @Value("#{@localizationService.restaurant.getAddress(target.restaurant)}")
    String getRestaurantAddress();

    @Value("#{target.client.photo != null ? target.client.photo.url : ''}")
    String getPhoto();
}
