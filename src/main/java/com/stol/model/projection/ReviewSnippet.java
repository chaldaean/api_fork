package com.stol.model.projection;

import com.stol.model.Review;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;

@Projection(name = "snippet", types = {Review.class})
public interface ReviewSnippet {
    LocalDateTime getCreationDate();
    Integer getRating();
    String getComment();
    @Value("#{target.user.name} #{target.user.surname}")
    String getUserName();
    @Value("#{@localizationService.restaurant.getName(target.restaurant)}")
    String getRestaurantName();
}
