package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "promotion_i18n", uniqueConstraints = {
        @UniqueConstraint(name = "PROMOTION_I18N_UK", columnNames = {"promotion_id", "language_id"})})
public class PromotionI18n implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "description", length = 90)
    private String description;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "language_id", foreignKey = @ForeignKey(name = "PROMOTION_I18N_LANGUAGE_ID_FK"))
    private Language language;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PromotionI18n that = (PromotionI18n) o;
        return Objects.equals(description, that.description) &&
                Objects.equals(language, that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, language);
    }
}
