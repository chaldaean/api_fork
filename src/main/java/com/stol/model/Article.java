package com.stol.model;

import com.stol.model.user.User;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

@Entity
@Table(name = "article")
public class Article implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "creation_date", nullable = false)
    private final LocalDateTime creationDate = LocalDateTime.now();

    @NotEmpty
    @Size(max = 255)
    @Column(name = "title", nullable = false)
    private String title;

    @NotEmpty
    @Lob
    @Column(name = "text", nullable = false)
    private String text;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "picture_id", foreignKey = @ForeignKey(name = "FK_ARTICLE_MEDIA_FILE_ID"))
    private MediaFile picture;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id", nullable = false, foreignKey = @ForeignKey(name = "FK_ARTICLE_USER_ID"))
    private User author;

    @Column(name = "active", nullable = false)
    private boolean active = true;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "article_id", nullable = false, foreignKey = @ForeignKey(name = "ARTICLE_I18N_ARTICLE_ID_FK"))
    private Set<ArticleI18n> i18ns = new HashSet<>();


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public MediaFile getPicture() {
        return picture;
    }

    public Article setPicture(MediaFile picture) {
        this.picture = picture;
        return this;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public User getAuthor() {
        return author;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setI18ns(Set<ArticleI18n> i18ns) {
        this.i18ns.clear();
        this.i18ns.addAll(i18ns);
    }

    public Set<ArticleI18n> getI18ns() {
        return i18ns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Article article = (Article) o;
        return active == article.active &&
                Objects.equals(creationDate, article.creationDate) &&
                Objects.equals(title, article.title) &&
                Objects.equals(text, article.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationDate, title, text, active);
    }
}
