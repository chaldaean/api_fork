package com.stol.model.property;

public enum UserRole {
    ROLE_SYSTEM_ADMIN("SYSTEM_ADMIN"),
    ROLE_RESTAURANT_ADMIN("RESTAURANT_ADMIN"),
    ROLE_USER("USER"),
    ROLE_NOT_SET("NOT_SET");

    private final String role;

    UserRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return role;
    }
}