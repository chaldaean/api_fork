package com.stol.model.property;

public enum UserStatus {
	NOT_CONFIRMED,
	ENABLE,
	DISABLE;
}