package com.stol.model.property;

public enum ReservationStatus {
    NEW,
    CONFIRMED,
    REJECTED,
    CANCELED;
}
