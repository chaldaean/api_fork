package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "restaurant_i18n", uniqueConstraints = {
        @UniqueConstraint(name = "RESTAURANT_I18N_UK", columnNames = {"restaurant_id", "language_id"})})
public class RestaurantI18n implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "description", length = 20000)
    private String description;

    @Column(name = "breakfast")
    private String breakfast;

    @Column(name = "parking")
    private String parking;

    @Column(name = "payment_method")
    private String paymentMethod;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "language_id", foreignKey = @ForeignKey(name = "RESTAURANT_I18N_LANGUAGE_ID_FK"))
    private Language language;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RestaurantI18n that = (RestaurantI18n) o;
        return  Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(description, that.description) &&
                Objects.equals(breakfast, that.breakfast) &&
                Objects.equals(parking, that.parking) &&
                Objects.equals(paymentMethod, that.paymentMethod) &&
                Objects.equals(language, that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, description, breakfast, parking, paymentMethod, language);
    }
}
