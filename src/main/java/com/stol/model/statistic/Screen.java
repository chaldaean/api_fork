package com.stol.model.statistic;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "screen")
public class Screen implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "creation_date", nullable = false)
    private final LocalDate date = LocalDate.now();

    @Column(name = "screen_number", nullable = false)
    private int screens = 1;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getScreens() {
        return screens;
    }

    public void setScreens(int screens) {
        this.screens = screens;
    }
}
