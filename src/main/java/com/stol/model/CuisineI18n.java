package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cuisine_i18n", uniqueConstraints = {
        @UniqueConstraint(name = "CUISINE_I18N_UK", columnNames = {"cuisine_id", "language_id"})})
public class CuisineI18n implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "language_id", foreignKey = @ForeignKey(name = "CUISINE_I18N_LANGUAGE_ID_FK"))
    private Language language;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CuisineI18n that = (CuisineI18n) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(language, that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, language);
    }
}
