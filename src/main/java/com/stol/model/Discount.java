package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "discount")
public class Discount implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "value", nullable = false)
    private int value;

    @NotNull
    @OneToOne
    @JoinColumn(name = "happy_hour_id", nullable = false, foreignKey = @ForeignKey(name = "FK_DISCOUNT_HAPPY_HOUR_ID"))
    private HappyHour happyHour;

    @Column(name = "is_used", nullable = false)
    private boolean isUsed = false;

    @Override
    public Long getId() {
        return id;
    }

    public Discount setId(Long id) {
        this.id = id;
        return this;
    }

    public int getValue() {
        return value;
    }

    public Discount setValue(int value) {
        this.value = value;
        return this;
    }

    public HappyHour getHappyHour() {
        return happyHour;
    }

    public Discount setHappyHour(HappyHour happyHour) {
        this.happyHour = happyHour;
        return this;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public Discount setUsed(boolean used) {
        isUsed = used;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Discount discount = (Discount) o;
        return value == discount.value &&
                isUsed == discount.isUsed &&
                Objects.equals(happyHour, discount.happyHour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, happyHour, isUsed);
    }
}
