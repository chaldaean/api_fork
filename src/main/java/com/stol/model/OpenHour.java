package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "open_hour")
public class OpenHour implements Identifiable<Long>, Comparable<OpenHour> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "weekday", nullable = false)
    private byte weekday;

    @Column(name = "open_time", nullable = false)
    private LocalTime openTime;

    @Column(name = "close_time", nullable = false)
    private LocalTime closeTime;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte getWeekday() {
        return weekday;
    }

    public void setWeekday(byte weekday) {
        this.weekday = weekday;
    }

    public LocalTime getOpenTime() {
        return openTime;
    }

    public void setOpenTime(LocalTime openTime) {
        this.openTime = openTime;
    }

    public LocalTime getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(LocalTime closeTime) {
        this.closeTime = closeTime;
    }

    @Override
    public int compareTo(OpenHour o) {
        if(o == null) {
            return 1;
        }

        return Integer.compare(weekday, o.weekday);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OpenHour)) return false;
        OpenHour openHour = (OpenHour) o;
        return weekday == openHour.weekday &&
                Objects.equals(openTime, openHour.openTime) &&
                Objects.equals(closeTime, openHour.closeTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weekday, openTime, closeTime);
    }
}
