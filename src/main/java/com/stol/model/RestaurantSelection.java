package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

@Entity
@Table(name = "restaurant_selection", uniqueConstraints = @UniqueConstraint(name = "UK_RESTAURANT_SELECTION_PICTURE", columnNames = "picture_id"))
public class RestaurantSelection implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "creation_date", nullable = false)
    private final LocalDateTime creationDate = LocalDateTime.now();

    @NotEmpty
    @Size(max = 255)
    @Column(name = "title", length = 255, nullable = false)
    private String title;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "picture_id", nullable = false, foreignKey = @ForeignKey(name = "FK_RESTAURANT_SELECTION_MEDIA_FILE_ID"))
    private MediaFile picture;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "restaurant_in_selection",
            joinColumns = @JoinColumn(name = "selection_id", nullable = false),
            foreignKey = @ForeignKey(name = "FK_RESTAURANT_SELECTION_SELECTION_ID"),
            inverseJoinColumns = @JoinColumn(name = "restaurant_id", nullable = false),
            inverseForeignKey = @ForeignKey(name = "FK_RESTAURANT_SELECTION_RESTAURANT_ID"))
    private Set<Restaurant> restaurants;

    @Column(name = "total", nullable = false)
    private int total = 0;

    @Column(name = "active", nullable = false)
    private boolean active = true;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "restaurant_selection_id", nullable = false, foreignKey = @ForeignKey(name = "RESTAURANT_SELECTION_I18N_RESTAURANT_SELECTION_ID_FK"))
    private Set<RestaurantSelectionI18n> i18ns = new HashSet<>();


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MediaFile getPicture() {
        return picture;
    }

    public void setPicture(MediaFile picture) {
        this.picture = picture;
    }

    public Set<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(Set<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<RestaurantSelectionI18n> getI18ns() {
        return i18ns;
    }

    public void setI18ns(Set<RestaurantSelectionI18n> i18ns) {
        this.i18ns.clear();
        this.i18ns.addAll(i18ns);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RestaurantSelection that = (RestaurantSelection) o;
        return total == that.total &&
                active == that.active &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationDate, title, total, active);
    }
}
