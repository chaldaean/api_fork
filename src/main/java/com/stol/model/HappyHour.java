package com.stol.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "happy_hour")
public class HappyHour implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id", nullable = false, foreignKey = @ForeignKey(name = "FK_HAPPY_HOUR_RESTAURANT_ID"))
    private Restaurant restaurant;

    @Column(name = "activation_date")
    private LocalDate activationDate;

    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    @Column(name = "quantity")
    private Integer quantity;

    @JsonIgnore
    @Column(name = "used_quantity", nullable = false)
    private Integer usedQuantity = 0;

    @Column(name = "discount", nullable = false)
    private Integer discount;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "happy_hour_times",
            uniqueConstraints = @UniqueConstraint(name = "UK_HAPPY_HOUR_TIME", columnNames = {"happy_hour_id", "time_id"}),
            joinColumns = @JoinColumn(name = "happy_hour_id", nullable = false),
            foreignKey = @ForeignKey(name = "FK_HAPPY_HOUR_TIME_HAPPY_HOUR_ID"),
            inverseJoinColumns = @JoinColumn(name = "time_id", nullable = false),
            inverseForeignKey = @ForeignKey(name = "HAPPY_HOUR_TIME_TIME_ID_FK"))
    @OrderBy("time asc")
    private Set<Time> times;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public LocalDate getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(LocalDate activationDate) {
        this.activationDate = activationDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Set<Time> getTimes() {
        return times;
    }

    public void setTimes(Set<Time> times) {
        if (this.times == null) {
            this.times = times;
        } else {
            this.times.clear();
            this.times.addAll(times);
        }
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        if ((this.active != active) && active) {
            this.usedQuantity = 0;
        }
        this.active = active;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getUsedQuantity() {
        return usedQuantity;
    }

    public void setUsedQuantity(Integer usedQuantity) {
        this.usedQuantity = usedQuantity;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }
}
