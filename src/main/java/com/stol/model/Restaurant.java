package com.stol.model;

import com.stol.model.user.User;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Stream;

@Entity
@Table(name = "restaurant", uniqueConstraints = {
        @UniqueConstraint(name = "UK_RESTAURANT_MENU", columnNames = "menu_id"),
        @UniqueConstraint(name = "UK_RESTAURANT_TOP_PICTURE", columnNames = "top_picture_id")})
public class Restaurant implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotEmpty
    @Column(name = "name", nullable = false)
    private String name;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "top_picture_id", foreignKey = @ForeignKey(name = "FK_RESTAURANT_PICTURE_MEDIA_FILE_ID"))
    @RestResource(path = "top_picture")
    private MediaFile topPicture;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinTable(name = "restaurant_pictures",
            uniqueConstraints = @UniqueConstraint(name = "UK_RESTAURANT_PICTURES_MEDIA_FILE_ID", columnNames = "media_file_id"),
            joinColumns = @JoinColumn(name = "restaurant_id", nullable = false),
            foreignKey = @ForeignKey(name = "FK_RESTAURANT_PICTURES_RESTAURANT_ID"),
            inverseJoinColumns = @JoinColumn(name = "media_file_id", nullable = false),
            inverseForeignKey = @ForeignKey(name = "FK_RESTAURANT_PICTURES_MEDIA_FILE_ID"))
    private List<MediaFile> pictures;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "restaurant_id", nullable = false, foreignKey = @ForeignKey(name = "OPEN_HOUR_RESTAURANT_ID_FK"))
    private List<OpenHour> openHours = new ArrayList<>();

    @NotEmpty
    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "latitude", nullable = false)
    private BigDecimal latitude;

    @Column(name = "longitude", nullable = false)
    private BigDecimal longitude;

    @Column(name = "description", length = 20000)
    private String description;

    @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "menu_id", foreignKey = @ForeignKey(name = "FK_RESTAURANT_MENU_MEDIA_FILE_ID"))
    private MediaFile menu;

    @Column(name = "phones")
    private String phoneNumber;

    @Column(name = "breakfast")
    private String breakfast;

    @Column(name = "average_ticket")
    private Double averageTicket;

    @Column(name = "parking")
    private String parking;

    @Column(name = "payment_method")
    private String paymentMethod;

    @Column(name = "rating")
    private Double rating;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "restaurant_tags",
            joinColumns = @JoinColumn(name = "restaurant_id", nullable = false),
            foreignKey = @ForeignKey(name = "FK_RESTAURANT_TAG_RESTAURANT_ID"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", nullable = false),
            inverseForeignKey = @ForeignKey(name = "FK_RESTAURANT_TAG_TAG_ID"))
    private Set<Tag> tags;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "restaurant_cuisines",
            joinColumns = @JoinColumn(name = "restaurant_id", nullable = false),
            foreignKey = @ForeignKey(name = "FK_RESTAURANT_CUISINE_RESTAURANT_ID"),
            inverseJoinColumns = @JoinColumn(name = "cuisine_id", nullable = false),
            inverseForeignKey = @ForeignKey(name = "FK_RESTAURANT_CUISINE_ID"))
    private Set<Cuisine> cuisines;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "restaurant_analogous",
            joinColumns = @JoinColumn(name = "restaurant_id", nullable = false),
            foreignKey = @ForeignKey(name = "FK_RESTAURANT_ANALOGOUS_RESTAURANT_ID"),
            inverseJoinColumns = @JoinColumn(name = "analogous_restaurant_id", nullable = false),
            inverseForeignKey = @ForeignKey(name = "FK_RESTAURANT_ANALOGOUS_ANALOGOUS_RESTAURANT_ID"))
    @Column(name = "analogous")
    private Set<Restaurant> analogous;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "restaurants_owners",
            joinColumns = @JoinColumn(name = "restaurant_id", nullable = false),
            foreignKey = @ForeignKey(name = "FK_RESTAURANT_USER_RESTAURANT_ID"),
            inverseJoinColumns = @JoinColumn(name = "user_id", nullable = false),
            inverseForeignKey = @ForeignKey(name = "FK_RESTAURANT_USER_USER_ID"))
    private Set<User> owners;

    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Promotion> promotions;

    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY, orphanRemoval = true)
    @OrderBy("discount asc")
    @RestResource(path = "happy_hours")
    private Set<HappyHour> happyHours = new HashSet<>();

    @OneToOne(mappedBy = "restaurant", fetch = FetchType.LAZY, orphanRemoval = true)
    @RestResource(path = "marker")
    private Marker marker;

    @Column(name = "active", nullable = false)
    private boolean active = true;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "restaurant_id", nullable = false, foreignKey = @ForeignKey(name = "RESTAURANT_I18N_RESTAURANT_ID_FK"))
    private Set<RestaurantI18n> i18ns = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MediaFile getTopPicture() {
        return topPicture;
    }

    public void setTopPicture(MediaFile topPicture) {
        this.topPicture = topPicture;
    }

    public List<MediaFile> getPictures() {
        return pictures;
    }

    public void setPictures(List<MediaFile> pictures) {
        if (this.pictures == null) {
            this.pictures = pictures;
        } else {
            this.pictures.clear();
            this.pictures.addAll(pictures);
        }
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MediaFile getMenu() {
        return menu;
    }

    public void setMenu(MediaFile menu) {
        this.menu = menu;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    public Double getAverageTicket() {
        return averageTicket;
    }

    public void setAverageTicket(Double averageTicket) {
        this.averageTicket = averageTicket;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Cuisine> getCuisines() {
        return cuisines;
    }

    public void setCuisines(Set<Cuisine> cuisines) {
        this.cuisines = cuisines;
    }

    public Set<Restaurant> getAnalogous() {
        return analogous;
    }

    public void setAnalogous(Set<Restaurant> analogous) {
        this.analogous = analogous;
    }

    public Set<User> getOwners() {
        return owners;
    }

    public void setOwners(Set<User> owners) {
        this.owners = owners;
    }

    public Set<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(Set<Promotion> promotions) {
        this.promotions = promotions;
    }

    public Set<HappyHour> getHappyHours() {
        return happyHours;
    }

    public void setHappyHours(Set<HappyHour> happyHours) {
        this.happyHours = happyHours;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<OpenHour> getOpenHours() {
        return openHours;
    }

    public Set<RestaurantI18n> getI18ns() {
        return i18ns;
    }

    public void setI18ns(Set<RestaurantI18n> i18ns) {
        this.i18ns.clear();
        this.i18ns.addAll(i18ns);
    }

    public void setOpenHours(List<OpenHour> openHours) {
        Collections.sort(openHours);
        this.openHours.clear();
        this.openHours.addAll(openHours);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Restaurant that = (Restaurant) o;
        return active == that.active &&
                Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude) &&
                Objects.equals(description, that.description) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(breakfast, that.breakfast) &&
                Objects.equals(averageTicket, that.averageTicket) &&
                Objects.equals(parking, that.parking) &&
                Objects.equals(paymentMethod, that.paymentMethod) &&
                Objects.equals(rating, that.rating);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, latitude, longitude, description, phoneNumber, breakfast, averageTicket, parking, paymentMethod, rating, active);
    }
}
