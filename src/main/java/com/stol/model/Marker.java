package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "marker", uniqueConstraints = {
        @UniqueConstraint(name = "UK_MARKER_RESTAURANT_ID", columnNames = "restaurant_id")})
public class Marker implements Identifiable<Long> {

    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "title", nullable = false, length = 3)
    private String title;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "marker_type_id", foreignKey = @ForeignKey(name = "MARKER_MARKER_TYPE_ID_FK"))
    private MarkerType type;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id", foreignKey = @ForeignKey(name = "MARKER_RESTAURANT_ID_FK"))
    private Restaurant restaurant;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public MarkerType getType() {
        return type;
    }

    public void setType(MarkerType type) {
        this.type = type;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Marker that = (Marker) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(type, that.type) &&
                Objects.equals(restaurant, that.restaurant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, type, restaurant);
    }
}
