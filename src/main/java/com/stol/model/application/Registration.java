package com.stol.model.application;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.stol.configuration.constant.Localization.DEFAULT_LANGUAGE;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Registration {
    @NotEmpty
    @Size(min = 2, max = 30)
    @Pattern(regexp = "^[\\p{L} ]+$")
    private String name;
    @NotEmpty
    @Size(min = 2, max = 30)
    @Pattern(regexp = "^[\\p{L} ]+$")
    private String surname;
    @NotEmpty
    @Size(max = 20)
    private String login;
    @NotEmpty
    @Size(min = 6, max = 100)
    private String password;
    private boolean customer;
    private String restaurantName;
    @Size(max = 40)
    private String email;
    @Size(max = 2)
    private String language = DEFAULT_LANGUAGE;


    public String getName() {
        return name;
    }

    public Registration setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public Registration setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public Registration setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Registration setPassword(String password) {
        this.password = password;
        return this;
    }

    public boolean isCustomer() {
        return customer;
    }

    public void setCustomer(boolean customer) {
        this.customer = customer;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
