package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "article_i18n", uniqueConstraints = {
        @UniqueConstraint(name = "ARTICLE_I18N_UK", columnNames = {"article_id", "language_id"})})
public class ArticleI18n implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "title")
    @Size(max = 255)
    private String title;

    @Lob
    @Column(name = "text")
    private String text;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "language_id", foreignKey = @ForeignKey(name = "ARTICLE_I18N_LANGUAGE_ID_FK"))
    private Language language;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleI18n that = (ArticleI18n) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(text, that.text) &&
                Objects.equals(language, that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, text, language);
    }
}
