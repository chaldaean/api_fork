package com.stol.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "time")
public class Time implements Identifiable<Long> {

    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @JsonFormat(pattern = "HH:mm")
    @Column(name = "time", nullable = false, unique = true)
    private LocalTime time;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
