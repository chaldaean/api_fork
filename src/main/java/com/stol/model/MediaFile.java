package com.stol.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "media_file",
        uniqueConstraints = @UniqueConstraint(name = "UK_MEDIA_FILE_NAME", columnNames = "name"))
public class MediaFile {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @NotEmpty
    @Size(min = 1, max = 10)
    @Column(name = "extension", nullable = false, length = 10)
    private String extension;

    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name = "media_type", nullable = false, length = 30)
    private String mediaType;

    @Column(name = "url")
    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MediaFile mediaFile = (MediaFile) o;
        return Objects.equals(name, mediaFile.name) &&
                Objects.equals(extension, mediaFile.extension) &&
                Objects.equals(mediaType, mediaFile.mediaType) &&
                Objects.equals(url, mediaFile.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, extension, mediaType, url);
    }
}
