package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "marker_type", uniqueConstraints = {
        @UniqueConstraint(name = "UK_MARKER_TYPE_NAME", columnNames = "name"),
        @UniqueConstraint(name = "UK_MARKER_TYPE_ICON_ID", columnNames = "icon_id")})
public class MarkerType implements Identifiable<Long> {

    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotEmpty
    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "icon_id", foreignKey = @ForeignKey(name = "MARKER_TYPE_MEDIA_FILE_ID_FK"))
    private MediaFile icon;

    @NotNull
    @Column(name = "active", nullable = false)
    private boolean active = true;

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getId() {
        return null;
    }

    public MediaFile getIcon() {
        return icon;
    }

    public void setIcon(MediaFile icon) {
        this.icon = icon;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MarkerType)) return false;
        MarkerType that = (MarkerType) o;
        return active == that.active &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(icon, that.icon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, icon, active);
    }
}
