package com.stol.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.stol.model.user.User;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "review_notification")
public class ReviewNotification implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "activation_time", nullable = false)
    private LocalDateTime activationTime;

    @Column(name = "sent", nullable = false)
    private Boolean sent = false;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "end_time", nullable = false)
    private LocalDateTime endTime;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reservation_id", nullable = false, foreignKey = @ForeignKey(name = "FK_REVIEW_NOTIFICATION_RESERVATION_ID"))
    private Reservation reservation;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, foreignKey = @ForeignKey(name = "FK_REVIEW_NOTIFICATION_USER_ID"))
    private User user;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id", nullable = false, foreignKey = @ForeignKey(name = "FK_REVIEW_NOTIFICATION_RESTAURANT_ID"))
    private Restaurant restaurant;

    @Override
    public Long getId() {
        return id;
    }

    public ReviewNotification setId(Long id) {
        this.id = id;
        return this;
    }

    public LocalDateTime getActivationTime() {
        return activationTime;
    }

    public ReviewNotification setActivationTime(LocalDateTime activationTime) {
        this.activationTime = activationTime;
        return this;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public ReviewNotification setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public ReviewNotification setReservation(Reservation reservation) {
        this.reservation = reservation;
        return this;
    }

    public User getUser() {
        return user;
    }

    public ReviewNotification setUser(User user) {
        this.user = user;
        return this;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public ReviewNotification setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
        return this;
    }

    public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReviewNotification that = (ReviewNotification) o;
        return Objects.equals(activationTime, that.activationTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(reservation, that.reservation) &&
                Objects.equals(user, that.user) &&
                Objects.equals(sent, that.sent) &&
                Objects.equals(restaurant, that.restaurant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(activationTime, endTime, reservation, user, restaurant, sent);
    }
}
