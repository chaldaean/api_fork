package com.stol.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Locale;
import java.util.Objects;

@Entity
@Table(name = "language", uniqueConstraints = {
        @UniqueConstraint(name = "LANGUAGE_NAME_UK", columnNames = "id"),
        @UniqueConstraint(name = "LANGUAGE_CODE_UK", columnNames = "code")})
public class Language implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotEmpty
    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @NotEmpty
    @Column(name = "code", nullable = false, length = 20)
    private String code;

    @Column(name = "active", nullable = false)
    private boolean active = true;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @JsonIgnore
    public Locale getLocale() {
        return Locale.forLanguageTag(code);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Language language = (Language) o;
        return active == language.active &&
                Objects.equals(name, language.name) &&
                Objects.equals(code, language.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, code, active);
    }
}
