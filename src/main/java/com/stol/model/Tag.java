package com.stol.model;

import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

@Entity
@Table(name = "tag", uniqueConstraints = {
        @UniqueConstraint(name = "UK_TAG_NAME", columnNames = "name"),
        @UniqueConstraint(name = "UK_TAG_ICON", columnNames = "icon_id")})
public class Tag implements Identifiable<Long> {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotEmpty
    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "icon_id", nullable = false, foreignKey = @ForeignKey(name = "FK_TAG_MEDIA_FILE_ID"))
    private MediaFile icon;

    @Column(name = "active", nullable = false)
    private boolean active = true;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "tag_id", nullable = false, foreignKey = @ForeignKey(name = "TAG_I18N_TAG_ID_FK"))
    private Set<TagI18n> i18ns = new HashSet<>();


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MediaFile getIcon() {
        return icon;
    }

    public void setIcon(MediaFile icon) {
        this.icon = icon;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<TagI18n> getI18ns() {
        return i18ns;
    }

    public void setI18ns(Set<TagI18n> i18ns) {
        this.i18ns.clear();
        this.i18ns.addAll(i18ns);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return active == tag.active &&
                Objects.equals(name, tag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, active);
    }
}
