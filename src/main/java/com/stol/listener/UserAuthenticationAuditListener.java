package com.stol.listener;

import com.stol.service.user.CurrentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.security.AuthenticationAuditListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;


public class UserAuthenticationAuditListener extends AuthenticationAuditListener {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent event) {
        if(event.getAuthentication() != null
                && event.getAuthentication().getPrincipal() != null
                && event.getAuthentication().getPrincipal() instanceof CurrentUser) {
            super.onApplicationEvent(event);
        }

        logger.debug("Authentication event skipped from auditing for " + event.toString());
    }
}
