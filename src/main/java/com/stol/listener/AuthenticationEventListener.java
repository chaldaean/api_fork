package com.stol.listener;

import com.stol.model.user.UserSystemInformation;
import com.stol.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;

import java.util.Objects;
import java.util.Optional;

public class AuthenticationEventListener {
    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public AuthenticationEventListener(UserService userService) {
        Objects.requireNonNull(userService);
        this.userService = userService;
    }

    @EventListener
    public void authenticationFailed(AuthenticationFailureBadCredentialsEvent event) {
        Object principal = event.getAuthentication().getPrincipal();
        Optional<UserSystemInformation> optional = userService.findUserSystemInformation(principal);
        if (optional.isPresent()) {
            UserSystemInformation info = optional.get();
            if (userService.hasLoginAttempts(info)) {
                info.setLoginAttempts(info.getLoginAttempts() + 1);
                userService.updateUserSystemInformation(info);
            } else if (userService.isLoginTryCooldownExpired(info)) {
                info.setLoginAttempts(1);
                userService.updateUserSystemInformation(info);
            }

            logger.info("User {} failed to log in, attempt {}", info.getPhoneNumber(), info.getLoginAttempts());
        }
    }
}
