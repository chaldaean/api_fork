package com.stol.converter;

import org.springframework.core.convert.converter.Converter;

public class UrlParameterStringToDouble implements Converter<String, Double> {

    @Override
    public Double convert(String source) {
        return Double.parseDouble(source);
    }
}
