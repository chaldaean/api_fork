USE `the_stol`;

create table if not exists language
(
	id bigint auto_increment primary key,
	name varchar(100) not null,
	code varchar(20) not null,
	`active` bit(1) not null default 1,
	constraint language_code_uk	unique (code),
	constraint language_name_uk	unique (name)
);

insert into language (code, name) values ('ru', 'Русский');
insert into language (code, name) values ('uk', 'Українська');
insert into language (code, name) values ('en', 'English');

create table if not exists article_i18n
(
	id bigint auto_increment
		primary key,
	article_id bigint not null,
	language_id bigint not null,
	title varchar(255) null,
	text longtext null,
	constraint article_i18n_uk
		unique (article_id, language_id),
	constraint article_i18n_article_id_fk
		foreign key (article_id) references article (id)
			on update cascade on delete cascade,
	constraint article_i18n_language_id_fk
		foreign key (language_id) references language (id)
)
;

create table if not exists cuisine_i18n
(
	id bigint auto_increment
		primary key,
	cuisine_id bigint not null,
	language_id bigint not null,
	name varchar(100) null,
	constraint cuisine_i18n_uk
		unique (cuisine_id, language_id),
	constraint cuisine_i18n_cuisine_id_fk
		foreign key (cuisine_id) references cuisine (id)
			on update cascade on delete cascade,
	constraint cuisine_i18n_language_id_fk
		foreign key (language_id) references language (id)
)
;

create table if not exists promotion_i18n
(
	id bigint auto_increment
		primary key,
	promotion_id bigint not null,
	language_id bigint not null,
	description varchar(90) null,
	constraint promotion_i18n_uk
		unique (language_id, promotion_id),
	constraint promotion_i18n_language_id_fk
		foreign key (language_id) references language (id),
	constraint promotion_i18n_promotion_id_fk
		foreign key (promotion_id) references promotion (id)
			on update cascade on delete cascade
)
;

create table if not exists restaurant_i18n
(
	id bigint auto_increment
		primary key,
	language_id bigint not null,
	restaurant_id bigint not null,
	address varchar(255) null,
	breakfast varchar(255) null,
	description varchar(10000) null,
	name varchar(255) null,
	parking varchar(255) null,
	payment_method varchar(255) null,
	constraint restaurant_i18n_uk
		unique (restaurant_id, language_id),
	constraint restaurant_i18n_language_id_fk
		foreign key (language_id) references language (id),
	constraint restaurant_i18n_restaurant_id_fk
		foreign key (restaurant_id) references restaurant (id)
			on update cascade on delete cascade
)
;


create table if not exists restaurant_selection_i18n
(
	id bigint auto_increment
		primary key,
	language_id bigint not null,
	restaurant_selection_id bigint not null,
	title varchar(255) null,
	constraint restaurant_selection_i18n_uk
		unique (restaurant_selection_id, language_id),
	constraint restaurant_selection_i18n_language_id_fk
		foreign key (language_id) references language (id),
	constraint restaurant_selection_i18n_restaurant_selection_id_fk
		foreign key (restaurant_selection_id) references restaurant_selection (id)
			on update cascade on delete cascade
)
;


create table if not exists tag_i18n
(
	id bigint auto_increment
		primary key,
	tag_id bigint not null,
	language_id bigint not null,
	name varchar(100) null,
	constraint tag_i18n_uk
		unique (tag_id, language_id),
	constraint tag_i18n_language_id_fk
		foreign key (language_id) references language (id),
	constraint tag_i18n_tag_id_fk
		foreign key (tag_id) references tag (id)
			on update cascade on delete cascade
)
;


alter table user add language_id bigint null
;

alter table user add constraint user_language_id_fk
  foreign key (language_id) references language (id);






