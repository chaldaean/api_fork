USE `the_stol`;

ALTER TABLE article DROP FOREIGN KEY FK_ARTICLE_USER_ID;
ALTER TABLE article ADD CONSTRAINT FK_ARTICLE_USER_ID FOREIGN KEY (author_id) REFERENCES user (id) ON DELETE CASCADE;

ALTER TABLE mobile_device DROP FOREIGN KEY FK_MOBILE_DEVICE_USER_ID;
ALTER TABLE mobile_device ADD CONSTRAINT FK_MOBILE_DEVICE_USER_ID FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;

ALTER TABLE reservation DROP FOREIGN KEY FK_RESERVATION_USER_ID;
ALTER TABLE reservation ADD CONSTRAINT FK_RESERVATION_USER_ID FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;

ALTER TABLE restaurants_owners DROP FOREIGN KEY FK_RESTAURANT_USER_USER_ID;
ALTER TABLE restaurants_owners ADD CONSTRAINT FK_RESTAURANT_USER_USER_ID FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;

ALTER TABLE review DROP FOREIGN KEY FK_REVIEW_USER_ID;
ALTER TABLE review ADD CONSTRAINT FK_REVIEW_USER_ID FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;

ALTER TABLE user_roles DROP FOREIGN KEY FK_USER_ROLE_USER_ID;
ALTER TABLE user_roles ADD CONSTRAINT FK_USER_ROLE_USER_ID FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;

ALTER TABLE user_saved_restaurants DROP FOREIGN KEY FK_USER_RESTAURANT_USER_ID;
ALTER TABLE user_saved_restaurants ADD CONSTRAINT FK_USER_RESTAURANT_USER_ID FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;

ALTER TABLE add_restaurant_request DROP FOREIGN KEY FK_ADD_RESTAURANT_REQUEST_USER_ID;
ALTER TABLE add_restaurant_request ADD CONSTRAINT FK_ADD_RESTAURANT_REQUEST_USER_ID FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;
