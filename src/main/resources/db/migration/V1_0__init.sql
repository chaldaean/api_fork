USE `the_stol`;

-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: the_stol
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.19.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `author_id` bigint(20) NOT NULL,
  `picture_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ARTICLE_USER_ID` (`author_id`),
  KEY `FK_ARTICLE_MEDIA_FILE_ID` (`picture_id`),
  CONSTRAINT `FK_ARTICLE_MEDIA_FILE_ID` FOREIGN KEY (`picture_id`) REFERENCES `media_file` (`id`),
  CONSTRAINT `FK_ARTICLE_USER_ID` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuisine`
--

DROP TABLE IF EXISTS `cuisine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuisine` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `name` varchar(100) NOT NULL,
  `icon_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_CUISINE_NAME` (`name`),
  UNIQUE KEY `UK_CUISINE_ICON` (`icon_id`),
  KEY `FK_CUISINE_MEDIA_FILE_ID` (`icon_id`),
  CONSTRAINT `FK_CUISINE_MEDIA_FILE_ID` FOREIGN KEY (`icon_id`) REFERENCES `media_file` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuisine`
--

LOCK TABLES `cuisine` WRITE;
/*!40000 ALTER TABLE `cuisine` DISABLE KEYS */;
INSERT INTO `cuisine` VALUES (1,_binary '','Американская',42),(2,_binary '','Аргентинская',43),(3,_binary '','Вьетнамская',44),(4,_binary '','Итальянская',45),(5,_binary '','Китайская',46),(6,_binary '','Мексиканская',47),(7,_binary '','Морепродукты',48),(8,_binary '','Тайская',49),(9,_binary '','Украинская',50),(10,_binary '','Японская',51),(11,_binary '','Европейская',52),(12,_binary '','Средиземноморская',53),(13,_binary '','Турецкая',54),(14,_binary '','Французская',55),(15,_binary '','Центральноевропейская',56);
/*!40000 ALTER TABLE `cuisine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `value` int(3) NOT NULL,
  `happy_hour_id` bigint(20) NOT NULL,
  `is_used` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_DISCOUNT_HAPPY_HOUR_ID` (`happy_hour_id`),
  CONSTRAINT `FK_DISCOUNT_HAPPY_HOUR_ID` FOREIGN KEY (`happy_hour_id`) REFERENCES `happy_hour` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discount`
--

LOCK TABLES `discount` WRITE;
/*!40000 ALTER TABLE `discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flyway_schema_history`
--

--
-- Table structure for table `happy_hour`
--

DROP TABLE IF EXISTS `happy_hour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happy_hour` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `restaurant_id` bigint(20) NOT NULL,
  `activation_date` date DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `used_quantity` int(11) NOT NULL,
  `expiration_date` date DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `active` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_HAPPY_HOUR_RESTAURANT_ID` (`restaurant_id`),
  CONSTRAINT `FK_HAPPY_HOUR_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `happy_hour`
--

LOCK TABLES `happy_hour` WRITE;
/*!40000 ALTER TABLE `happy_hour` DISABLE KEYS */;
/*!40000 ALTER TABLE `happy_hour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `happy_hour_times`
--

DROP TABLE IF EXISTS `happy_hour_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `happy_hour_times` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `happy_hour_id` bigint(20) NOT NULL,
  `time_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_HAPPY_HOUR_TIME` (`happy_hour_id`,`time_id`),
  KEY `FK_HAPPY_HOUR_TIME_HAPPY_HOUR_ID` (`happy_hour_id`),
  KEY `FK_HAPPY_HOUR_TIME_TIME_ID` (`time_id`),
  CONSTRAINT `FK_HAPPY_HOUR_TIME_HAPPY_HOUR_ID` FOREIGN KEY (`happy_hour_id`) REFERENCES `happy_hour` (`id`),
  CONSTRAINT `FK_HAPPY_HOUR_TIME_TIME_ID` FOREIGN KEY (`time_id`) REFERENCES `time` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `happy_hour_times`
--

LOCK TABLES `happy_hour_times` WRITE;
/*!40000 ALTER TABLE `happy_hour_times` DISABLE KEYS */;
/*!40000 ALTER TABLE `happy_hour_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marker`
--

DROP TABLE IF EXISTS `marker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marker` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(3) DEFAULT NULL,
  `marker_type_id` bigint(20) DEFAULT NULL,
  `restaurant_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_MARKER_RESTAURANT_ID` (`restaurant_id`),
  KEY `MARKER_MARKER_TYPE_ID_FK` (`marker_type_id`),
  CONSTRAINT `MARKER_MARKER_TYPE_ID_FK` FOREIGN KEY (`marker_type_id`) REFERENCES `marker_type` (`id`),
  CONSTRAINT `MARKER_RESTAURANT_ID_FK` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marker`
--

LOCK TABLES `marker` WRITE;
/*!40000 ALTER TABLE `marker` DISABLE KEYS */;
/*!40000 ALTER TABLE `marker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marker_type`
--

DROP TABLE IF EXISTS `marker_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marker_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `icon_id` bigint(20) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_MARKER_TYPE_NAME` (`name`),
  UNIQUE KEY `UK_MARKER_TYPE_ICON_ID` (`icon_id`),
  CONSTRAINT `MARKER_TYPE_MEDIA_FILE_ID_FK` FOREIGN KEY (`icon_id`) REFERENCES `media_file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marker_type`
--

LOCK TABLES `marker_type` WRITE;
/*!40000 ALTER TABLE `marker_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `marker_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_file`
--

DROP TABLE IF EXISTS `media_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `extension` varchar(10) NOT NULL,
  `media_type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_file`
--

LOCK TABLES `media_file` WRITE;
/*!40000 ALTER TABLE `media_file` DISABLE KEYS */;
INSERT INTO `media_file` VALUES (8,'png','image/png','2f83ebfa-77ef-4472-a003-bb40468fb6fe','/files/content/2f83ebfa-77ef-4472-a003-bb40468fb6fe.png'),(9,'png','image/png','755883e6-bc3c-493e-b461-9a1ab70d620c','/files/content/755883e6-bc3c-493e-b461-9a1ab70d620c.png'),(10,'png','image/png','4ac43c63-2eb9-4186-8a11-cf46c4219eed','/files/content/4ac43c63-2eb9-4186-8a11-cf46c4219eed.png'),(11,'png','image/png','dc50c924-e610-4d2d-9a1e-3943d2801512','/files/content/dc50c924-e610-4d2d-9a1e-3943d2801512.png'),(12,'png','image/png','10568817-8aec-4630-acc6-aa6dee2d7822','/files/content/10568817-8aec-4630-acc6-aa6dee2d7822.png'),(13,'png','image/png','63979dde-e26d-4630-b289-4bc57e26fd47','/files/content/63979dde-e26d-4630-b289-4bc57e26fd47.png'),(14,'jpg','image/jpeg','053a23ed-1a99-400e-b180-e2927b700de2','/files/content/053a23ed-1a99-400e-b180-e2927b700de2.jpg'),(15,'png','image/png','04cfb7f8-7e58-4f1d-ab45-fd46630b2d45','/files/content/04cfb7f8-7e58-4f1d-ab45-fd46630b2d45.png'),(16,'png','image/png','2cb311ee-9539-45f6-8b1f-51384c360efc','/files/content/2cb311ee-9539-45f6-8b1f-51384c360efc.png'),(17,'png','image/png','68daface-0ee4-49d2-8cb2-1fadb980ba83','/files/content/68daface-0ee4-49d2-8cb2-1fadb980ba83.png'),(18,'png','image/png','87cec316-8a5a-489b-a239-5073ac490315','/files/content/87cec316-8a5a-489b-a239-5073ac490315.png'),(19,'png','image/png','270ab65c-dcc0-4322-ad38-4f3934e24636','/files/content/270ab65c-dcc0-4322-ad38-4f3934e24636.png'),(20,'png','image/png','60bd7476-219c-4277-b2eb-4ea20136dfa7','/files/content/60bd7476-219c-4277-b2eb-4ea20136dfa7.png'),(21,'png','image/png','329226b9-f955-4b3c-b2d8-fcb0e258a8c8','/files/content/329226b9-f955-4b3c-b2d8-fcb0e258a8c8.png'),(22,'png','image/png','3c19c6ad-e3d7-4480-b16e-76315c7c263d','/files/content/3c19c6ad-e3d7-4480-b16e-76315c7c263d.png'),(23,'png','image/png','9e27f43c-3952-46e0-81e3-62470aaad8fe','/files/content/9e27f43c-3952-46e0-81e3-62470aaad8fe.png'),(24,'png','image/png','a5772091-ac16-4815-8f1b-f0e5f231f763','/files/content/a5772091-ac16-4815-8f1b-f0e5f231f763.png'),(25,'png','image/png','a1c088dc-3414-4a5e-af1d-666a8045049d','/files/content/a1c088dc-3414-4a5e-af1d-666a8045049d.png'),(26,'png','image/png','8da11ab3-d2d2-4a45-92ea-e811a1bd2168','/files/content/8da11ab3-d2d2-4a45-92ea-e811a1bd2168.png'),(27,'png','image/png','79e4c042-9289-4b06-a67c-b14d1450b814','/files/content/79e4c042-9289-4b06-a67c-b14d1450b814.png'),(28,'png','image/png','8f6c96e3-f027-4b2a-bc10-b8ba0a52f3fa','/files/content/8f6c96e3-f027-4b2a-bc10-b8ba0a52f3fa.png'),(29,'png','image/png','340eee59-517f-4916-b8d7-57491943f492','/files/content/340eee59-517f-4916-b8d7-57491943f492.png'),(30,'png','image/png','23f80e5c-2cd1-4891-841c-a3e99b4e1656','/files/content/23f80e5c-2cd1-4891-841c-a3e99b4e1656.png'),(31,'png','image/png','84ca1c52-8ee1-4c38-a496-5f26de69eed1','/files/content/84ca1c52-8ee1-4c38-a496-5f26de69eed1.png'),(32,'png','image/png','687d0884-13ef-44d9-bb0b-80e8b2b0af00','/files/content/687d0884-13ef-44d9-bb0b-80e8b2b0af00.png'),(33,'png','image/png','5fa764c4-67ee-4776-bfd7-93f7af6fc443','/files/content/5fa764c4-67ee-4776-bfd7-93f7af6fc443.png'),(34,'png','image/png','def3e450-06b9-49c1-8b1d-005fc3e2e67f','/files/content/def3e450-06b9-49c1-8b1d-005fc3e2e67f.png'),(35,'png','image/png','4a9d33ac-cf00-48b1-852d-84938bb57d42','/files/content/4a9d33ac-cf00-48b1-852d-84938bb57d42.png'),(36,'png','image/png','ecf7c3b5-86d7-41d3-8b49-97b268ff5959','/files/content/ecf7c3b5-86d7-41d3-8b49-97b268ff5959.png'),(37,'png','image/png','c7dfb520-3cb6-4e8b-a63f-4255029f2be9','/files/content/c7dfb520-3cb6-4e8b-a63f-4255029f2be9.png'),(38,'png','image/png','21d49d81-95ec-44da-ab5d-671b3d1daa4e','/files/content/21d49d81-95ec-44da-ab5d-671b3d1daa4e.png'),(39,'png','image/png','25ac2c76-6e39-4bd1-9a9f-5ec35c403206','/files/content/25ac2c76-6e39-4bd1-9a9f-5ec35c403206.png'),(40,'png','image/png','b4a5ff46-d1cc-47a0-93f4-ee48c7dcbd38','/files/content/b4a5ff46-d1cc-47a0-93f4-ee48c7dcbd38.png'),(41,'png','image/png','25b8f2fd-a73b-4082-991c-cdc7ff9813d7','/files/content/25b8f2fd-a73b-4082-991c-cdc7ff9813d7.png'),(42,'png','image/png','c6024d97-6d0a-429d-8f85-49946e5b2c0a','/files/content/c6024d97-6d0a-429d-8f85-49946e5b2c0a.png'),(43,'png','image/png','a66918ea-52b6-4ab6-b116-f50e7e9fe3b8','/files/content/a66918ea-52b6-4ab6-b116-f50e7e9fe3b8.png'),(44,'png','image/png','cbe430ee-a98b-47a8-9cd7-74e4c3ba2c76','/files/content/cbe430ee-a98b-47a8-9cd7-74e4c3ba2c76.png'),(45,'png','image/png','daefea08-3c9c-41cf-901b-9dedc13c5d70','/files/content/daefea08-3c9c-41cf-901b-9dedc13c5d70.png'),(46,'png','image/png','ef48e4e0-e9e1-4e12-bd27-2e962761afca','/files/content/ef48e4e0-e9e1-4e12-bd27-2e962761afca.png'),(47,'png','image/png','ffdd59ac-71c7-423e-a626-bae3268e5fce','/files/content/ffdd59ac-71c7-423e-a626-bae3268e5fce.png'),(48,'png','image/png','134aab57-89be-47a8-8c9e-08f153c3573b','/files/content/134aab57-89be-47a8-8c9e-08f153c3573b.png'),(49,'png','image/png','dda9992f-4849-4f50-8d06-8dffece32b47','/files/content/dda9992f-4849-4f50-8d06-8dffece32b47.png'),(50,'jpg','image/jpeg','629b9332-5768-43de-abcf-226031c200d0','/files/content/629b9332-5768-43de-abcf-226031c200d0.jpg'),(51,'png','image/png','f6a29045-524b-4459-a640-1203a52e4c01','/files/content/f6a29045-524b-4459-a640-1203a52e4c01.png'),(52,'png','image/png','2f988885-c3d5-41d0-a55c-a900b6a1b9a0','/files/content/2f988885-c3d5-41d0-a55c-a900b6a1b9a0.png'),(53,'png','image/png','31c42cb6-518c-40c4-b4d5-b531b5fb2152','/files/content/31c42cb6-518c-40c4-b4d5-b531b5fb2152.png'),(54,'png','image/png','62c83d02-0f33-4678-9349-1fdc4c2dc4ca','/files/content/62c83d02-0f33-4678-9349-1fdc4c2dc4ca.png'),(55,'png','image/png','5291e84b-c4b1-4e00-81eb-3b28323588e9','/files/content/5291e84b-c4b1-4e00-81eb-3b28323588e9.png'),(56,'png','image/png','44bc45fc-d8f1-4d5d-9446-e5a61d6c90a9','/files/content/44bc45fc-d8f1-4d5d-9446-e5a61d6c90a9.png');
/*!40000 ALTER TABLE `media_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobile_device`
--

DROP TABLE IF EXISTS `mobile_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_device` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `application_type` varchar(20) NOT NULL DEFAULT 'USER_APP',
  PRIMARY KEY (`id`),
  KEY `FK_ARTICLE_USER_ID` (`user_id`),
  CONSTRAINT `FK_MOBILE_DEVICE_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobile_device`
--

LOCK TABLES `mobile_device` WRITE;
/*!40000 ALTER TABLE `mobile_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `mobile_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_token`
--

DROP TABLE IF EXISTS `oauth_access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(255) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_client_details`
--

DROP TABLE IF EXISTS `oauth_client_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(255) NOT NULL,
  `resource_ids` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `authorized_grant_types` varchar(255) DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) DEFAULT NULL,
  `authorities` varchar(255) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_client_details`
--

LOCK TABLES `oauth_client_details` WRITE;
/*!40000 ALTER TABLE `oauth_client_details` DISABLE KEYS */;
INSERT INTO `oauth_client_details` VALUES ('clientadmin','','$2a$10$eX.SrnuTYhHmovZThwoAcu2UZhgaGuS4vSnKac789dc8AkKz3PMyW','actuator','refresh_token,client_credentials','','',604800,31536000,'{}',''),('clientapp','','$2a$10$yLTwUS9V1Li/QTy0/a1kduiJK8i5YvYVPyUe9AmJ7OUqZfhnwugxe','read,write','refresh_token,password','','',604800,31536000,'{}','');
/*!40000 ALTER TABLE `oauth_client_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_token`
--

DROP TABLE IF EXISTS `oauth_refresh_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` blob,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `open_hour`
--

DROP TABLE IF EXISTS `open_hour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `open_hour` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `restaurant_id` bigint(20) NOT NULL,
  `weekday` tinyint(4) NOT NULL,
  `open_time` time NOT NULL,
  `close_time` time NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `OPEN_HOUR_UK` (`restaurant_id`,`weekday`),
  CONSTRAINT `OPEN_HOUR_RESTAURANT_ID_FK` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `open_hour`
--

LOCK TABLES `open_hour` WRITE;
/*!40000 ALTER TABLE `open_hour` DISABLE KEYS */;
/*!40000 ALTER TABLE `open_hour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotion`
--

DROP TABLE IF EXISTS `promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `description` varchar(90) NOT NULL,
  `creation_date` datetime NOT NULL,
  `total` int(11) NOT NULL,
  `used` int(11) NOT NULL,
  `restaurant_id` bigint(20) NOT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PROMOTION_RESTAURANT_ID` (`restaurant_id`),
  CONSTRAINT `FK_PROMOTION_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotion`
--

LOCK TABLES `promotion` WRITE;
/*!40000 ALTER TABLE `promotion` DISABLE KEYS */;
/*!40000 ALTER TABLE `promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(2000) DEFAULT NULL,
  `created` datetime NOT NULL,
  `guest_number` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `restaurant_id` bigint(20) NOT NULL,
  `discount_id` bigint(20) DEFAULT NULL,
  `status_changed` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_RESERVATION_USER_ID` (`user_id`),
  KEY `FK_RESERVATION_RESTAURANT_ID` (`restaurant_id`),
  KEY `FK_RESERVATION_DISCOUNT_ID` (`discount_id`),
  CONSTRAINT `FK_RESERVATION_DISCOUNT_ID` FOREIGN KEY (`discount_id`) REFERENCES `discount` (`id`),
  CONSTRAINT `FK_RESERVATION_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_RESERVATION_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `address` varchar(255) NOT NULL,
  `breakfast` varchar(255) DEFAULT NULL,
  `close_time` time NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `latitude` decimal(18,15) NOT NULL,
  `longitude` decimal(18,15) NOT NULL,
  `name` varchar(255) NOT NULL,
  `open_time` time NOT NULL,
  `parking` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `average_ticket` double DEFAULT NULL,
  `phones` varchar(255) DEFAULT NULL,
  `menu_id` bigint(20) DEFAULT NULL,
  `rating` double DEFAULT NULL,
  `top_picture_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_RESTAURANT_MENU` (`menu_id`),
  UNIQUE KEY `UK_RESTAURANT_TOP_PICTURE` (`top_picture_id`),
  KEY `FK_RESTAURANT_MENU_MEDIA_FILE_ID` (`menu_id`),
  KEY `FK_RESTAURANT_PICTURE_MEDIA_FILE_ID` (`top_picture_id`),
  CONSTRAINT `FK_RESTAURANT_MENU_MEDIA_FILE_ID` FOREIGN KEY (`menu_id`) REFERENCES `media_file` (`id`),
  CONSTRAINT `FK_RESTAURANT_PICTURE_MEDIA_FILE_ID` FOREIGN KEY (`top_picture_id`) REFERENCES `media_file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant`
--

LOCK TABLES `restaurant` WRITE;
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_analogous`
--

DROP TABLE IF EXISTS `restaurant_analogous`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_analogous` (
  `restaurant_id` bigint(20) NOT NULL,
  `analogous_restaurant_id` bigint(20) NOT NULL,
  PRIMARY KEY (`restaurant_id`,`analogous_restaurant_id`),
  KEY `FK_RESTAURANT_ANALOGOUS_ANALOGOUS_RESTAURANT_ID` (`analogous_restaurant_id`),
  KEY `FK_RESTAURANT_ANALOGOUS_RESTAURANT_ID` (`restaurant_id`),
  CONSTRAINT `FK_RESTAURANT_ANALOGOUS_ANALOGOUS_RESTAURANT_ID` FOREIGN KEY (`analogous_restaurant_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_RESTAURANT_ANALOGOUS_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_analogous`
--

LOCK TABLES `restaurant_analogous` WRITE;
/*!40000 ALTER TABLE `restaurant_analogous` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_analogous` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_cuisines`
--

DROP TABLE IF EXISTS `restaurant_cuisines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_cuisines` (
  `restaurant_id` bigint(20) NOT NULL,
  `cuisine_id` bigint(20) NOT NULL,
  PRIMARY KEY (`restaurant_id`,`cuisine_id`),
  KEY `FK_RESTAURANT_CUISINE_ID` (`cuisine_id`),
  KEY `FK_RESTAURANT_CUISINE_RESTAURANT_ID` (`restaurant_id`),
  CONSTRAINT `FK_RESTAURANT_CUISINE_ID` FOREIGN KEY (`cuisine_id`) REFERENCES `cuisine` (`id`),
  CONSTRAINT `FK_RESTAURANT_CUISINE_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_cuisines`
--

LOCK TABLES `restaurant_cuisines` WRITE;
/*!40000 ALTER TABLE `restaurant_cuisines` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_cuisines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_in_selection`
--

DROP TABLE IF EXISTS `restaurant_in_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_in_selection` (
  `selection_id` bigint(20) NOT NULL,
  `restaurant_id` bigint(20) NOT NULL,
  PRIMARY KEY (`selection_id`,`restaurant_id`),
  KEY `FK_RESTAURANT_SELECTION_RESTAURANT_ID` (`restaurant_id`),
  KEY `FK_RESTAURANT_SELECTION_SELECTION_ID` (`selection_id`),
  CONSTRAINT `FK_RESTAURANT_SELECTION_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_RESTAURANT_SELECTION_SELECTION_ID` FOREIGN KEY (`selection_id`) REFERENCES `restaurant_selection` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_in_selection`
--

LOCK TABLES `restaurant_in_selection` WRITE;
/*!40000 ALTER TABLE `restaurant_in_selection` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_in_selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_pictures`
--

DROP TABLE IF EXISTS `restaurant_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_pictures` (
  `restaurant_id` bigint(20) NOT NULL,
  `media_file_id` bigint(20) NOT NULL,
  PRIMARY KEY (`restaurant_id`,`media_file_id`),
  UNIQUE KEY `UK_RESTAURANT_PICTURES_MEDIA_FILE_ID` (`media_file_id`),
  KEY `FK_RESTAURANT_PICTURES_MEDIA_FILE_ID` (`media_file_id`),
  KEY `FK_RESTAURANT_PICTURES_RESTAURANT_ID` (`restaurant_id`),
  CONSTRAINT `FK_RESTAURANT_PICTURES_MEDIA_FILE_ID` FOREIGN KEY (`media_file_id`) REFERENCES `media_file` (`id`),
  CONSTRAINT `FK_RESTAURANT_PICTURES_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_pictures`
--

LOCK TABLES `restaurant_pictures` WRITE;
/*!40000 ALTER TABLE `restaurant_pictures` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_pictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_selection`
--

DROP TABLE IF EXISTS `restaurant_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_selection` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `title` varchar(255) NOT NULL,
  `total` int(11) NOT NULL,
  `picture_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_RESTAURANT_SELECTION_PICTURE` (`picture_id`),
  KEY `FK_RESTAURANT_SELECTION_MEDIA_FILE_ID` (`picture_id`),
  CONSTRAINT `FK_RESTAURANT_SELECTION_MEDIA_FILE_ID` FOREIGN KEY (`picture_id`) REFERENCES `media_file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_selection`
--

LOCK TABLES `restaurant_selection` WRITE;
/*!40000 ALTER TABLE `restaurant_selection` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_tags`
--

DROP TABLE IF EXISTS `restaurant_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_tags` (
  `restaurant_id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`restaurant_id`,`tag_id`),
  KEY `FK_RESTAURANT_TAG_RESTAURANT_ID` (`restaurant_id`),
  KEY `FK_RESTAURANT_TAG_TAG_ID` (`tag_id`),
  CONSTRAINT `FK_RESTAURANT_TAG_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_RESTAURANT_TAG_TAG_ID` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_tags`
--

LOCK TABLES `restaurant_tags` WRITE;
/*!40000 ALTER TABLE `restaurant_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurants_owners`
--

DROP TABLE IF EXISTS `restaurants_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurants_owners` (
  `restaurant_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`restaurant_id`,`user_id`),
  KEY `FK_RESTAURANT_USER_RESTAURANT_ID` (`restaurant_id`),
  KEY `FK_RESTAURANT_USER_USER_ID` (`user_id`),
  CONSTRAINT `FK_RESTAURANT_USER_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_RESTAURANT_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurants_owners`
--

LOCK TABLES `restaurants_owners` WRITE;
/*!40000 ALTER TABLE `restaurants_owners` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurants_owners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `restaurant_id` bigint(20) NOT NULL,
  `reservation_id` bigint(20) NOT NULL,
  `rating` int(1) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_REVIEW_RESERVATION` (`reservation_id`),
  KEY `FK_REVIEW_USER_ID` (`user_id`),
  KEY `FK_REVIEW_RESTAURANT_ID` (`restaurant_id`),
  KEY `FK_REVIEW_RESERVATION_ID` (`reservation_id`),
  CONSTRAINT `FK_REVIEW_RESERVATION_ID` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`),
  CONSTRAINT `FK_REVIEW_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_REVIEW_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_notification`
--

DROP TABLE IF EXISTS `review_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activation_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `reservation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `restaurant_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_REVIEW_NOTIFICATION_RESERVATION_ID` (`reservation_id`),
  KEY `FK_REVIEW_NOTIFICATION_USER_ID` (`user_id`),
  KEY `FK_REVIEW_NOTIFICATION_RESTAURANT_ID` (`restaurant_id`),
  CONSTRAINT `FK_REVIEW_NOTIFICATION_RESERVATION_ID` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`),
  CONSTRAINT `FK_REVIEW_NOTIFICATION_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_REVIEW_NOTIFICATION_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_notification`
--

LOCK TABLES `review_notification` WRITE;
/*!40000 ALTER TABLE `review_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `review_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `screen`
--

DROP TABLE IF EXISTS `screen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `screen` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `creation_date` date NOT NULL,
  `screen_number` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `screen`
--

LOCK TABLES `screen` WRITE;
/*!40000 ALTER TABLE `screen` DISABLE KEYS */;
/*!40000 ALTER TABLE `screen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `icon_id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_TAG_ICON` (`icon_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `FK_TAG_MEDIA_FILE_ID` (`icon_id`),
  CONSTRAINT `FK_TAG_MEDIA_FILE_ID` FOREIGN KEY (`icon_id`) REFERENCES `media_file` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (8,_binary '',8,'WI-FI'),(9,_binary '',9,'Кальян'),(10,_binary '',10,'Живая музыка'),(11,_binary '',11,'Аниматоры'),(12,_binary '',12,'Парковка'),(13,_binary '',13,'Завтраки'),(14,_binary '',14,'Фейс-контроль'),(15,_binary '',15,'Караоке'),(16,_binary '',16,'Терраса'),(17,_binary '',17,'На свежем воздухе'),(18,_binary '',18,'Шоу-программа'),(19,_binary '',19,'DJ'),(20,_binary '',20,'Коктейли'),(21,_binary '',21,'Винная карта'),(22,_binary '',22,'Спортивные трансляции'),(23,_binary '',23,'Настольные игры'),(24,_binary '',24,'Меню для детей'),(25,_binary '',25,'Бизнес-ланчи'),(26,_binary '',26,'Тихое место'),(27,_binary '',27,'Для шумных компаний'),(28,_binary '',28,'Не курят'),(29,_binary '',29,'Крафтовое пиво'),(30,_binary '',30,'Книги'),(31,_binary '',31,'Халяль'),(32,_binary '',32,'Кошерная'),(33,_binary '',33,'Детская комната'),(34,_binary '',34,'Безглютеновые блюда'),(35,_binary '',35,'Барная атмосфера'),(36,_binary '',36,'Деловые встречи'),(37,_binary '',37,'Для особых случаев'),(38,_binary '',38,'Романтический'),(39,_binary '',39,'Деликатесы'),(40,_binary '',40,'Вечеринки'),(41,_binary '',41,'Банкет');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time`
--

DROP TABLE IF EXISTS `time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `time` time NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_TAG_ICON` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time`
--

LOCK TABLES `time` WRITE;
/*!40000 ALTER TABLE `time` DISABLE KEYS */;
INSERT INTO `time` VALUES (1,'00:00:00'),(2,'00:30:00'),(3,'01:00:00'),(4,'01:30:00'),(5,'02:00:00'),(6,'02:30:00'),(7,'03:00:00'),(8,'03:30:00'),(9,'04:00:00'),(10,'04:30:00'),(11,'05:00:00'),(12,'05:30:00'),(13,'06:00:00'),(14,'06:30:00'),(15,'07:00:00'),(16,'07:30:00'),(17,'08:00:00'),(18,'08:30:00'),(19,'09:00:00'),(20,'09:30:00'),(21,'10:00:00'),(22,'10:30:00'),(23,'11:00:00'),(24,'11:30:00'),(25,'12:00:00'),(26,'12:30:00'),(27,'13:00:00'),(28,'13:30:00'),(29,'14:00:00'),(30,'14:30:00'),(31,'15:00:00'),(32,'15:30:00'),(33,'16:00:00'),(34,'16:30:00'),(35,'17:00:00'),(36,'17:30:00'),(37,'18:00:00'),(38,'18:30:00'),(39,'19:00:00'),(40,'19:30:00'),(41,'20:00:00'),(42,'20:30:00'),(43,'21:00:00'),(44,'21:30:00'),(45,'22:00:00'),(46,'22:30:00'),(47,'23:00:00'),(48,'23:30:00');
/*!40000 ALTER TABLE `time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) DEFAULT NULL,
  `name` varchar(30) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `user_credential_id` bigint(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `photo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_USER_PHONE` (`phone`),
  UNIQUE KEY `UK_USER_EMAIL` (`email`),
  UNIQUE KEY `UK_USER_PHOTO` (`photo_id`),
  KEY `FK_USER_CREDENTIAL_ID` (`user_credential_id`),
  KEY `FK_USER_MEDIA_FILE_ID` (`photo_id`),
  CONSTRAINT `FK_USER_CREDENTIAL_ID` FOREIGN KEY (`user_credential_id`) REFERENCES `user_credential` (`id`),
  CONSTRAINT `FK_USER_MEDIA_FILE_ID` FOREIGN KEY (`photo_id`) REFERENCES `media_file` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@gmail.com','Admin','380000000000','admin',1,'ENABLE',NULL),(2,'chefchelios82@gmail.com','Aryan','380955504609','Khanani',2,'ENABLE',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_credential`
--

DROP TABLE IF EXISTS `user_credential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_credential` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `registration_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_USER_CREDENTIAL_LOGIN` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_credential`
--

LOCK TABLES `user_credential` WRITE;
/*!40000 ALTER TABLE `user_credential` DISABLE KEYS */;
INSERT INTO `user_credential` VALUES (1,'380000000000','$2a$10$w9tgflWcL9Ce1OMc5exO2.5WvMZomKWwYKltgol5Qnt92RNRCvYf6','2018-01-01 00:00:00'),(2,'380955504609','$2a$10$FAV.uRKxNizJi1rbsLW3Lej0DfsyGH2VGK5Ll2SOrjiKM.AM.fmlC','2019-06-28 12:14:05');
/*!40000 ALTER TABLE `user_credential` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_USER_ROLE_ROLE` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'ROLE_NOT_SET'),(3,'ROLE_RESTAURANT_ADMIN'),(2,'ROLE_SYSTEM_ADMIN'),(4,'ROLE_USER');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK_USER_ROLE_ROLE_ID` (`role_id`),
  KEY `FK_USER_ROLE_USER_ID` (`user_id`),
  CONSTRAINT `FK_USER_ROLE_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`),
  CONSTRAINT `FK_USER_ROLE_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,2),(2,2),(1,3),(1,4),(2,4);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_saved_restaurants`
--

DROP TABLE IF EXISTS `user_saved_restaurants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_saved_restaurants` (
  `user_id` bigint(20) NOT NULL,
  `restaurant_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`restaurant_id`),
  KEY `FK_USER_RESTAURANT_RESTAURANT_ID` (`restaurant_id`),
  KEY `FK_USER_RESTAURANT_USER_ID` (`user_id`),
  CONSTRAINT `FK_USER_RESTAURANT_RESTAURANT_ID` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_USER_RESTAURANT_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_saved_restaurants`
--

LOCK TABLES `user_saved_restaurants` WRITE;
/*!40000 ALTER TABLE `user_saved_restaurants` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_saved_restaurants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_system_information`
--

DROP TABLE IF EXISTS `user_system_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_system_information` (
  `phone` varchar(255) NOT NULL,
  `activation_attempts` int(11) NOT NULL,
  `activation_code` varchar(20) NOT NULL,
  `activation_code_creation_time` datetime NOT NULL,
  `activation_time` datetime DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `login_attempts` int(11) NOT NULL,
  `password_reset_attempts` int(11) NOT NULL,
  `password_reset_creation_time` datetime DEFAULT NULL,
  PRIMARY KEY (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_system_information`
--

LOCK TABLES `user_system_information` WRITE;
/*!40000 ALTER TABLE `user_system_information` DISABLE KEYS */;
INSERT INTO `user_system_information` VALUES ('380000000000',0,'0000','2018-01-01 00:00:00','2018-01-01 00:00:00','2019-06-29 21:11:12',0,0,NULL),('380955504609',0,'5131','2019-06-28 12:14:05','2019-06-28 12:14:37','2019-06-28 12:14:48',0,0,NULL);
/*!40000 ALTER TABLE `user_system_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-30  7:51:02
