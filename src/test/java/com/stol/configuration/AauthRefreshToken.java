package com.stol.configuration;

import javax.persistence.*;

@Entity
@Table(name = "oauth_refresh_token")
public class AauthRefreshToken {
    @Id
    @Column(name = "token_id")
    private String token_id;

    @Lob
    @Column(name = "token")
    private byte[] token;

    @Lob
    @Column(name = "authentication")
    private byte[] authentication;
}
