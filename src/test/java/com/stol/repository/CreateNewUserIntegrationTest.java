package com.stol.repository;

import com.stol.model.user.Credential;
import com.stol.model.user.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class CreateNewUserIntegrationTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void createNewUser() {
        User user = newUser();
        User u = userRepository.save(user);
        Assert.assertNotNull(u.getId());
    }

    @Test(expected = ConstraintViolationException.class)
    public void newUserShortName() {
        User user = newUser();
        user.setName("n");
        userRepository.save(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void newUserLongName() {
        User user = newUser();
        user.setName("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
        userRepository.save(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void newUserNotLettersInName() {
        User user = newUser();
        user.setName("123vdfg");
        userRepository.save(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void newUserShortSurname() {
        User user = newUser();
        user.setSurname("n");
        userRepository.save(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void newUserLongSurname() {
        User user = newUser();
        user.setSurname("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
        userRepository.save(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void newUserNotLettersInSurname() {
        User user = newUser();
        user.setSurname("123vdfg");
        userRepository.save(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void newUserEmailValidation() {
        User user = newUser();
        user.setEmail("123vdfg");
        userRepository.save(user);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void newUserUniqueEmail() {
        User user1 = newUser();
        userRepository.save(user1);
        User user2 = newUser();
        user2.setPhoneNumber("380000000001");
        userRepository.save(user2);
    }

    @Test(expected = ConstraintViolationException.class)
    public void newUserShortPassword() {
        User user = newUser();
        user.getCredential().setPassword("12345");
        userRepository.save(user);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void newUserUniquePhone() {
        User user1 = newUser();
        userRepository.save(user1);
        User user2 = newUser();
        user2.setEmail("1email@email.com");
        userRepository.save(user2);
    }

    private User newUser() {
        Credential c = new Credential();
        c.setLogin("380000000005");
        c.setPassword("password");

        User u = new User();
        u.setName("имяName");
        u.setSurname("фамилияSurname");
        u.setEmail("email@email.com");
        u.setCredential(c);
        u.setPhoneNumber("380000000005");
        return u;
    }
}
