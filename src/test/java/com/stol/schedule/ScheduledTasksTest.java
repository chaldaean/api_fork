package com.stol.schedule;

import com.github.database.rider.core.DBUnitRule;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import com.stol.model.Restaurant;
import com.stol.model.ReviewNotification;
import com.stol.model.user.User;
import com.stol.repository.ReviewNotificationRepository;
import com.stol.service.notification.NotificationService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.sql.DataSource;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@DBRider
public class ScheduledTasksTest {

    @Autowired
    ReviewNotificationRepository reviewNotificationRepository;

    @Autowired
    ScheduledTasks scheduledTasks;

    @MockBean
    NotificationService notificationService;

    @Autowired
    protected WebApplicationContext context;

    @Rule
    public DBUnitRule dbUnitRule = DBUnitRule.instance("system", () -> context.getBean(DataSource.class).getConnection());

    @Test
    public void deleteDetachedPictures() {
    }

    @Test
    @DataSet(value = "com/stol/schedule/scheduled_tasks_test/review_notification_push.xml", executorId = "system", disableConstraints = true)
    public void reviewNotificationPush() {
        scheduledTasks.reviewNotificationPush();

        verify(notificationService, only()).notifyUserAboutReviewNeeded(any(User.class), any(Restaurant.class));
        assertTrue(reviewNotificationRepository.findById(3L).map(ReviewNotification::getSent).orElse(false));
    }

    @Test
    public void cancelAllNotConfirmedReservations() {
    }

    @Test
    public void deleteOldReviewNotification() {
    }
}