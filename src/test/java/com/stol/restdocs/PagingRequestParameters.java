package com.stol.restdocs;


import org.springframework.restdocs.request.ParameterDescriptor;

import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;

public class PagingRequestParameters {
    public static ParameterDescriptor sort = parameterWithName("sort").optional().description("<field name>,asc|desc");
    public static ParameterDescriptor page = parameterWithName("page").optional().description("Page number (0-based)");
    public static ParameterDescriptor size = parameterWithName("size").optional().description("Page size");
}
