package com.stol.restdocs;

import org.springframework.restdocs.payload.FieldDescriptor;

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;

public class PagingResponseFields {
    public static FieldDescriptor totalElements = fieldWithPath("page.totalElements").description("Total number of elements");
    public static FieldDescriptor size = fieldWithPath("page.size").description("Page size");
    public static FieldDescriptor number = fieldWithPath("page.number").description("Page number");
    public static FieldDescriptor totalPages = fieldWithPath("page.totalPages").description("Total number of pages");
}
