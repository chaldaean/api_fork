package com.stol.service.sms;

import com.stol.configuration.beans.SmsSenderConfig;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SmsSenderConfig.class)
@ActiveProfiles("test")
@TestPropertySource("classpath:application.properties")
public class SmsServiceTest {
    @Autowired
    private SmsSenderService smsSenderService;

/*	@Test
	public void send() throws Exception {
		smsSenderService.send("+380970000000", "Test from The stol: 0987");
	}*/

    @Test
    public void creditBalance() throws Exception {
        String balance = smsSenderService.creditBalance();

        Assert.assertNotNull(balance);
        Assert.assertTrue(NumberUtils.isCreatable(balance));
    }
}