package com.stol.system.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

import static com.stol.system.SecurityAbstractSystemTest.ACCESS_DENIED;
import static com.stol.system.SecurityAbstractSystemTest.ACCESS_IS_DENIED;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserOwnerSecuritySystemTest extends AbstractSystemTest {

    @Test
    public void updateNotOwnerNotSystemAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/users/100").content(updateUserName())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    public void updateIsOwnerNotSystemAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/users/2").content(updateUserName())
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void updateNotOwnerIsSystemAdmin() throws Exception {
        mockMvc.perform(patch("/api/users/2").content(updateUserName())
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    private static String updateUserName() throws JsonProcessingException {
        Map<String, Object> user = new HashMap<>();
        user.put("name", "NewName");
        return new ObjectMapper().writeValueAsString(user);
    }
}
