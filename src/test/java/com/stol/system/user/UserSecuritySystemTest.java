package com.stol.system.user;

import com.stol.system.SecurityAbstractSystemTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserSecuritySystemTest extends SecurityAbstractSystemTest {
    @Test
    public void findUserNoSystemAdminRoleNotOwner() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "382000000000", PASSWORD));
        mockMvc.perform(get("/api/users/1")
                .header("Authorization", token))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void findAllUserNoSystemAdminRole() throws Exception {
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }
}
