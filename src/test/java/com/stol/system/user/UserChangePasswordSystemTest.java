package com.stol.system.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.model.user.User;
import com.stol.repository.UserRepository;
import com.stol.system.AbstractSystemTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static org.hamcrest.Matchers.isEmptyString;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserChangePasswordSystemTest extends AbstractSystemTest {
    private static final String NEW_PASSWORD = "admin";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void successfullyChangeUserPassword() throws Exception {
        String userPassword = userPasswordObject(PASSWORD, NEW_PASSWORD);
        mockMvc.perform(post("/api/user/password/change").content(userPassword)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()));

        User user = userRepository.findUserByCredentialLogin(LOGIN);
        Assert.assertTrue(passwordEncoder.matches(NEW_PASSWORD, user.getCredential().getPassword()));
    }

    @Test
    public void incorrectOldPassword() throws Exception {
        String userPassword = userPasswordObject("incorrect_password", NEW_PASSWORD);
        mockMvc.perform(post("/api/user/password/change").content(userPassword)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Ваш старый пароль неверен!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void incorrectOldPasswordLocalizedUk() throws Exception {
        String userPassword = userPasswordObject("incorrect_password", NEW_PASSWORD);
        mockMvc.perform(post("/api/user/password/change").content(userPassword)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Ваш старий пароль невірний!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void incorrectOldPasswordLocalizedEn() throws Exception {
        String userPassword = userPasswordObject("incorrect_password", NEW_PASSWORD);
        mockMvc.perform(post("/api/user/password/change").content(userPassword)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Your old password is incorrect!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void emptyNewPassword() throws Exception {
        String userPassword = userPasswordObject(PASSWORD, "");
        mockMvc.perform(post("/api/user/password/change").content(userPassword)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Ваш новый пароль пустой!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void emptyNewPassword_uk() throws Exception {
        String userPassword = userPasswordObject(PASSWORD, "");
        mockMvc.perform(post("/api/user/password/change").content(userPassword)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Ваш новий пароль пустий!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void emptyNewPassword_en() throws Exception {
        String userPassword = userPasswordObject(PASSWORD, "");
        mockMvc.perform(post("/api/user/password/change").content(userPassword)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Your new password is empty!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void userIsNotLoggedIn() throws Exception {
        String userPassword = userPasswordObject(PASSWORD, NEW_PASSWORD);
        mockMvc.perform(post("/api/user/password/change").content(userPassword)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value("unauthorized"))
                .andExpect(jsonPath("$.error_description").value("Full authentication is required to access this resource"));
    }

    private static String userPasswordObject(String oldPassword, String newPassword) throws JsonProcessingException {
        Map<String, String> userPassword = new HashMap<>();
        userPassword.put("oldPassword", oldPassword);
        userPassword.put("newPassword", newPassword);
        return new ObjectMapper().writeValueAsString(userPassword);
    }
}
