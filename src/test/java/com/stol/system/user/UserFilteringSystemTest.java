package com.stol.system.user;

import com.stol.system.AbstractSystemTest;
import org.junit.Test;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserFilteringSystemTest extends AbstractSystemTest {
    @Test
    public void filterByNameAndSurname() throws Exception {
        mockMvc.perform(get("/api/users?name=UserOn&surname=One")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.users.length()").value(1))
                .andExpect(jsonPath("$._embedded.users[0].name").value("UserOne"));
    }

    @Test
    public void filterByEmail() throws Exception {
        mockMvc.perform(get("/api/users?email=user2@gmail.com")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.users.length()").value(1))
                .andExpect(jsonPath("$._embedded.users[0].name").value("User2"));
    }

    @Test
    public void filterByRegistrationDate() throws Exception {
        mockMvc.perform(get("/api/users?credential.registrationDate=2017-10-23")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.users.length()").value(1))
                .andExpect(jsonPath("$._embedded.users[0].name").value("UserOne"));
    }

    @Test
    public void filterByStatus() throws Exception {
        mockMvc.perform(get("/api/users?status=NOT_CONFIRMED")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.users.length()").value(1))
                .andExpect(jsonPath("$._embedded.users[0].name").value("User3"));
    }

    @Test
    public void filterByRole() throws Exception {
        mockMvc.perform(get("/api/users?roles.role=ROLE_RESTAURANT_ADMIN")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.users.length()").value(1))
                .andExpect(jsonPath("$._embedded.users[0].name").value("UserOne"));
    }

    @Test
    public void filterByPhone() throws Exception {
        mockMvc.perform(get("/api/users?phoneNumber=381000000000")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.users.length()").value(1))
                .andExpect(jsonPath("$._embedded.users[0].name").value("UserOne"));
    }

}
