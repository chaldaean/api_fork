package com.stol.system.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.controller.model.UserLogoutRequest;
import com.stol.model.user.Credential;
import com.stol.repository.CredentialRepository;
import com.stol.service.AuthService;
import com.stol.service.MobileDeviceService;
import com.stol.system.AbstractSystemTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.transaction.TestTransaction;

import java.util.HashMap;
import java.util.Map;

import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static com.stol.model.property.ApplicationType.CLIENT_APP;
import static com.stol.model.property.UserRole.ROLE_SYSTEM_ADMIN;
import static com.stol.model.property.UserStatus.ENABLE;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.springframework.data.rest.webmvc.RestMediaTypes.TEXT_URI_LIST_VALUE;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserSystemTest extends AbstractSystemTest {
    private static final String PHONE = "380000000001";
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String EMAIL = "email@email.com";

    @Autowired
    private CredentialRepository credentialRepository;

    @SpyBean
    private AuthService authService;

    @MockBean
    private MobileDeviceService mobileDeviceService;

    @Test
    public void createNewUserNotAllowed() throws Exception {
        mockMvc.perform(post("/api/users").content(userObject())
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Request method 'POST' not supported"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(put("/api/users/2").content(userObject())
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Request method 'PUT' not supported"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void updateUser() throws Exception {
        mockMvc.perform(patch("/api/users/100").content(userObject())
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(7))
                .andExpect(jsonPath("$.name").value(NAME))
                .andExpect(jsonPath("$.surname").value(SURNAME))
                .andExpect(jsonPath("$.email").value(EMAIL))
                .andExpect(jsonPath("$.phoneNumber").value("382000000100"))
                .andExpect(jsonPath("$.status").value(ENABLE.toString()))
                .andExpect(jsonPath("$.addRestaurantRequest").isEmpty())
                .andExpect(jsonPath("$._links.length()").value(9))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/100"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/users/100{?projection}"))
                .andExpect(jsonPath("$._links.photo.href").value("http://localhost/api/users/100/photo"))
                .andExpect(jsonPath("$._links.resent_user_password.href").value("http://localhost/api/user/password/reset"))
                .andExpect(jsonPath("$._links.reservations.href").value("http://localhost/api/users/100/reservations{?projection}"))
                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/100/saved_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.ownRestaurants.href").value("http://localhost/api/users/100/own_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.roles.href").value("http://localhost/api/users/100/roles"))
                .andExpect(jsonPath("$._links.language.href").value("http://localhost/api/users/100/language{?projection}"));

        TestTransaction.flagForCommit();
        TestTransaction.end();

        Credential credential = credentialRepository.findById(100L).orElse(new Credential());
        Assert.assertEquals(credential.getPassword(), "$2a$10$OBGb6CGYgKssENw3imZJIeAvCFuR2AHEizWN37/9g5H67OKdmazGu");
        Assert.assertEquals(credential.getLogin(), "382000000100");

        mockMvc.perform(get("/api/users/100/photo")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture2"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/2.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/2"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/2"));

        mockMvc.perform(get("/api/files/101")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }

    @Test
    public void getUserById() throws Exception {
        mockMvc.perform(get("/api/users/1")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(7))
                .andExpect(jsonPath("$.name").value("Admin"))
                .andExpect(jsonPath("$.surname").value("Admin"))
                .andExpect(jsonPath("$.email").value("admin@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("380000000000"))
                .andExpect(jsonPath("$.status").value(ENABLE.toString()))
                .andExpect(jsonPath("$.addRestaurantRequest.restaurantName").value("Fine restaurant"))

                .andExpect(jsonPath("$._links.length()").value(9))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/1"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/users/1{?projection}"))
                .andExpect(jsonPath("$._links.photo.href").value("http://localhost/api/users/1/photo"))
                .andExpect(jsonPath("$._links.resent_user_password.href").value("http://localhost/api/user/password/reset"))
                .andExpect(jsonPath("$._links.reservations.href").value("http://localhost/api/users/1/reservations{?projection}"))
                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/1/saved_restaurants{?projection}"))                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/1/saved_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.ownRestaurants.href").value("http://localhost/api/users/1/own_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.roles.href").value("http://localhost/api/users/1/roles"))
                .andExpect(jsonPath("$._links.language.href").value("http://localhost/api/users/1/language{?projection}"));
    }

    @Test
    public void getUserByIdWithProjectionRoles() throws Exception {
        mockMvc.perform(get("/api/users/1?projection=user_roles")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(10))
                .andExpect(jsonPath("$.name").value("Admin"))
                .andExpect(jsonPath("$.surname").value("Admin"))
                .andExpect(jsonPath("$.email").value("admin@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("380000000000"))
                .andExpect(jsonPath("$.photo").value("/files/100.jpg"))
                .andExpect(jsonPath("$.registrationDate").value("2017-10-22T14:40:01"))
                .andExpect(jsonPath("$.roles.length()").value(1))
                .andExpect(jsonPath("$.roles[0].role").value(ROLE_SYSTEM_ADMIN.name()))
                .andExpect(jsonPath("$.status").value(ENABLE.toString()))
                .andExpect(jsonPath("$._links.length()").value(9))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/1"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/users/1{?projection}"))
                .andExpect(jsonPath("$._links.photo.href").value("http://localhost/api/users/1/photo"))
                .andExpect(jsonPath("$._links.resent_user_password.href").value("http://localhost/api/user/password/reset"))
                .andExpect(jsonPath("$._links.reservations.href").value("http://localhost/api/users/1/reservations{?projection}"))
                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/1/saved_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.ownRestaurants.href").value("http://localhost/api/users/1/own_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.roles.href").value("http://localhost/api/users/1/roles"))
                .andExpect(jsonPath("$._links.language.href").value("http://localhost/api/users/1/language{?projection}"));

    }

    @Test
    public void addLikedRestaurantToUser() throws Exception {
        mockMvc.perform(patch("/api/users/1/saved_restaurants").content("http://localhost/api/restaurants/2\nhttp://localhost/api/restaurants/3")
                .header(AUTHORIZATION, token)
                .header(CONTENT_TYPE, TEXT_URI_LIST_VALUE)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()));

        mockMvc.perform(get("/api/users/1/saved_restaurants")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[*].name").value(containsInAnyOrder("name1", "name2", "name3")))
                .andExpect(jsonPath("$._embedded.restaurants[*]._links.self.href").value(containsInAnyOrder("http://localhost/api/restaurants/1",
                        "http://localhost/api/restaurants/2", "http://localhost/api/restaurants/3")));
    }

    @Test
    public void deleteLikedRestaurantToUser() throws Exception {
        mockMvc.perform(delete("/api/users/2/saved_restaurants/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()));

        mockMvc.perform(get("/api/users/2/saved_restaurants")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name2"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.self.href").value("http://localhost/api/restaurants/2"));
    }

    @Test
    public void getUserByIdWithProjectionPage() throws Exception {
        mockMvc.perform(get("/api/users/2?projection=user_page")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(7))
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.name").value("UserOne"))
                .andExpect(jsonPath("$.surname").value("UserOne"))
                .andExpect(jsonPath("$.photo").isEmpty())
                .andExpect(jsonPath("$.totalReservations").value(3))
                .andExpect(jsonPath("$.totalLikedRestaurants").value(2))

                .andExpect(jsonPath("$._links.length()").value(8))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/2"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/users/2{?projection}"))
                .andExpect(jsonPath("$._links.photo.href").value("http://localhost/api/users/2/photo"))
                .andExpect(jsonPath("$._links.reservations.href").value("http://localhost/api/users/2/reservations{?projection}"))
                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/2/saved_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.ownRestaurants.href").value("http://localhost/api/users/2/own_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.roles.href").value("http://localhost/api/users/2/roles"))
                .andExpect(jsonPath("$._links.language.href").value("http://localhost/api/users/2/language{?projection}"));
    }

    @Test
    public void getLoggedInUser() throws Exception {
        mockMvc.perform(get("/api/user/current")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(10))
                .andExpect(jsonPath("$.name").value("Admin"))
                .andExpect(jsonPath("$.status").value(ENABLE.toString()))
                .andExpect(jsonPath("$.roles.length()").value(1))
                .andExpect(jsonPath("$.roles[0].role").value(ROLE_SYSTEM_ADMIN.name()))
                .andExpect(jsonPath("$.surname").value("Admin"))
                .andExpect(jsonPath("$.photo").value("/files/100.jpg"))
                .andExpect(jsonPath("$.phoneNumber").value("380000000000"))
                .andExpect(jsonPath("$.registrationDate").value("2017-10-22T14:40:01"))
                .andExpect(jsonPath("$.email").value("admin@gmail.com"))

                .andExpect(jsonPath("$._links.length()").value(8))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/1{?projection}"))
                .andExpect(jsonPath("$._links.roles.href").value("http://localhost/api/users/1/roles"))
                .andExpect(jsonPath("$._links.photo.href").value("http://localhost/api/users/1/photo"))
                .andExpect(jsonPath("$._links.reservations.href").value("http://localhost/api/users/1/reservations{?projection}"))
                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/1/saved_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.ownRestaurants.href").value("http://localhost/api/users/1/own_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.resent_user_password.href").value("http://localhost/api/user/password/reset"))
                .andExpect(jsonPath("$._links.language.href").value("http://localhost/api/users/1/language{?projection}"));
    }

    @Test
    public void getLoggedInUserWithProjectionPage() throws Exception {
        mockMvc.perform(get("/api/user/current?projection=user_page")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(7))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("Admin"))
                .andExpect(jsonPath("$.surname").value("Admin"))
                .andExpect(jsonPath("$.photo").value("/files/100.jpg"))
                .andExpect(jsonPath("$.totalReservations").value(4))
                .andExpect(jsonPath("$.totalLikedRestaurants").value(1))

                .andExpect(jsonPath("$._links.length()").value(7))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/1{?projection}"))
                .andExpect(jsonPath("$._links.photo.href").value("http://localhost/api/users/1/photo"))
                .andExpect(jsonPath("$._links.reservations.href").value("http://localhost/api/users/1/reservations{?projection}"))
                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/1/saved_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.ownRestaurants.href").value("http://localhost/api/users/1/own_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.roles.href").value("http://localhost/api/users/1/roles"))
                .andExpect(jsonPath("$._links.language.href").value("http://localhost/api/users/1/language{?projection}"));
    }

    @Test
    public void findAllUsers() throws Exception {
        mockMvc.perform(get("/api/users?page=0&size=10&sort=credential.registrationDate,desc&projection=user_roles")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.users.length()").value(5))
                .andExpect(jsonPath("$._embedded.users[0].name").value("User100"))
                .andExpect(jsonPath("$._embedded.users[1].name").value("User3"))
                .andExpect(jsonPath("$._embedded.users[2].name").value("User2"))
                .andExpect(jsonPath("$._embedded.users[3].name").value("UserOne"))
                .andExpect(jsonPath("$._embedded.users[4].name").value("Admin"))

                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users{?projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/users"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/users/search"))

                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(10))
                .andExpect(jsonPath("$.page.totalElements").value(5))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    @Test
    public void findAllRestaurantAdmins() throws Exception {
        mockMvc.perform(get("/api/users/search/restaurant_admins?page=0&size=10&&sort=name&projection=user_roles")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.users.length()").value(1))
                .andExpect(jsonPath("$._embedded.users[0].name").value("UserOne"))
                .andExpect(jsonPath("$._embedded.users[0].surname").value("UserOne"))
                .andExpect(jsonPath("$._embedded.users[0].email").value("user1@gmail.com"))
                .andExpect(jsonPath("$._embedded.users[0].status").value("ENABLE"))
                .andExpect(jsonPath("$._embedded.users[0].roles.length()").value("1"))
                .andExpect(jsonPath("$._embedded.users[0].roles[0].role").value("ROLE_RESTAURANT_ADMIN"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/search/restaurant_admins?projection=user_roles&page=0&size=10&sort=name,asc"))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(10))
                .andExpect(jsonPath("$.page.totalElements").value(1))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    @Test
    public void logoutCurrentUser() throws Exception {
        mockMvc.perform(post("/api/user/current/logout").content(getUserLoginLogoutRequest())
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent());

        Mockito.verify(mobileDeviceService, times(1)).deleteByUserAndApplicationType (any(), eq(CLIENT_APP));
        Mockito.verify(authService, times(1)).logout();
    }

    private String userObject() throws JsonProcessingException {
        Map<String, String> credential = new HashMap<>();
        credential.put("login", PHONE);
        credential.put("password", "some_new_password");

        Map<String, Object> user = new HashMap<>();
        user.put("name", NAME);
        user.put("surname", SURNAME);
        user.put("email", EMAIL);
        user.put("phoneNumber", PHONE);
        user.put("credential", credential);
        //TODO only for admin
//        user.put("status", DISABLE.toString());
        user.put("photo", "http://localhost:8080/api/files/2");
        return new ObjectMapper().writeValueAsString(user);
    }

    private String getUserLoginLogoutRequest() throws JsonProcessingException {
        UserLogoutRequest req = new UserLogoutRequest();
        req.setApplicationType(CLIENT_APP);

        return new ObjectMapper().writeValueAsString(req);
    }
}
