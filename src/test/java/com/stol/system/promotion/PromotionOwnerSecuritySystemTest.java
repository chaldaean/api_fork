package com.stol.system.promotion;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.Test;
import org.springframework.http.HttpHeaders;

import java.util.HashMap;
import java.util.Map;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static com.stol.configuration.constant.Messages.NOT_OWNER;
import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PromotionOwnerSecuritySystemTest extends AbstractSystemTest {

    @Test
    public void createPromotionNotRestaurantOwnerNotSystemAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(post("/api/promotions").content(promotionObject("2"))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Вы должны быть владельцем, чтобы сделать это!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(post("/api/promotions").content(promotionObject("2"))
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Ви маєте бути власником, щоб зробити це!"));

        mockMvc.perform(post("/api/promotions").content(promotionObject("2"))
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You must be an owner to do this!"));
    }

    @Test
    public void createPromotionNotRestaurantOwnerSystemAdmin() throws Exception {
        mockMvc.perform(post("/api/promotions").content(promotionObject("6"))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());
    }

    @Test
    public void createPromotionRestaurantOwnerNotSystemAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(post("/api/promotions").content(promotionObject("6"))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());
    }

    @Test
    public void updatePromotionNotRestaurantOwnerNotSystemAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/promotions/1").content(promotionObject("1"))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Вы должны быть владельцем, чтобы сделать это!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(patch("/api/promotions/1").content(promotionObject("1"))
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Ви маєте бути власником, щоб зробити це!"));

        mockMvc.perform(patch("/api/promotions/1").content(promotionObject("1"))
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You must be an owner to do this!"));
    }

    @Test
    public void updatePromotionNotRestaurantOwnerSystemAdmin() throws Exception {
        mockMvc.perform(patch("/api/promotions/2").content(promotionObject("6"))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void updatePromotionRestaurantOwnerNotSystemAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/promotions/2").content(promotionObject("6"))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void deletePromotionNotRestaurantOwnerNotSystemAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(delete("/api/promotions/1")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Вы должны быть владельцем, чтобы сделать это!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(delete("/api/promotions/1")
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Ви маєте бути власником, щоб зробити це!"));

        mockMvc.perform(delete("/api/promotions/1")
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You must be an owner to do this!"));
    }

    @Test
    public void deletePromotionNotRestaurantOwnerSystemAdmin() throws Exception {
        mockMvc.perform(delete("/api/promotions/2")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deletePromotionRestaurantOwnerNotSystemAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(delete("/api/promotions/2")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isNoContent());
    }

    private String promotionObject(String restaurantId) throws JsonProcessingException {
        Map<String, Object> p = new HashMap<>();
        p.put("description", "Promotion1");
        p.put("endDate", "2017-10-22");
        p.put("restaurant", "http://localhost:8080/api/restaurants/" + restaurantId);
        p.put("active", "true");
        p.put("total", "50");
        return new ObjectMapper().writeValueAsString(p);
    }
}
