package com.stol.system.promotion;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PromotionSystemTest extends AbstractSystemTest {
    private static String promotion;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        promotion = promotionObject();
    }

    @Test
    public void createNewPromotion() throws Exception {
        mockMvc.perform(post("/api/promotions").content(promotion)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.description").value("Promotion1"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$.endDate").isNotEmpty())
                .andExpect(jsonPath("$.total").value(50))
                .andExpect(jsonPath("$.used").value(0))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].description").value("опис"))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.promotion.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty());
    }

    @Test
    public void updatePromotion() throws Exception {
        Map<String, Object> p = new HashMap<>();
        p.put("description", "PromotionNew");
        //TODO it should not be possible to change a restaurant
//        p.put("restaurant", "http://localhost:8080/api/restaurants/2");
        p.put("active", "false");
        String promotion = new ObjectMapper().writeValueAsString(p);

        mockMvc.perform(patch("/api/promotions/1").content(promotion)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.description").value("PromotionNew"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$.endDate").isEmpty())
                .andExpect(jsonPath("$.total").value(1))
                .andExpect(jsonPath("$.used").value(0))
                .andExpect(jsonPath("$.active").value(false))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/promotions/1"))
                .andExpect(jsonPath("$._links.promotion.href").value("http://localhost/api/promotions/1{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/promotions/1/restaurant{?projection}"));

        mockMvc.perform(get("/api/promotions/1/restaurant?projection=card")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("name1"));
    }

    @Test
    public void deletePromotion() throws Exception {
        mockMvc.perform(delete("/api/promotions/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/promotions/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }

    @Test
    public void getPromotion() throws Exception {
        mockMvc.perform(get("/api/promotions/2?projection=restaurant")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.description").value("Test description2"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$.endDate").isEmpty())
                .andExpect(jsonPath("$.total").value(10))
                .andExpect(jsonPath("$.used").value(0))
                .andExpect(jsonPath("$.restaurantName").value("name6"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.promotion.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty());
    }

    @Test
    public void getPromotionById() throws Exception {
        mockMvc.perform(get("/api/promotions/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.description").value("Test description1"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$.endDate").isEmpty())
                .andExpect(jsonPath("$.total").value(1))
                .andExpect(jsonPath("$.used").value(0))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/promotions/1"))
                .andExpect(jsonPath("$._links.promotion.href").value("http://localhost/api/promotions/1{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/promotions/1/restaurant{?projection}"));

        mockMvc.perform(get("/api/promotions/1/restaurant")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(15))
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$._links.length()").value(12));
    }

    @Test
    public void getAllPromotion() throws Exception {
        mockMvc.perform(get("/api/promotions")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.promotions.length()").value(2))
                .andExpect(jsonPath("$._embedded.promotions[0].length()").value(8))
                .andExpect(jsonPath("$._embedded.promotions[0].description").value("Test description1"))
                .andExpect(jsonPath("$._embedded.promotions[0].endDate").isEmpty())
                .andExpect(jsonPath("$._embedded.promotions[0].creationDate").isNotEmpty())
                .andExpect(jsonPath("$._embedded.promotions[0].total").value(1))
                .andExpect(jsonPath("$._embedded.promotions[0].used").value(0))
                .andExpect(jsonPath("$._embedded.promotions[0].active").value(true))
                .andExpect(jsonPath("$._embedded.promotions[0]._links.length()").value(3))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void getAllPromotionsForLoggedInUser() throws Exception {
        mockMvc.perform(get("/api/promotions/search/my_restaurants")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.promotions.length()").value(1))
                .andExpect(jsonPath("$._embedded.promotions[0].length()").value(8))
                .andExpect(jsonPath("$._embedded.promotions[0].description").value("Test description1"))
                .andExpect(jsonPath("$._embedded.promotions[0].endDate").isEmpty())
                .andExpect(jsonPath("$._embedded.promotions[0].creationDate").isNotEmpty())
                .andExpect(jsonPath("$._embedded.promotions[0].total").value(1))
                .andExpect(jsonPath("$._embedded.promotions[0].used").value(0))
                .andExpect(jsonPath("$._embedded.promotions[0].active").value(true))
                .andExpect(jsonPath("$._embedded.promotions[0]._links.length()").value(3))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    static String promotionObject() throws JsonProcessingException {
        Map<String, Object> p = new HashMap<>();
        p.put("description", "Promotion1");
        p.put("endDate", "2017-10-22");
        p.put("restaurant", "http://localhost:8080/api/restaurants/2");
        p.put("active", "true");
        p.put("total", "50");

        Map<String, String> i18n = new HashMap<>();
        i18n.put("language", "http://localhost/api/languages/1");
        i18n.put("description", "опис");

        p.put("i18ns", Arrays.asList(i18n));

        return new ObjectMapper().writeValueAsString(p);
    }
}
