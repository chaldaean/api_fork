package com.stol.system.promotion;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.stol.system.SecurityAbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static com.stol.system.promotion.PromotionSystemTest.promotionObject;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PromotionSecuritySystemTest extends SecurityAbstractSystemTest {
    private static String promotion;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        promotion = promotionObject();
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void createPromotionNoSystemAdminOrRestaurantAdminRoles() throws Exception {
        mockMvc.perform(post("/api/promotions").content(promotion)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void findPromotionNoRole() throws Exception {
        mockMvc.perform(get("/api/promotions/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void deletePromotionNoSystemAdminOrRestaurantAdminRoles() throws Exception {
        mockMvc.perform(delete("/api/promotions/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void findAllMyPromotionNoRestaurantAndSystemAdminRoles() throws Exception {
        mockMvc.perform(get("/api/promotions/search/my_restaurants"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void updatePromotionNoRestaurantAdminOrSystemAdminRole() throws Exception {
        mockMvc.perform(patch("/api/promotions/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }
}
