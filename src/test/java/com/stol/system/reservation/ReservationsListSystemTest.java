package com.stol.system.reservation;

import com.github.database.rider.core.api.dataset.DataSet;
import com.stol.model.Reservation;
import com.stol.repository.ReservationRepository;
import com.stol.system.AbstractSystemTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.stol.model.property.ReservationStatus.*;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationsListSystemTest extends AbstractSystemTest {

    @Autowired
    private ReservationRepository reservationRepository;

    private LocalDateTime prepareNearestReservations() {
        LocalDateTime now = LocalDateTime.now();
        Reservation reservation = reservationRepository.findById(1L).get();
        reservation.setCreated(now);
        reservation.setStatusChanged(now);
        reservation.setDateTime(LocalDate.now().plusDays(1L).atTime(12, 0, 0));
        reservationRepository.save(reservation);

        reservation = reservationRepository.findById(2L).get();
        reservation.setCreated(now.plusHours(1L));
        reservation.setStatusChanged(now.plusHours(1L));
        reservation.setDateTime(LocalDate.now().plusDays(1L).atTime(17, 0, 0));
        reservationRepository.save(reservation);

        reservation = reservationRepository.findById(3L).get();
        reservation.setCreated(now.plusHours(3L));
        reservation.setStatusChanged(now.plusHours(3L));
        reservation.setDateTime(LocalDate.now().plusDays(2L).atTime(12, 0, 0));
        reservationRepository.save(reservation);

        reservation = reservationRepository.findById(4L).get();
        reservation.setCreated(now.minusHours(3L));
        reservation.setStatusChanged(now.minusHours(1L));
        reservation.setDateTime(LocalDate.now().plusDays(1L).atTime(12, 0, 0));
        reservationRepository.save(reservation);

        reservation = reservationRepository.findById(5L).get();
        reservation.setCreated(now.minusHours(4L));
        reservation.setStatusChanged(now.minusHours(1L));
        reservation.setDateTime(LocalDate.now().plusDays(1L).atTime(13, 0, 0));
        reservationRepository.save(reservation);

        reservation = reservationRepository.findById(6L).get();
        reservation.setCreated(now.minusHours(5L));
        reservation.setStatusChanged(now.minusMinutes(55L));
        reservation.setDateTime(LocalDate.now().plusDays(1L).atTime(13, 0, 0));
        reservationRepository.save(reservation);

        return now;
    }

    @Test
    @DataSet(value = "user_roles.xml,user_nearest_reservations.xml,oauth.xml", executorId = "system", disableConstraints = true)
    public void findNearestReservationsForLoggedinUser() throws Exception {
        final LocalDateTime now = prepareNearestReservations();

        mockMvc.perform(get("/api/reservations/search/forthcoming?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(6))
                .andExpect(jsonPath("$._embedded.reservations[0].id").value(3))
                .andExpect(jsonPath("$._embedded.reservations[0].time").value("12:00"))
                .andExpect(jsonPath("$._embedded.reservations[0].date").value(now.plusDays(2L).toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[0].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[1].id").value(2))
                .andExpect(jsonPath("$._embedded.reservations[1].time").value("17:00"))
                .andExpect(jsonPath("$._embedded.reservations[1].date").value(now.plusDays(1L).toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[1].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[2].id").value(1))
                .andExpect(jsonPath("$._embedded.reservations[2].time").value("12:00"))
                .andExpect(jsonPath("$._embedded.reservations[2].date").value(now.plusDays(1L).toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[2].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[3].id").value(4))
                .andExpect(jsonPath("$._embedded.reservations[3].time").value("12:00"))
                .andExpect(jsonPath("$._embedded.reservations[3].date").value(now.plusDays(1L).toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[3].status").value(REJECTED.toString()))
                .andExpect(jsonPath("$._embedded.reservations[4].id").value(5))
                .andExpect(jsonPath("$._embedded.reservations[4].time").value("13:00"))
                .andExpect(jsonPath("$._embedded.reservations[4].date").value(now.plusDays(1L).toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[4].status").value(CONFIRMED.toString()))
                .andExpect(jsonPath("$._embedded.reservations[5].id").value(6))
                .andExpect(jsonPath("$._embedded.reservations[5].time").value("13:00"))
                .andExpect(jsonPath("$._embedded.reservations[5].date").value(now.plusDays(1L).toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[5].status").value(CANCELED.toString()));

        mockMvc.perform(get("/api/reservations/search/past?projection=reviewed")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(0));
    }

    private LocalDateTime preparePastReservations() {
        LocalDateTime now = LocalDateTime.now();
        Reservation reservation = reservationRepository.findById(1L).get();
        reservation.setCreated(now.minusDays(2L));
        reservation.setStatusChanged(now.minusDays(2L));
        reservation.setDateTime(now.minusHours(2).minusMinutes(1));
        reservationRepository.save(reservation);

        reservation = reservationRepository.findById(2L).get();
        reservation.setCreated(now.minusDays(3L));
        reservation.setStatusChanged(now.minusDays(2L));
        reservation.setDateTime(now.minusHours(2).minusMinutes(1));
        reservationRepository.save(reservation);

        reservation = reservationRepository.findById(3L).get();
        reservation.setCreated(now.minusDays(4L));
        reservation.setStatusChanged(now.minusDays(2L));
        reservation.setDateTime(now.minusHours(2).minusMinutes(1));
        reservationRepository.save(reservation);

        reservation = reservationRepository.findById(4L).get();
        reservation.setCreated(now.minusDays(4L));
        reservation.setStatusChanged(now.minusDays(2L));
        reservation.setDateTime(now.minusHours(12).minusMinutes(1));
        reservationRepository.save(reservation);

        return now;
    }

    @Test
    @DataSet(value = "user_roles.xml,user_past_reservations.xml,oauth.xml", executorId = "system", disableConstraints = true)
    public void findPassedReservationsForLoggedinUser() throws Exception {
        final LocalDateTime now = preparePastReservations();

        mockMvc.perform(get("/api/reservations/search/past?projection=reviewed")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations[0].id").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].time").value(now.minusHours(2).minusMinutes(1).toLocalTime().format(ofPattern("HH:mm"))))
                .andExpect(jsonPath("$._embedded.reservations[0].date").value(now.minusHours(2).minusMinutes(1).toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[0].status").value(CONFIRMED.toString()))
                .andExpect(jsonPath("$._embedded.reservations[1].id").value(2))
                .andExpect(jsonPath("$._embedded.reservations[1].time").value(now.minusHours(2).minusMinutes(1).toLocalTime().format(ofPattern("HH:mm"))))
                .andExpect(jsonPath("$._embedded.reservations[1].date").value(now.minusHours(2).minusMinutes(1).toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[1].status").value(REJECTED.toString()))
                .andExpect(jsonPath("$._embedded.reservations[2].id").value(3))
                .andExpect(jsonPath("$._embedded.reservations[2].time").value(now.minusHours(2).minusMinutes(1).toLocalTime().format(ofPattern("HH:mm"))))
                .andExpect(jsonPath("$._embedded.reservations[2].date").value(now.minusHours(2).minusMinutes(1).toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[2].status").value(CANCELED.toString()));

        mockMvc.perform(get("/api/reservations/search/forthcoming?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(0));
    }
}
