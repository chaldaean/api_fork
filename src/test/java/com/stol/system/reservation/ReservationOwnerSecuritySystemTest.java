package com.stol.system.reservation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.Test;
import org.springframework.http.HttpHeaders;

import java.util.HashMap;
import java.util.Map;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static com.stol.configuration.constant.Messages.DO_NOT_HAVE_ACCESS_TO_THIS_RESOURCE;
import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static com.stol.system.SecurityAbstractSystemTest.ACCESS_DENIED;
import static com.stol.system.SecurityAbstractSystemTest.ACCESS_IS_DENIED;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationOwnerSecuritySystemTest extends AbstractSystemTest {
    @Test
    public void getAllReservationsNoRestaurantOrSystemAdminRole() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "382000000000", PASSWORD));
        mockMvc.perform(get("/api/reservations")
                .header(AUTHORIZATION, token))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    public void getAllReservationsAllowForSystemAdminRole() throws Exception {
        String token = getAdminCredentialToken();
        mockMvc.perform(get("/api/reservations")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(9));
    }

    @Test
    public void getAllReservationsHasRestaurantAdminRoleNoLoginParameter() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(get("/api/reservations")
                .header(AUTHORIZATION, token))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    public void getAllRestaurantReservationsForRestaurantAdminRole() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(get("/api/reservations?restaurant.owners.credential.login=381000000000")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(1));
    }

    @Test
    public void getAllRestaurantReservationsForRestaurantAdminRoleWrongOwner() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(get("/api/reservations?restaurant.owners.credential.login=382000000000")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    public void updateReservationNotOwnerNotRestaurantAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/reservations/5").content(updateReservation())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Вы не являетесь владельцем ресурса!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(patch("/api/reservations/5").content(updateReservation())
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Вы не є власником ресурсу!"));

        mockMvc.perform(patch("/api/reservations/5").content(updateReservation())
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You are not the owner of this resource!"));

    }

    @Test
    public void updateReservationNotOwnerAndRestaurantAdmin() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/reservations/8").content(updateReservation())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8));
    }

    @Test
    public void updateReservationNotOwnerAndSystemAdmin() throws Exception {
        mockMvc.perform(patch("/api/reservations/8").content(updateReservation())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8));
    }

    private String updateReservation() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("guestNumber", "2");
        r.put("comment", "Comment!");
        r.put("status", "CONFIRMED");
        return new ObjectMapper().writeValueAsString(r);
    }
}
