package com.stol.system.reservation;

import com.stol.system.AbstractSystemTest;
import org.junit.Test;

import static com.stol.model.property.ReservationStatus.REJECTED;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationFilteringSystemTest extends AbstractSystemTest {
    @Test
    public void getAllReservationsByStatus() throws Exception {
        mockMvc.perform(get("/api/reservations?status=REJECTED")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].status").value(REJECTED.toString()));
    }

    @Test
    public void getAllReservationsByRestaurantName() throws Exception {
        mockMvc.perform(get("/api/reservations?restaurant.name=name1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(4));
    }

    @Test
    public void getAllReservationsByQuestNumber() throws Exception {
        mockMvc.perform(get("/api/reservations?guestNumber=4")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].guestNumber").value(4));
    }

    @Test
    public void getAllReservationsByClientSurname() throws Exception {
        mockMvc.perform(get("/api/reservations?client.surname=Use")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(5));
    }

    @Test
    public void getAllReservationsByClientName() throws Exception {
        mockMvc.perform(get("/api/reservations?client.name=Adm")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(4));
    }

    //TODO how to select by time?
/*    @Test
    public void getAllReservationsByVisitTime() throws Exception {
        mockMvc.perform(get("/api/reservations?time.time=23:30&projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations[0].time").value("23:30"))
                .andExpect(jsonPath("$._embedded.reservations[1].time").value("23:30"))
                .andExpect(jsonPath("$._embedded.reservations[2].time").value("23:30"));
    }*/

    @Test
    public void getAllReservationsByVisitDate() throws Exception {
        mockMvc.perform(get("/api/reservations?dateTime=2017-10-22")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(2))
                .andExpect(jsonPath("$._embedded.reservations[0].dateTime").value("2017-10-22T11:00:00"))
                .andExpect(jsonPath("$._embedded.reservations[1].dateTime").value("2017-10-22T11:00:00"));
    }

    @Test
    public void getAllReservationsByCreationDate() throws Exception {
        mockMvc.perform(get("/api/reservations?created=2016-01-20")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].created").value("2016-01-20T14:40:08"));
    }

    @Test
    public void getAllReservationsByPredicates() throws Exception {
        mockMvc.perform(get("/api/reservations?restaurant.name=name1&guestNumber=2&status=REJECTED&comment=Some comment1&client.name=Admin&projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[0].id").value("1"))
                .andExpect(jsonPath("$._embedded.reservations[0].date").value("2017-10-22"))
                .andExpect(jsonPath("$._embedded.reservations[0].time").value("11:00"))
                .andExpect(jsonPath("$._embedded.reservations[0].guestNumber").value(2))
                .andExpect(jsonPath("$._embedded.reservations[0].comment").value("Some comment1"))
                .andExpect(jsonPath("$._embedded.reservations[0].status").value(REJECTED.toString()))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantAddress").value("address1"))
                .andExpect(jsonPath("$._embedded.reservations[0].topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.length()").value(4))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.profile.href").isNotEmpty())
                .andExpect(jsonPath("$._links.search.href").isNotEmpty())
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void getAllReservationsByDiscount() throws Exception {
        mockMvc.perform(get("/api/reservations?discount.happyHour.discount=20")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].created").value("2016-01-20T14:40:08"));
    }

    @Test
    public void getAllReservationsWithUnusedDiscount() throws Exception {
        mockMvc.perform(get("/api/reservations?discount.isUsed=false")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].created").value("2016-01-20T14:40:08"));
    }
}
