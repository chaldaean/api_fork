package com.stol.system.reservation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.stol.model.Reservation;
import com.stol.repository.ReservationRepository;
import com.stol.system.AbstractSystemTest;
import org.hamcrest.core.IsNull;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static com.stol.model.property.ReservationStatus.*;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationSystemTest extends AbstractSystemTest {
    private static String reservation;

    @Autowired
    ReservationRepository reservationRepository;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        reservation = reservationObject();
    }

    @Test
    public void createNewReservation() throws Exception {
        MvcResult response = mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.dateTime").value("2099-11-01T11:00:00"))
                .andExpect(jsonPath("$.guestNumber").value(2))
                .andExpect(jsonPath("$.comment").value("Comment!"))
                .andExpect(jsonPath("$.notes").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.status").value(NEW.toString()))
                .andExpect(jsonPath("$.created").isNotEmpty())
                .andExpect(jsonPath("$.discount.length()").value(3))
                .andExpect(jsonPath("$.discount.value").value(20))
                .andExpect(jsonPath("$.discount.used").value(Boolean.FALSE))
                .andExpect(jsonPath("$.discount._links.length()").value(1))
                .andExpect(jsonPath("$.discount._links.happyHour.href").value("http://localhost/api/happy_hours/1{?projection}"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.reservation.href").isNotEmpty())
                .andExpect(jsonPath("$._links.client.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty())
                .andReturn();

        String r = JsonPath.read(response.getResponse().getContentAsString(), "$._links.self.href");
        String id = r.substring(r.lastIndexOf("/") + 1);

        mockMvc.perform(get("/api/reservations/" + id + "/restaurant")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(15))
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$.address").value("address1"))
                .andExpect(jsonPath("$._links.length()").value(12))
                .andExpect(jsonPath("$._links.owners.href").isNotEmpty());
    }

    @Test
    public void changeNewReservationStatusAndNotes() throws Exception {
        MvcResult response = mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.status").value(NEW.toString()))
                .andReturn();

        String r = JsonPath.read(response.getResponse().getContentAsString(), "$._links.self.href");
        String id = r.substring(r.lastIndexOf("/") + 1);

        Reservation reservationOld = reservationRepository.findById(Long.parseLong(id)).get();
        LocalDateTime statusChangedOld = reservationOld.getStatusChanged();

        Thread.sleep(10);

        mockMvc.perform(patch("/api/reservations/{id}", id).content("{\"status\": \"CONFIRMED\", \"notes\": \"Allergic\"}")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(CONFIRMED.toString()))
                .andExpect(jsonPath("$.notes").value("Allergic"));

        Reservation reservationNew = reservationRepository.findById(Long.parseLong(id)).get();
        LocalDateTime dateTimeNew = reservationNew.getDateTime();
        LocalDateTime statusChangedNew = reservationNew.getStatusChanged();
        assertNotEquals(statusChangedNew, statusChangedOld);
    }

    @Test
    public void deactivatePromotionAfterReservation() throws Exception {
        mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        mockMvc.perform(get("/api/promotions/1")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.description").value("Test description1"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$.endDate").isEmpty())
                .andExpect(jsonPath("$.total").value(1))
                .andExpect(jsonPath("$.used").value(1))
                .andExpect(jsonPath("$.active").value(false))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.promotion.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty());
    }

    @Test
    public void getReservation() throws Exception {
        mockMvc.perform(get("/api/reservations/1?projection=snippet")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(9))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.created").value("2016-01-20T14:40:08"))
                .andExpect(jsonPath("$.clientName").value("Admin Admin"))
                .andExpect(jsonPath("$.restaurantName").value("name1"))
                .andExpect(jsonPath("$.dateTime").value("2017-10-22T11:00:00"))
                .andExpect(jsonPath("$.guestNumber").value(2))
                .andExpect(jsonPath("$.discount.length()").value(2))
                .andExpect(jsonPath("$.discount.value").value(20))
                .andExpect(jsonPath("$.discount.used").value(Boolean.FALSE))
                .andExpect(jsonPath("$.status").value(REJECTED.toString()))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.reservation.href").isNotEmpty())
                .andExpect(jsonPath("$._links.client.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty());

        mockMvc.perform(get("/api/reservations/1?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(10))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.date").value("2017-10-22"))
                .andExpect(jsonPath("$.guestNumber").value(2))
                .andExpect(jsonPath("$.comment").value("Some comment1"))
                .andExpect(jsonPath("$.status").value(REJECTED.toString()))
                .andExpect(jsonPath("$.time").value("11:00"))
                .andExpect(jsonPath("$.restaurantName").value("name1"))
                .andExpect(jsonPath("$.restaurantAddress").value("address1"))
                .andExpect(jsonPath("$.topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.reservation.href").isNotEmpty())
                .andExpect(jsonPath("$._links.client.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty());
    }

    @Test
    public void getReservationById() throws Exception {
        mockMvc.perform(get("/api/reservations/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.dateTime").value("2017-10-22T11:00:00"))
                .andExpect(jsonPath("$.guestNumber").value(2))
                .andExpect(jsonPath("$.comment").value("Some comment1"))
                .andExpect(jsonPath("$.notes").value("No tables"))
                .andExpect(jsonPath("$.status").value(REJECTED.toString()))
                .andExpect(jsonPath("$.created").value("2016-01-20T14:40:08"))
                .andExpect(jsonPath("$.discount").isNotEmpty())
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/1"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reservations/1{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reservations/1/restaurant{?projection}"))
                .andExpect(jsonPath("$._links.client.href").value("http://localhost/api/reservations/1/client{?projection}"));

        mockMvc.perform(get("/api/reservations/1/restaurant")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(15))
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$._links.length()").value(12));

        mockMvc.perform(get("/api/reservations/1/client")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(7))
                .andExpect(jsonPath("$.name").value("Admin"))
                .andExpect(jsonPath("$._links.length()").value(9));
    }

    @Test
    public void getAllReservations() throws Exception {
        mockMvc.perform(get("/api/reservations?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(9))
                .andExpect(jsonPath("$._embedded.reservations[0].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[1].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[2].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[3].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[4].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[4]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[5].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[5]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[6].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[6]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[7].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[7]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[8].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[8]._links.length()").value(4))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.profile.href").isNotEmpty())
                .andExpect(jsonPath("$._links.search.href").isNotEmpty())
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void getAllLoggedInUsersReservations() throws Exception {
        mockMvc.perform(get("/api/reservations/search/my?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[0].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[0].id").value(9))
                .andExpect(jsonPath("$._embedded.reservations[1].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[1].id").value(7))
                .andExpect(jsonPath("$._embedded.reservations[2].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[2].id").value(6))
                .andExpect(jsonPath("$._embedded.reservations[3].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[3].id").value(1))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/search/my?projection=time&page=0&size=20"))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(4))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    // TODO incorrect sorting!!!
   /* @Test
    public void getAllReservationsInLoggedInUserRestaurants() throws Exception {
        mockMvc.perform(get("/api/reservations/search/my_restaurants?projection=time")
                .param("page", "0").param("size", "3").param("sort", "dateTime,desc")
                .param("sort", "time,desc")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations[0].date").value("2017-10-21"))
                .andExpect(jsonPath("$._embedded.reservations[0].time").value("06:30"))
                .andExpect(jsonPath("$._embedded.reservations[1].date").value("2017-10-24"))
                .andExpect(jsonPath("$._embedded.reservations[1].time").value("23:30"))
                .andExpect(jsonPath("$._embedded.reservations[2].date").value("2017-10-22"))
                .andExpect(jsonPath("$._embedded.reservations[2].time").value("11:00"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/search/my_restaurants?projection=time&page=0&size=3&sort=dateTime,desc"))
                .andExpect(jsonPath("$._links.first.href").value("http://localhost/api/reservations/search/my_restaurants?projection=time&page=0&size=3&sort=dateTime,desc"))
                .andExpect(jsonPath("$._links.next.href").value("http://localhost/api/reservations/search/my_restaurants?projection=time&page=1&size=3&sort=dateTime,desc"))
                .andExpect(jsonPath("$._links.last.href").value("http://localhost/api/reservations/search/my_restaurants?projection=time&page=1&size=3&sort=dateTime,desc"))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(3))
                .andExpect(jsonPath("$.page.totalElements").value(5))
                .andExpect(jsonPath("$.page.totalPages").value(2))
                .andExpect(jsonPath("$.page.number").value(0));
    }*/

    @Test
    public void findAllReservationsByUser() throws Exception {
        mockMvc.perform(get("/api/reservations/search/by_user?id=http://localhost/api/users/2&projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations[0].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[1].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[2].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.length()").value(4))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/search/by_user?id=http://localhost/api/users/2&projection=time&page=0&size=20"))
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void deleteReservation() throws Exception {
        mockMvc.perform(delete("/api/reservations/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());
    }

    @Test
    public void countNewReservationsForRestaurantOwner() throws Exception {
        mockMvc.perform(get("/api/reservations/search/count_new").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(2));
    }

    static String reservationObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/1");
        r.put("dateTime", "2099-11-01T11:00:00");
        r.put("guestNumber", "2");
        r.put("comment", "Comment!");
        return new ObjectMapper().writeValueAsString(r);
    }
}
