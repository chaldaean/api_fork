package com.stol.system.reservation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.stol.system.SecurityAbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static com.stol.system.reservation.ReservationSystemTest.reservationObject;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationSecuritySystemTest extends SecurityAbstractSystemTest {
    private static String reservation;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        reservation = reservationObject();
    }

    @Test
    public void createNewReservationHaveUserRole() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "382000000000", PASSWORD));
        mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void createNewReservationNoUserRole() throws Exception {
        mockMvc.perform(post("/api/reservations").content(reservation)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void findAllReservationsByUserNoSystemAdminRole() throws Exception {
        mockMvc.perform(get("/api/reservations/search/by_user?id=http://localhost/api/users/2"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void findAllReservationsOfCurrentUserNoUserRole() throws Exception {
        mockMvc.perform(get("/api/reservations/search/my"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void deleteReservationNoSystemAdminRole() throws Exception {
        mockMvc.perform(delete("/api/reservations/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void findAllReservationsInOwnerRestaurantsNoRestaurantOrSystemAdminRole() throws Exception {
        mockMvc.perform(get("/api/reservations/search/my_restaurants"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    public void changeNotesNotRestaurantOwner() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "382000000000", PASSWORD));

        mockMvc.perform(patch("/api/reservations/2").content("{\"notes\": \"Some notes\"}")
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON_UTF8)
                    .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.message").value("Вы не являетесь владельцем ресурса!"));
    }

}
