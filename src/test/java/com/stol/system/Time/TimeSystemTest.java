package com.stol.system.Time;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TimeSystemTest extends AbstractSystemTest {
    private static String time;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        time = timeObject();
    }

    @Test
    public void createTime() throws Exception {
        mockMvc.perform(post("/api/times").content(time)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.time").value("14:30"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.time.href").isNotEmpty());
    }

    @Test
    public void timePeriodMustBeUnique() throws Exception {
        mockMvc.perform(post("/api/times").content(time)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/api/times").content(time)
                .header("Authorization", token)
                .contentType(VND_ERROR)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void findAllTime() throws Exception {
        mockMvc.perform(get("/api/times")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.times.length()").value(3))
                .andExpect(jsonPath("$._embedded.times[0].time").value("11:00"))
                .andExpect(jsonPath("$._embedded.times[1].time").value("06:30"))
                .andExpect(jsonPath("$._embedded.times[2].time").value("23:30"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());
    }

    private static String timeObject() throws JsonProcessingException {
        Map<String, Object> t = new HashMap<>();
        t.put("time", "14:30");
        return new ObjectMapper().writeValueAsString(t);
    }
}
