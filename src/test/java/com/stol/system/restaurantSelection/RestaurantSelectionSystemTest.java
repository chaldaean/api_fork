package com.stol.system.restaurantSelection;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.context.transaction.TestTransaction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestaurantSelectionSystemTest extends AbstractSystemTest {
    private static String selection;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        selection = restaurantSelectionObject();
    }

    @Test
    public void createNewRestaurantSelections() throws Exception {
        mockMvc.perform(post("/api/restaurant_selections").content(selection)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.title").value("Selection title"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$.total").value(2))
                .andExpect(jsonPath("$.active").value("true"))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].title").value("Вибрані"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurantSelection.href").isNotEmpty())
                .andExpect(jsonPath("$._links.picture.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurants.href").isNotEmpty());
    }

    @Test
    public void deleteRestaurantSelections() throws Exception {
        mockMvc.perform(delete("/api/restaurant_selections/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/files/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getRestaurantSelectionsWithIcon() throws Exception {
        mockMvc.perform(get("/api/restaurant_selections/1?projection=iconUrl")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.title").value("Selection title"))
                .andExpect(jsonPath("$.creationDate").value("2017-10-22T14:40:08"))
                .andExpect(jsonPath("$.total").value(1))
                .andExpect(jsonPath("$.active").value("true"))
                .andExpect(jsonPath("$.iconUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurantSelection.href").isNotEmpty())
                .andExpect(jsonPath("$._links.picture.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurants.href").isNotEmpty());
    }

    @Test
    public void getRestaurantSelectionsById() throws Exception {
        mockMvc.perform(get("/api/restaurant_selections/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.title").value("Selection title"))
                .andExpect(jsonPath("$.creationDate").value("2017-10-22T14:40:08"))
                .andExpect(jsonPath("$.total").value(1))
                .andExpect(jsonPath("$.active").value("true"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurant_selections/1"))
                .andExpect(jsonPath("$._links.restaurantSelection.href").value("http://localhost/api/restaurant_selections/1{?projection}"))
                .andExpect(jsonPath("$._links.picture.href").value("http://localhost/api/restaurant_selections/1/picture"))
                .andExpect(jsonPath("$._links.restaurants.href").value("http://localhost/api/restaurant_selections/1/restaurants{?projection}"));

        mockMvc.perform(get("/api/restaurant_selections/1/picture")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture1"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/1.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/1"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/1"));

        mockMvc.perform(get("/api/restaurant_selections/1/restaurants")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(1))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurant_selections/1/restaurants"));
    }

    @Test
    public void getAllActiveRestaurantSelections() throws Exception {
        mockMvc.perform(get("/api/restaurant_selections")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurantSelections.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].length()").value(6))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].title").value("Selection title"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].creationDate").value("2017-10-22T14:40:08"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].total").value(1))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].active").value("true"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0]._links.length()").value(4))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void getAllRestaurantSelections() throws Exception {
        mockMvc.perform(get("/api/restaurant_selections/search/all")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurantSelections.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].length()").value(6))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].title").value("Selection title"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].creationDate").value("2017-10-22T14:40:08"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].total").value(1))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].active").value("true"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.restaurantSelections[2].title").value("Selection title101"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void updateRestaurantSelections() throws Exception {
        mockMvc.perform(patch("/api/restaurant_selections/100").content(selection)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.title").value("Selection title"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$.total").value(2))
                .andExpect(jsonPath("$.active").value("true"))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].title").value("Вибрані"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurantSelection.href").isNotEmpty())
                .andExpect(jsonPath("$._links.picture.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurants.href").isNotEmpty());

        mockMvc.perform(get("/api/restaurant_selections/100/picture")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture2"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/2.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/2"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/2"));

        TestTransaction.flagForCommit();
        TestTransaction.end();

        mockMvc.perform(get("/api/files/13")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }

    static String restaurantSelectionObject() throws JsonProcessingException {
        Map<String, Object> p = new HashMap<>();
        p.put("title", "Selection title");
        p.put("picture", "http://localhost:8080/api/files/2");
        p.put("restaurants", Arrays.asList("http://localhost:8080/api/restaurants/2", "http://localhost:8080/api/restaurants/3"));
        p.put("total", "2");

        Map<String, String> i18n = new HashMap<>();
        i18n.put("language", "http://localhost/api/languages/1");
        i18n.put("title", "Вибрані");

        p.put("i18ns", Arrays.asList(i18n));

        return new ObjectMapper().writeValueAsString(p);
    }
}
