package com.stol.system.reviewNotification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.model.ReviewNotification;
import com.stol.system.AbstractSystemTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReviewNotificationSystemTest extends AbstractSystemTest {
    private final static String FETCH_SQL_BY_ID = "select * from review_notification where reservation_id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void createNewReviewNotification() throws Exception {
        Map<String, Object> r = new HashMap<>();
        r.put("status", "CONFIRMED");
        r.put("dateTime", "2099-11-01T11:00:00");
        String re = new ObjectMapper().writeValueAsString(r);

        mockMvc.perform(patch("/api/reservations/6").content(re)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ReviewNotification o = jdbcTemplate.queryForObject(FETCH_SQL_BY_ID, new Object[]{6L}, new ReviewNotificationMapper());

        Assert.assertTrue(o.getActivationTime().isEqual(LocalDateTime.parse("2099-11-01T13:00")));
        Assert.assertTrue(o.getEndTime().isEqual(LocalDateTime.parse("2099-11-15T11:00")));
    }

    @Test
    public void findAllUsersReviewNotification() throws Exception {
        mockMvc.perform(get("/api/review_notifications/search/my")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reviewNotifications.length()").value(3))
                .andExpect(jsonPath("$._embedded.reviewNotifications[0].activationTime").value("2018-01-01T01:00:00"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[0].endTime").value("2099-01-14T01:00:00"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[0]._links.length()").value(5))
                .andExpect(jsonPath("$._embedded.reviewNotifications[0]._links.self.href").value("http://localhost/api/review_notifications/1"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[0]._links.reviewNotification.href").value("http://localhost/api/review_notifications/1{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[0]._links.reservation.href").value("http://localhost/api/review_notifications/1/reservation{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[0]._links.user.href").value("http://localhost/api/review_notifications/1/user{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[0]._links.restaurant.href").value("http://localhost/api/review_notifications/1/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[1].activationTime").value("2018-02-01T01:00:00"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[1].endTime").value("2099-02-14T01:00:00"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[1]._links.length()").value(5))
                .andExpect(jsonPath("$._embedded.reviewNotifications[1]._links.self.href").value("http://localhost/api/review_notifications/2"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[1]._links.reviewNotification.href").value("http://localhost/api/review_notifications/2{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[1]._links.reservation.href").value("http://localhost/api/review_notifications/2/reservation{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[1]._links.user.href").value("http://localhost/api/review_notifications/2/user{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[1]._links.restaurant.href").value("http://localhost/api/review_notifications/2/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[2].activationTime").value("2018-01-01T01:00:00"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[2].endTime").value("2099-02-14T01:00:00"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[2]._links.length()").value(5))
                .andExpect(jsonPath("$._embedded.reviewNotifications[2]._links.self.href").value("http://localhost/api/review_notifications/4"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[2]._links.reviewNotification.href").value("http://localhost/api/review_notifications/4{?projection" +
                        "}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[2]._links.reservation.href").value("http://localhost/api/review_notifications/4/reservation" +
                        "{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[2]._links.user.href").value("http://localhost/api/review_notifications/4/user{?projection}"))
                .andExpect(jsonPath("$._embedded.reviewNotifications[2]._links.restaurant.href").value("http://localhost/api/review_notifications/4/restaurant" +
                        "{?projection}"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(3))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    @Test
    public void disableUsersReviewNotification() throws Exception {
        mockMvc.perform(delete("/api/review_notifications/1")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isNoContent());
    }

    static class ReviewNotificationMapper implements RowMapper<ReviewNotification> {
        @Override
        public ReviewNotification mapRow(ResultSet rs, int rowNum) throws SQLException {
            ReviewNotification r = new ReviewNotification();
            r.setId((long) rs.getInt("id"));
            r.setActivationTime((rs.getTimestamp("activation_time").toLocalDateTime()));
            r.setEndTime((rs.getTimestamp("end_time").toLocalDateTime()));
            return r;
        }
    }
}
