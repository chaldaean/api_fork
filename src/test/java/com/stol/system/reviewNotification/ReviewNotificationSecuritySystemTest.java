package com.stol.system.reviewNotification;

import com.stol.system.SecurityAbstractSystemTest;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static com.stol.configuration.constant.Messages.DO_NOT_HAVE_ACCESS_TO_THIS_RESOURCE;
import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReviewNotificationSecuritySystemTest extends SecurityAbstractSystemTest {

    @Test
    @WithMockUser(roles = {"NOT_SET", "USER", "SYSTEM_ADMIN", "RESTAURANT_ADMIN"})
    public void createNewReviewNotificationNotAllowed() throws Exception {
        mockMvc.perform(post("/api/review_notifications"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void findAllActiveUsersReviewNotificationNoRoles() throws Exception {
        mockMvc.perform(get("/api/review_notifications/search/my"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET", "USER", "SYSTEM_ADMIN", "RESTAURANT_ADMIN"})
    public void disableUsersReviewNotificationNotOwner() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));

        mockMvc.perform(delete("/api/review_notifications/1")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Вы не являетесь владельцем ресурса!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(delete("/api/review_notifications/1")
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Вы не є власником ресурсу!"));

        mockMvc.perform(delete("/api/review_notifications/1")
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You are not the owner of this resource!"));
    }
}
