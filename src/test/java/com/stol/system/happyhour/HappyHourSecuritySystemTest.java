package com.stol.system.happyhour;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.SecurityAbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class HappyHourSecuritySystemTest extends SecurityAbstractSystemTest {
    private static String happyHour;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        happyHour = happyHourObject();
    }

    @Test
    @WithMockUser()
    public void getActiveHappyHoursForRestaurantWithUserRole() throws Exception {
        mockMvc.perform(get("/api/happy_hours/search/active")
                .param("restaurant", "http://localhost:8080/api/restaurants/1")
                .param("date", "2018-07-28")
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"RESTAURANT_ADMIN"})
    public void getHappyHoursHaveRestaurantAdminRole() throws Exception {
        mockMvc.perform(get("/api/happy_hours")
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"RESTAURANT_ADMIN"})
    public void createNewHappyHaveRestaurantAdminRole() throws Exception {
        mockMvc.perform(post("/api/happy_hours").content(happyHour)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(roles = {"RESTAURANT_ADMIN"})
    public void updateHappyHourHaveRestaurantAdminRole() throws Exception {
        mockMvc.perform(patch("/api/happy_hours/1").content(happyHour)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"SYSTEM_ADMIN"})
    public void getHappyHoursHaveAdminRole() throws Exception {
        mockMvc.perform(get("/api/happy_hours")
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"SYSTEM_ADMIN"})
    public void createNewHappyHaveAdminRole() throws Exception {
        mockMvc.perform(post("/api/happy_hours").content(happyHour)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(roles = {"SYSTEM_ADMIN"})
    public void updateHappyHourHaveAdminRole() throws Exception {
        mockMvc.perform(patch("/api/happy_hours/1").content(happyHour)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"NOT_SET, USER"})
    public void getHappyHoursWithoutProperRole() throws Exception {
        mockMvc.perform(get("/api/happy_hours")
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET, USER"})
    public void createNewHappyHourWithoutProperRole() throws Exception {
        mockMvc.perform(post("/api/happy_hours").content(happyHour)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET, USER"})
    public void updateHappyHourWithoutProperRole() throws Exception {
        mockMvc.perform(patch("/api/happy_hours/1").content(happyHour)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    public void getActiveHappyHoursForRestaurantWhenLoggedOut() throws Exception {
        mockMvc.perform(get("/api/happy_hours/search/active")
                .param("restaurant", "http://localhost:8080/api/restaurants/1")
                .param("date", "2018-07-28")
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());
    }

    private static String happyHourObject() throws JsonProcessingException {
        Map<String, Object> hh = new HashMap<>();
        hh.put("restaurant", "http://localhost:8080/api/restaurants/1");
        hh.put("activationDate", "2017-11-01");
        hh.put("expirationDate", "2017-12-05");
        hh.put("quantity", 100);
        hh.put("discount", 30);
        hh.put("times", Collections.singletonList("http://localhost:8080/api/times/3"));
        hh.put("active", true);
        return new ObjectMapper().writeValueAsString(hh);
    }
}
