package com.stol.system.happyhour;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static com.stol.configuration.constant.Messages.HAPPY_HOURS_NO_OVERLAPPING_TIME_ALLOWED;
import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class HappyHourSystemTest extends AbstractSystemTest {
    private static String happyHour;
    private static String reservation;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        happyHour = happyHourObject();
        reservation = reservationObject();
    }

    @Test
    public void createHappyHour() throws Exception {
        mockMvc.perform(post("/api/happy_hours").content(happyHour)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.quantity").value(1))
                .andExpect(jsonPath("$.activationDate").value("2017-11-01"))
                .andExpect(jsonPath("$.expirationDate").value("2017-12-05"))
                .andExpect(jsonPath("$.discount").value(30))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.happyHour.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty())
                .andExpect(jsonPath("$._links.times.href").isNotEmpty());
    }

    @Test
    public void createHappyHourNoOverlappingTimeAllowed() throws Exception {
        mockMvc.perform(post("/api/happy_hours").content(happyHourObjectOverlappingTime())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Счастливые часы не могут перекрываться в пределах одного ресторана!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(post("/api/happy_hours").content(happyHourObjectOverlappingTime())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Щасливі години не можут пересікатися для одного ресторану!"));

        mockMvc.perform(post("/api/happy_hours").content(happyHourObjectOverlappingTime())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Happy hours cannot have overlapping times withing the same restaurant!"));
    }

    @Test
    @WithMockUser()
    public void getActiveHappyHoursForRestaurantWithUserRole() throws Exception {
        mockMvc.perform(get("/api/happy_hours/search/active")
                .param("restaurant", "http://localhost:8080/api/restaurants/1")
                .param("date", "2099-07-28")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.happyHours.length()").value(1))
                .andExpect(jsonPath("$._embedded.happyHours[0].activationDate").value("2017-10-22"))
                .andExpect(jsonPath("$._embedded.happyHours[0].expirationDate").value("2099-12-22"))
                .andExpect(jsonPath("$._embedded.happyHours[0].quantity").value(1))
                .andExpect(jsonPath("$._embedded.happyHours[0].discount").value(20))
                .andExpect(jsonPath("$._embedded.happyHours[0].active").value(true))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());


        mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8));

        mockMvc.perform(get("/api/happy_hours/search/active")
                .param("restaurant", "http://localhost:8080/api/restaurants/1")
                .param("date", "2099-07-28")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$._embedded.happyHours.length()").value(0));
    }

    @Test
    public void findAllHappyHour() throws Exception {
        mockMvc.perform(get("/api/happy_hours")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.happyHours.length()").value(2))
                .andExpect(jsonPath("$._embedded.happyHours[0].activationDate").value("2017-10-22"))
                .andExpect(jsonPath("$._embedded.happyHours[0].expirationDate").value("2099-12-22"))
                .andExpect(jsonPath("$._embedded.happyHours[0].quantity").value(1))
                .andExpect(jsonPath("$._embedded.happyHours[0].discount").value(20))
                .andExpect(jsonPath("$._embedded.happyHours[0].active").value(true))
                .andExpect(jsonPath("$._embedded.happyHours[1].activationDate").isEmpty())
                .andExpect(jsonPath("$._embedded.happyHours[1].expirationDate").isEmpty())
                .andExpect(jsonPath("$._embedded.happyHours[1].quantity").value(50))
                .andExpect(jsonPath("$._embedded.happyHours[1].discount").value(30))
                .andExpect(jsonPath("$._embedded.happyHours[1].active").value(false))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());
    }

    @Test
    public void findByID() throws Exception {
        mockMvc.perform(get("/api/happy_hours/1")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.activationDate").value("2017-10-22"))
                .andExpect(jsonPath("$.expirationDate").value("2099-12-22"))
                .andExpect(jsonPath("$.quantity").value(1))
                .andExpect(jsonPath("$.discount").value(20))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());
    }

    @Test
    public void findHappyHourTimes() throws Exception {
        mockMvc.perform(get("/api/happy_hours/1/times")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.times.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[0].time").value("06:30"))
                .andExpect(jsonPath("$._embedded.times[1].time").value("11:00"));
    }

    @Test
    public void update() throws Exception {
        mockMvc.perform(patch("/api/happy_hours/1")
                .content("{\"times\": [\"http://localhost:8080/api/times/1\"], \"active\": false}")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.activationDate").value("2017-10-22"))
                .andExpect(jsonPath("$.expirationDate").value("2099-12-22"))
                .andExpect(jsonPath("$.quantity").value(1))
                .andExpect(jsonPath("$.discount").value(20))
                .andExpect(jsonPath("$.active").value(false))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());
    }

    private static String happyHourObject() throws JsonProcessingException {
        Map<String, Object> hh = new HashMap<>();
        hh.put("restaurant", "http://localhost:8080/api/restaurants/1");
        hh.put("activationDate", "2017-11-01");
        hh.put("expirationDate", "2017-12-05");
        hh.put("quantity", 1);
        hh.put("discount", 30);
        hh.put("times", Collections.singletonList("http://localhost:8080/api/times/3"));
        hh.put("active", true);
        return new ObjectMapper().writeValueAsString(hh);
    }

    private static String happyHourObjectOverlappingTime() throws JsonProcessingException {
        Map<String, Object> hh = new HashMap<>();
        hh.put("restaurant", "http://localhost:8080/api/restaurants/1");
        hh.put("activationDate", "2017-11-01");
        hh.put("expirationDate", "2017-12-05");
        hh.put("quantity", 1);
        hh.put("discount", 30);
        hh.put("times", Collections.singletonList("http://localhost:8080/api/times/1"));
        hh.put("active", true);
        return new ObjectMapper().writeValueAsString(hh);
    }

    private static String reservationObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/1");
        r.put("dateTime", "2099-11-01T11:00");
        r.put("guestNumber", "1");
        return new ObjectMapper().writeValueAsString(r);
    }
}
