package com.stol.system.cuisine;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.context.transaction.TestTransaction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CuisineSystemTest extends AbstractSystemTest {
    private static String cuisine;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        cuisine = cuisineObject();
    }

    @Test
    public void createNewCuisine() throws Exception {
        mockMvc.perform(post("/api/cuisines").content(cuisine)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.name").value("cuisineName1"))
                .andExpect(jsonPath("$.active").value(Boolean.TRUE))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].name").value("ім`я"))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.cuisine.href").isNotEmpty())
                .andExpect(jsonPath("$._links.icon.href").isNotEmpty());
    }

    @Test
    public void deleteCuisine() throws Exception {
        mockMvc.perform(delete("/api/cuisines/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/files/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getCuisineWithIconUrl() throws Exception {
        mockMvc.perform(get("/api/cuisines/1?projection=iconUrl")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.name").value("cuisine1"))
                .andExpect(jsonPath("$.iconUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$.active").value(Boolean.TRUE))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.cuisine.href").isNotEmpty())
                .andExpect(jsonPath("$._links.icon.href").isNotEmpty());
    }

    @Test
    public void getCuisineById() throws Exception {
        mockMvc.perform(get("/api/cuisines/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.name").value("cuisine1"))
                .andExpect(jsonPath("$.active").value(Boolean.TRUE))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/cuisines/1"))
                .andExpect(jsonPath("$._links.cuisine.href").value("http://localhost/api/cuisines/1{?projection}"))
                .andExpect(jsonPath("$._links.icon.href").value("http://localhost/api/cuisines/1/icon"));

        mockMvc.perform(get("/api/cuisines/1/icon")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture1"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/1.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/1"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/1"));
    }

    @Test
    public void getAllActiveCuisines() throws Exception {
        mockMvc.perform(get("/api/cuisines")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines.length()").value(7))
                .andExpect(jsonPath("$._embedded.cuisines[0].length()").value(4))
                .andExpect(jsonPath("$._embedded.cuisines[0].name").value("cuisine1"))
                .andExpect(jsonPath("$._embedded.cuisines[0]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[1].length()").value(4))
                .andExpect(jsonPath("$._embedded.cuisines[1].name").value("cuisine2"))
                .andExpect(jsonPath("$._embedded.cuisines[1]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[2].length()").value(4))
                .andExpect(jsonPath("$._embedded.cuisines[2].name").value("cuisine3"))
                .andExpect(jsonPath("$._embedded.cuisines[2]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[3].length()").value(4))
                .andExpect(jsonPath("$._embedded.cuisines[3].name").value("cuisine4"))
                .andExpect(jsonPath("$._embedded.cuisines[3]._links.length()").value(3))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void getAllCuisines() throws Exception {
        mockMvc.perform(get("/api/cuisines/search/all")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines.length()").value(8))
                .andExpect(jsonPath("$._embedded.cuisines[0].length()").value(4))
                .andExpect(jsonPath("$._embedded.cuisines[0].name").value("cuisine1"))
                .andExpect(jsonPath("$._embedded.cuisines[0]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[1].length()").value(4))
                .andExpect(jsonPath("$._embedded.cuisines[1].name").value("cuisine2"))
                .andExpect(jsonPath("$._embedded.cuisines[1]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[2].length()").value(4))
                .andExpect(jsonPath("$._embedded.cuisines[2].name").value("cuisine3"))
                .andExpect(jsonPath("$._embedded.cuisines[2]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[3].length()").value(4))
                .andExpect(jsonPath("$._embedded.cuisines[3].name").value("cuisine4"))
                .andExpect(jsonPath("$._embedded.cuisines[3]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[7].name").value("cuisine6"))
                .andExpect(jsonPath("$._embedded.cuisines[7]._links.length()").value(3))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$.page.length()").value(4));
    }


    @Test
    public void updateCuisine() throws Exception {
        mockMvc.perform(patch("/api/cuisines/100").content(cuisine)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.name").value("cuisineName1"))
                .andExpect(jsonPath("$.active").value(Boolean.TRUE))
                .andExpect(jsonPath("$.i18ns[0].name").value("ім`я"))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.cuisine.href").isNotEmpty())
                .andExpect(jsonPath("$._links.icon.href").isNotEmpty());

        mockMvc.perform(get("/api/cuisines/100/icon")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture17"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/17.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/17"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/17"));

        TestTransaction.flagForCommit();
        TestTransaction.end();

        mockMvc.perform(get("/api/files/18")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }

    static String cuisineObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("name", "cuisineName1");
        r.put("icon", "http://localhost:8080/api/files/17");

        Map<String, String> i18n = new HashMap<>();
        i18n.put("language", "http://localhost/api/languages/1");
        i18n.put("name", "ім`я");

        r.put("i18ns", Arrays.asList(i18n));

        return new ObjectMapper().writeValueAsString(r);
    }
}
