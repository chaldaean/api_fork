package com.stol.system.markertype;

import com.stol.system.SecurityAbstractSystemTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class MarkerTypeSecuritySystemTest extends SecurityAbstractSystemTest {

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void createMarkerTypeNoSystemAdminRole() throws Exception {
        mockMvc.perform(post("/api/marker_types").content("{}")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void updateMarkerTypeNoSystemAdminRole() throws Exception {
        mockMvc.perform(patch("/api/marker_types/1").content("{}")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void findMarkerTypesNoRole() throws Exception {
        mockMvc.perform(get("/api/marker_types"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void deleteMarkerTypeNoSystemAdminRole() throws Exception {
        mockMvc.perform(delete("/api/marker_type/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }
}
