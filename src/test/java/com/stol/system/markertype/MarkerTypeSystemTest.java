package com.stol.system.markertype;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class MarkerTypeSystemTest extends AbstractSystemTest {

    @Test
    public void createNewMarkerType() throws Exception {
        mockMvc.perform(post("/api/marker_types").content(markerTypeObject())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.name").value("openday"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.markerType.href").isNotEmpty())
                .andExpect(jsonPath("$._links.icon.href").isNotEmpty());
    }

    @Test
    public void deleteMarkerType() throws Exception {
        mockMvc.perform(delete("/api/marker_types/4")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());
    }

    @Test
    public void getAllMarkerTypes() throws Exception {
        mockMvc.perform(get("/api/marker_types")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes.length()").value(4))
                .andExpect(jsonPath("$._embedded.markerTypes[0].length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[0].name").value("discount"))
                .andExpect(jsonPath("$._embedded.markerTypes[0].active").value(true))
                .andExpect(jsonPath("$._embedded.markerTypes[0]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[1].length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[1].name").value("bonus"))
                .andExpect(jsonPath("$._embedded.markerTypes[1].active").value(true))
                .andExpect(jsonPath("$._embedded.markerTypes[1]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[2].length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[2].name").value("gift"))
                .andExpect(jsonPath("$._embedded.markerTypes[2].active").value(true))
                .andExpect(jsonPath("$._embedded.markerTypes[2]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[3].length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[3].name").value("smile"))
                .andExpect(jsonPath("$._embedded.markerTypes[3].active").value(true))
                .andExpect(jsonPath("$._embedded.markerTypes[3]._links.length()").value(3))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void getAllMarkerTypesWithIconUrl() throws Exception {
        mockMvc.perform(get("/api/marker_types?projection=iconUrl")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes.length()").value(4))
                .andExpect(jsonPath("$._embedded.markerTypes[0].name").value("discount"))
                .andExpect(jsonPath("$._embedded.markerTypes[0].iconUrl").value("/files/19.jpg"))
                .andExpect(jsonPath("$._embedded.markerTypes[0].active").value(true))
                .andExpect(jsonPath("$._embedded.markerTypes[0]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[1].length()").value(4))
                .andExpect(jsonPath("$._embedded.markerTypes[1].name").value("bonus"))
                .andExpect(jsonPath("$._embedded.markerTypes[1].iconUrl").value("/files/20.jpg"))
                .andExpect(jsonPath("$._embedded.markerTypes[1].active").value(true))
                .andExpect(jsonPath("$._embedded.markerTypes[1]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[2].length()").value(4))
                .andExpect(jsonPath("$._embedded.markerTypes[2].name").value("gift"))
                .andExpect(jsonPath("$._embedded.markerTypes[2].iconUrl").value("/files/21.jpg"))
                .andExpect(jsonPath("$._embedded.markerTypes[2].active").value(true))
                .andExpect(jsonPath("$._embedded.markerTypes[2]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.markerTypes[3].length()").value(4))
                .andExpect(jsonPath("$._embedded.markerTypes[3].name").value("smile"))
                .andExpect(jsonPath("$._embedded.markerTypes[3].iconUrl").value(""))
                .andExpect(jsonPath("$._embedded.markerTypes[3].active").value(true))
                .andExpect(jsonPath("$._embedded.markerTypes[3]._links.length()").value(3))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void getMarkerTypeById() throws Exception {
        mockMvc.perform(get("/api/marker_types/1")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.name").value("discount"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/marker_types/1"))
                .andExpect(jsonPath("$._links.markerType.href").value("http://localhost/api/marker_types/1{?projection}"))
                .andExpect(jsonPath("$._links.icon.href").value("http://localhost/api/marker_types/1/icon"));

        mockMvc.perform(get("/api/marker_types/1/icon")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture19"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/19.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/19"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/19"));
    }

    @Test
    public void updateMarkerType() throws Exception {
        mockMvc.perform(patch("/api/marker_types/1").content("{\"name\": \"discounted\"}")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.name").value("discounted"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/marker_types/1"))
                .andExpect(jsonPath("$._links.markerType.href").value("http://localhost/api/marker_types/1{?projection}"))
                .andExpect(jsonPath("$._links.icon.href").value("http://localhost/api/marker_types/1/icon"));
    }

    @Test
    public void deleteMarkerTypeIcon() throws Exception {
        mockMvc.perform(delete("/api/marker_types/1/icon")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/marker_types/1/icon")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound());
    }

    private String markerTypeObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("name", "openday");
        r.put("icon", "http://localhost:8080/api/files/22");
        return new ObjectMapper().writeValueAsString(r);
    }
}
