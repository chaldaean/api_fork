package com.stol.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.DBUnitRule;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import com.stol.SpringWebTest;
import com.stol.service.FileService;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;

import javax.sql.DataSource;
import java.util.Map;
import java.util.Objects;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@DBRider
@DataSet(value = "user_roles.xml,users.xml,data.xml,oauth.xml", executorId = "system", disableConstraints = true)
public abstract class AbstractSystemTest extends SpringWebTest {
    public static final String LOGIN = "380000000000";
    public static final String PASSWORD = "password";
    public static final String OAUTH_CONTENT = String.format("grant_type=password&username=%s&password=%s", LOGIN, PASSWORD);
    @Value("${stol.oauth2.client.application.client-id}")
    public String CLIENT_ID;
    @Value("${stol.oauth2.client.application.client-secret}")
    public String CLIENT_SECRET;
    @Value("${stol.file.upload-path}")
    protected String UPLOAD_PATH;

    @SpyBean
    protected FileService fileService;

    protected String token;

    @Rule
    public DBUnitRule dbUnitRule = DBUnitRule.instance("system", () -> context.getBean(DataSource.class).getConnection());

    @Before
    public void before() throws Exception {
        if (Objects.isNull(token)) {
            token = getAdminCredentialToken();
        }
        Mockito.doNothing().when(fileService).deleteFile(any());
    }

    public String getCredentialToken(String oauthContent) throws Exception {
        String oauth = mockMvc.perform(post("/oauth/token").content(oauthContent)
            .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
            .andReturn().getResponse().getContentAsString();
        return "Bearer " + new ObjectMapper().readValue(oauth, Map.class).get("access_token");
    }

    protected String getAdminCredentialToken() throws Exception {
        String oauth = mockMvc.perform(post("/oauth/token").content(OAUTH_CONTENT)
            .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
            .andReturn().getResponse().getContentAsString();
        return "Bearer " + new ObjectMapper().readValue(oauth, Map.class).get("access_token");
    }
}
