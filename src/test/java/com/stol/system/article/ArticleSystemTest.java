package com.stol.system.article;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.model.Article;
import com.stol.restdocs.ConstrainedFields;
import com.stol.system.AbstractSystemTest;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.restdocs.hypermedia.LinkDescriptor;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.transaction.TestTransaction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ArticleSystemTest extends AbstractSystemTest {
    private static String article;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        article = articleObject();
    }

    @Test
    public void createNewArticle() throws Exception {
        mockMvc.perform(post("/api/articles").content(article)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$.title").value("Title"))
                .andExpect(jsonPath("$.text").value("Some text"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].title").value("Заголовок"))
                .andExpect(jsonPath("$.i18ns[0].text").value("Текст"))
                .andExpect(jsonPath("$.i18ns[0]._links").isNotEmpty())
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.article.href").isNotEmpty())
                .andExpect(jsonPath("$._links.author.href").isNotEmpty())
                .andExpect(jsonPath("$._links.picture.href").isNotEmpty())
                .andDo(document("system/{class-name}/{method-name}",
                        requestFields(articleRequestFields),
                        responseFields(articleResponseFields),
                        links(halLinks(), articleRelationLinks)
                ));
    }

    @Test
    public void deleteArticle() throws Exception {
        mockMvc.perform(delete("/api/articles/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent())
                .andDo(document("system/{class-name}/{method-name}"));

        mockMvc.perform(get("/api/files/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getArticle() throws Exception {
        mockMvc.perform(get("/api/articles/1?projection=authorFullName")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.creationDate").value("2017-10-22T14:40:08"))
                .andExpect(jsonPath("$.title").value("Title"))
                .andExpect(jsonPath("$.text").value("Some text"))
                .andExpect(jsonPath("$.authorName").value("Admin Admin"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/articles/1"))
                .andExpect(jsonPath("$._links.article.href").value("http://localhost/api/articles/1{?projection}"))
                .andExpect(jsonPath("$._links.author.href").value("http://localhost/api/articles/1/author{?projection}"))
                .andExpect(jsonPath("$._links.picture.href").value("http://localhost/api/articles/1/picture"))
                .andDo(document("system/{class-name}/{method-name}",
                        responseFields(articleWithAuthorResponseFields),
                        links(halLinks(), articleRelationLinks)
                ));
    }

    @Test
    public void getAllActiveArticles() throws Exception {
        mockMvc.perform(get("/api/articles?page=0&size=10")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.articles.length()").value(2))
                .andExpect(jsonPath("$._embedded.articles[0].length()").value(6))
                .andExpect(jsonPath("$._embedded.articles[0].creationDate").value("2017-10-22T14:40:08"))
                .andExpect(jsonPath("$._embedded.articles[0].title").value("Title"))
                .andExpect(jsonPath("$._embedded.articles[0].text").value("Some text"))
                .andExpect(jsonPath("$._embedded.articles[0].active").value(true))
                .andExpect(jsonPath("$._embedded.articles[0]._links.length()").value(4))
                .andExpect(jsonPath("$._links.length()").value(3));
    }

    @Test
    public void getAllArticles() throws Exception {
        mockMvc.perform(get("/api/articles/search/all?page=0&size=10")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.articles.length()").value(3))
                .andExpect(jsonPath("$._embedded.articles[0].length()").value(6))
                .andExpect(jsonPath("$._embedded.articles[0].creationDate").value("2017-10-22T14:40:08"))
                .andExpect(jsonPath("$._embedded.articles[0].title").value("Title"))
                .andExpect(jsonPath("$._embedded.articles[0].text").value("Some text"))
                .andExpect(jsonPath("$._embedded.articles[0].active").value(true))
                .andExpect(jsonPath("$._embedded.articles[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.articles[2].length()").value(6))
                .andExpect(jsonPath("$._embedded.articles[2].creationDate").value("2017-10-22T14:40:08"))
                .andExpect(jsonPath("$._embedded.articles[2].title").value("Title2"))
                .andExpect(jsonPath("$._embedded.articles[2].text").value("Some text"))
                .andExpect(jsonPath("$._embedded.articles[2].active").value(false))
                .andExpect(jsonPath("$._embedded.articles[2]._links.length()").value(4))
                .andExpect(jsonPath("$._links.length()").value(1));
    }

    @Test
    public void updateArticle() throws Exception {
        mockMvc.perform(patch("/api/articles/100").content(article)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$.title").value("Title"))
                .andExpect(jsonPath("$.text").value("Some text"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].title").value("Заголовок"))
                .andExpect(jsonPath("$.i18ns[0].text").value("Текст"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.article.href").isNotEmpty())
                .andExpect(jsonPath("$._links.author.href").isNotEmpty())
                .andExpect(jsonPath("$._links.picture.href").isNotEmpty())
                .andDo(document("system/{class-name}/{method-name}",
                        requestFields(articleRequestFields),
                        responseFields(articleResponseFields),
                        links(halLinks(), articleRelationLinks)
                ));

        mockMvc.perform(get("/api/articles/100/picture")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture2"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/2.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/2"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/2"));

        TestTransaction.flagForCommit();
        TestTransaction.end();

        mockMvc.perform(get("/api/files/12")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }

    static String articleObject() throws JsonProcessingException {
        Map<String, Object> p = new HashMap<>();
        p.put("title", "Title");
        p.put("text", "Some text");
        p.put("picture", "http://localhost/api/files/2");
        p.put("active", "true");

        Map<String, String> i18n = new HashMap<>();
        i18n.put("language", "http://localhost/api/languages/1");
        i18n.put("title", "Заголовок");
        i18n.put("text", "Текст");

        p.put("i18ns", Arrays.asList(i18n));

        return new ObjectMapper().writeValueAsString(p);
    }

    private ConstrainedFields constrainedFields = new ConstrainedFields(Article.class);

    private FieldDescriptor[] articleResponseFields = {
            fieldWithPath("creationDate").description("Determines if article is active"),
            fieldWithPath("title").description("Title"),
            fieldWithPath("active").description("Html of the article"),
            fieldWithPath("text").description("Html of the article"),
            fieldWithPath("i18ns[].title").description("Localized title"),
            fieldWithPath("i18ns[].text").description("Localized text"),
            fieldWithPath("i18ns[]._links.language.href").description("Language resource"),
            subsectionWithPath("i18ns[]._links.language.templated").ignored(),
            subsectionWithPath("_links").description("Links to other resources")
    };

    private FieldDescriptor[] articleWithAuthorResponseFields = {
            fieldWithPath("creationDate").description("Determines if article is active"),
            fieldWithPath("title").description("Title"),
            fieldWithPath("active").description("Html of the article"),
            fieldWithPath("text").description("Html of the article"),
            fieldWithPath("authorName").description("Author name"),
            subsectionWithPath("_links").description("Links to other resources")
    };

    private FieldDescriptor[] articleRequestFields = {
            constrainedFields.withPath("active").description("Determines if article is active"),
            constrainedFields.withPath("text").description("Html of the article"),
            constrainedFields.withPath("title").description("Title"),
            constrainedFields.withPath("picture").description("Url of the picture"),
            fieldWithPath("i18ns[].title").description("Localized title"),
            fieldWithPath("i18ns[].text").description("Localized text"),
            fieldWithPath("i18ns[].language").description("Language resource"),

    };

    private LinkDescriptor[] articleRelationLinks = {
            linkWithRel("self").description("Url of the article resource"),
            linkWithRel("article").description("Url of the article resource"),
            linkWithRel("author").description("Url of the article resource"),
            linkWithRel("picture").description("Url of the article resource")
    };
}
