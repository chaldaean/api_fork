package com.stol.system.article;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.stol.system.SecurityAbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static com.stol.system.article.ArticleSystemTest.articleObject;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ArticleSecuritySystemTest extends SecurityAbstractSystemTest {
    private static String article;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        article = articleObject();
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void createArticleNoSystemAdminRole() throws Exception {
        mockMvc.perform(post("/api/articles").content(article)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void findArticleNoRole() throws Exception {
        mockMvc.perform(get("/api/articles/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void findAllArticlesNoRole() throws Exception {
        mockMvc.perform(get("/api/articles"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void deleteArticleNoSystemAdminRole() throws Exception {
        mockMvc.perform(delete("/api/articles/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }
}
