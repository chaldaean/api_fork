package com.stol.system.review;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.stol.model.Restaurant;
import com.stol.repository.RestaurantRepository;
import com.stol.system.AbstractSystemTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MvcResult;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static com.stol.configuration.constant.Messages.*;
import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReviewSystemTest extends AbstractSystemTest {

    @Autowired
    private RestaurantRepository restaurantRepository;

    private static String review;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        review = reviewObject();
    }

    @Test
    public void createNewReview() throws Exception {
        prepareCorrectReservation();

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.rating").value(3))
                .andExpect(jsonPath("$.comment").value("Review comment"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$._links.length()").value(5))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reviews/101"))
                .andExpect(jsonPath("$._links.review.href").value("http://localhost/api/reviews/101{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reviews/101/restaurant{?projection}"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reviews/101/reservation{?projection}"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/reviews/101/user{?projection}"));
    }

    @Test
    public void createNewReviewOnlyOneReviewAllowed() throws Exception {
        prepareCorrectReservation();

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Вы можете оставить только один отзыв об одном посещении!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(post("/api/reviews").content(review)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Ви можете залишити лише один відгук про відвідування!"));

        mockMvc.perform(post("/api/reviews").content(review)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You can create only one review for a reservation!"));
    }

    @Test
    public void createNewReviewUserIsNotOwnerOfReservation() throws Exception {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/1");
        r.put("reservation", "http://localhost:8080/api/reservation/2");
        r.put("rating", "4");
        r.put("comment", "Review comment!");
        String review =  new ObjectMapper().writeValueAsString(r);

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Вы должны иметь резервацию в ресторане, чтобы оставить отзыв!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Ви повинні мати резервацию в ресторані, щоб залишити відгук!"));

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You need to make a reservation in this restaurant to create a review!"));
    }

    @Test
    public void createNewReviewTimesOut() throws Exception {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/1");
        r.put("reservation", "http://localhost:8080/api/reservation/9");
        r.put("rating", "4");
        r.put("comment", "Review comment!");
        String review =  new ObjectMapper().writeValueAsString(r);

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Вы можете оставить отзыв только в течении 14-ти дней после посещения!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Ви можете залишити відгук тілько протягом 14 днів після візиту!"));

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You can leave a review within 14 days after the visit!"));
    }

    @Test
    public void createEmptyReviewIsNotAllowed() throws Exception {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/100");
        r.put("reservation", "http://localhost:8080/api/reservation/7");
        String review =  new ObjectMapper().writeValueAsString(r);

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Рейтинг или комментарий должны быть заполнены!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Рейтинг або коментарій мають бути заповнені!"));

        mockMvc.perform(post("/api/reviews").content(review)
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Rating or comment must be filled in!"));
    }

    @Test
    public void getReviewById() throws Exception {
        mockMvc.perform(get("/api/reviews/100")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.rating").value(5))
                .andExpect(jsonPath("$.comment").value("Comment100"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$._links.length()").value(5))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reviews/100"))
                .andExpect(jsonPath("$._links.review.href").value("http://localhost/api/reviews/100{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reviews/100/restaurant{?projection}"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reviews/100/reservation{?projection}"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/reviews/100/user{?projection}"));
    }

    @Test
    public void getAllReview() throws Exception {
        mockMvc.perform(get("/api/reviews")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reviews.length()").value(5))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reviews{?page,size,sort,projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/reviews"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/reviews/search"))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(5))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    @Test
    public void getAllReviewForRestaurant() throws Exception {
        mockMvc.perform(get("/api/reviews/search/findAllByRestaurant?restaurant=http://localhost:8080/api/restaurants/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reviews.length()").value(3))
                .andExpect(jsonPath("$._embedded.reviews[0].length()").value(4))
                .andExpect(jsonPath("$._embedded.reviews[1].length()").value(4))
                .andExpect(jsonPath("$._embedded.reviews[2].length()").value(4))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(3))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    @Test
    public void getAllUsersReview() throws Exception {
        mockMvc.perform(get("/api/reviews/search/findAllByUser?user=http://localhost:8080/api/users/2")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reviews.length()").value(3))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(3))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    @Test
    public void getUserReviewForRestaurant() throws Exception {
        mockMvc.perform(get("/api/reviews/search/my?restaurant=http://localhost:8080/api/restaurants/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reviews.length()").value(1))
                .andExpect(jsonPath("$._embedded.reviews[0].rating").value(4))
                .andExpect(jsonPath("$._embedded.reviews[0].comment").value("Comment1"))
                .andExpect(jsonPath("$._embedded.reviews[0].creationDate").value("2017-10-22T15:40:08"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(1))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    @Test
    public void updateRestaurantRating() throws Exception {
        String res1Id = prepareCorrectReservation();
        String res2Id = prepareCorrectReservation();
        String res3Id = prepareCorrectReservation();

        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/100");
        r.put("reservation", "http://localhost:8080/api/reservation/" + res1Id);
        r.put("rating", "5");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/reviews").content(objectMapper.writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());

        r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/100");
        r.put("reservation", "http://localhost:8080/api/reservation/" + res2Id);
        r.put("rating", "4");

        mockMvc.perform(post("/api/reviews").content(objectMapper.writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());

        r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/100");
        r.put("reservation", "http://localhost:8080/api/reservation/" + res3Id);
        r.put("rating", "1");

        mockMvc.perform(post("/api/reviews").content(objectMapper.writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());



        Double ratingAfter = restaurantRepository.findById(100L).orElse(new Restaurant()).getRating();

        Assert.assertEquals(3.3, ratingAfter, 0);
    }

    @Test
    public void updateRestaurantRatingWhenNotEnoughRatingsToAggregate() throws Exception {
        String res1Id = prepareCorrectReservation();
        String res2Id = prepareCorrectReservation();

        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/100");
        r.put("reservation", "http://localhost:8080/api/reservation/" + res1Id);
        r.put("rating", "5");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/reviews").content(objectMapper.writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());

        r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/100");
        r.put("reservation", "http://localhost:8080/api/reservation/" + res2Id);
        r.put("rating", "4");

        mockMvc.perform(post("/api/reviews").content(objectMapper.writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());

        Double ratingAfter = restaurantRepository.findById(100L).orElseThrow(EntityNotFoundException::new).getRating();

        Assert.assertNull(ratingAfter);
    }

    @Test
    public void deleteReview() throws Exception {
        mockMvc.perform(delete("/api/reviews/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());
    }

    private String prepareCorrectReservation() throws Exception {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/100");
        r.put("dateTime", LocalDate.now().plusDays(1).toString() + "T11:00");
        r.put("guestNumber", "2");
        String reservation = new ObjectMapper().writeValueAsString(r);

        MvcResult res = mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andReturn();

        r = new HashMap<>(Collections.singletonMap("status", "CONFIRMED"));
        reservation =  new ObjectMapper().writeValueAsString(r);

        String response = JsonPath.read(res.getResponse().getContentAsString(), "$._links.self.href");
        String id = response.substring(response.lastIndexOf("/") + 1);

        mockMvc.perform(patch("/api/reservations/" + id).content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        return id;
    }

    private static String reviewObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/100");
        r.put("reservation", "http://localhost:8080/api/reservation/7");
        r.put("rating", "3");
        r.put("comment", "Review comment");
        return new ObjectMapper().writeValueAsString(r);
    }
}
