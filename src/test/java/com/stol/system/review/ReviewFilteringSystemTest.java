package com.stol.system.review;

import com.stol.system.AbstractSystemTest;
import org.junit.Test;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReviewFilteringSystemTest extends AbstractSystemTest {
    @Test
    public void getReviewsWithFilterAndPaging() throws Exception {
        mockMvc.perform(get("/api/reviews?creationDate=2017-10-22&rating=4&restaurant.name=name1&user.surname=Admin&page=0&size=2&sort=creationDate,desc&projection=snippet")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reviews.length()").value(1))
                .andExpect(jsonPath("$._embedded.reviews[0].creationDate").value("2017-10-22T15:40:08"))
                .andExpect(jsonPath("$._embedded.reviews[0].rating").value(4))
                .andExpect(jsonPath("$._embedded.reviews[0].comment").value("Comment1"))
                .andExpect(jsonPath("$._embedded.reviews[0].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reviews[0].userName").value("Admin Admin"));
    }
}
