package com.stol.system.notification;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.messaging.Message;
import com.stol.model.MobileDevice;
import com.stol.model.property.ApplicationType;
import com.stol.model.user.User;
import com.stol.service.MobileDeviceService;
import com.stol.service.notification.MessageContent;
import com.stol.service.notification.NotificationContext;
import com.stol.service.notification.push.FirebaseService;
import com.stol.service.notification.sms.SmsNotifier;
import com.stol.service.sms.SmsSenderService;
import com.stol.system.AbstractSystemTest;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.TestPropertySource;

import java.util.*;

import static com.stol.model.property.ApplicationType.USER_APP;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestPropertySource(properties = { "stol.firebase.enable=true", "stol.turbosms.enable=true" })
public class NotificationSystemTest extends AbstractSystemTest {
    @SpyBean
    private FirebaseService firebaseService;

    @SpyBean
    private SmsSenderService smsSenderService;

    @SpyBean
    private SmsNotifier smsNotifier;

    @SpyBean
    private MobileDeviceService mobileDeviceService;

    @Test
    public void saveUsersDeviceTokenAlreadyExist() throws Exception {

        mockMvc.perform(post("/api/mobile_device").content(mobileDevice())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.device").value("Iphone X"))
                .andExpect(jsonPath("$.deviceToken").value("b8f3b2513caa"))
                .andExpect(jsonPath("$.applicationType").value(USER_APP.toString()))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/mobile_device/100"))
                .andExpect(jsonPath("$._links.mobileDevice.href").value("http://localhost/api/mobile_device/100"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/mobile_device/100/user{?projection}"));
    }

    @Test
    public void sendOnlyPushNotificationOnReservationStatusChanged() throws Exception {
        String r = new ObjectMapper().writeValueAsString(Collections.singletonMap("status", "CONFIRMED"));

        MobileDevice mobileDevice = new MobileDevice();
        mobileDevice.setDevice("iphone");
        mobileDevice.setDeviceToken(UUID.randomUUID().toString());

        when(mobileDeviceService.mobileDeviceByUserAndApplicationType(any(User.class), any(ApplicationType.class))).thenReturn(Optional.of(mobileDevice));

        doAnswer(u -> null).when(firebaseService).sendPushNotificationToDevice(any());
        doAnswer(u -> null).when(smsNotifier).sendNotification(any(NotificationContext.class), any(MessageContent.class));

        mockMvc.perform(patch("/api/reservations/6").content(r)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        verify(firebaseService, times(1)).sendPushNotificationToDevice(any(Message.class));
        verify(smsNotifier, never()).sendNotification(any(NotificationContext.class), any(MessageContent.class));
    }

    @Test
    public void sendSmsAndWebPushNotificationOnReservationStatusChanged() throws Exception {
        String r = new ObjectMapper().writeValueAsString(Collections.singletonMap("status", "REJECTED"));

        MobileDevice mobileDevice = new MobileDevice();
        mobileDevice.setDevice("web");
        mobileDevice.setDeviceToken(UUID.randomUUID().toString());

        when(mobileDeviceService.mobileDeviceByUserAndApplicationType(any(User.class), any(ApplicationType.class))).thenReturn(Optional.of(mobileDevice));

        doAnswer(u -> null).when(firebaseService).sendPushNotificationToDevice(any());
        doAnswer(u -> null).when(smsSenderService).sendSms(any(String.class), any(String.class));

        mockMvc.perform(patch("/api/reservations/6").content(r)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        verify(firebaseService, times(1)).sendPushNotificationToDevice(any(Message.class));
        verify(smsSenderService, times(1)).sendSms(any(String.class), any(String.class));
    }

    public static String mobileDevice() throws JsonProcessingException {
        Map<String, String> device = new HashMap<>();
        device.put("device", "Iphone X");
        device.put("deviceToken", "b8f3b2513caa");
        device.put("applicationType", USER_APP.toString());
        return new ObjectMapper().writeValueAsString(device);
    }
}
