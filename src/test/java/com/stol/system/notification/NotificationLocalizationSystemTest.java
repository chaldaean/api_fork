package com.stol.system.notification;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.service.notification.MessageContent;
import com.stol.service.notification.push.PushNotifier;
import com.stol.service.notification.sms.SmsNotifier;
import com.stol.system.AbstractSystemTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class NotificationLocalizationSystemTest extends AbstractSystemTest {
    private static String reservation;

    @SpyBean
    private PushNotifier pushNotifier;

    @MockBean
    private SmsNotifier smsNotifier;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        reservation = reservationObject();
    }

    @Before
    public void before() throws Exception {
        super.before();

        doAnswer(u -> null).when(pushNotifier).sendNotification(any(), any());
        doAnswer(u -> null).when(smsNotifier).sendNotification(any(), any());
    }

    @Test
    public void newReservation() throws Exception {
        mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Новый разерв!", msg.getTitle());
        Assert.assertEquals("У Вас новая заявка на бронирование.", msg.getBody());
    }

    @Test
    public void newReservation_uk() throws Exception {
        changeClientLanguageToUk();

        mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Нова резервація!", msg.getTitle());
        Assert.assertEquals("У Вас нова заявка на бронювання.", msg.getBody());
    }

    @Test
    public void newReservation_en() throws Exception {
        changeClientLanguageToEn();

        mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("New reservation!", msg.getTitle());
        Assert.assertEquals("You have a new reservation.", msg.getBody());
    }

    @Test
    public void canceledReservation() throws Exception {
        Map<String, Object> r = new HashMap<>();
        r.put("status", "CANCELED");

        mockMvc.perform(patch("/api/reservations/5").content(new ObjectMapper().writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Бронирование отменено!", msg.getTitle());
        Assert.assertEquals("Клиент отменил свое бронирование.", msg.getBody());
    }

    @Test
    public void canceledReservation_uk() throws Exception {
        changeClientLanguageToUk();

        Map<String, Object> r = new HashMap<>();
        r.put("status", "CANCELED");

        mockMvc.perform(patch("/api/reservations/5").content(new ObjectMapper().writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Бронювання скасоване!", msg.getTitle());
        Assert.assertEquals("Клієнт скасував бронювання.", msg.getBody());
    }

    @Test
    public void canceledReservation_en() throws Exception {
        changeClientLanguageToEn();

        Map<String, Object> r = new HashMap<>();
        r.put("status", "CANCELED");

        mockMvc.perform(patch("/api/reservations/5").content(new ObjectMapper().writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Reservation canceled!", msg.getTitle());
        Assert.assertEquals("Client has canceled the reservation.", msg.getBody());
    }

    @Test
    public void confirmedReservation() throws Exception {
        Map<String, Object> r = new HashMap<>();
        r.put("status", "CONFIRMED");

        mockMvc.perform(patch("/api/reservations/5").content(new ObjectMapper().writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Готово! \uD83D\uDECE", msg.getTitle());
        Assert.assertEquals("Ваше бронирование подтвержденно.", msg.getBody());
    }

    @Test
    public void confirmedReservation_uk() throws Exception {
        changeUserLanguageToUk();

        Map<String, Object> r = new HashMap<>();
        r.put("status", "CONFIRMED");

        mockMvc.perform(patch("/api/reservations/5").content(new ObjectMapper().writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Готово! \uD83D\uDECE", msg.getTitle());
        Assert.assertEquals("Ваше бронювання підтверджено.", msg.getBody());
    }

    @Test
    public void confirmedReservation_en() throws Exception {
        changeUserLanguageToEn();

        Map<String, Object> r = new HashMap<>();
        r.put("status", "CONFIRMED");

        mockMvc.perform(patch("/api/reservations/5").content(new ObjectMapper().writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Ready! \uD83D\uDECE", msg.getTitle());
        Assert.assertEquals("Your reservation confirmed.", msg.getBody());
    }

    @Test
    public void rejectedReservation() throws Exception {
        Map<String, Object> r = new HashMap<>();
        r.put("status", "REJECTED");

        mockMvc.perform(patch("/api/reservations/5").content(new ObjectMapper().writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Ваше бронирование отклонено!", msg.getTitle());
        Assert.assertEquals("К сожалению, у нас нету свободных столиков :(", msg.getBody());
    }

    @Test
    public void rejectedReservation_uk() throws Exception {
        changeUserLanguageToUk();

        Map<String, Object> r = new HashMap<>();
        r.put("status", "REJECTED");

        mockMvc.perform(patch("/api/reservations/5").content(new ObjectMapper().writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Ваше бронювання відхилене!", msg.getTitle());
        Assert.assertEquals("На жаль, у нас не залишилося вільних столів :(", msg.getBody());
    }

    @Test
    public void rejectedReservation_en() throws Exception {
        changeUserLanguageToEn();

        Map<String, Object> r = new HashMap<>();
        r.put("status", "REJECTED");

        mockMvc.perform(patch("/api/reservations/5").content(new ObjectMapper().writeValueAsString(r))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        ArgumentCaptor<MessageContent> messageArgument = ArgumentCaptor.forClass(MessageContent.class);
        verify(pushNotifier, times(1)).sendNotification(any(), messageArgument.capture());
        MessageContent msg = messageArgument.getValue();

        Assert.assertEquals("Your reservation rejected!", msg.getTitle());
        Assert.assertEquals("Unfortunately, we have no tables left :(", msg.getBody());
    }

    static String reservationObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/1");
        r.put("dateTime", "2099-11-01T11:00:00");
        r.put("guestNumber", "2");
        r.put("comment", "Comment!");
        return new ObjectMapper().writeValueAsString(r);
    }

    protected void changeClientLanguageToUk() throws Exception {
        changeLanguage(1, 1);
    }

    protected void changeClientLanguageToEn() throws Exception {
        changeLanguage(1, 2);
    }

    protected void changeUserLanguageToUk() throws Exception {
        changeLanguage(3, 1);
    }

    protected void changeUserLanguageToEn() throws Exception {
        changeLanguage(3, 2);
    }

    protected void changeLanguage(int userId, int languageId) throws Exception {
        Map<String, String> userData = new HashMap<>();
        userData.put("language", "http://localhost/api/languages/" + languageId);

        mockMvc.perform(patch("/api/users/{0}", userId).content(new ObjectMapper().writeValueAsString(userData))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

    }
}
