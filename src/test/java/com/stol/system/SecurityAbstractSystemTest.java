package com.stol.system;

import com.stol.configuration.ResourceServerConfigurationTest;
import org.junit.Before;
import org.springframework.context.annotation.Import;

@Import(ResourceServerConfigurationTest.class)
public abstract class SecurityAbstractSystemTest extends AbstractSystemTest {
    public static final String ACCESS_IS_DENIED = "Access is denied";
    public static final String ACCESS_DENIED = "access_denied";

    @Before
    public void before() {
    }
}
