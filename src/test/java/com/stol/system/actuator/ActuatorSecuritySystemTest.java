package com.stol.system.actuator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.SecurityAbstractSystemTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import java.util.Map;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(properties = "server.port=8080")
public class ActuatorSecuritySystemTest extends SecurityAbstractSystemTest {
    @Value("${stol.oauth2.client.actuator.client-id}")
    public String CLIENT_ID;
    @Value("${stol.oauth2.client.actuator.client-secret}")
    public String CLIENT_SECRET;

    @Test
    public void getActuatorWithNoToken() throws Exception {
        mockMvc.perform(get("/actuator")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isUnauthorized())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value("unauthorized"))
                .andExpect(jsonPath("$.error_description").value("Full authentication is required to access this resource"));
    }

    @Test
    public void getActuatorWithInvalidScope() throws Exception {
        mockMvc.perform(get("/actuator")
                .contentType(APPLICATION_JSON_UTF8)
                .header(AUTHORIZATION, getAdminCredentialToken()))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.error").value("insufficient_scope"))
                .andExpect(jsonPath("$.error_description").isNotEmpty())
                .andExpect(jsonPath("$.scope").isNotEmpty());
    }

    @Test
    public void getActuatorWithValidScope() throws Exception {
        mockMvc.perform(get("/actuator")
                .header(AUTHORIZATION, getActuatorCredentialToken()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/vnd.spring-boot.actuator.v2+json;charset=UTF-8"))
                .andExpect(jsonPath("$._links").exists());
    }

    @Test
    public void actuatorCredentialsNotAllowedForApi() throws Exception {
        mockMvc.perform(get("/api/user/current")
                .header(AUTHORIZATION, getActuatorCredentialToken())
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    private String getActuatorCredentialToken() throws Exception {
        String oauth = mockMvc.perform(post("/oauth/token")
                .content("grant_type=client_credentials")
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andReturn().getResponse().getContentAsString();
        return "Bearer " + new ObjectMapper().readValue(oauth, Map.class).get("access_token");
    }
}
