package com.stol.system.mediaFile;

import com.stol.system.AbstractSystemTest;
import org.junit.Test;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class MediaFileSystemTest extends AbstractSystemTest {
    @Test
    public void getFileById() throws Exception {
        mockMvc.perform(get("/api/files/1")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture1"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/1.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/1"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/1"));
    }

    @Test
    public void deleteFileById() throws Exception {
        mockMvc.perform(delete("/api/files/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());
    }
}
