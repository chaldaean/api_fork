package com.stol.system.mediaFile;

import com.jayway.jsonpath.JsonPath;
import com.stol.repository.MediaFileRepository;
import com.stol.system.AbstractSystemTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8_VALUE;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class FileUploadControllerSystemTest extends AbstractSystemTest {
    @Autowired
    MediaFileRepository fileRepository;

    @Test
    public void uploadFileAndSaveAndDelete() throws Exception {
        MockMultipartFile file = new MockMultipartFile("data", "filename.jpg", "image/jpeg", "some bytes".getBytes(Charset.forName("UTF-8")));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/upload")
                .file(file)
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$._embedded.mediaFiles.length()").value(1))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].length()").value(5))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].name").isNotEmpty())
                .andExpect(jsonPath("$._embedded.mediaFiles[0].extension").value("jpg"))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].url").isNotEmpty())
                .andExpect(jsonPath("$._embedded.mediaFiles[0]._links.length()").value(1))
                .andExpect(jsonPath("$._embedded.mediaFiles[0]._links.self.href").isNotEmpty())
                .andReturn();

        String directory = JsonPath.read(mvcResult.getResponse().getContentAsString(), "$._embedded.mediaFiles[0].url");
        Path path = Paths.get(UPLOAD_PATH).resolve(directory.substring(directory.lastIndexOf("/") + 1));
        Assert.assertTrue(Files.exists(path));

        String url = JsonPath.read(mvcResult.getResponse().getContentAsString(), "$._embedded.mediaFiles[0]._links.self.href");
        String id = url.substring(url.lastIndexOf("/") + 1);
        Mockito.doCallRealMethod().when(fileService).deleteFile(any());
        mockMvc.perform(delete("/api/files/" + id)
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());

        TestTransaction.flagForCommit();
        TestTransaction.end();

        Assert.assertFalse(Files.exists(path));
    }
}
