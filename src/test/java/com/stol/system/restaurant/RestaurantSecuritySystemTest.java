package com.stol.system.restaurant;

import com.stol.system.SecurityAbstractSystemTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestaurantSecuritySystemTest extends SecurityAbstractSystemTest {

    @Test
    @WithMockUser(roles = {"USER"})
    public void createNewRestaurantNoRestaurantAdminOrSystemRole() throws Exception {
        mockMvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void findRestaurantNoRole() throws Exception {
        mockMvc.perform(get("/api/restaurants/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void getRestaurantsMenuNoRole() throws Exception {
        mockMvc.perform(get("/api/restaurants/1/menu"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void findAllRestaurantsNoRole() throws Exception {
        mockMvc.perform(get("/api/restaurants/search?page=0&sorting=rating&size=10&projection=feed"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void deleteRestaurantNoRestaurantAdminOrSystemRole() throws Exception {
        mockMvc.perform(delete("/api/restaurants/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void findAllRestaurantForLoggedInUserNoRestaurantAdminOrSystemRole() throws Exception {
        mockMvc.perform(get("/api/restaurants/search/my"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }
}
