package com.stol.system.restaurant;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.stol.model.property.UserStatus;
import com.stol.system.AbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Boolean.TRUE;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestaurantSystemTest extends AbstractSystemTest {
    private static String restaurant;
    private static String marker;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        restaurant = restaurantObject();
        marker = markerObject();
    }

    @Test
    public void createNewRestaurant() throws Exception {

        mockMvc.perform(get("/api/users/1")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.addRestaurantRequest.restaurantName").value("Fine restaurant"));

        mockMvc.perform(post("/api/restaurants").content(restaurant)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(15))
                .andExpect(jsonPath("$.name").value("Restaurant123"))
                .andExpect(jsonPath("$.address").value("address 123"))
                .andExpect(jsonPath("$.latitude").value("2.32"))
                .andExpect(jsonPath("$.longitude").value("3.044"))
                .andExpect(jsonPath("$.description").value("Description Description"))
                .andExpect(jsonPath("$.phoneNumber").value("+380972223344"))
                .andExpect(jsonPath("$.breakfast").value("Breakfast/Business lunch"))
                .andExpect(jsonPath("$.averageTicket").value(500.6))
                .andExpect(jsonPath("$.parking").value("Yes"))
                .andExpect(jsonPath("$.paymentMethod").value("Visa/MC"))
                .andExpect(jsonPath("$.rating").value(4.5))
                .andExpect(jsonPath("$.openHours.length()").value(3))
                .andExpect(jsonPath("$.openHours[0].weekday").value(1))
                .andExpect(jsonPath("$.openHours[0].openTime").value("10:00:00"))
                .andExpect(jsonPath("$.openHours[0].closeTime").value("23:00:00"))
                .andExpect(jsonPath("$.openHours[1].weekday").value(6))
                .andExpect(jsonPath("$.openHours[1].openTime").value("10:00:00"))
                .andExpect(jsonPath("$.openHours[1].closeTime").value("02:00:00"))
                .andExpect(jsonPath("$.openHours[2].weekday").value(7))
                .andExpect(jsonPath("$.openHours[2].openTime").value("10:00:00"))
                .andExpect(jsonPath("$.openHours[2].closeTime").value("01:00:00"))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].name").value("Ресторан123"))
                .andExpect(jsonPath("$.active").value("true"))
                .andExpect(jsonPath("$._links.length()").value(12))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurants/101"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/restaurants/101{?projection}"))
                .andExpect(jsonPath("$._links.happyHours.href").value("http://localhost/api/restaurants/101/happy_hours{?projection}"))
                .andExpect(jsonPath("$._links.menu.href").value("http://localhost/api/restaurants/101/menu"))
                .andExpect(jsonPath("$._links.tags.href").value("http://localhost/api/restaurants/101/tags{?projection}"))
                .andExpect(jsonPath("$._links.cuisines.href").value("http://localhost/api/restaurants/101/cuisines{?projection}"))
                .andExpect(jsonPath("$._links.topPicture.href").value("http://localhost/api/restaurants/101/top_picture"))
                .andExpect(jsonPath("$._links.pictures.href").value("http://localhost/api/restaurants/101/pictures"))
                .andExpect(jsonPath("$._links.analogous.href").value("http://localhost/api/restaurants/101/analogous{?projection}"))
                .andExpect(jsonPath("$._links.marker.href").value("http://localhost/api/restaurants/101/marker"))
                .andExpect(jsonPath("$._links.owners.href").value("http://localhost/api/restaurants/101/owners{?projection}"))
                .andExpect(jsonPath("$._links.promotions.href").value("http://localhost/api/restaurants/101/promotions{?projection}"))
                .andReturn();

        mockMvc.perform(get("/api/users/1")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.addRestaurantRequest").isEmpty());

        mockMvc.perform(get("/api/restaurants/101/tags")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.tags.length()").value(2))
                .andExpect(jsonPath("$._embedded.tags[0].name").value("tag1"))
                .andExpect(jsonPath("$._embedded.tags[1].name").value("tag3"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());

        mockMvc.perform(get("/api/restaurants/101/cuisines")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.cuisines.length()").value(2))
                .andExpect(jsonPath("$._embedded.cuisines[0].name").value("cuisine2"))
                .andExpect(jsonPath("$._embedded.cuisines[1].name").value("cuisine3"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());

        mockMvc.perform(get("/api/restaurants/101/analogous")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].address").value("address1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name2"))
                .andExpect(jsonPath("$._embedded.restaurants[1].address").value("address2"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());

        mockMvc.perform(get("/api/restaurants/101/owners")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.users.length()").value(1))
                .andExpect(jsonPath("$._embedded.users[0].name").value("Admin"))
                .andExpect(jsonPath("$._embedded.users[0].surname").value("Admin"))
                .andExpect(jsonPath("$._embedded.users[0].email").value("admin@gmail.com"))
                .andExpect(jsonPath("$._embedded.users[0].phoneNumber").value(LOGIN))
                .andExpect(jsonPath("$._embedded.users[0].status").value(UserStatus.ENABLE.toString()))
                .andExpect(jsonPath("$._embedded.users[0]._links.length()").value(9))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());

        mockMvc.perform(get("/api/restaurants/101/pictures")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.mediaFiles.length()").value(3))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].name").value("picture2"))
                .andExpect(jsonPath("$._embedded.mediaFiles[1].name").value("picture3"))
                .andExpect(jsonPath("$._embedded.mediaFiles[2].name").value("picture4"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());

        mockMvc.perform(get("/api/restaurants/101/top_picture")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture2"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/2.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/2"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/2"));
    }

    @Test
    public void findAllActiveRestaurants() throws Exception {
        mockMvc.perform(get("/api/restaurants?projection=snippet&sort=name,asc")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(8))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name100"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("name2"))
                .andExpect(jsonPath("$._embedded.restaurants[3].name").value("name3"))
                .andExpect(jsonPath("$._embedded.restaurants[4].name").value("name4"))
                .andExpect(jsonPath("$._embedded.restaurants[5].name").value("name5"))
                .andExpect(jsonPath("$._embedded.restaurants[6].name").value("name6"))
                .andExpect(jsonPath("$._embedded.restaurants[7].name").value("name7"))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurants{?projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/restaurants"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/restaurants/search"))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(8))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    @Test
    public void findAllRestaurant() throws Exception {
        mockMvc.perform(get("/api/restaurants/search/all?sort=name,asc")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(10))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name100"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("name2"))
                .andExpect(jsonPath("$._embedded.restaurants[3].name").value("name3"))
                .andExpect(jsonPath("$._embedded.restaurants[4].name").value("name4"))
                .andExpect(jsonPath("$._embedded.restaurants[5].name").value("name5"))
                .andExpect(jsonPath("$._embedded.restaurants[6].name").value("name6"))
                .andExpect(jsonPath("$._embedded.restaurants[7].name").value("name7"))
                .andExpect(jsonPath("$._embedded.restaurants[8].name").value("name8"))
                .andExpect(jsonPath("$._embedded.restaurants[9].name").value("name9"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurants/search/all?page=0&size=20&sort=name,asc"))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(10))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));
    }

    @Test
    public void getRestaurantByIdProjectionSnippet() throws Exception {
        mockMvc.perform(get("/api/restaurants/1?projection=snippet")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(7))
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$.tags").exists())
                .andExpect(jsonPath("$.averageTicket").value(400.0))
                .andExpect(jsonPath("$.cuisines.length()").value(0))
                .andExpect(jsonPath("$.topPicture").isNotEmpty())
                .andExpect(jsonPath("$.rating").value(4.5))
                .andExpect(jsonPath("$._links.length()").value(12))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty())
                .andExpect(jsonPath("$._links.menu.href").isNotEmpty())
                .andExpect(jsonPath("$._links.tags.href").isNotEmpty())
                .andExpect(jsonPath("$._links.cuisines.href").isNotEmpty())
                .andExpect(jsonPath("$._links.pictures.href").isNotEmpty())
                .andExpect(jsonPath("$._links.topPicture.href").value("http://localhost/api/restaurants/1/top_picture"))
                .andExpect(jsonPath("$._links.analogous.href").isNotEmpty())
                .andExpect(jsonPath("$._links.marker.href").isNotEmpty())
                .andExpect(jsonPath("$._links.owners.href").isNotEmpty())
                .andExpect(jsonPath("$._links.happyHours.href").isNotEmpty())
                .andExpect(jsonPath("$._links.promotions.href").isNotEmpty());
    }

    @Test
    public void findAllRestaurantsByNameContains() throws Exception {
        mockMvc.perform(get("/api/restaurants/search/findAllByNameContains?text=name5")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name5"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());
    }

    @Test
    public void findAllRestaurantsForLoggedInUser() throws Exception {
        mockMvc.perform(get("/api/restaurants/search/my")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(6))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());
    }

    @Test
    public void getRestaurantProjectionPart() throws Exception {
        mockMvc.perform(get("/api/restaurants/1?projection=name")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$.ownerNames.length()").value(1))
                .andExpect(jsonPath("$.ownerNames[0]").value("Admin Admin"))
                .andExpect(jsonPath("$.topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$.active").value(TRUE))
                .andExpect(jsonPath("$._links.length()").value(12))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty())
                .andExpect(jsonPath("$._links.menu.href").isNotEmpty())
                .andExpect(jsonPath("$._links.tags.href").isNotEmpty())
                .andExpect(jsonPath("$._links.cuisines.href").isNotEmpty())
                .andExpect(jsonPath("$._links.topPicture.href").value("http://localhost/api/restaurants/1/top_picture"))
                .andExpect(jsonPath("$._links.pictures.href").isNotEmpty())
                .andExpect(jsonPath("$._links.analogous.href").isNotEmpty())
                .andExpect(jsonPath("$._links.marker.href").isNotEmpty())
                .andExpect(jsonPath("$._links.owners.href").isNotEmpty())
                .andExpect(jsonPath("$._links.happyHours.href").isNotEmpty())
                .andExpect(jsonPath("$._links.promotions.href").isNotEmpty());
    }

    @Test
    public void getRestaurantProjectionPartWithLocale() throws Exception {
        mockMvc.perform(get("/api/restaurants/1?projection=name")
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, "uk")
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("ім'я"))
                .andExpect(jsonPath("$.ownerNames.length()").value(1))
                .andExpect(jsonPath("$.ownerNames[0]").value("Admin Admin"))
                .andExpect(jsonPath("$.topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$.active").value(TRUE))
                .andExpect(jsonPath("$._links.length()").value(12))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.restaurant.href").isNotEmpty())
                .andExpect(jsonPath("$._links.menu.href").isNotEmpty())
                .andExpect(jsonPath("$._links.tags.href").isNotEmpty())
                .andExpect(jsonPath("$._links.cuisines.href").isNotEmpty())
                .andExpect(jsonPath("$._links.topPicture.href").value("http://localhost/api/restaurants/1/top_picture"))
                .andExpect(jsonPath("$._links.pictures.href").isNotEmpty())
                .andExpect(jsonPath("$._links.analogous.href").isNotEmpty())
                .andExpect(jsonPath("$._links.marker.href").isNotEmpty())
                .andExpect(jsonPath("$._links.owners.href").isNotEmpty())
                .andExpect(jsonPath("$._links.happyHours.href").isNotEmpty())
                .andExpect(jsonPath("$._links.promotions.href").isNotEmpty());
    }


    @Test
    public void updateRestaurant() throws Exception {
        mockMvc.perform(patch("/api/restaurants/100").content(restaurant)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(15))
                .andExpect(jsonPath("$.name").value("Restaurant123"))
                .andExpect(jsonPath("$.address").value("address 123"))
                .andExpect(jsonPath("$.latitude").value("2.32"))
                .andExpect(jsonPath("$.longitude").value("3.044"))
                .andExpect(jsonPath("$.description").value("Description Description"))
                .andExpect(jsonPath("$.phoneNumber").value("+380972223344"))
                .andExpect(jsonPath("$.breakfast").value("Breakfast/Business lunch"))
                .andExpect(jsonPath("$.averageTicket").value(500.6))
                .andExpect(jsonPath("$.parking").value("Yes"))
                .andExpect(jsonPath("$.paymentMethod").value("Visa/MC"))
                .andExpect(jsonPath("$.rating").value(4.5))
                .andExpect(jsonPath("$.active").value("true"))
                .andExpect(jsonPath("$.openHours.length()").value(3))
                .andExpect(jsonPath("$.openHours[0].weekday").value(1))
                .andExpect(jsonPath("$.openHours[0].openTime").value("10:00:00"))
                .andExpect(jsonPath("$.openHours[0].closeTime").value("23:00:00"))
                .andExpect(jsonPath("$.openHours[1].weekday").value(6))
                .andExpect(jsonPath("$.openHours[1].openTime").value("10:00:00"))
                .andExpect(jsonPath("$.openHours[1].closeTime").value("02:00:00"))
                .andExpect(jsonPath("$.openHours[2].weekday").value(7))
                .andExpect(jsonPath("$.openHours[2].openTime").value("10:00:00"))
                .andExpect(jsonPath("$.openHours[2].closeTime").value("01:00:00"))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].name").value("Ресторан123"))
                .andExpect(jsonPath("$._links.length()").value(12))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurants/100"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/restaurants/100{?projection}"))
                .andExpect(jsonPath("$._links.menu.href").value("http://localhost/api/restaurants/100/menu"))
                .andExpect(jsonPath("$._links.tags.href").value("http://localhost/api/restaurants/100/tags{?projection}"))
                .andExpect(jsonPath("$._links.cuisines.href").value("http://localhost/api/restaurants/100/cuisines{?projection}"))
                .andExpect(jsonPath("$._links.topPicture.href").value("http://localhost/api/restaurants/100/top_picture"))
                .andExpect(jsonPath("$._links.pictures.href").value("http://localhost/api/restaurants/100/pictures"))
                .andExpect(jsonPath("$._links.analogous.href").value("http://localhost/api/restaurants/100/analogous{?projection}"))
                .andExpect(jsonPath("$._links.marker.href").value("http://localhost/api/restaurants/100/marker"))
                .andExpect(jsonPath("$._links.owners.href").value("http://localhost/api/restaurants/100/owners{?projection}"))
                .andExpect(jsonPath("$._links.promotions.href").value("http://localhost/api/restaurants/100/promotions{?projection}"));



        MvcResult res = mockMvc.perform(post("/api/markers").content(marker)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andReturn();

        String markerUrl = JsonPath.read(res.getResponse().getContentAsString(), "$._links.self.href");

        Map<String, Object> markerPatch = new HashMap<>();
        markerPatch.put("marker", markerUrl.replace("localhost", "localhost:8080"));

        mockMvc.perform(patch("/api/restaurants/100")
                .content(new ObjectMapper().writeValueAsString(markerPatch))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/restaurants/100/marker")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.title").value("10%"));

        mockMvc.perform(get("/api/restaurants/100/top_picture")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture2"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/2.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/2"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/2"));

        TestTransaction.flagForCommit();
        TestTransaction.end();

        mockMvc.perform(get("/api/files/14")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));

        mockMvc.perform(get("/api/restaurants/100/pictures")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.mediaFiles.length()").value(3))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].name").value("picture2"))
                .andExpect(jsonPath("$._embedded.mediaFiles[1].name").value("picture3"))
                .andExpect(jsonPath("$._embedded.mediaFiles[2].name").value("picture4"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty());

        mockMvc.perform(get("/api/files/15")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }

    @Test
    public void deleteRestaurant() throws Exception {
        mockMvc.perform(delete("/api/restaurants/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/files/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/api/files/16")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound());
    }

    static String markerObject() throws JsonProcessingException {
        Map<String, Object> m = new HashMap<>();

        m.put("title", "10%");
        m.put("type", "http://localhost:8080/api/marker_type/1");

        return new ObjectMapper().writeValueAsString(m);
    }

    static String restaurantObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("name", "Restaurant123");
        r.put("topPicture", "http://localhost:8080/api/files/2");
        r.put("pictures", Arrays.asList("http://localhost:8080/api/files/2",
                "http://localhost:8080/api/files/3", "http://localhost:8080/api/files/4"));
        r.put("address", "address 123");
        r.put("latitude", "2.32");
        r.put("longitude", "3.044");
        r.put("openTime", "09:00:00");
        r.put("closeTime", "20:00:00");
        r.put("tags", Arrays.asList("http://localhost:8080/api/tags/1", "http://localhost:8080/api/tags/3"));
        r.put("cuisines", Arrays.asList("http://localhost:8080/api/cuisines/2", "http://localhost:8080/api/cuisines/3"));
        r.put("description", "Description Description");
        r.put("menu", "http://localhost:8080/api/files/7");
        r.put("phoneNumber", "+380972223344");
        r.put("breakfast", "Breakfast/Business lunch");
        r.put("averageTicket", "500.60");
        r.put("parking", "Yes");
        r.put("paymentMethod", "Visa/MC");
        r.put("rating", "4.5");
        r.put("analogous", Arrays.asList("http://localhost:8080/api/restaurants/1", "http://localhost:8080/api/restaurants/2"));
        r.put("owners", Collections.singletonList("http://localhost:8080/api/users/1"));
        r.put("active", "true");

        Map<String, String> openHour1 = new HashMap<>();
        openHour1.put("openTime", "10:00:00");
        openHour1.put("closeTime", "02:00:00");
        openHour1.put("weekday", "6");


        Map<String, String> openHour2 = new HashMap<>();
        openHour2.put("openTime", "10:00:00");
        openHour2.put("closeTime", "23:00:00");
        openHour2.put("weekday", "1");

        Map<String, String> openHour3 = new HashMap<>();
        openHour3.put("openTime", "10:00:00");
        openHour3.put("closeTime", "01:00:00");
        openHour3.put("weekday", "7");

        r.put("openHours", Arrays.asList(openHour1, openHour2, openHour3));

        Map<String, String> i18n = new HashMap<>();
        i18n.put("language_id", "1");
        i18n.put("name", "Ресторан123");

        r.put("i18ns", Arrays.asList(i18n));

        return new ObjectMapper().writeValueAsString(r);
    }
}
