package com.stol.system.restaurant;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.Test;
import org.springframework.http.HttpHeaders;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static com.stol.configuration.constant.Messages.DO_NOT_ALLOW_REMOVE_YOURSELF;
import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static com.stol.system.SecurityAbstractSystemTest.ACCESS_DENIED;
import static com.stol.system.SecurityAbstractSystemTest.ACCESS_IS_DENIED;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestaurantOwnerSecuritySystemTest extends AbstractSystemTest {
    @Test
    public void updateRestaurantNotOwnerAndNoAdminRole() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/restaurants/1").content(updateRestaurant())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    public void updateRestaurantIsNotRestaurantOwnerHasSystemAdminRole() throws Exception {
        mockMvc.perform(patch("/api/restaurants/6").content(updateRestaurant())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(15));
    }

    @Test
    public void updateRestaurantIsRestaurantOwnerNoSystemAdminRole() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/restaurants/6").content(updateRestaurant())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(15));
    }

    @Test
    public void updateRestaurantIsRestaurantOwnerNoSystemAdminRoleDeleteItselfFromOwner() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/restaurants/6").content(updateRestaurantInvalidOwner())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Вы не можете удалить себя из владельцев ресторана!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void updateRestaurantIsRestaurantOwnerNoSystemAdminRoleDeleteItselfFromOwner_uk() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));

        mockMvc.perform(patch("/api/restaurants/6").content(updateRestaurantInvalidOwner())
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Ви не можете видалити себе із власників реторану!"));
    }

    @Test
    public void updateRestaurantIsRestaurantOwnerNoSystemAdminRoleDeleteItselfFromOwner_en() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));

        mockMvc.perform(patch("/api/restaurants/6").content(updateRestaurantInvalidOwner())
                .header(AUTHORIZATION, token)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You can't remove yourself from restaurant's owners!"));
    }

    @Test
    public void updateRestaurantIsRestaurantOwnerNoSystemAdminRoleAddNewOwner() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        mockMvc.perform(patch("/api/restaurants/6").content(updateRestaurantAddOwner())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(15));
    }

    private String updateRestaurant() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("name", "Restaurant123");
        return new ObjectMapper().writeValueAsString(r);
    }

    private String updateRestaurantInvalidOwner() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("name", "Restaurant123");
        r.put("owners", Collections.singletonList("http://localhost:8080/api/users/1"));
        return new ObjectMapper().writeValueAsString(r);
    }

    private String updateRestaurantAddOwner() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("name", "Restaurant123");
        r.put("owners", Arrays.asList("http://localhost:8080/api/users/1", "http://localhost:8080/api/users/2"));
        return new ObjectMapper().writeValueAsString(r);
    }
}
