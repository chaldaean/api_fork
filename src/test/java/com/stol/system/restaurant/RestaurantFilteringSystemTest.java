package com.stol.system.restaurant;

import com.stol.system.AbstractSystemTest;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestaurantFilteringSystemTest extends AbstractSystemTest {

    @Test
    public void getAllRestaurantsSortedByNameAscending() throws Exception {
        mockMvc.perform(get("/api/restaurants?sort=name,asc")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(8))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name100"))
                .andExpect(jsonPath("$._embedded.restaurants[6].name").value("name6"))
                .andExpect(jsonPath("$._embedded.restaurants[7].name").value("name7"));
    }

    @Test
    public void getAllRestaurantsSortedByNameDescending() throws Exception {
        mockMvc.perform(get("/api/restaurants?sort=name,desc")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(8))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name7"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name6"))
                .andExpect(jsonPath("$._embedded.restaurants[6].name").value("name100"))
                .andExpect(jsonPath("$._embedded.restaurants[7].name").value("name1"));
    }

    @Test
    public void getAllRestaurantsSortedByRatingAscending() throws Exception {
        mockMvc.perform(get("/api/restaurants?sort=rating,asc")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(8))
                .andExpect(jsonPath("$._embedded.restaurants[0].rating").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].rating").value("0.0"))
                .andExpect(jsonPath("$._embedded.restaurants[5].rating").value("2.6"))
                .andExpect(jsonPath("$._embedded.restaurants[6].rating").value("3.4"))
                .andExpect(jsonPath("$._embedded.restaurants[7].rating").value("4.5"));
    }

    @Test
    public void getAllRestaurantsSortedByRatingDescending() throws Exception {
        mockMvc.perform(get("/api/restaurants?sort=rating,desc")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(8))
                .andExpect(jsonPath("$._embedded.restaurants[0].rating").value("4.5"))
                .andExpect(jsonPath("$._embedded.restaurants[1].rating").value("3.4"))
                .andExpect(jsonPath("$._embedded.restaurants[5].rating").value("1.0"))
                .andExpect(jsonPath("$._embedded.restaurants[6].rating").value("0.0"))
                .andExpect(jsonPath("$._embedded.restaurants[7].rating").isEmpty());
    }

    @Test
    public void getAllRestaurantsFilteringByAverageTicketRangeAscending() throws Exception {
        mockMvc.perform(get("/api/restaurants?averageTicket=200&averageTicket=600&sort=averageTicket,asc")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(9))
                .andExpect(jsonPath("$._embedded.restaurants[0].averageTicket").value("400.0"))
                .andExpect(jsonPath("$._embedded.restaurants[1].averageTicket").value("400.0"))
                .andExpect(jsonPath("$._embedded.restaurants[6].averageTicket").value("500.0"))
                .andExpect(jsonPath("$._embedded.restaurants[7].averageTicket").value("600.0"));
    }

    @Test
    public void getAllRestaurantsFilteringByOneAverageTicketValue() throws Exception {
        mockMvc.perform(get("/api/restaurants?averageTicket=600")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].averageTicket").value("600.0"))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name7"))
                .andExpect(jsonPath("$._embedded.restaurants[1].averageTicket").value("600.0"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name8"));
    }

    @Test
    public void getNoRestaurantsFilteringByOneAverageTicketValue() throws Exception {
        mockMvc.perform(get("/api/restaurants?averageTicket=200")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(0));
    }

    @Test
    public void getAllRestaurantsFilteringByAverageTicketRangeDescending() throws Exception {
        mockMvc.perform(get("/api/restaurants?sort=averageTicket,desc")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(8))
                .andExpect(jsonPath("$._embedded.restaurants[0].averageTicket").value("600.0"))
                .andExpect(jsonPath("$._embedded.restaurants[7].averageTicket").value("400.0"));

    }

    @Test
    public void getAllRestaurantsFilteringByCuisines() throws Exception {
        mockMvc.perform(get("/api/restaurants?cuisines=5&cuisines=6")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name6"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name7"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("name8"));
    }

    @Test
    public void getAllRestaurantsFilteringByTags() throws Exception {
        mockMvc.perform(get("/api/restaurants?tags=5&tags=6")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name6"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name7"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("name8"));
    }

    @Test
    public void getAllRestaurantsByComplexFilter() throws Exception {
        mockMvc.perform(get("/api/restaurants?" +
                        "&averageTicket=200" +
                        "&averageTicket=600" +
                        "&cuisines=5" +
                        "&cuisines=6" +
                        "&tags=5" +
                        "&tags=6" +
                        "&sort=rating,desc" +
                        "&sort=averageTicket,asc")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name8"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name7"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("name6"));
    }
}
