package com.stol.system.tag;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.context.transaction.TestTransaction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TagSystemTest extends AbstractSystemTest {
    private static String tag;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        tag = tagObject();
    }

    @Test
    public void createNewTag() throws Exception {
        mockMvc.perform(post("/api/tags").content(tag)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.name").value("tagName1"))
                .andExpect(jsonPath("$.active").value(Boolean.FALSE))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].name").value("тег"))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.tag.href").isNotEmpty())
                .andExpect(jsonPath("$._links.icon.href").isNotEmpty());
    }

    @Test
    public void deleteTag() throws Exception {
        mockMvc.perform(delete("/api/tags/2")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/files/6")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getTagWithIconUrl() throws Exception {
        mockMvc.perform(get("/api/tags/2?projection=iconUrl")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.name").value("tag2"))
                .andExpect(jsonPath("$.iconUrl").value("/files/6.jpg"))
                .andExpect(jsonPath("$.active").value(Boolean.TRUE))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.tag.href").isNotEmpty())
                .andExpect(jsonPath("$._links.icon.href").isNotEmpty());
    }

    @Test
    public void getAllActiveTags() throws Exception {
        mockMvc.perform(get("/api/tags")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags.length()").value(7))
                .andExpect(jsonPath("$._embedded.tags[0].length()").value(4))
                .andExpect(jsonPath("$._embedded.tags[0].name").value("tag1"))
                .andExpect(jsonPath("$._embedded.tags[0]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[1].length()").value(4))
                .andExpect(jsonPath("$._embedded.tags[1].name").value("tag2"))
                .andExpect(jsonPath("$._embedded.tags[1]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[2].length()").value(4))
                .andExpect(jsonPath("$._embedded.tags[2].name").value("tag3"))
                .andExpect(jsonPath("$._embedded.tags[2]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[3].length()").value(4))
                .andExpect(jsonPath("$._embedded.tags[3].name").value("tag4"))
                .andExpect(jsonPath("$._embedded.tags[3]._links.length()").value(3))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$.page.length()").value(4));
    }

    @Test
    public void getAllTags() throws Exception {
        mockMvc.perform(get("/api/tags/search/all")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags.length()").value(8))
                .andExpect(jsonPath("$._embedded.tags[0].length()").value(4))
                .andExpect(jsonPath("$._embedded.tags[0].name").value("tag1"))
                .andExpect(jsonPath("$._embedded.tags[0]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[1].length()").value(4))
                .andExpect(jsonPath("$._embedded.tags[1].name").value("tag2"))
                .andExpect(jsonPath("$._embedded.tags[1]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[2].length()").value(4))
                .andExpect(jsonPath("$._embedded.tags[2].name").value("tag3"))
                .andExpect(jsonPath("$._embedded.tags[2]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[3].length()").value(4))
                .andExpect(jsonPath("$._embedded.tags[3].name").value("tag4"))
                .andExpect(jsonPath("$._embedded.tags[3]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[7].length()").value(4))
                .andExpect(jsonPath("$._embedded.tags[7].name").value("tag101"))
                .andExpect(jsonPath("$._embedded.tags[7]._links.length()").value(3))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$.page.length()").value(4));
    }


    @Test
    public void getTagById() throws Exception {
        mockMvc.perform(get("/api/tags/2")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.name").value("tag2"))
                .andExpect(jsonPath("$.active").value(Boolean.TRUE))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/tags/2"))
                .andExpect(jsonPath("$._links.tag.href").value("http://localhost/api/tags/2{?projection}"))
                .andExpect(jsonPath("$._links.icon.href").value("http://localhost/api/tags/2/icon"));

        mockMvc.perform(get("/api/tags/2/icon")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture6"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/6.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/6"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/6"));
    }

    @Test
    public void updateTag() throws Exception {
        mockMvc.perform(get("/api/files/9")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk());

        mockMvc.perform(patch("/api/tags/100").content(tag)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.name").value("tagName1"))
                .andExpect(jsonPath("$.active").value(Boolean.FALSE))
                .andExpect(jsonPath("$.i18ns").exists())
                .andExpect(jsonPath("$.i18ns[0].name").value("тег"))
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/tags/100"))
                .andExpect(jsonPath("$._links.tag.href").value("http://localhost/api/tags/100{?projection}"))
                .andExpect(jsonPath("$._links.icon.href").value("http://localhost/api/tags/100/icon"));

        mockMvc.perform(get("/api/tags/100/icon")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture1"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/1.jpg"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/1"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/1"));

        TestTransaction.flagForCommit();
        TestTransaction.end();

        mockMvc.perform(get("/api/files/9")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }

    static String tagObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("name", "tagName1");
        r.put("active", "false");
        r.put("icon", "http://localhost:8080/api/files/1");

        Map<String, String> i18n = new HashMap<>();
        i18n.put("language", "http://localhost/api/languages/1");
        i18n.put("name", "тег");

        r.put("i18ns", Arrays.asList(i18n));


        return new ObjectMapper().writeValueAsString(r);
    }
}
