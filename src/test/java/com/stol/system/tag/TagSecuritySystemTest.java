package com.stol.system.tag;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.stol.system.SecurityAbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static com.stol.system.tag.TagSystemTest.tagObject;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TagSecuritySystemTest extends SecurityAbstractSystemTest {
    private static String tag;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        tag = tagObject();
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void createCuisineNoSystemAdminRole() throws Exception {
        mockMvc.perform(post("/api/cuisines").content(tag)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void findCuisineNoRole() throws Exception {
        mockMvc.perform(get("/api/cuisines/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void deleteCuisineNoSystemAdminRole() throws Exception {
        mockMvc.perform(delete("/api/cuisines/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }
}
