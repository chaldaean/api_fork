package com.stol.system.role;

import com.stol.system.AbstractSystemTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RoleSystemTest extends AbstractSystemTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getRoleById() throws Exception {
        mockMvc.perform(get("/api/roles/1")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.role").value("ROLE_SYSTEM_ADMIN"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/roles/1"))
                .andExpect(jsonPath("$._links.role.href").value("http://localhost/api/roles/1"));
    }

    @Test
    public void getAllRoles() throws Exception {
        mockMvc.perform(get("/api/roles")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.roles.length()").value(4))
                .andExpect(jsonPath("$._embedded.roles[0].role").value("ROLE_SYSTEM_ADMIN"))
                .andExpect(jsonPath("$._embedded.roles[1].role").value("ROLE_USER"))
                .andExpect(jsonPath("$._embedded.roles[2].role").value("ROLE_RESTAURANT_ADMIN"))
                .andExpect(jsonPath("$._embedded.roles[3].role").value("ROLE_NOT_SET"))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/roles"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/roles"));
    }
}
