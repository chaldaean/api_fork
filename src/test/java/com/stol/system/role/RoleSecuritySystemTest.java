package com.stol.system.role;

import com.stol.system.SecurityAbstractSystemTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RoleSecuritySystemTest extends SecurityAbstractSystemTest {
    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void getRoleByIdNoNoRole() throws Exception {
        mockMvc.perform(get("/api/roles/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET"})
    public void getAllRolesNoRole() throws Exception {
        mockMvc.perform(get("/api/roles"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }
}
