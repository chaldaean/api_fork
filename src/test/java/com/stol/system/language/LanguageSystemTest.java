package com.stol.system.language;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class LanguageSystemTest extends AbstractSystemTest {
    private static String language;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        language = languageObject();
    }

    @Test
    public void createNewLanguage() throws Exception {
        mockMvc.perform(post("/api/languages").content(language)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.code").value("pl"))
                .andExpect(jsonPath("$.name").value("Polish"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.language.href").isNotEmpty());
    }

    @Test
    public void deleteLanguage() throws Exception {
        mockMvc.perform(delete("/api/languages/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent())
                .andDo(document("system/{class-name}/{method-name}"));
    }

    @Test
    public void getLanguage() throws Exception {
        mockMvc.perform(get("/api/languages/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.code").value("uk"))
                .andExpect(jsonPath("$.name").value("Українська"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.language.href").isNotEmpty());
    }

    @Test
    public void getAllActiveLanguages() throws Exception {
        mockMvc.perform(get("/api/languages?page=0&size=10")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.languages.length()").value(2))
                .andExpect(jsonPath("$._embedded.languages[0].length()").value(4))
                .andExpect(jsonPath("$._embedded.languages[0].code").value("uk"))
                .andExpect(jsonPath("$._embedded.languages[0].name").value("Українська"))
                .andExpect(jsonPath("$._embedded.languages[0].active").value(true))
                .andExpect(jsonPath("$._embedded.languages[1].code").value("en"))
                .andExpect(jsonPath("$._embedded.languages[1].name").value("English"))
                .andExpect(jsonPath("$._embedded.languages[1].active").value(true))
                .andExpect(jsonPath("$._links.length()").value(3));
    }

    @Test
    public void getAllLanguages() throws Exception {
        mockMvc.perform(get("/api/languages/search/all?page=0&size=10")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.languages.length()").value(3))
                .andExpect(jsonPath("$._embedded.languages[0].length()").value(4))
                .andExpect(jsonPath("$._embedded.languages[0].code").value("uk"))
                .andExpect(jsonPath("$._embedded.languages[0].name").value("Українська"))
                .andExpect(jsonPath("$._embedded.languages[0].active").value(true))
                .andExpect(jsonPath("$._embedded.languages[1].length()").value(4))
                .andExpect(jsonPath("$._embedded.languages[1].code").value("en"))
                .andExpect(jsonPath("$._embedded.languages[1].name").value("English"))
                .andExpect(jsonPath("$._embedded.languages[1].active").value(true))
                .andExpect(jsonPath("$._embedded.languages[2].length()").value(4))
                .andExpect(jsonPath("$._embedded.languages[2].code").value("fr"))
                .andExpect(jsonPath("$._embedded.languages[2].name").value("French"))
                .andExpect(jsonPath("$._embedded.languages[2].active").value(false))
                .andExpect(jsonPath("$._links.length()").value(1));
    }


    @Test
    public void updateLanguage() throws Exception {
        mockMvc.perform(patch("/api/languages/1").content(language)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.code").value("pl"))
                .andExpect(jsonPath("$.name").value("Polish"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").isNotEmpty())
                .andExpect(jsonPath("$._links.language.href").isNotEmpty());
    }

    static String languageObject() throws JsonProcessingException {
        Map<String, Object> p = new HashMap<>();
        p.put("code", "pl");
        p.put("name", "Polish");
        return new ObjectMapper().writeValueAsString(p);
    }

}
