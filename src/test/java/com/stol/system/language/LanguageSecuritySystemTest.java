package com.stol.system.language;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import com.stol.system.SecurityAbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class LanguageSecuritySystemTest extends SecurityAbstractSystemTest {

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void createNewLanguageNoAdminRole() throws Exception {
        mockMvc.perform(post("/api/languages"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void deleteLanguageNoAdminRole() throws Exception {
        mockMvc.perform(delete("/api/languages/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void getLanguageNoAdmin() throws Exception {
        mockMvc.perform(get("/api/languages/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    public void getAllActiveLanguagesLoggedOut() throws Exception {
        mockMvc.perform(get("/api/languages?page=0&size=10"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3));
    }

    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void getAllLanguagesNoAdmin() throws Exception {
        mockMvc.perform(get("/api/languages/search/all?page=0&size=10"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }


    @Test
    @WithMockUser(roles = {"USER", "RESTAURANT_ADMIN"})
    public void updateLanguage() throws Exception {
        mockMvc.perform(patch("/api/languages/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));

    }

}
