package com.stol.system.statistic;

import com.stol.system.AbstractSystemTest;
import org.junit.Test;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class StatisticSystemTest extends AbstractSystemTest {
    @Test
    public void numberOfRegisteredUsers() throws Exception {
        mockMvc.perform(get("/api/statistic/accounts?date=2017-10-23")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.total").value(4))
                .andExpect(jsonPath("$.activated").value(3));
    }

    @Test
    public void numberOfReservations() throws Exception {
        mockMvc.perform(get("/api/statistic/reservations?date=2016-02-10")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$.total").value(6));
    }

    @Test
    public void numberOfReviews() throws Exception {
        mockMvc.perform(get("/api/statistic/reviews?date=2017-10-23")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$.total").value(3));
    }

    @Test
    public void numberOfActiveUsers() throws Exception {
        getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "381000000000", PASSWORD));
        getCredentialToken(String.format("grant_type=password&username=%s&password=%s", "382000000000", PASSWORD));

        mockMvc.perform(get("/api/statistic/accounts/active?date=2016-02-10")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$.active_users").value(1));
    }

    @Test
    public void usersNumberOfScreensCount() throws Exception {
        mockMvc.perform(post("/api/statistic/screens")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    @Test
    public void averageNumberOfScreens() throws Exception {
        mockMvc.perform(get("/api/statistic/screens?date=2018-01-01")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$.average").value(8));
    }
}
