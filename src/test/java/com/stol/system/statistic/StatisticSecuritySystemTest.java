package com.stol.system.statistic;

import com.stol.system.SecurityAbstractSystemTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class StatisticSecuritySystemTest extends SecurityAbstractSystemTest {

    @Test
    @WithMockUser(roles = {"NOT_SET", "USER", "RESTAURANT_ADMIN"})
    public void numberOfRegisteredUsersNoAdminRole() throws Exception {
        mockMvc.perform(get("/api/statistic/accounts")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET", "USER", "RESTAURANT_ADMIN"})
    public void numberOfReservationsNoAdminRole() throws Exception {
        mockMvc.perform(get("/api/statistic/reservations")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET", "USER", "RESTAURANT_ADMIN"})
    public void numberOfReviewsNoAdminRole() throws Exception {
        mockMvc.perform(get("/api/statistic/reviews")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET", "USER", "RESTAURANT_ADMIN"})
    public void numberOfActiveUsersNoAdminRole() throws Exception {
        mockMvc.perform(get("/api/statistic/accounts/active")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET", "USER", "RESTAURANT_ADMIN"})
    @WithUserDetails("customUsername")
    public void averageNumberOfScreensNoAdminRole() throws Exception {
        mockMvc.perform(get("/api/statistic/screens")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }

    @Test
    @WithMockUser(roles = {"NOT_SET", "USER", "RESTAURANT_ADMIN"})
    public void usersNumberOfScreensCountNoAdminRole() throws Exception {
        mockMvc.perform(post("/api/statistic/screens")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value(ACCESS_DENIED))
                .andExpect(jsonPath("$.error_description").value(ACCESS_IS_DENIED));
    }
}
