package com.stol.system.discount;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.system.AbstractSystemTest;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DiscountSystemTest extends AbstractSystemTest {
    private static String reservation;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        reservation = reservationObject();
    }

    @Test
    public void createNewReservationWithDiscount() throws Exception {
        mockMvc.perform(post("/api/reservations").content(reservation)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.discount.length()").value(3))
                .andExpect(jsonPath("$.discount.value").value(20))
                .andExpect(jsonPath("$.discount.used").value(Boolean.FALSE))
                .andExpect(jsonPath("$.discount._links.length()").value(1))
                .andExpect(jsonPath("$.discount._links.happyHour.href").value("http://localhost/api/happy_hours/1{?projection}"));
    }

    static String reservationObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/1");
        r.put("dateTime", LocalDate.now().plusDays(1).toString() + "T11:00");
        r.put("guestNumber", "2");
        r.put("comment", "Comment!");
        return new ObjectMapper().writeValueAsString(r);
    }
}
