package com.stol.application.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.DBUnitRule;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import com.stol.SpringWebTest;
import org.junit.Before;
import org.junit.Rule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.restdocs.headers.RequestHeadersSnippet;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DBRider
@DataSet(value = "user_roles.xml,application.xml,oauth.xml", executorId = "application", disableConstraints = true)
public abstract class ApplicationSystemTest extends SpringWebTest {
    protected static final String LOGIN = "381000000000";
    protected static final String PASSWORD = "password";
    protected static final String OAUTH_CONTENT = String.format("grant_type=password&username=%s&password=%s", LOGIN, PASSWORD);
    protected final RequestHeadersSnippet tokenHeadersSnippet = requestHeaders(headerWithName("Authorization").description("Bearer link:#_login_page[access token]"));

    @Value("${stol.oauth2.client.application.client-id}")
    protected String CLIENT_ID;
    @Value("${stol.oauth2.client.application.client-secret}")
    protected String CLIENT_SECRET;

    protected String token;



    @Rule
    public DBUnitRule dbUnitRule = DBUnitRule.instance("application", () -> context.getBean(DataSource.class).getConnection());

    @Before
    public void before() throws Exception {
        if (Objects.isNull(token)) {
            token = getCredentialToken(OAUTH_CONTENT);
        }
    }

    protected String getCredentialToken(String oauthContent) throws Exception {
        String oauth = mockMvc.perform(post("/oauth/token").content(oauthContent)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andReturn().getResponse().getContentAsString();
        return "Bearer " + new ObjectMapper().readValue(oauth, Map.class).get("access_token");
    }

    protected void changeLanguageToUk() throws Exception {
        Map<String, String> userData = new HashMap<>();
        userData.put("language", "http://localhost/api/languages/1");

        mockMvc.perform(patch("/api/users/100").content(new ObjectMapper().writeValueAsString(userData))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk());

    }
}
