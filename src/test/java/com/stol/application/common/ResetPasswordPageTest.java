package com.stol.application.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.controller.model.UserLogin;
import com.stol.model.user.UserSystemInformation;
import com.stol.repository.CredentialRepository;
import com.stol.repository.UserSystemInformationRepository;
import com.stol.service.sms.SmsSenderService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;

import static com.stol.configuration.constant.Localization.*;
import static com.stol.configuration.constant.Messages.*;
import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.Mockito.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ResetPasswordPageTest extends ApplicationSystemTest {
    private static String USER_PHONE;

    @Autowired
    private CredentialRepository credentialRepository;
    @Autowired
    private UserSystemInformationRepository smsRepository;

    @MockBean
    private SmsSenderService smsSenderService;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        USER_PHONE = new ObjectMapper().writeValueAsString(new UserLogin(LOGIN));
    }

    @Test
    public void resetUserPassword() throws Exception {
        when(smsSenderService.sendSms(eq(USER_PHONE), anyString())).thenReturn("");

        String oldPassword = credentialRepository.findByLogin(LOGIN).getPassword();

        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()))
                .andDo(document("common/{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("login").description("Phone number")
                        )
                ));

        String newPassword = credentialRepository.findByLogin(LOGIN).getPassword();
        Assert.assertNotEquals(newPassword, oldPassword);

        verify(smsSenderService).sendSms(eq(LOGIN), startsWith("Для Вас сгенерирован новый пароль"));
    }

    @Test
    public void resetUserPassword_uk() throws Exception {
        when(smsSenderService.sendSms(eq(USER_PHONE), anyString())).thenReturn("");

        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK));

        verify(smsSenderService).sendSms(eq(LOGIN), startsWith("Для Вас згенеровано новий пароль"));
    }

    @Test
    public void resetUserPassword_en() throws Exception {
        when(smsSenderService.sendSms(eq(USER_PHONE), anyString())).thenReturn("");

        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN));

        verify(smsSenderService).sendSms(eq(LOGIN), startsWith("A new password has been generated for you"));
    }

    @Test
    public void resetUserPasswordNoUserWithSuchPhone() throws Exception {
        String content = new ObjectMapper().writeValueAsString(new UserLogin("+381000000000"));
        mockMvc.perform(post("/api/user/password/reset").content(content)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());
    }

    @Test
    public void resetUserPasswordTryLimits() throws Exception {
        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8));
        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8));
        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8));

        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Использованы все попытки. Попробуйте еще рез через 24 часа!"))
                .andExpect(jsonPath("$.links").isEmpty())
                .andDo(document("common/{class-name}/{method-name}",
                                requestFields(
                                        fieldWithPath("login").description("Phone number")
                                ),
                                responseFields(
                                        fieldWithPath("message").description("Использованы все попытки. Попробуйте еще рез через 24 часа!"),
                                        subsectionWithPath("logref").ignored(),
                                        subsectionWithPath("links").ignored()
                                )
                ));

        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("Використані всі спроби. Спробуйте через 24 години!"));

        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message").value("You have reached maximum number of tries. Try after 24 hours!"));
    }

    @Test
    public void resetUserPasswordTimeLimitExpired() throws Exception {
        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8));
        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8));
        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8));

        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden());

        minus24HoursToPasswordRestTime();

        mockMvc.perform(post("/api/user/password/reset").content(USER_PHONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void resetPasswordNoSuchUser() throws Exception {
        String p = "+380011111111";
        UserLogin login = new UserLogin(p);

        mockMvc.perform(post("/api/user/password/reset").content(new ObjectMapper().writeValueAsString(login))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Нет пользователя с логином: " + p + "!"))
                .andExpect(jsonPath("$.links").isEmpty())
                .andDo(document("common/{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("login").description("Phone number")
                        ),
                        responseFields(
                                fieldWithPath("message").description(NO_USER_WITH_LOGIN + p),
                                subsectionWithPath("logref").ignored(),
                                subsectionWithPath("links").ignored()
                        )
                ));

        mockMvc.perform(post("/api/user/password/reset").content(new ObjectMapper().writeValueAsString(login))
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Немає користувача з логіном: " + p + "!"));

        mockMvc.perform(post("/api/user/password/reset").content(new ObjectMapper().writeValueAsString(login))
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("There is no user with login: " + p + "!"));
    }

    @Test
    public void resetPasswordUserMustBeActivatedFirst() throws Exception {
        UserLogin login = new UserLogin("381000000002");
        mockMvc.perform(post("/api/user/password/reset").content(new ObjectMapper().writeValueAsString(login))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Пользователь должен быть активирован сперва!"))
                .andExpect(jsonPath("$.links").isEmpty())
                .andDo(document("common/{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("login").description("Phone number")
                        ),
                        responseFields(
                                fieldWithPath("message").description("Пользователь должен быть активирован сперва!"),
                                subsectionWithPath("logref").ignored(),
                                subsectionWithPath("links").ignored()
                        )
                ));
    }

    @Test
    public void resetPasswordUserMustBeActivatedFirst_uk() throws Exception {
        UserLogin login = new UserLogin("381000000002");
        mockMvc.perform(post("/api/user/password/reset").content(new ObjectMapper().writeValueAsString(login))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Користувач спочатку має бути активований!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void resetPasswordUserMustBeActivatedFirst_en() throws Exception {
        UserLogin login = new UserLogin("381000000002");
        mockMvc.perform(post("/api/user/password/reset").content(new ObjectMapper().writeValueAsString(login))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").isNotEmpty())
                .andExpect(jsonPath("$.message").value("User must be activated first!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    private void minus24HoursToPasswordRestTime() {
        UserSystemInformation userSystemInformation = smsRepository.findByPhoneNumber(LOGIN);
        LocalDateTime newTime = LocalDateTime.now(Clock.offset(Clock.systemUTC(), Duration.ofHours(-24)));
        userSystemInformation.setPasswordResetCreation(newTime);
        smsRepository.save(userSystemInformation);
    }
}
