package com.stol.application.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.controller.model.UserLogoutRequest;
import org.junit.Test;
import org.springframework.http.MediaType;

import static com.stol.model.property.ApplicationType.CLIENT_APP;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LogoutTest extends ApplicationSystemTest {

    @Test
    public void logoutCurrentUser() throws Exception {
        UserLogoutRequest req = new UserLogoutRequest();
        req.setApplicationType(CLIENT_APP);

        mockMvc.perform(post("/api/user/current/logout")
                .content(new ObjectMapper().writeValueAsString(req))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andDo(document("common/{class-name}/{method-name}",
                    tokenHeadersSnippet,
                    requestFields(
                        fieldWithPath("applicationType").description("Type of application. Can be 'USER_APP' for users or 'CLIENT_APP' for restaurant managers")
                )));
    }
}
