package com.stol.application.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.model.user.UserSystemInformation;
import com.stol.repository.UserSystemInformationRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.restdocs.headers.RequestHeadersSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.*;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class LoginPageTest extends ApplicationSystemTest {
    @Value("${stol.login.max-attempts}")
    private int MAX_LOGIN_ATTEMPTS;
    @Value("${stol.login.cooldown.expiration-time}")
    private long EXPIRATION_TIME_SECONDS;

    @Autowired
    private UserSystemInformationRepository smsRepository;

    private final RequestHeadersSnippet headersSnippet = requestHeaders(headerWithName("Authorization").description("Basic auth credentials"));

    @Before
    public void before() {
    }

    private final ResponseFieldsSnippet accessTokenResponseSnippet = responseFields(
            fieldWithPath("access_token").description("Access token to be used in the subsequent requests to API"),
            fieldWithPath("token_type").description("Token type"),
            fieldWithPath("refresh_token").description("Refresh token to be used for obtaining a new access token"),
            fieldWithPath("expires_in").description("Access token expiration"),
            fieldWithPath("scope").description("Granted scopes"),
            fieldWithPath("user.id").description("User's Id"));

    @Test
    public void loginUserInApplicationAndGetOauthToken() throws Exception {
        mockMvc.perform(post("/oauth/token").content(OAUTH_CONTENT)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(APPLICATION_FORM_URLENCODED_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.access_token").isNotEmpty())
                .andExpect(jsonPath("$.token_type").value("bearer"))
                .andExpect(jsonPath("$.refresh_token").isNotEmpty())
                .andExpect(jsonPath("$.expires_in").isNotEmpty())
                .andExpect(jsonPath("$.scope").value("read write"))
                .andExpect(jsonPath("$.user.length()").value(1))
                .andExpect(jsonPath("$.user.id").value(100))
                .andDo(document("common/{class-name}/{method-name}",
                        headersSnippet,
                        accessTokenResponseSnippet
                ));
    }

    @Test
    public void loginUserInApplicationNoUserWithThisLogin() throws Exception {
        String content = "grant_type=password&username=380000000001&password=" + PASSWORD;
        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value("invalid_grant"))
                .andExpect(jsonPath("$.error_description").value("Неверный логин и/или пароль!"));

        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_description").value("Невірний логін та/чи пароль!"));

        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_description").value("Invalid login and/or password!"));
    }

    @Test
    public void loginUserInApplicationInvalidPassword() throws Exception {
        String content = "grant_type=password&username=" + LOGIN + "&password=password1";
        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(APPLICATION_FORM_URLENCODED_VALUE)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value("invalid_grant"))
                .andExpect(jsonPath("$.error_description").value("Неверный логин и/или пароль!"))
                .andDo(document("common/{class-name}/{method-name}",
                        headersSnippet,
                        responseFields(
                                fieldWithPath("error").description("invalid_grant"),
                                fieldWithPath("error_description").description("Bad credentials")
                        )
                ));

        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(APPLICATION_FORM_URLENCODED_VALUE)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_description").value("Невірний логін та/чи пароль!"));

        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(APPLICATION_FORM_URLENCODED_VALUE)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_description").value("Invalid login and/or password!"));
    }

    @Test
    public void loginUserInApplicationNotActivatedOrDisabled() throws Exception {
        String content = "grant_type=password&username=381000000001&password=" +PASSWORD;
        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(APPLICATION_FORM_URLENCODED_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value("invalid_grant"))
                .andExpect(jsonPath("$.error_description").value("User is disabled"))
                .andDo(document("common/{class-name}/{method-name}",
                        headersSnippet,
                        responseFields(
                                fieldWithPath("error").description("invalid_grant"),
                                fieldWithPath("error_description").description("User is not confirmed or was disabled")
                        )
                ));
    }

    @Test
    public void loginUserInApplicationInvalidPasswordAndLockAccount() throws Exception {
        String content = "grant_type=password&username=" + LOGIN + "&password=password1";
        for (int i = 0; i < 5; i++) {
            mockMvc.perform(post("/oauth/token").content(content)
                    .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                    .andExpect(status().isBadRequest());
        }
        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value("invalid_grant"))
                .andExpect(jsonPath("$.error_description").value("User account is locked"))
                .andDo(document("common/{class-name}/{method-name}",
                        headersSnippet,
                        responseFields(
                                fieldWithPath("error").description("invalid_grant"),
                                fieldWithPath("error_description").description("User account is locked")
                        )
                ));

        UserSystemInformation userSystemInformation = smsRepository.findByPhoneNumber(LOGIN);
        Assert.assertEquals(MAX_LOGIN_ATTEMPTS, userSystemInformation.getLoginAttempts());
    }

    @Test
    public void loginUserInApplicationAndLockAndUnlockAccountAfterTime() throws Exception {
        String content = "grant_type=password&username=" + LOGIN + "&password=password1";
        for (int i = 0; i < 5; i++) {
            mockMvc.perform(post("/oauth/token").content(content)
                    .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                    .andExpect(status().isBadRequest());
        }
        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value("invalid_grant"))
                .andExpect(jsonPath("$.error_description").value("User account is locked"));

        minus15MinutesToLoginTry();

        mockMvc.perform(post("/oauth/token").content(OAUTH_CONTENT)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.access_token").isNotEmpty())
                .andExpect(jsonPath("$.token_type").value("bearer"))
                .andExpect(jsonPath("$.refresh_token").isNotEmpty())
                .andExpect(jsonPath("$.expires_in").isNotEmpty())
                .andExpect(jsonPath("$.scope").value("read write"))
                .andExpect(jsonPath("$.user.id").value(100));
    }

    @Test
    public void gettingDataAccessTokenExpired() throws Exception {
        String oauth = mockMvc.perform(post("/oauth/token").content(OAUTH_CONTENT)
                .with(httpBasic("expired", CLIENT_SECRET))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andReturn().getResponse().getContentAsString();
        token = "Bearer " + new ObjectMapper().readValue(oauth, Map.class).get("access_token");
        Thread.sleep(1000);

        mockMvc.perform(get("/api/user/current")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.error").value("invalid_token"))
                .andExpect(jsonPath("$.error_description").value("Access token expired: " + token.replace("Bearer ", "")))
                .andDo(document("common/{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("error").description("invalid_token"),
                                fieldWithPath("error_description").description("Access token expired: " + token.replace("Bearer ", ""))
                        )
                ));
    }

    @Test
    public void getNewAccessTokenUsingRefreshToken() throws Exception {
        String refreshToken = getRefreshToken();

        String content = "grant_type=refresh_token&refresh_token=" + refreshToken;
        mockMvc.perform(post("/oauth/token").content(content)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(6))
                .andExpect(jsonPath("$.access_token").isNotEmpty())
                .andExpect(jsonPath("$.token_type").value("bearer"))
                .andExpect(jsonPath("$.refresh_token").value(refreshToken))
                .andExpect(jsonPath("$.expires_in").isNotEmpty())
                .andExpect(jsonPath("$.scope").value("read write"))
                .andExpect(jsonPath("$.user.id").value(100))
                .andDo(document("common/{class-name}/{method-name}",
                        headersSnippet,
                        accessTokenResponseSnippet
                ));
    }

    private void minus15MinutesToLoginTry() {
        UserSystemInformation userSystemInformation = smsRepository.findByPhoneNumber(LOGIN);
        LocalDateTime newTime = LocalDateTime.now(Clock.offset(Clock.systemUTC(), Duration.ofHours(-EXPIRATION_TIME_SECONDS)));
        userSystemInformation.setLastLoginTime(newTime);
        smsRepository.save(userSystemInformation);
    }

    private String getRefreshToken() throws Exception {
        String oauth = mockMvc.perform(post("/oauth/token").content(OAUTH_CONTENT)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andReturn().getResponse().getContentAsString();
        return new ObjectMapper().readValue(oauth, Map.class).get("refresh_token").toString();
    }
}
