package com.stol.application.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.stol.model.property.ApplicationType.USER_APP;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class NotificationTest extends ApplicationSystemTest {

    @Test
    public void saveUsersDeviceToken() throws Exception {
        mockMvc.perform(post("/api/mobile_device").content(mobileDevice())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.device").value("Iphone X"))
                .andExpect(jsonPath("$.deviceToken").value("b8f3b2513caa"))
                .andExpect(jsonPath("$.applicationType").value(USER_APP.toString()))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/mobile_device/1"))
                .andExpect(jsonPath("$._links.mobileDevice.href").value("http://localhost/api/mobile_device/1"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/mobile_device/1/user{?projection}"))
                .andDo(document("common/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestFields(
                                fieldWithPath("device").description("Device name"),
                                fieldWithPath("deviceToken").description("Device token"),
                                fieldWithPath("applicationType").description("Type of application. Can be 'USER_APP' for users or 'CLIENT_APP' for restaurant managers")
                        ),
                        responseFields(
                                fieldWithPath("device").description("Device name"),
                                fieldWithPath("deviceToken").description("Device token"),
                                fieldWithPath("applicationType").description("Type of application. Can be 'USER_APP' for users or 'CLIENT_APP' for restaurant managers'"),
                                fieldWithPath("creationDate").description("Creation date"),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    public static String mobileDevice() throws JsonProcessingException {
        Map<String, String> device = new HashMap<>();
        device.put("device", "Iphone X");
        device.put("deviceToken", "b8f3b2513caa");
        device.put("applicationType", USER_APP.toString());
        return new ObjectMapper().writeValueAsString(device);
    }
}
