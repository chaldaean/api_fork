package com.stol.application.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.restdocs.PagingRequestParameters;
import com.stol.restdocs.PagingResponseFields;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserLanguageTest extends ApplicationSystemTest {

    @Test
    public void getLanguages() throws Exception {
        mockMvc.perform(get("/api/languages?page=0&size=100&projection=snippet")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$._embedded.languages.length()").value(2))
                .andExpect(jsonPath("$._embedded.languages[0].length()").value(4))
                .andExpect(jsonPath("$._embedded.languages[0].id").value(1))
                .andExpect(jsonPath("$._embedded.languages[0].code").value("uk"))
                .andExpect(jsonPath("$._embedded.languages[0].name").value("Українська"))
                .andExpect(jsonPath("$._embedded.languages[0]._links.self.href").value("http://localhost/api/languages/1"))
                .andExpect(jsonPath("$._embedded.languages[1].length()").value(4))
                .andExpect(jsonPath("$._embedded.languages[1].id").value(2))
                .andExpect(jsonPath("$._embedded.languages[1].code").value("en"))
                .andExpect(jsonPath("$._embedded.languages[1].name").value("English"))
                .andExpect(jsonPath("$._embedded.languages[1]._links.self.href").value("http://localhost/api/languages/2"))
                .andExpect(jsonPath("$._links.length()").value(3))

                .andDo(document("common/{class-name}/{method-name}",
                        requestParameters(
                                PagingRequestParameters.size,
                                PagingRequestParameters.page,
                                parameterWithName("projection").description("snippet")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.languages[].id").description("ID"),
                                fieldWithPath("_embedded.languages[].code").description("Code"),
                                fieldWithPath("_embedded.languages[].name").description("Name"),
                                fieldWithPath("_embedded.languages[]._links.self.href").description("Names of customer's restaurants"),
                                subsectionWithPath("_embedded.languages[]._links.language").ignored(),
                                subsectionWithPath("_links").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size
                        )));
    }

    @Test
    public void getLanguageOfCurrentUserIfNotSet() throws Exception {
        mockMvc.perform(get("/api/users/search/current_language")
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.language").isEmpty())

                .andDo(document("common/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        responseFields(
                                fieldWithPath("language").description("Language is null")
                        )));
    }

    @Test
    public void getLanguageOfCurrentUserIfSet() throws Exception {
        Map<String, String> userData = new HashMap<>();
        userData.put("language", "http://localhost/api/languages/2");

        mockMvc.perform(patch("/api/users/100").content(new ObjectMapper().writeValueAsString(userData))
                .header(AUTHORIZATION, token));

        mockMvc.perform(get("/api/users/search/current_language")
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.language.id").value(2))
                .andExpect(jsonPath("$.language.code").value("en"))
                .andExpect(jsonPath("$.language.name").value("English"))

                .andDo(document("common/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        responseFields(
                                fieldWithPath("language.id").description("ID"),
                                fieldWithPath("language.code").description("Code"),
                                fieldWithPath("language.name").description("Name")
                        )));
    }

    @Test
    public void setLanguageForCurrentUser() throws Exception {

        Map<String, String> userData = new HashMap<>();
        userData.put("language", "http://localhost/api/languages/2");

        mockMvc.perform(patch("/api/users/100").content(new ObjectMapper().writeValueAsString(userData))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))

                .andDo(document("common/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestFields(
                                fieldWithPath("language").description("Language resource url")
                        )));

    }

}
