package com.stol.application.customerflow;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.IsNull;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.stol.model.property.ReservationStatus.CONFIRMED;
import static com.stol.model.property.ReservationStatus.REJECTED;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationPageTest extends CustomerApplicationSystemTest {

    @Test
    public void reservationDescriptionPopup() throws Exception {

        mockMvc.perform(get("/api/reservations/4/?projection=description")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(9))
                .andExpect(jsonPath("$.time").value("09:30"))
                .andExpect(jsonPath("$.comment").value("Some comment4"))
                .andExpect(jsonPath("$.notes").value("Some notes 4"))
                .andExpect(jsonPath("$.fullName").value("UserOneHundred UserOneHundred"))
                .andExpect(jsonPath("$.photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$.guestNumber").value(2))
                .andExpect(jsonPath("$.visitNumber").value(0))
                .andExpect(jsonPath("$.phoneNumber").value("381000000000"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/4"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reservations/4{?projection}"))
                .andExpect(jsonPath("$._links.client.href").value("http://localhost/api/reservations/4/client{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reservations/4/restaurant{?projection}"))

                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("description")
                        ),
                        responseFields(
                                fieldWithPath("time").description("Visit time"),
                                fieldWithPath("comment").description("Сomment to the reservation"),
                                fieldWithPath("notes").description("Restaurant notes"),
                                fieldWithPath("fullName").description("Visitor full name"),
                                fieldWithPath("photo").description("Restaurant's top picture url"),
                                fieldWithPath("guestNumber").description("Number of guests"),
                                fieldWithPath("visitNumber").description("Number of visits"),
                                fieldWithPath("phoneNumber").description("Visitor phone number (available within 14 days after the visit)"),
                                subsectionWithPath("_links").ignored()
                        )));

        mockMvc.perform(get("/api/reservations/1/?projection=description")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.phoneNumber").value(""))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/1"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reservations/1{?projection}"))
                .andExpect(jsonPath("$._links.client.href").value("http://localhost/api/reservations/1/client{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reservations/1/restaurant{?projection}"));
    }

    @Test
    public void rejectedNewReservation() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=381000000008&password=%s", PASSWORD));
        String confirmed = new ObjectMapper().writeValueAsString(Collections.singletonMap("status", REJECTED));

        mockMvc.perform(patch("/api/reservations/3").content(confirmed)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.dateTime").value("2089-10-21T10:00:00"))
                .andExpect(jsonPath("$.guestNumber").value(3))
                .andExpect(jsonPath("$.comment").value("Some comment3"))
                .andExpect(jsonPath("$.notes").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.status").value(REJECTED.toString()))
                .andExpect(jsonPath("$.created").value("2017-10-23T14:42:05"))
                .andExpect(jsonPath("$.discount").isEmpty())
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/3"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reservations/3{?projection}"))
                .andExpect(jsonPath("$._links.client.href").value("http://localhost/api/reservations/3/client{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reservations/3/restaurant{?projection}"))

                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestFields(
                                fieldWithPath("status").description(REJECTED)
                        ),
                        responseFields(
                                fieldWithPath("dateTime").description("Reservation date and time"),
                                fieldWithPath("guestNumber").description("Number of guests"),
                                fieldWithPath("comment").description("Comment to the reservation"),
                                fieldWithPath("notes").description("Restaurant notes"),
                                fieldWithPath("status").description("Status of the reservation. Must be REJECTED"),
                                fieldWithPath("created").description("Reservation creation date"),
                                fieldWithPath("discount").description("Discount %"),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void confirmNewReservation() throws Exception {
        String token = getCredentialToken(String.format("grant_type=password&username=381000000008&password=%s", PASSWORD));

        Map<String, Object> data = new HashMap<>();
        data.put("status", CONFIRMED);
        data.put("notes", "Some notes");

        String confirmed = new ObjectMapper().writeValueAsString(data);

        mockMvc.perform(patch("/api/reservations/3").content(confirmed)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.dateTime").value("2089-10-21T10:00:00"))
                .andExpect(jsonPath("$.guestNumber").value(3))
                .andExpect(jsonPath("$.comment").value("Some comment3"))
                .andExpect(jsonPath("$.notes").value("Some notes"))
                .andExpect(jsonPath("$.status").value(CONFIRMED.toString()))
                .andExpect(jsonPath("$.created").value("2017-10-23T14:42:05"))
                .andExpect(jsonPath("$.discount").isEmpty())
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/3"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reservations/3{?projection}"))
                .andExpect(jsonPath("$._links.client.href").value("http://localhost/api/reservations/3/client{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reservations/3/restaurant{?projection}"))

                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestFields(
                                fieldWithPath("status").description(CONFIRMED),
                                fieldWithPath("notes").description("Restaurant notes")
                        ),
                        responseFields(
                                fieldWithPath("dateTime").description("Reservation date and time"),
                                fieldWithPath("guestNumber").description("Number of guests"),
                                fieldWithPath("comment").description("Comment to the reservation"),
                                fieldWithPath("notes").description("Restaurant notes"),
                                fieldWithPath("status").description("Status of the reservation. Must be CONFIRMED"),
                                fieldWithPath("created").description("Reservation creation date"),
                                fieldWithPath("discount").description("Discount %"),
                                subsectionWithPath("_links").ignored()
                        )));
    }
}
