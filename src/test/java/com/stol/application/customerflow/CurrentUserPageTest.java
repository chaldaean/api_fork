package com.stol.application.customerflow;

import com.github.database.rider.core.api.dataset.DataSet;
import com.stol.application.common.ApplicationSystemTest;
import org.hamcrest.Matchers;
import org.junit.Test;

import static com.stol.model.property.UserRole.ROLE_RESTAURANT_ADMIN;
import static com.stol.model.property.UserRole.ROLE_USER;
import static com.stol.model.property.UserStatus.ENABLE;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@DataSet(value = "user_roles.xml,application_restaurant_admin.xml,oauth.xml", executorId = "application", disableConstraints = true)
public class CurrentUserPageTest extends ApplicationSystemTest {

    @Test
    public void getCurrentLoggedInRestaurantAdminInformation() throws Exception {
        mockMvc.perform(get("/api/user/current?projection=manager")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(13))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("name"))
                .andExpect(jsonPath("$.surname").value("surname"))
                .andExpect(jsonPath("$.status").value(ENABLE.toString()))
                .andExpect(jsonPath("$.roles.length()").value(2))
                .andExpect(jsonPath("$.roles[*].role", Matchers.containsInAnyOrder(ROLE_RESTAURANT_ADMIN.name(), ROLE_USER.name())))
                .andExpect(jsonPath("$.photo").value("/files/1.jpg"))
                .andExpect(jsonPath("$.phoneNumber").value("381000000000"))
                .andExpect(jsonPath("$.registrationDate").value("2017-10-23T14:40:08"))
                .andExpect(jsonPath("$.email").value("user1@gmail.com"))
                .andExpect(jsonPath("$.restaurantIds.length()").value(2))
                .andExpect(jsonPath("$.restaurantIds[0]").value(1))
                .andExpect(jsonPath("$.restaurantIds[1]").value(2))
                .andExpect(jsonPath("$.restaurantNames[0]").value("name1"))
                .andExpect(jsonPath("$.restaurantNames[1]").value("name2"))

                .andExpect(jsonPath("$._links.length()").value(8))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/1{?projection}"))
                .andExpect(jsonPath("$._links.roles.href").value("http://localhost/api/users/1/roles"))
                .andExpect(jsonPath("$._links.photo.href").value("http://localhost/api/users/1/photo"))
                .andExpect(jsonPath("$._links.reservations.href").value("http://localhost/api/users/1/reservations{?projection}"))
                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/1/saved_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.ownRestaurants.href").value("http://localhost/api/users/1/own_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.resent_user_password.href").value("http://localhost/api/user/password/reset"))
                .andExpect(jsonPath("$._links.language.href").value("http://localhost/api/users/1/language{?projection}"))
                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("manager")
                        ),
                        responseFields(
                                fieldWithPath("id").description("Customer's ID"),
                                fieldWithPath("name").description("Name"),
                                fieldWithPath("surname").description("Surname"),
                                fieldWithPath("status").description("Status"),
                                fieldWithPath("roles[]").description("Roles"),
                                fieldWithPath("roles[].role").description("Role name"),
                                fieldWithPath("addRestaurantName").description("Name of the restaurant to add"),
                                fieldWithPath("photo").description("Photo resource url"),
                                fieldWithPath("phoneNumber").description("Phone number"),
                                fieldWithPath("email").description("Email"),
                                fieldWithPath("registrationDate").description("Customer registration date"),
                                fieldWithPath("restaurantIds[]").description("Ids of customer's restaurants"),
                                fieldWithPath("restaurantNames[]").description("Names of customer's restaurants"),
                                subsectionWithPath("_links").ignored()
                        )));

    }
}
