package com.stol.application.customerflow;

import com.stol.restdocs.PagingRequestParameters;
import com.stol.restdocs.PagingResponseFields;
import org.junit.Test;

import static com.stol.model.property.ReservationStatus.NEW;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class NewReservationsListPageTest extends CustomerApplicationSystemTest {
    @Test
    public void numberOfNewReservationsInRestaurantForLoggedInRestaurantAdmin() throws Exception {
        mockMvc.perform(get("/api/reservations/search/count_new")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$").value(4))
                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet)
                );
    }

    @Test
    public void allNewReservationsForLoggedInRestaurantAdmin() throws Exception {
        mockMvc.perform(get("/api/reservations/search/my_restaurants_new?projection=client")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[0].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[0].id").value("11"))
                .andExpect(jsonPath("$._embedded.reservations[0].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[0].date").value("2017-10-19"))
                .andExpect(jsonPath("$._embedded.reservations[0].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[0].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].comment").value("Some comment11"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantAddress").value("address1"))
                .andExpect(jsonPath("$._embedded.reservations[0].photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.self.href").value("http://localhost/api/reservations/11"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.reservation.href").value("http://localhost/api/reservations/11{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.client.href").value("http://localhost/api/reservations/11/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.restaurant.href").value("http://localhost/api/reservations/11/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[1].id").value("12"))
                .andExpect(jsonPath("$._embedded.reservations[1].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[1].date").value("2017-10-21"))
                .andExpect(jsonPath("$._embedded.reservations[1].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[1].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[1].comment").value("Some comment12"))
                .andExpect(jsonPath("$._embedded.reservations[1].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reservations[1].restaurantAddress").value("address1"))
                .andExpect(jsonPath("$._embedded.reservations[1].photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.self.href").value("http://localhost/api/reservations/12"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.reservation.href").value("http://localhost/api/reservations/12{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.client.href").value("http://localhost/api/reservations/12/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.restaurant.href").value("http://localhost/api/reservations/12/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[2].id").value("2"))
                .andExpect(jsonPath("$._embedded.reservations[2].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[2].date").value("2017-10-21"))
                .andExpect(jsonPath("$._embedded.reservations[2].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[2].guestNumber").value(2))
                .andExpect(jsonPath("$._embedded.reservations[2].comment").value("Some comment2"))
                .andExpect(jsonPath("$._embedded.reservations[2].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reservations[2].restaurantAddress").value("address1"))
                .andExpect(jsonPath("$._embedded.reservations[2].photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.self.href").value("http://localhost/api/reservations/2"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.reservation.href").value("http://localhost/api/reservations/2{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.client.href").value("http://localhost/api/reservations/2/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.restaurant.href").value("http://localhost/api/reservations/2/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[3].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[3].id").value("10"))
                .andExpect(jsonPath("$._embedded.reservations[3].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[3].date").value("2017-10-20"))
                .andExpect(jsonPath("$._embedded.reservations[3].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[3].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[3].comment").value("Some comment10"))
                .andExpect(jsonPath("$._embedded.reservations[3].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reservations[3].restaurantAddress").value("address1"))
                .andExpect(jsonPath("$._embedded.reservations[3].photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.self.href").value("http://localhost/api/reservations/10"))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.reservation.href").value("http://localhost/api/reservations/10{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.client.href").value("http://localhost/api/reservations/10/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.restaurant.href").value("http://localhost/api/reservations/10/restaurant{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/search/my_restaurants_new?projection=client"))

                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("client")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.reservations[]").description("List of reservations"),
                                fieldWithPath("_embedded.reservations[].id").description("Reservation's ID"),
                                fieldWithPath("_embedded.reservations[].date").description("Date"),
                                fieldWithPath("_embedded.reservations[].time").description("Time"),
                                fieldWithPath("_embedded.reservations[].guestNumber").description("Number of guests"),
                                fieldWithPath("_embedded.reservations[].status").description("Status"),
                                fieldWithPath("_embedded.reservations[].comment").description("Сomment to the reservation"),
                                fieldWithPath("_embedded.reservations[].restaurantName").description("Restaurant's name"),
                                fieldWithPath("_embedded.reservations[].restaurantAddress").description("Restaurant's address"),
                                fieldWithPath("_embedded.reservations[].photo").description("Restaurant's top picture url"),
                                fieldWithPath("_embedded.reservations[]._links.self.href").description("Reservation resource url"),
                                subsectionWithPath("_embedded.reservations[]._links.reservation").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.client").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.restaurant").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void allNewReservationsForLoggedInRestaurantAdminWithFilters() throws Exception {
        String url = String.format("/api/reservations?page=0&size=10&sort=created,desc&projection=client&restaurant.owners.credential.login=%S&status=NEW", LOGIN);
        mockMvc.perform(get(url)
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[0].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[0].id").value("11"))
                .andExpect(jsonPath("$._embedded.reservations[0].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[0].date").value("2017-10-19"))
                .andExpect(jsonPath("$._embedded.reservations[0].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[0].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].comment").value("Some comment11"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantAddress").value("address1"))
                .andExpect(jsonPath("$._embedded.reservations[0].photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.self.href").value("http://localhost/api/reservations/11"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.reservation.href").value("http://localhost/api/reservations/11{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.client.href").value("http://localhost/api/reservations/11/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.restaurant.href").value("http://localhost/api/reservations/11/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[1].id").value("12"))
                .andExpect(jsonPath("$._embedded.reservations[1].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[1].date").value("2017-10-21"))
                .andExpect(jsonPath("$._embedded.reservations[1].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[1].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[1].comment").value("Some comment12"))
                .andExpect(jsonPath("$._embedded.reservations[1].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reservations[1].restaurantAddress").value("address1"))
                .andExpect(jsonPath("$._embedded.reservations[1].photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.self.href").value("http://localhost/api/reservations/12"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.reservation.href").value("http://localhost/api/reservations/12{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.client.href").value("http://localhost/api/reservations/12/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.restaurant.href").value("http://localhost/api/reservations/12/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[2].id").value("2"))
                .andExpect(jsonPath("$._embedded.reservations[2].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[2].date").value("2017-10-21"))
                .andExpect(jsonPath("$._embedded.reservations[2].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[2].guestNumber").value(2))
                .andExpect(jsonPath("$._embedded.reservations[2].comment").value("Some comment2"))
                .andExpect(jsonPath("$._embedded.reservations[2].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reservations[2].restaurantAddress").value("address1"))
                .andExpect(jsonPath("$._embedded.reservations[2].photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.self.href").value("http://localhost/api/reservations/2"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.reservation.href").value("http://localhost/api/reservations/2{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.client.href").value("http://localhost/api/reservations/2/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.restaurant.href").value("http://localhost/api/reservations/2/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[3].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[3].id").value("10"))
                .andExpect(jsonPath("$._embedded.reservations[3].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[3].date").value("2017-10-20"))
                .andExpect(jsonPath("$._embedded.reservations[3].status").value(NEW.toString()))
                .andExpect(jsonPath("$._embedded.reservations[3].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[3].comment").value("Some comment10"))
                .andExpect(jsonPath("$._embedded.reservations[3].restaurantName").value("name1"))
                .andExpect(jsonPath("$._embedded.reservations[3].restaurantAddress").value("address1"))
                .andExpect(jsonPath("$._embedded.reservations[3].photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.self.href").value("http://localhost/api/reservations/10"))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.reservation.href").value("http://localhost/api/reservations/10{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.client.href").value("http://localhost/api/reservations/10/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[3]._links.restaurant.href").value("http://localhost/api/reservations/10/restaurant{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations{?projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/reservations"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/reservations/search"))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(10))
                .andExpect(jsonPath("$.page.totalElements").value(4))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("client"),
                                parameterWithName("restaurant.owners.credential.login").description("Customer phone number"),
                                parameterWithName("status").description("NEW"),
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size
                        ),
                        responseFields(
                                fieldWithPath("_embedded.reservations[]").description("List of reservations"),
                                fieldWithPath("_embedded.reservations[].id").description("Reservation's ID"),
                                fieldWithPath("_embedded.reservations[].date").description("Date"),
                                fieldWithPath("_embedded.reservations[].time").description("Time"),
                                fieldWithPath("_embedded.reservations[].guestNumber").description("Number of guests"),
                                fieldWithPath("_embedded.reservations[].status").description("Status"),
                                fieldWithPath("_embedded.reservations[].comment").description("Сomment to the reservation"),
                                fieldWithPath("_embedded.reservations[].restaurantName").description("Restaurant's name"),
                                fieldWithPath("_embedded.reservations[].restaurantAddress").description("Restaurant's address"),
                                fieldWithPath("_embedded.reservations[].photo").description("Restaurant's top picture url"),
                                fieldWithPath("_embedded.reservations[]._links.self.href").description("Reservation resource url"),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_embedded.reservations[]._links.reservation").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.client").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.restaurant").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));
    }
}
