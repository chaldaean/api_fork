package com.stol.application.customerflow;

import com.stol.model.Reservation;
import com.stol.repository.ReservationRepository;
import com.stol.restdocs.PagingResponseFields;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

import static com.stol.model.property.ReservationStatus.CONFIRMED;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ConfirmedReservationsListPageTest extends CustomerApplicationSystemTest {
    @Autowired
    private ReservationRepository reservationRepository;

    @Test
    public void allConfirmedReservationsBeforeVisitTimeForLoggedInRestaurantAdmin() throws Exception {
        mockMvc.perform(get("/api/reservations/search/my_restaurants_confirmed?projection=client")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(0));

        LocalDateTime visitDate = LocalDateTime.now().plusHours(1);
        Reservation confirmedReservation = reservationRepository.findById(6L).get();
        confirmedReservation.setDateTime(visitDate);
        reservationRepository.save(confirmedReservation);

        mockMvc.perform(get("/api/reservations/search/my_restaurants_confirmed?projection=client")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].length()").value(10))
                .andExpect(jsonPath("$._embedded.reservations[0].id").value(6))
                .andExpect(jsonPath("$._embedded.reservations[0].time").value(visitDate.toLocalTime().toString().substring(0, 5)))
                .andExpect(jsonPath("$._embedded.reservations[0].date").value(visitDate.toLocalDate().toString()))
                .andExpect(jsonPath("$._embedded.reservations[0].status").value(CONFIRMED.toString()))
                .andExpect(jsonPath("$._embedded.reservations[0].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].comment").value("Some comment6"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantName").value("name4"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantAddress").value("address4"))
                .andExpect(jsonPath("$._embedded.reservations[0].photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.self.href").value("http://localhost/api/reservations/6"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.reservation.href").value("http://localhost/api/reservations/6{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.client.href").value("http://localhost/api/reservations/6/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.restaurant.href").value("http://localhost/api/reservations/6/restaurant{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/search/my_restaurants_confirmed?projection=client&page=0&size=20"))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(1))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))
                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("client")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.reservations[]").description("List of reservations"),
                                fieldWithPath("_embedded.reservations[].id").description("Reservation's ID"),
                                fieldWithPath("_embedded.reservations[].date").description("Date"),
                                fieldWithPath("_embedded.reservations[].time").description("Time"),
                                fieldWithPath("_embedded.reservations[].guestNumber").description("Number of guests"),
                                fieldWithPath("_embedded.reservations[].comment").description("Сomment to the reservation"),
                                fieldWithPath("_embedded.reservations[].status").description("Status"),
                                fieldWithPath("_embedded.reservations[].restaurantName").description("Restaurant's name"),
                                fieldWithPath("_embedded.reservations[].restaurantAddress").description("Restaurant's address"),
                                fieldWithPath("_embedded.reservations[].photo").description("Restaurant's top picture url"),
                                fieldWithPath("_embedded.reservations[]._links.self.href").description("Reservation resource url"),
                                subsectionWithPath("_embedded.reservations[]._links.reservation").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.client").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.restaurant").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    //TODO add this functionality. Is it possible?
    public void allConfirmedReservationsBeforeVisitTimeForLoggedInRestaurantAdminWithFilters() throws Exception {
    }
}
