package com.stol.application.customerflow;

import com.stol.application.common.ApplicationSystemTest;
import org.junit.Before;

import java.util.Objects;

public abstract class CustomerApplicationSystemTest extends ApplicationSystemTest {
    protected static final String LOGIN = "381000000006";

    @Before
    public void before() throws Exception {
        if (Objects.isNull(token)) {
            token = getCredentialToken(String.format("grant_type=password&username=%s&password=%s", LOGIN, PASSWORD));
        }
    }
}
