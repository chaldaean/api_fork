package com.stol.application.customerflow;

import com.github.database.rider.core.api.dataset.DataSet;
import com.stol.application.common.ApplicationSystemTest;
import org.junit.Test;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@DataSet(value = "user_roles.xml,application_restaurant_admin.xml,oauth.xml", executorId = "application", disableConstraints = true)
public class MyRestaurantPageTest extends ApplicationSystemTest {

    @Test
    public void findAllRestaurantsForLoggedInRestaurantAdmin() throws Exception {
        mockMvc.perform(get("/api/restaurants/search/my?projection=name")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].id").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].active").value(true))
                .andExpect(jsonPath("$._embedded.restaurants[0].topPictureUrl").value("/files/2.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].ownerNames.length()").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].ownerNames[0])").value("name surname"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.self.href").value("http://localhost/api/restaurants/1"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.restaurant.href").value("http://localhost/api/restaurants/1{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.promotions.href").value("http://localhost/api/restaurants/1/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.owners.href").value("http://localhost/api/restaurants/1/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.happyHours.href").value("http://localhost/api/restaurants/1/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.topPicture.href").value("http://localhost/api/restaurants/1/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.cuisines.href").value("http://localhost/api/restaurants/1/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.pictures.href").value("http://localhost/api/restaurants/1/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.menu.href").value("http://localhost/api/restaurants/1/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.analogous.href").value("http://localhost/api/restaurants/1/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.marker.href").value("http://localhost/api/restaurants/1/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.tags.href").value("http://localhost/api/restaurants/1/tags{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name2"))
                .andExpect(jsonPath("$._embedded.restaurants[1].id").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[1].active").value(false))
                .andExpect(jsonPath("$._embedded.restaurants[1].topPictureUrl").value("/files/4.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[1].ownerNames.length()").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[1].ownerNames[0])").value("name surname"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.self.href").value("http://localhost/api/restaurants/2"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.restaurant.href").value("http://localhost/api/restaurants/2{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.promotions.href").value("http://localhost/api/restaurants/2/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.owners.href").value("http://localhost/api/restaurants/2/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.happyHours.href").value("http://localhost/api/restaurants/2/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.topPicture.href").value("http://localhost/api/restaurants/2/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.cuisines.href").value("http://localhost/api/restaurants/2/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.pictures.href").value("http://localhost/api/restaurants/2/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.menu.href").value("http://localhost/api/restaurants/2/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.analogous.href").value("http://localhost/api/restaurants/2/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.marker.href").value("http://localhost/api/restaurants/2/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.tags.href").value("http://localhost/api/restaurants/2/tags{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurants/search/my?projection=name"))

                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("feed")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.restaurants[]").description("List of restaurants"),
                                fieldWithPath("_embedded.restaurants[].id").description("Restaurant ID"),
                                fieldWithPath("_embedded.restaurants[].name").description("Restaurant name"),
                                fieldWithPath("_embedded.restaurants[].active").optional().description("True if active, otherwise false"),
                                fieldWithPath("_embedded.restaurants[].topPictureUrl").description("Main picture url"),
                                fieldWithPath("_embedded.restaurants[].ownerNames").optional().description("Names of restaurant owners"),
                                subsectionWithPath("_embedded.restaurants[]._links").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));
    }
}
