package com.stol.application.customerflow;

import com.stol.restdocs.PagingRequestParameters;
import com.stol.restdocs.PagingResponseFields;
import org.junit.Test;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationsStatisticPageTest extends CustomerApplicationSystemTest {

    @Test
    public void reservationsStatisticForLoggedInRestaurantAdminWithFilters() throws Exception {
        String url = "/api/reservations?page=0&size=10&sort=created,desc&status=CONFIRMED&projection=statistic&restaurant.owners.credential.login=%s";
        mockMvc.perform(get(String.format(url, LOGIN))
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[0].id").value(9))
                .andExpect(jsonPath("$._embedded.reservations[0].date").value("2000-10-22"))
                .andExpect(jsonPath("$._embedded.reservations[0].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].userId").value(100))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.self.href").value("http://localhost/api/reservations/9"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.reservation.href").value("http://localhost/api/reservations/9{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.client.href").value("http://localhost/api/reservations/9/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.restaurant.href").value("http://localhost/api/reservations/9/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1].id").value(6))
                .andExpect(jsonPath("$._embedded.reservations[1].date").value("2000-10-20"))
                .andExpect(jsonPath("$._embedded.reservations[1].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[1].userId").value(100))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.self.href").value("http://localhost/api/reservations/6"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.reservation.href").value("http://localhost/api/reservations/6{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.client.href").value("http://localhost/api/reservations/6/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.restaurant.href").value("http://localhost/api/reservations/6/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2].id").value(8))
                .andExpect(jsonPath("$._embedded.reservations[2].date").value("2000-10-22"))
                .andExpect(jsonPath("$._embedded.reservations[2].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[2].userId").value(100))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.self.href").value("http://localhost/api/reservations/8"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.reservation.href").value("http://localhost/api/reservations/8{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.client.href").value("http://localhost/api/reservations/8/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.restaurant.href").value("http://localhost/api/reservations/8/restaurant{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations{?projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/reservations"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/reservations/search"))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(10))
                .andExpect(jsonPath("$.page.totalElements").value(4))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("customer-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("statistic"),
                                parameterWithName("restaurant.owners.credential.login").description("Customer phone number"),
                                parameterWithName("status").description("CONFIRMED"),
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size
                        ),
                        responseFields(
                                fieldWithPath("_embedded.reservations[]").description("List of reservations"),
                                fieldWithPath("_embedded.reservations[].id").description("Reservation's ID"),
                                fieldWithPath("_embedded.reservations[].date").description("Date"),
                                fieldWithPath("_embedded.reservations[].guestNumber").description("Number of guests"),
                                fieldWithPath("_embedded.reservations[].userId").description("Restaurant's top picture url"),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_embedded.reservations[]._links").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));

    }
}
