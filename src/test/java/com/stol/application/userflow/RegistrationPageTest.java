package com.stol.application.userflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.application.common.ApplicationSystemTest;
import com.stol.controller.UserActivationController;
import com.stol.model.user.Role;
import com.stol.model.user.User;
import com.stol.service.user.UserService;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static com.stol.exception.ServerExceptionHandler.INVALID_FIELDS_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static com.stol.exception.UserAlreadyExist.USER_ALREADY_EXIST_LOG_REF;
import static com.stol.model.property.UserRole.ROLE_USER;
import static com.stol.model.property.UserStatus.NOT_CONFIRMED;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RegistrationPageTest extends ApplicationSystemTest {
    private final static String NAME = "name";
    private final static String SURNAME = "surname";
    private final static String PHONE = "380975553322";
    private final static String PASSWORD = "password";

    @SpyBean
    private UserService userService;

    @SpyBean
    private UserActivationController userActivationController;

    @Test
    public void registerNewUserInApplication() throws Exception {
        doAnswer(u -> null).when(userActivationController).activateUser((any()));
        
        String user = user(NAME, SURNAME, PHONE, PASSWORD);
        mockMvc.perform(post("/api/user/registration").content(user)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", containsString("http://localhost/api/users/")))
                .andExpect(content().string(""))
                .andDo(document("user-flow/{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("name").description("Name")
                                        .attributes(key("constraints").value("Not empty")),
                                fieldWithPath("surname").description("Surname"),
                                fieldWithPath("login").description("Phone number")
                                        .attributes(key("constraints").value("Not empty. Without leading \"+\"")),
                                fieldWithPath("password").description("Password")
                                        .attributes(key("constraints").value("Not empty")),
                                fieldWithPath("language").description("Language code (possible values: ru, uk, en)")
                        )
                ));

        ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
        verify(userService).saveUser(argument.capture());
        User expectedUser = argument.getValue();
        assertEquals("name", expectedUser.getName());
        assertEquals("surname", expectedUser.getSurname());
        assertEquals("380975553322", expectedUser.getPhoneNumber());
        assertEquals(NOT_CONFIRMED, expectedUser.getStatus());

        assertNull(expectedUser.getAddRestaurantRequest());

        List<Role> expectedRoles = new ArrayList<>(expectedUser.getRoles());
        assertEquals(1, expectedRoles.size());
        assertEquals(ROLE_USER, expectedRoles.get(0).getRole());


        verify(userActivationController, only()).activateUser(any());
    }

    @Test
    public void registerNewUserEmptyFields() throws Exception {
        String user = user("", "", "", "");

        String[] errors = new String[]{
                "Field \"name\": must match \"^[\\p{L} ]+$\"",
                "Field \"login\": must not be empty",
                "Field \"password\": must not be empty",
                "Field \"name\": must not be empty",
                "Field \"surname\": must not be empty",
                "Field \"name\": size must be between 2 and 30",
                "Field \"password\": size must be between 6 and 100",
                "Field \"surname\": size must be between 2 and 30",
                "Field \"surname\": must match \"^[\\p{L} ]+$\""
        };

        mockMvc.perform(post("/api/user/registration").content(user)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, "en"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(9))
                .andExpect(jsonPath("$[*].logref").value(everyItem(equalTo(INVALID_FIELDS_LOG_REF))))
                .andExpect(jsonPath("$[*].message").value(containsInAnyOrder(errors)))
                .andExpect(jsonPath("$[*].links").value(everyItem(empty())))
                .andDo(document("user-flow/{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("[].logref").description(INVALID_FIELDS_LOG_REF),
                                fieldWithPath("[].message").description("The name of the empty field"),
                                fieldWithPath("[].links").ignored()
                        )
                ));
    }

    @Test
    public void registerNewUserLoginAlreadyExist() throws Exception {
        String login = "381000000000";
        String user = user(NAME, SURNAME, login, PASSWORD);

        mockMvc.perform(post("/api/user/registration").content(user)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].length()").value(3))
                .andExpect(jsonPath("$[0].logref").value(USER_ALREADY_EXIST_LOG_REF))
                .andExpect(jsonPath("$[0].message").value("Пользователь с логином: " + login + " уже существует!"))
                .andExpect(jsonPath("$[0].links").isEmpty())
                .andDo(document("user-flow/{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("[].logref").description(INVALID_FIELDS_LOG_REF),
                                fieldWithPath("[].message").description("Пользователь с логином: " + login + " уже существует!"),
                                fieldWithPath("[].links").ignored()
                        )
                ));

        mockMvc.perform(post("/api/user/registration").content(user)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$[0].message").value("Користувач з логіном: " + login +" вже існує!"));

        mockMvc.perform(post("/api/user/registration").content(user)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$[0].message").value("User with login: " + login + " already exist!"));
    }

    public static String user(String name, String surname, String login, String password) throws JsonProcessingException {
        Map<String, String> user = new HashMap<>();
        user.put("name", name);
        user.put("surname", surname);
        user.put("login", login);
        user.put("password", password);
        user.put("language", LANGUAGE_UK);

        return new ObjectMapper().writeValueAsString(user);
    }
}
