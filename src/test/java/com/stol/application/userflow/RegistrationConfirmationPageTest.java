package com.stol.application.userflow;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.application.common.ApplicationSystemTest;
import com.stol.controller.model.UserActivation;
import com.stol.controller.model.UserLogin;
import com.stol.model.user.UserSystemInformation;
import com.stol.repository.UserSystemInformationRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;

import static com.stol.configuration.constant.Localization.LANGUAGE_EN;
import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static com.stol.configuration.constant.Messages.*;
import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static org.hamcrest.Matchers.isEmptyString;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RegistrationConfirmationPageTest extends ApplicationSystemTest {
    private final static String PHONE_NOT_CONFIRMED_1 = "381000000001";
    private final static String PHONE_NOT_CONFIRMED_2 = "381000000002";
    private final static String PHONE_ALREADY_ACTIVATED = "381000000000";
    private final static String NOT_CONFIRMED_ACTIVATION_CODE_1 = "3455";
    private final static String ACTIVATION_CODE__ALREADY_ACTIVATED = "1234";

    @Value("${stol.login.cooldown.expiration-time}")
    private long EXPIRATION_TIME_SECONDS;

    @Autowired
    private UserSystemInformationRepository smsRepository;

    @Test
    public void registrationConfirmationBySMSCode() throws Exception {
        UserActivation act = new UserActivation(PHONE_NOT_CONFIRMED_1, NOT_CONFIRMED_ACTIVATION_CODE_1);
        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()))
                .andDo(document("user-flow/{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("phone").description("Phone number"),
                                fieldWithPath("code").description("Activation code")
                        )
                ));
    }

    @Test
    public void registrationConfirmationBySMSCodeNoSuchUser() throws Exception {
        String login = "+380011111111";
        UserActivation act = new UserActivation(login, "0000");

        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Нет пользователя с логином: " + login + "!"))
                .andExpect(jsonPath("$.links").isEmpty())
                .andDo(document("user-flow/{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("logref").description(DEFAULT_LOG_REF),
                                fieldWithPath("message").description("Нет пользователя с логином: " + login + "!"),
                                fieldWithPath("links").ignored()
                        )
                ));

        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Немає користувача з логіном: " + login + "!"));

        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("There is no user with login: " + login + "!"));
    }

    @Test
    public void registrationConfirmationBySMSCodeInvalidCode() throws Exception {
        UserActivation act = new UserActivation(PHONE_NOT_CONFIRMED_2, "0000");
        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].length()").value(3))
                .andExpect(jsonPath("$[0].logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$[0].message").value("Неверный код активации!"))
                .andExpect(jsonPath("$[0].links").isEmpty())
                .andDo(document("user-flow/{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("[].logref").description(DEFAULT_LOG_REF),
                                fieldWithPath("[].message").description("Неверный код активации!"),
                                fieldWithPath("[].links").ignored()
                        )
                ));

        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].length()").value(3))
                .andExpect(jsonPath("$[0].logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$[0].message").value("Невірний код активації!"))
                .andExpect(jsonPath("$[0].links").isEmpty());

        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].length()").value(3))
                .andExpect(jsonPath("$[0].logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$[0].message").value("Activation code is invalid!"))
                .andExpect(jsonPath("$[0].links").isEmpty());
    }

    @Test
    public void registrationConfirmationBySMSCodeInvalidCode_uk() throws Exception {
        UserActivation act = new UserActivation(PHONE_NOT_CONFIRMED_2, "0000");
        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].length()").value(3))
                .andExpect(jsonPath("$[0].logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$[0].message").value("Невірний код активації!"))
                .andExpect(jsonPath("$[0].links").isEmpty());
    }

    @Test
    public void registrationConfirmationBySMSCodeInvalidCode_en() throws Exception {
        UserActivation act = new UserActivation(PHONE_NOT_CONFIRMED_2, "0000");
        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].length()").value(3))
                .andExpect(jsonPath("$[0].logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$[0].message").value("Activation code is invalid!"))
                .andExpect(jsonPath("$[0].links").isEmpty());
    }


    @Test
    public void registrationConfirmationBySMSCodeReachNumberOfTriesAndTimeLimitExpired() throws Exception {
        String phone = "381000000003";
        String content = new ObjectMapper().writeValueAsString(new UserLogin(phone));

        mockMvc.perform(post("/api/user/activation/resend").content(content)
                .contentType(APPLICATION_JSON_UTF8));
        mockMvc.perform(post("/api/user/activation/resend").content(content)
                .contentType(APPLICATION_JSON_UTF8));
        mockMvc.perform(post("/api/user/activation/resend").content(content)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden());

        minus24HoursToActivateUserTime(phone);

        mockMvc.perform(post("/api/user/activation/resend").content(content)
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void registrationConfirmationBySMSCodeAlreadyActivatedUser() throws Exception {
        UserActivation act = new UserActivation(PHONE_ALREADY_ACTIVATED, ACTIVATION_CODE__ALREADY_ACTIVATED);

        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Пользователь уже активирован!"))
                .andExpect(jsonPath("$.links").isEmpty())
                .andDo(document("user-flow/{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("logref").description(DEFAULT_LOG_REF),
                                fieldWithPath("message").description("Пользователь уже активирован!"),
                                fieldWithPath("links").ignored()
                        )
                ));

        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Користувач вже активований!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(post("/api/user/activation").content(new ObjectMapper().writeValueAsString(act))
                .contentType(APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("User already activated!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    @Test
    public void registrationConfirmationBySMSCodeResendActivationCode() throws Exception {
        String login = "381000000004";

        mockMvc.perform(post("/api/user/activation/resend").content(new ObjectMapper().writeValueAsString(new UserLogin(login)))
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()))
                .andDo(document("user-flow/{class-name}/{method-name}",
                        requestFields(
                                fieldWithPath("login").description("Phone number")
                        )
                ));
    }

    @Test
    public void registrationConfirmationBySMSCodeResendLimit() throws Exception {
        String login = "381000000005";
        String content = new ObjectMapper().writeValueAsString(new UserLogin(login));
        mockMvc.perform(post("/api/user/activation/resend").content(content)
                .contentType(MediaType.APPLICATION_JSON_UTF8));
        mockMvc.perform(post("/api/user/activation/resend").content(content)
                .contentType(MediaType.APPLICATION_JSON_UTF8));

        mockMvc.perform(post("/api/user/activation/resend").content(content)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Использованы все попытки. Попробуйте еще рез через 24 часа!"))
                .andExpect(jsonPath("$.links").isEmpty())
                .andDo(document("user-flow/{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath("logref").description(DEFAULT_LOG_REF),
                                fieldWithPath("message").description("Использованы все попытки. Попробуйте еще рез через 24 часа!"),
                                fieldWithPath("links").ignored()
                        )
                ));

        mockMvc.perform(post("/api/user/activation/resend").content(content)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Використані всі спроби. Спробуйте через 24 години!"))
                .andExpect(jsonPath("$.links").isEmpty());

        mockMvc.perform(post("/api/user/activation/resend").content(content)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_EN)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("You have reached maximum number of tries. Try after 24 hours!"))
                .andExpect(jsonPath("$.links").isEmpty());
    }

    private void minus24HoursToActivateUserTime(String phone) {
        UserSystemInformation userSystemInformation = smsRepository.findByPhoneNumber(phone);
        LocalDateTime newTime = LocalDateTime.now(Clock.offset(Clock.systemUTC(), Duration.ofHours(-EXPIRATION_TIME_SECONDS)));
        userSystemInformation.setActivationCodeCreation(newTime);
        smsRepository.save(userSystemInformation);
    }
}
