package com.stol.application.userflow;

import com.stol.application.common.ApplicationSystemTest;
import com.stol.restdocs.PagingRequestParameters;
import com.stol.restdocs.PagingResponseFields;
import org.junit.Test;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestaurantSelectionPageTest extends ApplicationSystemTest {

    @Test
    public void restaurantSelectionsFeed() throws Exception {
        mockMvc.perform(get("/api/restaurant_selections?projection=feed")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurantSelections.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].length()").value(4))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].title").value("Selection title1"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].total").value(2))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].icon").value("/files/23.jpg"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0]._links.self.href").value("http://localhost/api/restaurant_selections/1"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0]._links.restaurantSelection.href").value("http://localhost/api/restaurant_selections/1{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0]._links.picture.href").value("http://localhost/api/restaurant_selections/1/picture"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0]._links.restaurants.href").value("http://localhost/api/restaurant_selections/1/restaurants{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[1].length()").value(4))
                .andExpect(jsonPath("$._embedded.restaurantSelections[1].title").value("Selection title3"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[1].total").value(3))
                .andExpect(jsonPath("$._embedded.restaurantSelections[1].icon").value("/files/25.jpg"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[1]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.restaurantSelections[1]._links.self.href").value("http://localhost/api/restaurant_selections/3"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[1]._links.restaurantSelection.href").value("http://localhost/api/restaurant_selections/3{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[1]._links.picture.href").value("http://localhost/api/restaurant_selections/3/picture"))
                .andExpect(jsonPath("$._embedded.restaurantSelections[1]._links.restaurants.href").value("http://localhost/api/restaurant_selections/3/restaurants{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurant_selections{?page,size,sort,projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/restaurant_selections"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/restaurant_selections/search"))

                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(2))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size,
                                parameterWithName("projection").description("feed")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.restaurantSelections[]").description("List of restaurant selections"),
                                fieldWithPath("_embedded.restaurantSelections[].title").description("Title"),
                                fieldWithPath("_embedded.restaurantSelections[].total").description("Total number of restaurants"),
                                fieldWithPath("_embedded.restaurantSelections[].icon").description("Icon url"),
                                fieldWithPath("_embedded.restaurantSelections[]._links.restaurants.href").description("Restaurants list resource url"),
                                fieldWithPath("_embedded.restaurantSelections[]._links.restaurants.templated").ignored(),
                                subsectionWithPath("_embedded.restaurantSelections[]._links.self").ignored(),
                                subsectionWithPath("_embedded.restaurantSelections[]._links.restaurantSelection").ignored(),
                                subsectionWithPath("_embedded.restaurantSelections[]._links.picture").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void restaurantSelectionsFeedLocalized() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/restaurant_selections?projection=feed")
                .header(AUTHORIZATION, token))
                .andExpect(jsonPath("$._embedded.restaurantSelections[0].title").value("Підбірка1"));
    }

    @Test
    public void articlesFeed() throws Exception {
        mockMvc.perform(get("/api/articles?projection=feed")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.articles.length()").value(2))
                .andExpect(jsonPath("$._embedded.articles[0].length()").value(3))
                .andExpect(jsonPath("$._embedded.articles[0].title").value("Title1"))
                .andExpect(jsonPath("$._embedded.articles[0].icon").value("/files/26.jpg"))
                .andExpect(jsonPath("$._embedded.articles[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.articles[0]._links.self.href").value("http://localhost/api/articles/1"))
                .andExpect(jsonPath("$._embedded.articles[0]._links.article.href").value("http://localhost/api/articles/1{?projection}"))
                .andExpect(jsonPath("$._embedded.articles[0]._links.picture.href").value("http://localhost/api/articles/1/picture"))
                .andExpect(jsonPath("$._embedded.articles[0]._links.author.href").value("http://localhost/api/articles/1/author{?projection}"))
                .andExpect(jsonPath("$._embedded.articles[1].length()").value(3))
                .andExpect(jsonPath("$._embedded.articles[1].title").value("Title3"))
                .andExpect(jsonPath("$._embedded.articles[1].icon").value("/files/28.jpg"))
                .andExpect(jsonPath("$._embedded.articles[1]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.articles[1]._links.self.href").value("http://localhost/api/articles/3"))
                .andExpect(jsonPath("$._embedded.articles[1]._links.article.href").value("http://localhost/api/articles/3{?projection}"))
                .andExpect(jsonPath("$._embedded.articles[1]._links.picture.href").value("http://localhost/api/articles/3/picture"))
                .andExpect(jsonPath("$._embedded.articles[1]._links.author.href").value("http://localhost/api/articles/3/author{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/articles{?page,size,sort,projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/articles"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/articles/search"))

                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(2))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size,
                                parameterWithName("projection").description("feed")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.articles[]").description("List of articles"),
                                fieldWithPath("_embedded.articles[].title").description("Title"),
                                fieldWithPath("_embedded.articles[].icon").description("Icon url"),
                                fieldWithPath("_embedded.articles[]._links.self.href").description("Article resource url"),
                                subsectionWithPath("_embedded.articles[]._links.article").ignored(),
                                subsectionWithPath("_embedded.articles[]._links.picture").ignored(),
                                subsectionWithPath("_embedded.articles[]._links.author").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void articlesFeedLocalized() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/articles?projection=feed")
                .header(AUTHORIZATION, token))
                .andExpect(jsonPath("$._embedded.articles[0].title").value("Заголовок1"))
                .andExpect(jsonPath("$._embedded.articles[1].title").value("Заголовок3"));
    }

    @Test
    public void allRestaurantsFromSelections() throws Exception {
        mockMvc.perform(get("/api/restaurant_selections/3/restaurants?projection=feed")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(3))

                .andExpect(jsonPath("$._embedded.restaurants[0].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[0].id").value("5"))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name5"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.name").value("tag1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.iconUrl").value("/files/7.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].averageTicket").value(400))
                .andExpect(jsonPath("$._embedded.restaurants[0].topPictureUrl").value("/files/5.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.name").value("cuisine1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.iconUrl").value("/files/10.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].totalReviews").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].rating").value(5.0))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts.length()").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].quantity").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].discount").value(10))
                .andExpect(jsonPath("$._embedded.restaurants[0].latitude").value(15.00))
                .andExpect(jsonPath("$._embedded.restaurants[0].longitude").value(13.00))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerTitle").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerName").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerUrl").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[0].openHours.length()").exists())
                .andExpect(jsonPath("$._embedded.restaurants[0].saved").value(Boolean.FALSE))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.self.href").value("http://localhost/api/restaurants/5"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.restaurant.href").value("http://localhost/api/restaurants/5{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.promotions.href").value("http://localhost/api/restaurants/5/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.owners.href").value("http://localhost/api/restaurants/5/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.happyHours.href").value("http://localhost/api/restaurants/5/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.topPicture.href").value("http://localhost/api/restaurants/5/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.cuisines.href").value("http://localhost/api/restaurants/5/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.pictures.href").value("http://localhost/api/restaurants/5/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.menu.href").value("http://localhost/api/restaurants/5/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.analogous.href").value("http://localhost/api/restaurants/5/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.marker.href").value("http://localhost/api/restaurants/5/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.tags.href").value("http://localhost/api/restaurants/5/tags{?projection}"))

                .andExpect(jsonPath("$._embedded.restaurants[1].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[1].id").value("1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].tag.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[1].tag.name").value("tag3"))
                .andExpect(jsonPath("$._embedded.restaurants[1].tag.iconUrl").value("/files/9.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[1].averageTicket").value(400))
                .andExpect(jsonPath("$._embedded.restaurants[1].topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[1].cuisine.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[1].cuisine.name").value("cuisine1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].cuisine.iconUrl").value("/files/10.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[1].totalReviews").value(4))
                .andExpect(jsonPath("$._embedded.restaurants[1].rating").value(4.5))
                .andExpect(jsonPath("$._embedded.restaurants[1].discounts.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[1].discounts[0].quantity").value(5))
                .andExpect(jsonPath("$._embedded.restaurants[1].discounts[0].discount").value(90))
                .andExpect(jsonPath("$._embedded.restaurants[1].discounts[1].quantity").value(10))
                .andExpect(jsonPath("$._embedded.restaurants[1].discounts[1].discount").value(20))
                .andExpect(jsonPath("$._embedded.restaurants[1].latitude").value(1.33))
                .andExpect(jsonPath("$._embedded.restaurants[1].longitude").value(2.00))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerTitle").value("10%"))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerName").value("discount"))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerUrl").value("/files/29.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours.length()").exists())
                .andExpect(jsonPath("$._embedded.restaurants[1].saved").value(Boolean.TRUE))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.self.href").value("http://localhost/api/restaurants/1"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.restaurant.href").value("http://localhost/api/restaurants/1{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.promotions.href").value("http://localhost/api/restaurants/1/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.owners.href").value("http://localhost/api/restaurants/1/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.happyHours.href").value("http://localhost/api/restaurants/1/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.topPicture.href").value("http://localhost/api/restaurants/1/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.cuisines.href").value("http://localhost/api/restaurants/1/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.pictures.href").value("http://localhost/api/restaurants/1/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.menu.href").value("http://localhost/api/restaurants/1/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.analogous.href").value("http://localhost/api/restaurants/1/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.marker.href").value("http://localhost/api/restaurants/1/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.tags.href").value("http://localhost/api/restaurants/1/tags{?projection}"))

                .andExpect(jsonPath("$._embedded.restaurants[2].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[2].id").value("6"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("name6"))
                .andExpect(jsonPath("$._embedded.restaurants[2].tag").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].averageTicket").value(200))
                .andExpect(jsonPath("$._embedded.restaurants[2].topPictureUrl").value("/files/6.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[2].cuisine").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].totalReviews").value(0))
                .andExpect(jsonPath("$._embedded.restaurants[2].rating").value(4.0))
                .andExpect(jsonPath("$._embedded.restaurants[2].discounts").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].latitude").value(1.20))
                .andExpect(jsonPath("$._embedded.restaurants[2].longitude").value(3.30))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerTitle").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerName").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerUrl").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].openHours.length()").value(7))
                .andExpect(jsonPath("$._embedded.restaurants[2].saved").value(Boolean.TRUE))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.self.href").value("http://localhost/api/restaurants/6"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.restaurant.href").value("http://localhost/api/restaurants/6{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.promotions.href").value("http://localhost/api/restaurants/6/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.owners.href").value("http://localhost/api/restaurants/6/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.happyHours.href").value("http://localhost/api/restaurants/6/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.topPicture.href").value("http://localhost/api/restaurants/6/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.cuisines.href").value("http://localhost/api/restaurants/6/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.pictures.href").value("http://localhost/api/restaurants/6/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.menu.href").value("http://localhost/api/restaurants/6/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.analogous.href").value("http://localhost/api/restaurants/6/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.marker.href").value("http://localhost/api/restaurants/6/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.tags.href").value("http://localhost/api/restaurants/6/tags{?projection}"))


                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurant_selections/3/restaurants?projection=feed"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("feed")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.restaurants[]").description("List of restaurants"),
                                fieldWithPath("_embedded.restaurants[].id").description("Restaurant ID"),
                                fieldWithPath("_embedded.restaurants[].name").description("Name"),
                                fieldWithPath("_embedded.restaurants[].tag").optional().description("First tag attached"),
                                fieldWithPath("_embedded.restaurants[].tag.iconUrl").optional().description("Url of the tag icon"),
                                fieldWithPath("_embedded.restaurants[].tag.name").optional().description("Tag name"),
                                fieldWithPath("_embedded.restaurants[].averageTicket").description("Average bill"),
                                fieldWithPath("_embedded.restaurants[].topPictureUrl").description("Main picture url"),
                                fieldWithPath("_embedded.restaurants[].cuisine").optional().description("First cuisine"),
                                fieldWithPath("_embedded.restaurants[].cuisine.name").optional().description("Cuisine name"),
                                fieldWithPath("_embedded.restaurants[].cuisine.iconUrl").optional().description("Url of the cuisine icon"),
                                fieldWithPath("_embedded.restaurants[].totalReviews").description("Number of reviews"),
                                fieldWithPath("_embedded.restaurants[].rating").description("Rating"),
                                fieldWithPath("_embedded.restaurants[].discounts[]").description("Discounts available"),
                                fieldWithPath("_embedded.restaurants[].discounts[].quantity").description("Quantity of discounts"),
                                fieldWithPath("_embedded.restaurants[].discounts[].discount").description("Discount %"),
                                subsectionWithPath("_embedded.restaurants[].discounts[]._links").ignored(),
                                fieldWithPath("_embedded.restaurants[].latitude").description("Latitude"),
                                fieldWithPath("_embedded.restaurants[].longitude").description("Longitude"),
                                fieldWithPath("_embedded.restaurants[].markerTitle").description("Marker title"),
                                fieldWithPath("_embedded.restaurants[].markerName").description("Marker name"),
                                fieldWithPath("_embedded.restaurants[].markerUrl").description("Marker icon url"),
                                fieldWithPath("_embedded.restaurants[].openHours[]").description("Open hours by weekday"),
                                fieldWithPath("_embedded.restaurants[].openHours[].weekday").description("Weekday number (Monday = 1)"),
                                fieldWithPath("_embedded.restaurants[].openHours[].openTime").description("Open time"),
                                fieldWithPath("_embedded.restaurants[].openHours[].closeTime").description("Close time"),
                                fieldWithPath("_embedded.restaurants[].saved").description("If this restaurant is added to the user's favourite list"),
                                subsectionWithPath("_embedded.restaurants[]._links").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));
    }
}
