package com.stol.application.userflow;

import com.stol.application.common.ApplicationSystemTest;
import org.junit.Test;

import static org.hamcrest.Matchers.isEmptyString;
import static org.springframework.data.rest.webmvc.RestMediaTypes.TEXT_URI_LIST_VALUE;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class FavouriteRestaurantPageTest extends ApplicationSystemTest {
    @Test
    public void addRestaurantToUserFavorites() throws Exception {
        mockMvc.perform(patch("/api/users/100/saved_restaurants").content("http://localhost/api/restaurants/2")
                .header(AUTHORIZATION, token)
                .header(CONTENT_TYPE, TEXT_URI_LIST_VALUE)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet
                ));
    }

    @Test
    public void deleteLikedRestaurantToUser() throws Exception {
        mockMvc.perform(delete("/api/users/100/saved_restaurants/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet
                ));
    }
}
