package com.stol.application.userflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.application.common.ApplicationSystemTest;
import com.stol.model.Review;
import com.stol.restdocs.ConstrainedFields;
import com.stol.restdocs.PagingRequestParameters;
import com.stol.restdocs.PagingResponseFields;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReviewNotificationPageTest extends ApplicationSystemTest {
    @Test
    public void findAllUsersReviewNotification() throws Exception {
        String rn = "$._embedded.reviewNotifications";
        mockMvc.perform(get("/api/review_notifications/search/my?projection=check")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath(rn + ".length()").value(2))
                .andExpect(jsonPath(rn + "[0].id").value(1))
                .andExpect(jsonPath(rn + "[0].reservationId").value(5))
                .andExpect(jsonPath(rn + "[0].guestNumber").value(1))
                .andExpect(jsonPath(rn + "[0].topPictureUrl").value("/files/4.jpg"))
                .andExpect(jsonPath(rn + "[0].time").value("10:00"))
                .andExpect(jsonPath(rn + "[0].date").value("2089-10-23"))
                .andExpect(jsonPath(rn + "[0].restaurantId").value(4))
                .andExpect(jsonPath(rn + "[0].restaurantRating").value(1.0))
                .andExpect(jsonPath(rn + "[0].restaurantName").value("name4"))
                .andExpect(jsonPath(rn + "[0].restaurantAddress").value("address4"))
                .andExpect(jsonPath(rn + "[0]._links.length()").value(5))
                .andExpect(jsonPath(rn + "[0]._links.self.href").value("http://localhost/api/review_notifications/1"))
                .andExpect(jsonPath(rn + "[0]._links.reviewNotification.href").value("http://localhost/api/review_notifications/1{?projection}"))
                .andExpect(jsonPath(rn + "[0]._links.reservation.href").value("http://localhost/api/review_notifications/1/reservation{?projection}"))
                .andExpect(jsonPath(rn + "[0]._links.user.href").value("http://localhost/api/review_notifications/1/user{?projection}"))
                .andExpect(jsonPath(rn + "[0]._links.restaurant.href").value("http://localhost/api/review_notifications/1/restaurant{?projection}"))
                .andExpect(jsonPath(rn + "[1].id").value(3))
                .andExpect(jsonPath(rn + "[1].reservationId").value(7))
                .andExpect(jsonPath(rn + "[1].guestNumber").value(1))
                .andExpect(jsonPath(rn + "[1].topPictureUrl").value("/files/4.jpg"))
                .andExpect(jsonPath(rn + "[1].time").value("11:00"))
                .andExpect(jsonPath(rn + "[1].date").value("2017-10-22"))
                .andExpect(jsonPath(rn + "[1].restaurantId").value(4))
                .andExpect(jsonPath(rn + "[1].restaurantRating").value(1.0))
                .andExpect(jsonPath(rn + "[1].restaurantName").value("name4"))
                .andExpect(jsonPath(rn + "[1].restaurantAddress").value("address4"))
                .andExpect(jsonPath(rn + "[1]._links.length()").value(5))
                .andExpect(jsonPath(rn + "[1]._links.self.href").value("http://localhost/api/review_notifications/3"))
                .andExpect(jsonPath(rn + "[1]._links.reviewNotification.href").value("http://localhost/api/review_notifications/3{?projection}"))
                .andExpect(jsonPath(rn + "[1]._links.reservation.href").value("http://localhost/api/review_notifications/3/reservation{?projection}"))
                .andExpect(jsonPath(rn + "[1]._links.user.href").value("http://localhost/api/review_notifications/3/user{?projection}"))
                .andExpect(jsonPath(rn + "[1]._links.restaurant.href").value("http://localhost/api/review_notifications/3/restaurant{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/review_notifications/search/my?projection=check&page=0&size=20"))

                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(2))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size,
                                parameterWithName("projection").description("check")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.reviewNotifications[]").description("List of notifications to leave a review"),
                                fieldWithPath("_embedded.reviewNotifications[].id").description("Notification's ID"),
                                fieldWithPath("_embedded.reviewNotifications[].reservationId").description("Reservation's ID"),
                                fieldWithPath("_embedded.reviewNotifications[].guestNumber").description("Number of guests"),
                                fieldWithPath("_embedded.reviewNotifications[].topPictureUrl").description("Restaurant top picture url"),
                                fieldWithPath("_embedded.reviewNotifications[].date").description("Reservation date"),
                                fieldWithPath("_embedded.reviewNotifications[].time").description("Reservation time"),
                                fieldWithPath("_embedded.reviewNotifications[].restaurantId").description("Restaurant id"),
                                fieldWithPath("_embedded.reviewNotifications[].restaurantName").description("Restaurant name"),
                                fieldWithPath("_embedded.reviewNotifications[].restaurantAddress").description("Restaurant address"),
                                fieldWithPath("_embedded.reviewNotifications[].restaurantRating").description("Restaurant rating"),
                                fieldWithPath("_embedded.reviewNotifications[]._links.reservation.href").description("Reservation relation resource url"),
                                fieldWithPath("_embedded.reviewNotifications[]._links.reservation.templated").ignored(),
                                subsectionWithPath("_embedded.reviewNotifications[]._links.self").ignored(),
                                subsectionWithPath("_embedded.reviewNotifications[]._links.reviewNotification").ignored(),
                                subsectionWithPath("_embedded.reviewNotifications[]._links.user").ignored(),
                                subsectionWithPath("_embedded.reviewNotifications[]._links.restaurant").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void findAllUsersReviewNotificationLocalized() throws Exception {
        changeLanguageToUk();

        String rn = "$._embedded.reviewNotifications";
        mockMvc.perform(get("/api/review_notifications/search/my?projection=check")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(jsonPath(rn + "[0].restaurantName").value("Ім`я4"))
                .andExpect(jsonPath(rn + "[0].restaurantAddress").value("адреса4"))
                .andExpect(jsonPath(rn + "[1].restaurantName").value("Ім`я4"))
                .andExpect(jsonPath(rn + "[1].restaurantAddress").value("адреса4"));
    }


    @Test
    public void showReviewPopupForReservation() throws Exception {
        mockMvc.perform(get("/api/reservations/2?projection=review")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(12))
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$.restaurantName").value("name1"))
                .andExpect(jsonPath("$.restaurantId").value("1"))
                .andExpect(jsonPath("$.restaurantRating").value("4.5"))
                .andExpect(jsonPath("$.restaurantAddress").value("address1"))
                .andExpect(jsonPath("$.time").value("09:30"))
                .andExpect(jsonPath("$.date").value("2017-10-21"))
                .andExpect(jsonPath("$.status").value("NEW"))
                .andExpect(jsonPath("$.guestNumber").value(2))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/2"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reservations/2{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reservations/2/restaurant{?projection}"))
                .andExpect(jsonPath("$._links.client.href").value("http://localhost/api/reservations/2/client{?projection}"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("review")
                        ),
                        responseFields(
                                fieldWithPath("id").description("Reservation's ID"),
                                fieldWithPath("topPictureUrl").description("Restaurant's top picture url"),
                                fieldWithPath("restaurantName").description("Restaurant's name"),
                                fieldWithPath("restaurantId").description("Restaurant's Id"),
                                fieldWithPath("restaurantRating").description("Restaurant's rating"),
                                fieldWithPath("restaurantAddress").description("Restaurant's address"),
                                fieldWithPath("date").description("Reservation date"),
                                fieldWithPath("time").description("Reservation time"),
                                fieldWithPath("guestNumber").description("Number of guests"),
                                fieldWithPath("comment").description("Comment to the reservation"),
                                fieldWithPath("status").description("Reservation status"),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void showReviewPopupForReservationLocalized() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/reservations/2?projection=review")
                .header(AUTHORIZATION, token))
                .andExpect(jsonPath("$.restaurantName").value("Ім`я1"))
                .andExpect(jsonPath("$.restaurantAddress").value("адреса1"));
    }

    @Test
    public void createNewReviewForSelectedReservation() throws Exception {
        ConstrainedFields constrainedFields = new ConstrainedFields(Review.class);

        mockMvc.perform(post("/api/reviews").content(reviewObject())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(4))
                .andExpect(jsonPath("$.rating").value(3))
                .andExpect(jsonPath("$.comment").value("Review comment"))
                .andExpect(jsonPath("$.creationDate").isNotEmpty())
                .andExpect(jsonPath("$._links.length()").value(5))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reviews/101"))
                .andExpect(jsonPath("$._links.review.href").value("http://localhost/api/reviews/101{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reviews/101/restaurant{?projection}"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reviews/101/reservation{?projection}"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/reviews/101/user{?projection}"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestFields(
                                fieldWithPath("restaurant").description("Restaurant resource url"),
                                fieldWithPath("reservation").description("Reservation resource url"),
                                constrainedFields.withPath("rating").description("Rating"),
                                constrainedFields.withPath("comment").description("Client comment")
                        ),
                        responseFields(
                                fieldWithPath("rating").description("Rating"),
                                fieldWithPath("comment").description("Client comment"),
                                fieldWithPath("creationDate").description("Creation date"),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void disableUsersReviewNotification() throws Exception {
        mockMvc.perform(delete("/api/review_notifications/1")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andDo(document("user-flow/{class-name}/{method-name}",
                       requestHeaders(headerWithName("Authorization").description("Bearer link:#_login_page[access token]"))
                ));
    }

    public static String reviewObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/4");
        r.put("reservation", "http://localhost:8080/api/reservations/13");
        r.put("rating", "3");
        r.put("comment", "Review comment");
        return new ObjectMapper().writeValueAsString(r);
    }
}
