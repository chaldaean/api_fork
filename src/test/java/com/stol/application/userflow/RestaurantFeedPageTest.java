package com.stol.application.userflow;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.application.common.ApplicationSystemTest;
import com.stol.restdocs.PagingRequestParameters;
import com.stol.restdocs.PagingResponseFields;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

import static com.stol.configuration.constant.Localization.LANGUAGE_UK;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestaurantFeedPageTest extends ApplicationSystemTest {

      @Test
    public void allRestaurants() throws Exception {
        mockMvc.perform(get("/api/restaurants?projection=feed&sort=name,asc&size=3")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[0].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[0].id").value("1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.name").value("tag3"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.iconUrl").value("/files/9.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].averageTicket").value(400))
                .andExpect(jsonPath("$._embedded.restaurants[0].topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.name").value("cuisine1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.iconUrl").value("/files/10.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].totalReviews").value(4))
                .andExpect(jsonPath("$._embedded.restaurants[0].rating").value(4.5))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].quantity").value(5))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].discount").value(90))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[1].quantity").value(10))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[1].discount").value(20))
                .andExpect(jsonPath("$._embedded.restaurants[0].latitude").value(1.33))
                .andExpect(jsonPath("$._embedded.restaurants[0].longitude").value(2.00))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerTitle").value("10%"))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerName").value("discount"))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerUrl").value("/files/29.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].openHours").exists())
                .andExpect(jsonPath("$._embedded.restaurants[0].saved").value(Boolean.TRUE))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.self.href").value("http://localhost/api/restaurants/1"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.restaurant.href").value("http://localhost/api/restaurants/1{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.promotions.href").value("http://localhost/api/restaurants/1/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.owners.href").value("http://localhost/api/restaurants/1/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.happyHours.href").value("http://localhost/api/restaurants/1/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.topPicture.href").value("http://localhost/api/restaurants/1/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.cuisines.href").value("http://localhost/api/restaurants/1/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.pictures.href").value("http://localhost/api/restaurants/1/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.menu.href").value("http://localhost/api/restaurants/1/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.analogous.href").value("http://localhost/api/restaurants/1/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.marker.href").value("http://localhost/api/restaurants/1/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.tags.href").value("http://localhost/api/restaurants/1/tags{?projection}"))

                .andExpect(jsonPath("$._embedded.restaurants[1].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[1].id").value("2"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name2"))
                .andExpect(jsonPath("$._embedded.restaurants[1].tag").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].averageTicket").value(250))
                .andExpect(jsonPath("$._embedded.restaurants[1].topPictureUrl").value("/files/2.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[1].cuisine").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].totalReviews").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[1].rating").value(3.5))
                .andExpect(jsonPath("$._embedded.restaurants[1].discounts").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].latitude").value(50.00))
                .andExpect(jsonPath("$._embedded.restaurants[1].longitude").value(3.00))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerTitle").value("20%"))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerName").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerUrl").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours").exists())
                .andExpect(jsonPath("$._embedded.restaurants[1].saved").value(Boolean.FALSE))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.self.href").value("http://localhost/api/restaurants/2"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.restaurant.href").value("http://localhost/api/restaurants/2{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.promotions.href").value("http://localhost/api/restaurants/2/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.owners.href").value("http://localhost/api/restaurants/2/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.happyHours.href").value("http://localhost/api/restaurants/2/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.topPicture.href").value("http://localhost/api/restaurants/2/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.cuisines.href").value("http://localhost/api/restaurants/2/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.pictures.href").value("http://localhost/api/restaurants/2/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.menu.href").value("http://localhost/api/restaurants/2/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.analogous.href").value("http://localhost/api/restaurants/2/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.marker.href").value("http://localhost/api/restaurants/2/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.tags.href").value("http://localhost/api/restaurants/2/tags{?projection}"))

                .andExpect(jsonPath("$._embedded.restaurants[2].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[2].id").value("4"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("name4"))
                .andExpect(jsonPath("$._embedded.restaurants[2].tag").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].averageTicket").value(550))
                .andExpect(jsonPath("$._embedded.restaurants[2].topPictureUrl").value("/files/4.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[2].cuisine").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].totalReviews").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[2].rating").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[2].discounts").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].latitude").value(100.00))
                .andExpect(jsonPath("$._embedded.restaurants[2].longitude").value(3.00))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerTitle").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerName").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerUrl").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].openHours").exists())
                .andExpect(jsonPath("$._embedded.restaurants[2].saved").value(Boolean.TRUE))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.self.href").value("http://localhost/api/restaurants/4"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.restaurant.href").value("http://localhost/api/restaurants/4{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.promotions.href").value("http://localhost/api/restaurants/4/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.owners.href").value("http://localhost/api/restaurants/4/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.happyHours.href").value("http://localhost/api/restaurants/4/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.topPicture.href").value("http://localhost/api/restaurants/4/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.cuisines.href").value("http://localhost/api/restaurants/4/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.pictures.href").value("http://localhost/api/restaurants/4/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.menu.href").value("http://localhost/api/restaurants/4/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.analogous.href").value("http://localhost/api/restaurants/4/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.marker.href").value("http://localhost/api/restaurants/4/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.tags.href").value("http://localhost/api/restaurants/4/tags{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(6))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurants{?projection}"))
                .andExpect(jsonPath("$._links.first.href").value("http://localhost/api/restaurants?page=0&size=3&sort=name,asc"))
                .andExpect(jsonPath("$._links.next.href").value("http://localhost/api/restaurants?page=1&size=3&sort=name,asc"))
                .andExpect(jsonPath("$._links.last.href").value("http://localhost/api/restaurants?page=1&size=3&sort=name,asc"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/restaurants"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/restaurants/search"))

                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(3))
                .andExpect(jsonPath("$.page.totalElements").value(5))
                .andExpect(jsonPath("$.page.totalPages").value(2))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size,
                                parameterWithName("projection").description("feed")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.restaurants[]").description("List of restaurants"),
                                fieldWithPath("_embedded.restaurants[].id").description("Restaurant ID"),
                                fieldWithPath("_embedded.restaurants[].name").description("Name"),
                                fieldWithPath("_embedded.restaurants[].tag").optional().description("First tag attached"),
                                fieldWithPath("_embedded.restaurants[].tag.iconUrl").optional().description("Url of the tag icon"),
                                fieldWithPath("_embedded.restaurants[].tag.name").optional().description("Tag name"),
                                fieldWithPath("_embedded.restaurants[].averageTicket").description("Average bill"),
                                fieldWithPath("_embedded.restaurants[].topPictureUrl").description("Main picture url"),
                                fieldWithPath("_embedded.restaurants[].cuisine").optional().description("First cuisine"),
                                fieldWithPath("_embedded.restaurants[].cuisine.name").optional().description("Cuisine name"),
                                fieldWithPath("_embedded.restaurants[].cuisine.iconUrl").optional().description("Url of the cuisine icon"),
                                fieldWithPath("_embedded.restaurants[].totalReviews").description("Number of reviews"),
                                fieldWithPath("_embedded.restaurants[].rating").description("Rating"),
                                fieldWithPath("_embedded.restaurants[].discounts[]").description("Discounts available"),
                                fieldWithPath("_embedded.restaurants[].discounts[].quantity").description("Quantity of discounts"),
                                fieldWithPath("_embedded.restaurants[].discounts[].discount").description("Discount %"),
                                subsectionWithPath("_embedded.restaurants[].discounts[]._links").ignored(),
                                fieldWithPath("_embedded.restaurants[].latitude").description("Latitude"),
                                fieldWithPath("_embedded.restaurants[].longitude").description("Longitude"),
                                fieldWithPath("_embedded.restaurants[].markerTitle").description("Marker title"),
                                fieldWithPath("_embedded.restaurants[].markerName").description("Marker name"),
                                fieldWithPath("_embedded.restaurants[].markerUrl").description("Marker icon url"),
                                fieldWithPath("_embedded.restaurants[].openHours[]").description("Open hours by weekday"),
                                fieldWithPath("_embedded.restaurants[].openHours[].weekday").description("Weekday number (Monday = 1)"),
                                fieldWithPath("_embedded.restaurants[].openHours[].openTime").description("Open time"),
                                fieldWithPath("_embedded.restaurants[].openHours[].closeTime").description("Close time"),
                                fieldWithPath("_embedded.restaurants[].saved").description("If this restaurant is added to the user's favourite list"),

                                subsectionWithPath("_embedded.restaurants[]._links").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void allRestaurantsForLoggedOutUser_Uk() throws Exception {
        mockMvc.perform(get("/api/restaurants?projection=feed&sort=name,asc&size=3")
                .header(HttpHeaders.ACCEPT_LANGUAGE, LANGUAGE_UK)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("Ім`я1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.name").value("Тег3"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.name").value("Кухня1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("Ім`я2"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("Ім`я4"))
                .andDo(document("user-flow/{class-name}/{method-name}",
                        requestHeaders(headerWithName("Accept-Language").description("One of the supported languages (default is the language set for the user, otherwise - Russian)")),
                        requestParameters(
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size,
                                parameterWithName("projection").description("feed")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.restaurants[]").description("List of restaurants"),
                                fieldWithPath("_embedded.restaurants[].id").description("Restaurant ID"),
                                fieldWithPath("_embedded.restaurants[].name").description("Name"),
                                fieldWithPath("_embedded.restaurants[].tag").optional().description("First tag attached"),
                                fieldWithPath("_embedded.restaurants[].tag.iconUrl").optional().description("Url of the tag icon"),
                                fieldWithPath("_embedded.restaurants[].tag.name").optional().description("Tag name"),
                                fieldWithPath("_embedded.restaurants[].averageTicket").description("Average bill"),
                                fieldWithPath("_embedded.restaurants[].topPictureUrl").description("Main picture url"),
                                fieldWithPath("_embedded.restaurants[].cuisine").optional().description("First cuisine"),
                                fieldWithPath("_embedded.restaurants[].cuisine.name").optional().description("Cuisine name"),
                                fieldWithPath("_embedded.restaurants[].cuisine.iconUrl").optional().description("Url of the cuisine icon"),
                                fieldWithPath("_embedded.restaurants[].totalReviews").description("Number of reviews"),
                                fieldWithPath("_embedded.restaurants[].rating").description("Rating"),
                                fieldWithPath("_embedded.restaurants[].discounts[]").description("Discounts available"),
                                fieldWithPath("_embedded.restaurants[].discounts[].quantity").description("Quantity of discounts"),
                                fieldWithPath("_embedded.restaurants[].discounts[].discount").description("Discount %"),
                                subsectionWithPath("_embedded.restaurants[].discounts[]._links").ignored(),
                                fieldWithPath("_embedded.restaurants[].latitude").description("Latitude"),
                                fieldWithPath("_embedded.restaurants[].longitude").description("Longitude"),
                                fieldWithPath("_embedded.restaurants[].markerTitle").description("Marker title"),
                                fieldWithPath("_embedded.restaurants[].markerName").description("Marker name"),
                                fieldWithPath("_embedded.restaurants[].markerUrl").description("Marker icon url"),
                                fieldWithPath("_embedded.restaurants[].openHours[]").description("Open hours by weekday"),
                                fieldWithPath("_embedded.restaurants[].openHours[].weekday").description("Weekday number (Monday = 1)"),
                                fieldWithPath("_embedded.restaurants[].openHours[].openTime").description("Open time"),
                                fieldWithPath("_embedded.restaurants[].openHours[].closeTime").description("Close time"),
                                fieldWithPath("_embedded.restaurants[].saved").description("If this restaurant is added to the user's favourite list"),

                                subsectionWithPath("_embedded.restaurants[]._links").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }


    @Test
    public void allRestaurantsLoggedInUser_Uk() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/restaurants?projection=feed&sort=name,asc&size=3")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("Ім`я1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.name").value("Тег3"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.name").value("Кухня1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("Ім`я2"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("Ім`я4"))
                .andExpect(jsonPath("$._embedded.restaurants[2].tag").isEmpty());
    }


    @Test
    public void allRestaurantsForMap() throws Exception {
        mockMvc.perform(get("/api/restaurants?projection=feed&latitude=1.12345678&latitude=100.12345678&longitude=1.12345678&longitude=3.12345678&active=true")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[0].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[0].id").value("1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.name").value("tag3"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.iconUrl").value("/files/9.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].averageTicket").value(400))
                .andExpect(jsonPath("$._embedded.restaurants[0].topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.name").value("cuisine1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.iconUrl").value("/files/10.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].totalReviews").value(4))
                .andExpect(jsonPath("$._embedded.restaurants[0].rating").value(4.5))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].quantity").value(5))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].discount").value(90))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[1].quantity").value(10))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[1].discount").value(20))
                .andExpect(jsonPath("$._embedded.restaurants[0].latitude").value(1.33))
                .andExpect(jsonPath("$._embedded.restaurants[0].longitude").value(2.00))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerTitle").value("10%"))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerName").value("discount"))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerUrl").value("/files/29.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].openHours").exists())
                .andExpect(jsonPath("$._embedded.restaurants[1].saved").value(Boolean.FALSE))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.self.href").value("http://localhost/api/restaurants/1"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.restaurant.href").value("http://localhost/api/restaurants/1{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.promotions.href").value("http://localhost/api/restaurants/1/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.owners.href").value("http://localhost/api/restaurants/1/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.happyHours.href").value("http://localhost/api/restaurants/1/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.topPicture.href").value("http://localhost/api/restaurants/1/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.cuisines.href").value("http://localhost/api/restaurants/1/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.pictures.href").value("http://localhost/api/restaurants/1/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.menu.href").value("http://localhost/api/restaurants/1/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.analogous.href").value("http://localhost/api/restaurants/1/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.marker.href").value("http://localhost/api/restaurants/1/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.tags.href").value("http://localhost/api/restaurants/1/tags{?projection}"))

                .andExpect(jsonPath("$._embedded.restaurants[1].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[1].id").value("2"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name2"))
                .andExpect(jsonPath("$._embedded.restaurants[1].tag").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].averageTicket").value(250))
                .andExpect(jsonPath("$._embedded.restaurants[1].topPictureUrl").value("/files/2.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[1].cuisine").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].totalReviews").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[1].rating").value(3.5))
                .andExpect(jsonPath("$._embedded.restaurants[1].discounts").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].latitude").value(50.00))
                .andExpect(jsonPath("$._embedded.restaurants[1].longitude").value(3.00))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerTitle").value("20%"))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerName").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerUrl").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours").exists())
                .andExpect(jsonPath("$._embedded.restaurants[1].saved").value(Boolean.FALSE))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.self.href").value("http://localhost/api/restaurants/2"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.restaurant.href").value("http://localhost/api/restaurants/2{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.promotions.href").value("http://localhost/api/restaurants/2/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.owners.href").value("http://localhost/api/restaurants/2/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.happyHours.href").value("http://localhost/api/restaurants/2/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.topPicture.href").value("http://localhost/api/restaurants/2/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.cuisines.href").value("http://localhost/api/restaurants/2/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.pictures.href").value("http://localhost/api/restaurants/2/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.menu.href").value("http://localhost/api/restaurants/2/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.analogous.href").value("http://localhost/api/restaurants/2/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.marker.href").value("http://localhost/api/restaurants/2/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.tags.href").value("http://localhost/api/restaurants/2/tags{?projection}"))

                .andExpect(jsonPath("$._embedded.restaurants[2].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[2].id").value("4"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("name4"))
                .andExpect(jsonPath("$._embedded.restaurants[2].tag").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].averageTicket").value(550))
                .andExpect(jsonPath("$._embedded.restaurants[2].topPictureUrl").value("/files/4.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[2].cuisine").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].totalReviews").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[2].rating").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[2].discounts").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].latitude").value(100.00))
                .andExpect(jsonPath("$._embedded.restaurants[2].longitude").value(3.00))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerTitle").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerName").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerUrl").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].openHours").exists())
                .andExpect(jsonPath("$._embedded.restaurants[1].saved").value(Boolean.FALSE))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.self.href").value("http://localhost/api/restaurants/4"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.restaurant.href").value("http://localhost/api/restaurants/4{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.promotions.href").value("http://localhost/api/restaurants/4/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.owners.href").value("http://localhost/api/restaurants/4/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.happyHours.href").value("http://localhost/api/restaurants/4/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.topPicture.href").value("http://localhost/api/restaurants/4/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.cuisines.href").value("http://localhost/api/restaurants/4/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.pictures.href").value("http://localhost/api/restaurants/4/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.menu.href").value("http://localhost/api/restaurants/4/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.analogous.href").value("http://localhost/api/restaurants/4/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.marker.href").value("http://localhost/api/restaurants/4/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.tags.href").value("http://localhost/api/restaurants/4/tags{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurants{?page,size,sort,projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/restaurants"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/restaurants/search"))
                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("feed"),
                                parameterWithName("latitude").description("First and second latitude points. (South West and North East latitude parameters)"),
                                parameterWithName("longitude").description("First and second longitude points. (South West and North East longitude parameters)"),
                                parameterWithName("active").description("Find only active restaurants. Must always be 'true'")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.restaurants[]").description("List of restaurants"),
                                fieldWithPath("_embedded.restaurants[].id").description("Restaurant ID"),
                                fieldWithPath("_embedded.restaurants[].name").description("Name"),
                                fieldWithPath("_embedded.restaurants[].tag").optional().description("First tag attached"),
                                fieldWithPath("_embedded.restaurants[].tag.iconUrl").optional().description("Url of the tag icon"),
                                fieldWithPath("_embedded.restaurants[].tag.name").optional().description("Tag name"),
                                fieldWithPath("_embedded.restaurants[].averageTicket").description("Average bill"),
                                fieldWithPath("_embedded.restaurants[].topPictureUrl").description("Main picture url"),
                                fieldWithPath("_embedded.restaurants[].cuisine").optional().description("First cuisine"),
                                fieldWithPath("_embedded.restaurants[].cuisine.name").optional().description("Cuisine name"),
                                fieldWithPath("_embedded.restaurants[].cuisine.iconUrl").optional().description("Url of the cuisine icon"),
                                fieldWithPath("_embedded.restaurants[].totalReviews").description("Number of reviews"),
                                fieldWithPath("_embedded.restaurants[].rating").description("Rating"),
                                fieldWithPath("_embedded.restaurants[].discounts[]").description("Discounts available"),
                                fieldWithPath("_embedded.restaurants[].discounts[].quantity").description("Quantity of discounts"),
                                fieldWithPath("_embedded.restaurants[].discounts[].discount").description("Discount %"),
                                subsectionWithPath("_embedded.restaurants[].discounts[]._links").ignored(),
                                fieldWithPath("_embedded.restaurants[].latitude").description("Latitude"),
                                fieldWithPath("_embedded.restaurants[].longitude").description("Longitude"),
                                fieldWithPath("_embedded.restaurants[].markerTitle").description("Marker title"),
                                fieldWithPath("_embedded.restaurants[].markerName").description("Marker name"),
                                fieldWithPath("_embedded.restaurants[].markerUrl").description("Marker icon url"),
                                fieldWithPath("_embedded.restaurants[].openHours[]").description("Open hours by weekday"),
                                fieldWithPath("_embedded.restaurants[].openHours[].weekday").description("Weekday number (Monday = 1)"),
                                fieldWithPath("_embedded.restaurants[].openHours[].openTime").description("Open time"),
                                fieldWithPath("_embedded.restaurants[].openHours[].closeTime").description("Close time"),
                                fieldWithPath("_embedded.restaurants[].saved").description("If this restaurant is added to the user's favourite list"),

                                subsectionWithPath("_embedded.restaurants[]._links").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void allAvailableTimePeriods() throws Exception {
        mockMvc.perform(get("/api/times")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.times.length()").value(5))
                .andExpect(jsonPath("$._embedded.times[0].time").value("08:00"))
                .andExpect(jsonPath("$._embedded.times[0]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[0]._links.self.href").value("http://localhost/api/times/1"))
                .andExpect(jsonPath("$._embedded.times[0]._links.time.href").value("http://localhost/api/times/1"))
                .andExpect(jsonPath("$._embedded.times[1].time").value("08:30"))
                .andExpect(jsonPath("$._embedded.times[1]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[1]._links.self.href").value("http://localhost/api/times/2"))
                .andExpect(jsonPath("$._embedded.times[1]._links.time.href").value("http://localhost/api/times/2"))
                .andExpect(jsonPath("$._embedded.times[2].time").value("09:00"))
                .andExpect(jsonPath("$._embedded.times[2]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[2]._links.self.href").value("http://localhost/api/times/3"))
                .andExpect(jsonPath("$._embedded.times[2]._links.time.href").value("http://localhost/api/times/3"))
                .andExpect(jsonPath("$._embedded.times[3].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.times[3]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[3]._links.self.href").value("http://localhost/api/times/4"))
                .andExpect(jsonPath("$._embedded.times[3]._links.time.href").value("http://localhost/api/times/4"))
                .andExpect(jsonPath("$._embedded.times[4].time").value("10:00"))
                .andExpect(jsonPath("$._embedded.times[4]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[4]._links.self.href").value("http://localhost/api/times/5"))
                .andExpect(jsonPath("$._embedded.times[4]._links.time.href").value("http://localhost/api/times/5"))

                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/times"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/times"))

                .andDo(document("user-flow/{class-name}/all-times",
                        tokenHeadersSnippet,
                        responseFields(
                                fieldWithPath("_embedded.times[]").description("List of time periods"),
                                fieldWithPath("_embedded.times[].time").description("Time period"),
                                fieldWithPath("_embedded.times[]._links.self.href").description("Time period resource url"),
                                subsectionWithPath("_embedded.times[]._links.time").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void allCuisines() throws Exception {
        mockMvc.perform(get("/api/cuisines?projection=id")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[0].length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[0].id").value("1"))
                .andExpect(jsonPath("$._embedded.cuisines[0].name").value("cuisine1"))
                .andExpect(jsonPath("$._embedded.cuisines[0]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[0]._links.self.href").value("http://localhost/api/cuisines/1"))
                .andExpect(jsonPath("$._embedded.cuisines[0]._links.cuisine.href").value("http://localhost/api/cuisines/1{?projection}"))
                .andExpect(jsonPath("$._embedded.cuisines[0]._links.icon.href").value("http://localhost/api/cuisines/1/icon"))
                .andExpect(jsonPath("$._embedded.cuisines[1].length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[1].id").value("2"))
                .andExpect(jsonPath("$._embedded.cuisines[1].name").value("cuisine2"))
                .andExpect(jsonPath("$._embedded.cuisines[1]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[1]._links.self.href").value("http://localhost/api/cuisines/2"))
                .andExpect(jsonPath("$._embedded.cuisines[1]._links.cuisine.href").value("http://localhost/api/cuisines/2{?projection}"))
                .andExpect(jsonPath("$._embedded.cuisines[1]._links.icon.href").value("http://localhost/api/cuisines/2/icon"))
                .andExpect(jsonPath("$._embedded.cuisines[2].length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[2].id").value("3"))
                .andExpect(jsonPath("$._embedded.cuisines[2].name").value("cuisine3"))
                .andExpect(jsonPath("$._embedded.cuisines[2]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.cuisines[2]._links.self.href").value("http://localhost/api/cuisines/3"))
                .andExpect(jsonPath("$._embedded.cuisines[2]._links.cuisine.href").value("http://localhost/api/cuisines/3{?projection}"))
                .andExpect(jsonPath("$._embedded.cuisines[2]._links.icon.href").value("http://localhost/api/cuisines/3/icon"))

                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/cuisines{?page,size,sort,projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/cuisines"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/cuisines/search"))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(3))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/all-cuisines",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("id"),
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size
                        ),
                        responseFields(
                                fieldWithPath("_embedded.cuisines[]").description("List of cuisines"),
                                fieldWithPath("_embedded.cuisines[].id").description("Id"),
                                fieldWithPath("_embedded.cuisines[].name").description("Name"),
                                fieldWithPath("_embedded.cuisines[]._links.self.href").description("Cuisine resource url"),
                                fieldWithPath("_embedded.cuisines[]._links.icon.href").description("Cuisine icon resource url"),
                                subsectionWithPath("_embedded.cuisines[]._links.cuisine").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void allTags() throws Exception {
        mockMvc.perform(get("/api/tags?projection=id")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[0].length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[0].id").value("1"))
                .andExpect(jsonPath("$._embedded.tags[0].name").value("tag1"))
                .andExpect(jsonPath("$._embedded.tags[0]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[0]._links.self.href").value("http://localhost/api/tags/1"))
                .andExpect(jsonPath("$._embedded.tags[0]._links.tag.href").value("http://localhost/api/tags/1{?projection}"))
                .andExpect(jsonPath("$._embedded.tags[0]._links.icon.href").value("http://localhost/api/tags/1/icon"))
                .andExpect(jsonPath("$._embedded.tags[1].length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[1].id").value("2"))
                .andExpect(jsonPath("$._embedded.tags[1].name").value("tag2"))
                .andExpect(jsonPath("$._embedded.tags[1]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[1]._links.self.href").value("http://localhost/api/tags/2"))
                .andExpect(jsonPath("$._embedded.tags[1]._links.tag.href").value("http://localhost/api/tags/2{?projection}"))
                .andExpect(jsonPath("$._embedded.tags[1]._links.icon.href").value("http://localhost/api/tags/2/icon"))
                .andExpect(jsonPath("$._embedded.tags[2].length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[2].id").value("3"))
                .andExpect(jsonPath("$._embedded.tags[2].name").value("tag3"))
                .andExpect(jsonPath("$._embedded.tags[2]._links.length()").value(3))
                .andExpect(jsonPath("$._embedded.tags[2]._links.self.href").value("http://localhost/api/tags/3"))
                .andExpect(jsonPath("$._embedded.tags[2]._links.tag.href").value("http://localhost/api/tags/3{?projection}"))
                .andExpect(jsonPath("$._embedded.tags[2]._links.icon.href").value("http://localhost/api/tags/3/icon"))

                .andExpect(jsonPath("$._links.length()").value(3))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/tags{?page,size,sort,projection}"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/tags"))
                .andExpect(jsonPath("$._links.search.href").value("http://localhost/api/tags/search"))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(3))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/all-tags",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("id"),
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size
                        ),
                        responseFields(
                                fieldWithPath("_embedded.tags[]").description("List of cuisines"),
                                fieldWithPath("_embedded.tags[].id").description("Id"),
                                fieldWithPath("_embedded.tags[].name").description("Name"),
                                fieldWithPath("_embedded.tags[]._links.self.href").description("Tag resource url"),
                                fieldWithPath("_embedded.tags[]._links.icon.href").description("Tag icon resource url"),
                                subsectionWithPath("_embedded.tags[]._links.tag").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void allTagsLocalized() throws Exception {
        mockMvc.perform(get("/api/tags?projection=id")
                .accept(HAL_JSON_UTF8)
                .header(HttpHeaders.ACCEPT_LANGUAGE, "uk"))
                .andExpect(jsonPath("$._embedded.tags[0].name").value("Тег1"))
                .andExpect(jsonPath("$._embedded.tags[1].name").value("Тег2"))
                .andExpect(jsonPath("$._embedded.tags[2].name").value("Тег3"));
    }

    @Test
    public void findAllRestaurantsSortByDistance() throws Exception {
        mockMvc.perform(get("/api/restaurants/search")
                .param("avgTicketFrom", "300")
                .param("avgTicketTo", "900")
                .param("cuisines", "1", "2")
                .param("tags", "1", "3")
                .param("longitude", "50.0")
                .param("latitude", "50.0")
                .param("page", "0")
                .param("size", "5")
                .param("sorting", "distance")
                .param("projection", "feed")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())

                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name5"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name1"))
                .andExpect(jsonPath("$.page.size").value(5))
                .andExpect(jsonPath("$.page.totalElements").value(2))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        requestParameters(
                                parameterWithName("avgTicketFrom").description("Average ticket from"),
                                parameterWithName("avgTicketTo").description("Average ticket to"),
                                parameterWithName("cuisines").description("Cuisine id (multiple)"),
                                parameterWithName("tags").description("Tag id (multiple)"),
                                parameterWithName("page").description("Page (0-based)"),
                                parameterWithName("size").description("Page size"),
                                parameterWithName("sorting").description("distance"),
                                parameterWithName("longitude").description("Longitude"),
                                parameterWithName("latitude").description("Latitude"),
                                parameterWithName("projection").description("feed")
                        )));
    }

    @Test
    public void findAllRestaurantsSortByRating() throws Exception {
        mockMvc.perform(get("/api/restaurants/search")
                .param("avgTicketFrom", "300")
                .param("avgTicketTo", "900")
                .param("cuisines", "1", "2")
                .param("tags", "1", "3")
                .param("page", "0")
                .param("size", "5")
                .param("sorting", "rating")
                .param("projection", "feed")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())

                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name5"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name1"))
                .andExpect(jsonPath("$.page.size").value(5))
                .andExpect(jsonPath("$.page.totalElements").value(2))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        requestParameters(
                                parameterWithName("avgTicketFrom").description("Average ticket from"),
                                parameterWithName("avgTicketTo").description("Average ticket to"),
                                parameterWithName("cuisines").description("Cuisine id (multiple)"),
                                parameterWithName("tags").description("Tag id (multiple)"),
                                parameterWithName("page").description("Page (0-based)"),
                                parameterWithName("size").description("Page size"),
                                parameterWithName("sorting").description("rating"),
                                parameterWithName("projection").description("feed")
                        )));
    }

    @Test
    public void findAllRestaurantsSortByName() throws Exception {
        mockMvc.perform(get("/api/restaurants/search")
                .param("avgTicketFrom", "300")
                .param("avgTicketTo", "900")
                .param("cuisines", "1", "2")
                .param("tags", "1", "3")
                .param("page", "0")
                .param("size", "5")
                .param("sorting", "name")
                .param("projection", "feed")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())

                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name5"))
                .andExpect(jsonPath("$.page.size").value(5))
                .andExpect(jsonPath("$.page.totalElements").value(2))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        requestParameters(
                                parameterWithName("avgTicketFrom").description("Average ticket from"),
                                parameterWithName("avgTicketTo").description("Average ticket to"),
                                parameterWithName("cuisines").description("Cuisine id (multiple)"),
                                parameterWithName("tags").description("Tag id (multiple)"),
                                parameterWithName("page").description("Page (0-based)"),
                                parameterWithName("size").description("Page size"),
                                parameterWithName("sorting").description("distance"),
                                parameterWithName("projection").description("feed")
                        )));
    }

    @Test
    public void findAllRestaurantsSortByName_uk() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/restaurants/search")
                .param("avgTicketFrom", "300")
                .param("avgTicketTo", "900")
                .param("cuisines", "1", "2")
                .param("tags", "1", "3")
                .param("page", "0")
                .param("size", "5")
                .param("sorting", "name")
                .param("projection", "feed")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())

                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("Ім`я1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("Ім`я5"));
    }

    @Test
    public void minMaxRestaurantsAverageCheck() throws Exception {
        mockMvc.perform(get("/api/restaurants/average_check")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.min").value(200))
                .andExpect(jsonPath("$.max").value(550))

                .andDo(document("user-flow/{class-name}/min-max-check",
                        tokenHeadersSnippet,
                        responseFields(
                                fieldWithPath("min").description("Minimum value for restaurant's average check."),
                                fieldWithPath("max").description("Maximum value for restaurant's average check.")
                        )));
    }
}
