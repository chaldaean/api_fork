package com.stol.application.userflow;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.application.common.ApplicationSystemTest;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestaurantCardPageTest extends ApplicationSystemTest {

    @Test
    public void restaurantById() throws Exception {
        mockMvc.perform(get("/api/restaurants/1?projection=card")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(23))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.address").value("address1"))
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$.description").value("description1"))
                .andExpect(jsonPath("$.tags.length()").value(3))
                .andExpect(jsonPath("$.tags[0].name").value("tag3"))
                .andExpect(jsonPath("$.tags[0].iconUrl").value("/files/9.jpg"))
                .andExpect(jsonPath("$.tags[1].name").value("tag2"))
                .andExpect(jsonPath("$.tags[1].iconUrl").value("/files/8.jpg"))
                .andExpect(jsonPath("$.tags[2].name").value("tag1"))
                .andExpect(jsonPath("$.tags[2].iconUrl").value("/files/7.jpg"))
                .andExpect(jsonPath("$.cuisines.length()").value(3))
                .andExpect(jsonPath("$.cuisines[0].name").value("cuisine1"))
                .andExpect(jsonPath("$.cuisines[0].iconUrl").value("/files/10.jpg"))
                .andExpect(jsonPath("$.cuisines[1].name").value("cuisine2"))
                .andExpect(jsonPath("$.cuisines[1].iconUrl").value("/files/11.jpg"))
                .andExpect(jsonPath("$.cuisines[2].name").value("cuisine3"))
                .andExpect(jsonPath("$.cuisines[2].iconUrl").value("/files/12.jpg"))
                .andExpect(jsonPath("$.openHours").exists())
                .andExpect(jsonPath("$.menu").value("/files/13.jpg"))
                .andExpect(jsonPath("$.pictures.length()").value(2))
                .andExpect(jsonPath("$.pictures[0]").value("/files/14.jpg"))
                .andExpect(jsonPath("$.pictures[1]").value("/files/15.jpg"))
                .andExpect(jsonPath("$.rating").value(4.5))
                .andExpect(jsonPath("$.averageTicket").value(400))
                .andExpect(jsonPath("$.latitude").value(1.33))
                .andExpect(jsonPath("$.longitude").value(2.00))
                .andExpect(jsonPath("$.parking").value("yes"))
                .andExpect(jsonPath("$.phoneNumber").value("380978882233 380965552233"))
                .andExpect(jsonPath("$.discounts.length()").value(2))
                .andExpect(jsonPath("$.discounts[0].quantity").value(5))
                .andExpect(jsonPath("$.discounts[0].discount").value(90))
                .andExpect(jsonPath("$.discounts[1].quantity").value(10))
                .andExpect(jsonPath("$.discounts[1].discount").value(20))
                .andExpect(jsonPath("$.totalReviews").value(4))
                .andExpect(jsonPath("$.review.length()").value(4))
                .andExpect(jsonPath("$.review.rating").value(4))
                .andExpect(jsonPath("$.review.comment").value("Comment1"))
                .andExpect(jsonPath("$.review.userName").value("user user"))
                .andExpect(jsonPath("$.review.creationDate").value("2017-10-22T15:40:08"))
                .andExpect(jsonPath("$.topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$.breakfast").value("yes"))
                .andExpect(jsonPath("$.paymentMethod").value("Visa/MC"))
                .andExpect(jsonPath("$.saved").value(Boolean.TRUE))

                .andExpect(jsonPath("$._links.length()").value(12))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurants/1"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/restaurants/1{?projection}"))
                .andExpect(jsonPath("$._links.promotions.href").value("http://localhost/api/restaurants/1/promotions{?projection}"))
                .andExpect(jsonPath("$._links.owners.href").value("http://localhost/api/restaurants/1/owners{?projection}"))
                .andExpect(jsonPath("$._links.happyHours.href").value("http://localhost/api/restaurants/1/happy_hours{?projection}"))
                .andExpect(jsonPath("$._links.topPicture.href").value("http://localhost/api/restaurants/1/top_picture"))
                .andExpect(jsonPath("$._links.cuisines.href").value("http://localhost/api/restaurants/1/cuisines{?projection}"))
                .andExpect(jsonPath("$._links.pictures.href").value("http://localhost/api/restaurants/1/pictures"))
                .andExpect(jsonPath("$._links.menu.href").value("http://localhost/api/restaurants/1/menu"))
                .andExpect(jsonPath("$._links.analogous.href").value("http://localhost/api/restaurants/1/analogous{?projection}"))
                .andExpect(jsonPath("$._links.marker.href").value("http://localhost/api/restaurants/1/marker"))
                .andExpect(jsonPath("$._links.tags.href").value("http://localhost/api/restaurants/1/tags{?projection}"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("card")
                        ),
                        responseFields(
                                fieldWithPath("id").description("Restaurant ID"),
                                fieldWithPath("name").description("Name"),
                                fieldWithPath("description").description("Description"),
                                fieldWithPath("phoneNumber").description("Phone number"),
                                fieldWithPath("address").description("Address"),
                                fieldWithPath("latitude").description("Latitude"),
                                fieldWithPath("longitude").description("Longitude"),
                                fieldWithPath("parking").description("If parking place available"),
                                fieldWithPath("menu").description("Url of the menu picture"),
                                fieldWithPath("cuisines[]").description("List of cuisines"),
                                fieldWithPath("cuisines[].name").description("Cuisine name"),
                                fieldWithPath("cuisines[].iconUrl").description("Cuisine icon url"),
                                fieldWithPath("breakfast").description("If breakfast is served"),
                                fieldWithPath("paymentMethod").description("Available payment methods"),
                                fieldWithPath("pictures").description("List of pictures urls"),
                                fieldWithPath("topPictureUrl").description("Main picture url"),
                                fieldWithPath("rating").description("Rating"),
                                fieldWithPath("averageTicket").description("Average bill"),
                                fieldWithPath("tags[]").description("Tags attached"),
                                fieldWithPath("tags[].name").description("Tag name"),
                                fieldWithPath("tags[].iconUrl").description("Tag icon url"),
                                fieldWithPath("review").description("Latest review"),
                                fieldWithPath("review.rating").description("Review rating"),
                                fieldWithPath("review.comment").description("Review text"),
                                fieldWithPath("review.creationDate").description("Review creation date"),
                                fieldWithPath("review.userName").description("Reviewer"),
                                fieldWithPath("totalReviews").description("Total number of reviews"),
                                fieldWithPath("discounts[]").description("List of discounts"),
                                fieldWithPath("discounts[].quantity").description("Quantity of discounts"),
                                fieldWithPath("discounts[].discount").description("Discount %"),
                                fieldWithPath("openHours[]").description("Open hours by weekday"),
                                fieldWithPath("openHours[].weekday").description("Weekday number (Monday = 1)"),
                                fieldWithPath("openHours[].openTime").description("Open time"),
                                fieldWithPath("openHours[].closeTime").description("Close time"),
                                fieldWithPath("openHours[].closeTime").description("Close time"),
                                fieldWithPath(".saved").description("If this restaurant is added to the user's favourite list"),
                                subsectionWithPath("discounts[]._links").ignored(),
                                subsectionWithPath("_links").ignored()))
                );
    }

    @Test
    public void restaurantByIdLocalized() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/restaurants/1?projection=card")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(23))
                .andExpect(jsonPath("$.address").value("адреса1"))
                .andExpect(jsonPath("$.name").value("Ім`я1"))
                .andExpect(jsonPath("$.description").value("опис1"))
                .andExpect(jsonPath("$.tags[0].name").value("Тег3"))
                .andExpect(jsonPath("$.tags[1].name").value("Тег2"))
                .andExpect(jsonPath("$.tags[2].name").value("Тег1"))
                .andExpect(jsonPath("$.cuisines[0].name").value("Кухня1"))
                .andExpect(jsonPath("$.cuisines[1].name").value("Кухня2"))
                .andExpect(jsonPath("$.cuisines[2].name").value("Кухня3"))
                .andExpect(jsonPath("$.parking").value("так"))
                .andExpect(jsonPath("$.breakfast").value("так"))
                .andExpect(jsonPath("$.paymentMethod").value("Віза"));
    }

    @Test
    public void restaurantMenu() throws Exception {
        mockMvc.perform(get("/api/restaurants/1/menu")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(5))
                .andExpect(jsonPath("$.name").value("picture13"))
                .andExpect(jsonPath("$.extension").value("jpg"))
                .andExpect(jsonPath("$.mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$.url").value("/files/13.jpg"))

                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/files/13"))
                .andExpect(jsonPath("$._links.mediaFile.href").value("http://localhost/api/files/13"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        responseFields(
                                fieldWithPath("name").description("File name"),
                                fieldWithPath("extension").description("File extension (f.e. '.jpg')"),
                                fieldWithPath("mediaType").description("File media type"),
                                fieldWithPath("url").description("Relative file's url. File can be opened by this url. Authorization is not required"),
                                subsectionWithPath("_links").ignored()))
                );
    }

    @Test
    public void restaurantAnalogous() throws Exception {
        mockMvc.perform(get("/api/restaurants/1/analogous?projection=feed")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants.length()").value(2))

                .andExpect(jsonPath("$._embedded.restaurants[0].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[0].id").value("5"))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name5"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.name").value("tag1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.iconUrl").value("/files/7.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].averageTicket").value(400))
                .andExpect(jsonPath("$._embedded.restaurants[0].topPictureUrl").value("/files/5.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.name").value("cuisine1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.iconUrl").value("/files/10.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].totalReviews").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].rating").value(5.0))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts.length()").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].quantity").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].discount").value(10))
                .andExpect(jsonPath("$._embedded.restaurants[0].latitude").value(15.00))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerTitle").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerName").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerUrl").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[0].openHours.length()").exists())
                .andExpect(jsonPath("$._embedded.restaurants[0].saved").value(Boolean.FALSE))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.self.href").value("http://localhost/api/restaurants/5"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.restaurant.href").value("http://localhost/api/restaurants/5{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.promotions.href").value("http://localhost/api/restaurants/5/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.owners.href").value("http://localhost/api/restaurants/5/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.happyHours.href").value("http://localhost/api/restaurants/5/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.topPicture.href").value("http://localhost/api/restaurants/5/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.cuisines.href").value("http://localhost/api/restaurants/5/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.pictures.href").value("http://localhost/api/restaurants/5/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.menu.href").value("http://localhost/api/restaurants/5/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.analogous.href").value("http://localhost/api/restaurants/5/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.marker.href").value("http://localhost/api/restaurants/5/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.tags.href").value("http://localhost/api/restaurants/5/tags{?projection}"))

                .andExpect(jsonPath("$._embedded.restaurants[1].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[1].id").value("6"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name6"))
                .andExpect(jsonPath("$._embedded.restaurants[1].tag").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].averageTicket").value(200))
                .andExpect(jsonPath("$._embedded.restaurants[1].topPictureUrl").value("/files/6.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[1].cuisine").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].totalReviews").value(0))
                .andExpect(jsonPath("$._embedded.restaurants[1].rating").value(4.0))
                .andExpect(jsonPath("$._embedded.restaurants[1].discounts.length()").value(0))
                .andExpect(jsonPath("$._embedded.restaurants[1].latitude").value(1.20))
                .andExpect(jsonPath("$._embedded.restaurants[1].longitude").value(3.30))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours.length()").value(7))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[0].weekday").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[0].openTime").value("10:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[0].closeTime").value("23:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[1].weekday").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[1].openTime").value("10:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[1].closeTime").value("23:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[2].weekday").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[2].openTime").value("10:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[2].closeTime").value("23:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[3].weekday").value(4))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[3].openTime").value("10:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[3].closeTime").value("23:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[4].weekday").value(5))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[4].openTime").value("10:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[4].closeTime").value("23:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[5].weekday").value(6))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[5].openTime").value("11:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[5].closeTime").value("02:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[6].weekday").value(7))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[6].openTime").value("11:00:00"))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours[6].closeTime").value("02:00:00"))

                .andExpect(jsonPath("$._embedded.restaurants[1].markerTitle").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerName").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[1].markerUrl").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[1].saved").value(Boolean.TRUE))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.self.href").value("http://localhost/api/restaurants/6"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.restaurant.href").value("http://localhost/api/restaurants/6{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.promotions.href").value("http://localhost/api/restaurants/6/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.owners.href").value("http://localhost/api/restaurants/6/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.happyHours.href").value("http://localhost/api/restaurants/6/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.topPicture.href").value("http://localhost/api/restaurants/6/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.cuisines.href").value("http://localhost/api/restaurants/6/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.pictures.href").value("http://localhost/api/restaurants/6/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.menu.href").value("http://localhost/api/restaurants/6/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.analogous.href").value("http://localhost/api/restaurants/6/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.marker.href").value("http://localhost/api/restaurants/6/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.tags.href").value("http://localhost/api/restaurants/6/tags{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/restaurants/1/analogous?projection=feed"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("card")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.restaurants[]").description("List of restaurants"),
                                fieldWithPath("_embedded.restaurants[].id").description("Restaurant ID"),
                                fieldWithPath("_embedded.restaurants[].name").description("Name"),
                                fieldWithPath("_embedded.restaurants[].tag").optional().description("First tag attached"),
                                fieldWithPath("_embedded.restaurants[].tag.name").optional().description("Tags name"),
                                fieldWithPath("_embedded.restaurants[].tag.iconUrl").optional().description("Tags icon url"),
                                fieldWithPath("_embedded.restaurants[].averageTicket").description("Average bill"),
                                fieldWithPath("_embedded.restaurants[].topPictureUrl").description("Main picture url"),
                                fieldWithPath("_embedded.restaurants[].cuisine").optional().description("First cuisine"),
                                fieldWithPath("_embedded.restaurants[].cuisine.name").optional().description("Cuisine name"),
                                fieldWithPath("_embedded.restaurants[].cuisine.iconUrl").optional().description("Cuisine icon url"),
                                fieldWithPath("_embedded.restaurants[].totalReviews").description("Total number of reviews"),
                                fieldWithPath("_embedded.restaurants[].rating").description("Rating"),
                                fieldWithPath("_embedded.restaurants[].discounts[]").description("List of discounts"),
                                fieldWithPath("_embedded.restaurants[].discounts[].quantity").description("Quantity of discounts"),
                                fieldWithPath("_embedded.restaurants[].discounts[].discount").description("Discount %"),
                                subsectionWithPath("_embedded.restaurants[].discounts[]._links").description("Discount %"),
                                fieldWithPath("_embedded.restaurants[].latitude").description("Latitude"),
                                fieldWithPath("_embedded.restaurants[].longitude").description("Longitude"),
                                fieldWithPath("_embedded.restaurants[].latitude").description("Latitude"),
                                fieldWithPath("_embedded.restaurants[].longitude").description("Longitude"),
                                fieldWithPath("_embedded.restaurants[].markerTitle").description("Marker title"),
                                fieldWithPath("_embedded.restaurants[].markerName").description("Marker name"),
                                fieldWithPath("_embedded.restaurants[].markerUrl").description("Marker icon url"),
                                fieldWithPath("_embedded.restaurants[].openHours[]").description("Open hours by weekday"),
                                fieldWithPath("_embedded.restaurants[].openHours[].weekday").description("Weekday number (Monday = 1)"),
                                fieldWithPath("_embedded.restaurants[].openHours[].openTime").description("Open time"),
                                fieldWithPath("_embedded.restaurants[].openHours[].closeTime").description("Close time"),
                                fieldWithPath("_embedded.restaurants[].saved").description("If this restaurant is added to the user's favourite list"),
                                subsectionWithPath("_embedded.restaurants[]._links").ignored(),
                                subsectionWithPath("_links").ignored()))
                );
    }
}
