package com.stol.application.userflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.application.common.ApplicationSystemTest;
import com.stol.model.Reservation;
import com.stol.restdocs.ConstrainedFields;
import org.hamcrest.core.IsNull;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.stol.exception.ServerExceptionHandler.DEFAULT_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.INVALID_FIELDS_LOG_REF;
import static com.stol.exception.ServerExceptionHandler.VND_ERROR;
import static com.stol.model.property.ReservationStatus.NEW;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationPageTest extends ApplicationSystemTest {

    @Test
    public void allAvailableTimePeriods() throws Exception {
        mockMvc.perform(get("/api/times")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.times.length()").value(5))
                .andExpect(jsonPath("$._embedded.times[0].time").value("08:00"))
                .andExpect(jsonPath("$._embedded.times[0]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[0]._links.self.href").value("http://localhost/api/times/1"))
                .andExpect(jsonPath("$._embedded.times[0]._links.time.href").value("http://localhost/api/times/1"))
                .andExpect(jsonPath("$._embedded.times[1].time").value("08:30"))
                .andExpect(jsonPath("$._embedded.times[1]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[1]._links.self.href").value("http://localhost/api/times/2"))
                .andExpect(jsonPath("$._embedded.times[1]._links.time.href").value("http://localhost/api/times/2"))
                .andExpect(jsonPath("$._embedded.times[2].time").value("09:00"))
                .andExpect(jsonPath("$._embedded.times[2]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[2]._links.self.href").value("http://localhost/api/times/3"))
                .andExpect(jsonPath("$._embedded.times[2]._links.time.href").value("http://localhost/api/times/3"))
                .andExpect(jsonPath("$._embedded.times[3].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.times[3]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[3]._links.self.href").value("http://localhost/api/times/4"))
                .andExpect(jsonPath("$._embedded.times[3]._links.time.href").value("http://localhost/api/times/4"))
                .andExpect(jsonPath("$._embedded.times[4].time").value("10:00"))
                .andExpect(jsonPath("$._embedded.times[4]._links.length()").value(2))
                .andExpect(jsonPath("$._embedded.times[4]._links.self.href").value("http://localhost/api/times/5"))
                .andExpect(jsonPath("$._embedded.times[4]._links.time.href").value("http://localhost/api/times/5"))

                .andExpect(jsonPath("$._links.length()").value(2))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/times"))
                .andExpect(jsonPath("$._links.profile.href").value("http://localhost/api/profile/times"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        responseFields(
                                fieldWithPath("_embedded.times[]").description("List of time values"),
                                fieldWithPath("_embedded.times[].time").description("Time"),
                                fieldWithPath("_embedded.times[]._links.self.href").description("Time resource url"),
                                subsectionWithPath("_embedded.times[]._links.time").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));

    }

    @Test
    public void allAvailableDiscountForSelectedRestaurantAndDate() throws Exception {
        mockMvc.perform(get("/api/happy_hours/search/active?projection=discount")
                .param("restaurant", "http://localhost:8080/api/restaurants/1")
                .param("date", "2090-12-22")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.happyHours.length()").value(2))
                .andExpect(jsonPath("$._embedded.happyHours[0].discount").value(90))
                .andExpect(jsonPath("$._embedded.happyHours[0].quantity").value(5))
                .andExpect(jsonPath("$._embedded.happyHours[0].times.length()").value(1))
                .andExpect(jsonPath("$._embedded.happyHours[0].times[0]").value("10:00"))
                .andExpect(jsonPath("$._embedded.happyHours[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.happyHours[0]._links.self.href").value("http://localhost/api/happy_hours/3"))
                .andExpect(jsonPath("$._embedded.happyHours[0]._links.happyHour.href").value("http://localhost/api/happy_hours/3{?projection}"))
                .andExpect(jsonPath("$._embedded.happyHours[0]._links.times.href").value("http://localhost/api/happy_hours/3/times"))
                .andExpect(jsonPath("$._embedded.happyHours[0]._links.restaurant.href").value("http://localhost/api/happy_hours/3/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.happyHours[1].discount").value(20))
                .andExpect(jsonPath("$._embedded.happyHours[1].quantity").value(10))
                .andExpect(jsonPath("$._embedded.happyHours[1].times.length()").value(3))
                .andExpect(jsonPath("$._embedded.happyHours[1].times[0]").value("08:00"))
                .andExpect(jsonPath("$._embedded.happyHours[1].times[1]").value("08:30"))
                .andExpect(jsonPath("$._embedded.happyHours[1].times[2]").value("09:00"))
                .andExpect(jsonPath("$._embedded.happyHours[1]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.happyHours[1]._links.self.href").value("http://localhost/api/happy_hours/1"))
                .andExpect(jsonPath("$._embedded.happyHours[1]._links.happyHour.href").value("http://localhost/api/happy_hours/1{?projection}"))
                .andExpect(jsonPath("$._embedded.happyHours[1]._links.times.href").value("http://localhost/api/happy_hours/1/times"))
                .andExpect(jsonPath("$._embedded.happyHours[1]._links.restaurant.href").value("http://localhost/api/happy_hours/1/restaurant{?projection}"))

                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/happy_hours/search/active?projection=discount"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").optional().description("discount"),
                                parameterWithName("restaurant").description("Restaurant resource url"),
                                parameterWithName("date").description("Date (YYYY-MM-DD)")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.happyHours[]").description("List of active  happy hours for the restaurant on the given date"),
                                fieldWithPath("_embedded.happyHours[].discount").description("Discount %"),
                                fieldWithPath("_embedded.happyHours[].quantity").optional().description("Quantity of discounts"),
                                fieldWithPath("_embedded.happyHours[].times").description("Time the discount is actual for"),
                                subsectionWithPath("_embedded.happyHours[]._links").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void createNewReservationForSelectedRestaurant() throws Exception {
        ConstrainedFields constrainedFields = new ConstrainedFields(Reservation.class);

        mockMvc.perform(post("/api/reservations").content(reservationObject())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.dateTime").value("2099-11-01T09:00:00"))
                .andExpect(jsonPath("$.guestNumber").value(2))
                .andExpect(jsonPath("$.comment").value("Comment!"))
                .andExpect(jsonPath("$.notes").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.status").value(NEW.toString()))
                .andExpect(jsonPath("$.created").isNotEmpty())
                .andExpect(jsonPath("$.discount.length()").value(3))
                .andExpect(jsonPath("$.discount.value").value(20))
                .andExpect(jsonPath("$.discount.used").value(Boolean.FALSE))
                .andExpect(jsonPath("$.discount._links.length()").value(1))
                .andExpect(jsonPath("$.discount._links.happyHour.href").value("http://localhost/api/happy_hours/1{?projection}"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/14"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reservations/14{?projection}"))
                .andExpect(jsonPath("$._links.client.href").value("http://localhost/api/reservations/14/client{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reservations/14/restaurant{?projection}"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestFields(
                                fieldWithPath("restaurant").description("Restaurant resource url"),
                                constrainedFields.withPath("dateTime").description("Reservation date and time. Available time value must be taken from \"/api/times\""),
                                constrainedFields.withPath("guestNumber").description("Number of guests"),
                                constrainedFields.withPath("comment").description("Comment to the reservation")
                        ),
                        responseFields(
                                fieldWithPath("dateTime").description("Reservation date and time"),
                                fieldWithPath("guestNumber").description("Number of guests"),
                                fieldWithPath("comment").description("Comment to the reservation"),
                                fieldWithPath("notes").ignored(),
                                fieldWithPath("status").description("Status of the reservation. Possible values: NEW, CONFIRMED, REJECTED, CANCELED"),
                                fieldWithPath("created").description("Reservation creation date"),
                                fieldWithPath("discount.value").description("Discount %"),
                                fieldWithPath("discount.used").description("Indicates if the discount was used"),
                                subsectionWithPath("discount._links").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void createNewReservationWithPastVisitTime() throws Exception {
        ConstrainedFields constrainedFields = new ConstrainedFields(Reservation.class);

        mockMvc.perform(post("/api/reservations").content(reservationWithPastTimeObject())
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(VND_ERROR))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$.logref").value(DEFAULT_LOG_REF))
                .andExpect(jsonPath("$.message").value("Restaurant id 1, user id 100, visit time 2000-11-01T09:00: Reservation visit time cannot be in past"))
                .andExpect(jsonPath("$.links.length()").value(0))
                .andDo(document("user-flow/{class-name}/{method-name}",
                        responseFields(
                                fieldWithPath(".logref").description(DEFAULT_LOG_REF),
                                fieldWithPath(".message").description("Error message"),
                                fieldWithPath(".links").ignored()
                        )
                ));
    }


    private String reservationObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/1");
        r.put("dateTime", "2099-11-01T09:00");
        r.put("guestNumber", "2");
        r.put("comment", "Comment!");
        return new ObjectMapper().writeValueAsString(r);
    }

    private String reservationWithPastTimeObject() throws JsonProcessingException {
        Map<String, Object> r = new HashMap<>();
        r.put("restaurant", "http://localhost:8080/api/restaurants/1");
        r.put("dateTime", "2000-11-01T09:00");
        r.put("guestNumber", "1");
        return new ObjectMapper().writeValueAsString(r);
    }
}
