package com.stol.application.userflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.stol.application.common.ApplicationSystemTest;
import com.stol.model.user.User;
import com.stol.restdocs.ConstrainedFields;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import static com.stol.model.property.UserStatus.ENABLE;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8_VALUE;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProfilePageTest extends ApplicationSystemTest {
    @Value("${stol.file.upload-path}")
    protected String UPLOAD_PATH;
    private final String NAME = "newName";
    private final String SURNAME = "newSurname";

    @Test
    public void currentLoggedInUser() throws Exception {
        mockMvc.perform(get("/api/user/current?projection=user_page")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(7))
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.name").value("UserOneHundred"))
                .andExpect(jsonPath("$.surname").value("UserOneHundred"))
                .andExpect(jsonPath("$.photo").value("/files/22.jpg"))
                .andExpect(jsonPath("$.totalLikedRestaurants").value(3))
                .andExpect(jsonPath("$.totalReservations").value(13))

                .andExpect(jsonPath("$._links.length()").value(7))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/100{?projection}"))
                .andExpect(jsonPath("$._links.reservations.href").value("http://localhost/api/users/100/reservations{?projection}"))
                .andExpect(jsonPath("$._links.roles.href").value("http://localhost/api/users/100/roles"))
                .andExpect(jsonPath("$._links.photo.href").value("http://localhost/api/users/100/photo"))
                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/100/saved_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.language.href").value("http://localhost/api/users/100/language{?projection}"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("user_page")
                        ),
                        responseFields(
                                fieldWithPath("id").description("User's ID"),
                                fieldWithPath("name").description("Name"),
                                fieldWithPath("surname").description("Surname"),
                                fieldWithPath("photo").description("Photo resource url"),
                                fieldWithPath("totalLikedRestaurants").description("Total number of saved restaurants"),
                                fieldWithPath("totalReservations").description("Total number of reservations"),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void likedUserRestaurants() throws Exception {
        mockMvc.perform(get("/api/users/100/saved_restaurants?projection=feed")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[0].id").value("1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("name1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.name").value("tag3"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.iconUrl").value("/files/9.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].averageTicket").value(400))
                .andExpect(jsonPath("$._embedded.restaurants[0].topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.name").value("cuisine1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.iconUrl").value("/files/10.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].totalReviews").value(4))
                .andExpect(jsonPath("$._embedded.restaurants[0].rating").value(4.5))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts.length()").value(2))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].quantity").value(5))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[0].discount").value(90))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[1].quantity").value(10))
                .andExpect(jsonPath("$._embedded.restaurants[0].discounts[1].discount").value(20))
                .andExpect(jsonPath("$._embedded.restaurants[0].latitude").value(1.33))
                .andExpect(jsonPath("$._embedded.restaurants[0].longitude").value(2.00))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerTitle").value("10%"))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerName").value("discount"))
                .andExpect(jsonPath("$._embedded.restaurants[0].markerUrl").value("/files/29.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[0].openHours").exists())
                .andExpect(jsonPath("$._embedded.restaurants[0].saved").value(Boolean.TRUE))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.self.href").value("http://localhost/api/restaurants/1"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.restaurant.href").value("http://localhost/api/restaurants/1{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.promotions.href").value("http://localhost/api/restaurants/1/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.owners.href").value("http://localhost/api/restaurants/1/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.happyHours.href").value("http://localhost/api/restaurants/1/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.topPicture.href").value("http://localhost/api/restaurants/1/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.cuisines.href").value("http://localhost/api/restaurants/1/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.pictures.href").value("http://localhost/api/restaurants/1/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.menu.href").value("http://localhost/api/restaurants/1/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.analogous.href").value("http://localhost/api/restaurants/1/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.marker.href").value("http://localhost/api/restaurants/1/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[0]._links.tags.href").value("http://localhost/api/restaurants/1/tags{?projection}"))

                .andExpect(jsonPath("$._embedded.restaurants[1].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[1].id").value("4"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("name4"))
                .andExpect(jsonPath("$._embedded.restaurants[1].tag").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].averageTicket").value(550))
                .andExpect(jsonPath("$._embedded.restaurants[1].topPictureUrl").value("/files/4.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[1].cuisine").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].totalReviews").value(1))
                .andExpect(jsonPath("$._embedded.restaurants[1].rating").value(1.0))
                .andExpect(jsonPath("$._embedded.restaurants[1].discounts").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[1].latitude").value(100.00))
                .andExpect(jsonPath("$._embedded.restaurants[1].longitude").value(3.00))
                .andExpect(jsonPath("$._embedded.restaurants[1].openHours").exists())
                .andExpect(jsonPath("$._embedded.restaurants[1].saved").value(Boolean.TRUE))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.self.href").value("http://localhost/api/restaurants/4"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.restaurant.href").value("http://localhost/api/restaurants/4{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.promotions.href").value("http://localhost/api/restaurants/4/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.owners.href").value("http://localhost/api/restaurants/4/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.happyHours.href").value("http://localhost/api/restaurants/4/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.topPicture.href").value("http://localhost/api/restaurants/4/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.cuisines.href").value("http://localhost/api/restaurants/4/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.pictures.href").value("http://localhost/api/restaurants/4/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.menu.href").value("http://localhost/api/restaurants/4/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.analogous.href").value("http://localhost/api/restaurants/4/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.marker.href").value("http://localhost/api/restaurants/4/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[1]._links.tags.href").value("http://localhost/api/restaurants/4/tags{?projection}"))

                .andExpect(jsonPath("$._embedded.restaurants.length()").value(3))
                .andExpect(jsonPath("$._embedded.restaurants[2].length()").value(17))
                .andExpect(jsonPath("$._embedded.restaurants[2].id").value("6"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("name6"))
                .andExpect(jsonPath("$._embedded.restaurants[2].tag").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].averageTicket").value(200))
                .andExpect(jsonPath("$._embedded.restaurants[2].topPictureUrl").value("/files/6.jpg"))
                .andExpect(jsonPath("$._embedded.restaurants[2].cuisine").isEmpty())
                .andExpect(jsonPath("$._embedded.restaurants[2].totalReviews").value(0))
                .andExpect(jsonPath("$._embedded.restaurants[2].rating").value(4.0))
                .andExpect(jsonPath("$._embedded.restaurants[2].discounts.length()").value(0))
                .andExpect(jsonPath("$._embedded.restaurants[2].latitude").value(1.20))
                .andExpect(jsonPath("$._embedded.restaurants[2].longitude").value(3.30))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerTitle").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerName").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].markerUrl").value(""))
                .andExpect(jsonPath("$._embedded.restaurants[2].openHours.length()").value(7))

                .andExpect(jsonPath("$._embedded.restaurants[2].saved").value(Boolean.TRUE))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.length()").value(12))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.self.href").value("http://localhost/api/restaurants/6"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.restaurant.href").value("http://localhost/api/restaurants/6{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.promotions.href").value("http://localhost/api/restaurants/6/promotions{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.owners.href").value("http://localhost/api/restaurants/6/owners{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.happyHours.href").value("http://localhost/api/restaurants/6/happy_hours{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.topPicture.href").value("http://localhost/api/restaurants/6/top_picture"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.cuisines.href").value("http://localhost/api/restaurants/6/cuisines{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.pictures.href").value("http://localhost/api/restaurants/6/pictures"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.menu.href").value("http://localhost/api/restaurants/6/menu"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.analogous.href").value("http://localhost/api/restaurants/6/analogous{?projection}"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.marker.href").value("http://localhost/api/restaurants/6/marker"))
                .andExpect(jsonPath("$._embedded.restaurants[2]._links.tags.href").value("http://localhost/api/restaurants/6/tags{?projection}"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/100/saved_restaurants?projection=feed"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("feed")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.restaurants[]").description("List of restaurants"),
                                fieldWithPath("_embedded.restaurants[].id").description("Restaurant ID"),
                                fieldWithPath("_embedded.restaurants[].name").description("Name"),
                                fieldWithPath("_embedded.restaurants[].tag").optional().description("First tag attached"),
                                fieldWithPath("_embedded.restaurants[].tag.iconUrl").optional().description("Url of the tag icon"),
                                fieldWithPath("_embedded.restaurants[].tag.name").description("Tag name"),
                                fieldWithPath("_embedded.restaurants[].averageTicket").description("Average bill"),
                                fieldWithPath("_embedded.restaurants[].topPictureUrl").description("Main picture url"),
                                fieldWithPath("_embedded.restaurants[].cuisine").optional().description("First cuisine"),
                                fieldWithPath("_embedded.restaurants[].cuisine.name").description("Cuisine name"),
                                fieldWithPath("_embedded.restaurants[].cuisine.iconUrl").optional().description("Url of the cuisine icon"),
                                fieldWithPath("_embedded.restaurants[].totalReviews").description("Number of reviews"),
                                fieldWithPath("_embedded.restaurants[].rating").description("Rating"),
                                fieldWithPath("_embedded.restaurants[].discounts[]").description("Discounts available"),
                                fieldWithPath("_embedded.restaurants[].discounts[].quantity").description("Quantity of discounts"),
                                fieldWithPath("_embedded.restaurants[].discounts[].discount").description("Discount %"),
                                subsectionWithPath("_embedded.restaurants[].discounts[]._links").ignored(),
                                fieldWithPath("_embedded.restaurants[].latitude").description("Latitude"),
                                fieldWithPath("_embedded.restaurants[].longitude").description("Longitude"),
                                fieldWithPath("_embedded.restaurants[].markerTitle").description("Marker title"),
                                fieldWithPath("_embedded.restaurants[].markerName").description("Marker name"),
                                fieldWithPath("_embedded.restaurants[].markerUrl").description("Marker icon url"),
                                fieldWithPath("_embedded.restaurants[].openHours[]").description("Open hours by weekday"),
                                fieldWithPath("_embedded.restaurants[].openHours[].weekday").description("Weekday number (Monday = 1)"),
                                fieldWithPath("_embedded.restaurants[].openHours[].openTime").description("Open time"),
                                fieldWithPath("_embedded.restaurants[].openHours[].closeTime").description("Close time"),
                                fieldWithPath("_embedded.restaurants[].saved").description("If this restaurant is added to the user's favourite list"),
                                subsectionWithPath("_embedded.restaurants[]._links").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void likedUserRestaurantsLocalized() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/users/100/saved_restaurants?projection=feed")
                .header(AUTHORIZATION, token)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$._embedded.restaurants[0].name").value("Ім`я1"))
                .andExpect(jsonPath("$._embedded.restaurants[0].tag.name").value("Тег3"))
                .andExpect(jsonPath("$._embedded.restaurants[0].cuisine.name").value("Кухня1"))
                .andExpect(jsonPath("$._embedded.restaurants[1].name").value("Ім`я4"))
                .andExpect(jsonPath("$._embedded.restaurants[2].name").value("Ім`я6"));
    }

    @Test
    public void editUser() throws Exception {
        MockMultipartFile file = new MockMultipartFile("data", "filename.jpg", "image/jpeg", "some bytes".getBytes(Charset.forName("UTF-8")));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/upload")
                .file(file)
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$._embedded.mediaFiles.length()").value(1))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].length()").value(5))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].name").isNotEmpty())
                .andExpect(jsonPath("$._embedded.mediaFiles[0].extension").value("jpg"))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].mediaType").value("image/jpeg"))
                .andExpect(jsonPath("$._embedded.mediaFiles[0].url").isNotEmpty())
                .andExpect(jsonPath("$._embedded.mediaFiles[0]._links.length()").value(1))
                .andExpect(jsonPath("$._embedded.mediaFiles[0]._links.self.href").value("http://localhost/api/files/31"))

                .andDo(document("user-flow/{class-name}/{method-name}/change-photo",
                        tokenHeadersSnippet,
                        requestParts(
                                partWithName("data").description("The media file to upload")),
                        responseFields(
                                fieldWithPath("_embedded.mediaFiles[]").description("Uploaded files"),
                                fieldWithPath("_embedded.mediaFiles[].name").description("Name"),
                                fieldWithPath("_embedded.mediaFiles[].extension").description("Extension"),
                                fieldWithPath("_embedded.mediaFiles[].mediaType").description("Media type"),
                                fieldWithPath("_embedded.mediaFiles[].url").description("Url"),
                                subsectionWithPath("_embedded.mediaFiles[]._links").ignored()
                        )))

                .andReturn();


        ConstrainedFields constrainedFields = new ConstrainedFields(User.class);

        String url = JsonPath.read(mvcResult.getResponse().getContentAsString(), "$._embedded.mediaFiles[0]._links.self.href");

        mockMvc.perform(patch("/api/users/100").content(userObject(url))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(7))
                .andExpect(jsonPath("$.name").value(NAME))
                .andExpect(jsonPath("$.surname").value(SURNAME))
                .andExpect(jsonPath("$.email").value("user100@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("381000000000"))
                .andExpect(jsonPath("$.status").value(ENABLE.toString()))
                .andExpect(jsonPath("$.addRestaurantRequest").isEmpty())
                .andExpect(jsonPath("$._links.length()").value(9))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/users/100"))
                .andExpect(jsonPath("$._links.user.href").value("http://localhost/api/users/100{?projection}"))
                .andExpect(jsonPath("$._links.photo.href").value("http://localhost/api/users/100/photo"))
                .andExpect(jsonPath("$._links.resent_user_password.href").value("http://localhost/api/user/password/reset"))
                .andExpect(jsonPath("$._links.reservations.href").value("http://localhost/api/users/100/reservations{?projection}"))
                .andExpect(jsonPath("$._links.savedRestaurants.href").value("http://localhost/api/users/100/saved_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.ownRestaurants.href").value("http://localhost/api/users/100/own_restaurants{?projection}"))
                .andExpect(jsonPath("$._links.roles.href").value("http://localhost/api/users/100/roles"))
                .andExpect(jsonPath("$._links.language.href").value("http://localhost/api/users/100/language{?projection}"))

                .andDo(document("user-flow/{class-name}/{method-name}/change-profile",
                        tokenHeadersSnippet,
                        requestFields(
                                constrainedFields.withPath("name").description("Name"),
                                constrainedFields.withPath("surname").description("Surname"),
                                fieldWithPath("photo").description("Photo resource url")
                        ),
                        responseFields(
                                fieldWithPath("name").description("Name"),
                                fieldWithPath("surname").description("Surname"),
                                fieldWithPath("email").description("Email"),
                                fieldWithPath("phoneNumber").description("Phone number"),
                                fieldWithPath("status").description("Status"),
                                fieldWithPath("addRestaurantRequest").description("Request for adding a restaurant from the customer"),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    private String userObject(String url) throws JsonProcessingException {
        Map<String, Object> user = new HashMap<>();
        user.put("name", NAME);
        user.put("surname", SURNAME);
        user.put("photo", url);
        return new ObjectMapper().writeValueAsString(user);
    }
}
