package com.stol.application.userflow;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stol.application.common.ApplicationSystemTest;
import com.stol.restdocs.PagingRequestParameters;
import com.stol.restdocs.PagingResponseFields;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.stol.model.property.ReservationStatus.CANCELED;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationsListPageTest extends ApplicationSystemTest {

    @Test
    public void findNearestReservationsForLoggedinUser() throws Exception {
        final String id = "4";

        mockMvc.perform(get("/api/reservations/search/forthcoming?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(8))
                .andExpect(jsonPath("$._embedded.reservations[1].id").value(3))
                .andExpect(jsonPath("$._embedded.reservations[1].time").value("10:00"))
                .andExpect(jsonPath("$._embedded.reservations[1].comment").value("Some comment3"))
                .andExpect(jsonPath("$._embedded.reservations[1].date").value("2089-10-21"))
                .andExpect(jsonPath("$._embedded.reservations[1].status").value("NEW"))
                .andExpect(jsonPath("$._embedded.reservations[1].guestNumber").value(3))
                .andExpect(jsonPath("$._embedded.reservations[1].topPictureUrl").value("/files/6.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[1].restaurantAddress").value("address6"))
                .andExpect(jsonPath("$._embedded.reservations[1].restaurantName").value("name6"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.self.href").value("http://localhost/api/reservations/3"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.reservation.href").value("http://localhost/api/reservations/3{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.client.href").value("http://localhost/api/reservations/3/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[1]._links.restaurant.href").value("http://localhost/api/reservations/3/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2].id").value(id))
                .andExpect(jsonPath("$._embedded.reservations[2].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[2].comment").value("Some comment4"))
                .andExpect(jsonPath("$._embedded.reservations[2].date").value("2089-10-22"))
                .andExpect(jsonPath("$._embedded.reservations[2].status").value("CONFIRMED"))
                .andExpect(jsonPath("$._embedded.reservations[2].guestNumber").value(2))
                .andExpect(jsonPath("$._embedded.reservations[2].topPictureUrl").value("/files/5.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[2].restaurantAddress").value("address5"))
                .andExpect(jsonPath("$._embedded.reservations[2].restaurantName").value("name5"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.self.href").value("http://localhost/api/reservations/4"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.reservation.href").value("http://localhost/api/reservations/4{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.client.href").value("http://localhost/api/reservations/4/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.restaurant.href").value("http://localhost/api/reservations/4/restaurant{?projection}"))
                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("time")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.reservations[]").description("List of reservations"),
                                fieldWithPath("_embedded.reservations[].id").description("Reservation's ID"),
                                fieldWithPath("_embedded.reservations[].date").description("Date"),
                                fieldWithPath("_embedded.reservations[].time").description("Time"),
                                fieldWithPath("_embedded.reservations[].guestNumber").description("Number of guests"),
                                fieldWithPath("_embedded.reservations[].comment").description("Сomment to the reservation"),
                                fieldWithPath("_embedded.reservations[].status").description("Status"),
                                fieldWithPath("_embedded.reservations[].restaurantName").description("Restaurant's name"),
                                fieldWithPath("_embedded.reservations[].restaurantAddress").description("Restaurant's address"),
                                fieldWithPath("_embedded.reservations[].topPictureUrl").description("Restaurant's top picture url"),
                                fieldWithPath("_embedded.reservations[]._links.self.href").description("Reservation resource url"),
                                subsectionWithPath("_embedded.reservations[]._links.reservation").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.client").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.restaurant").ignored(),
                                subsectionWithPath("_links").ignored()
                        )));

        mockMvc.perform(patch("/api/reservations/{id}", id)
                .content(String.format("{\"status\": \"CONFIRMED\", \"date\": \"%s\"}", LocalDate.now().plusDays(1L).toString()))
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8));

        mockMvc.perform(get("/api/reservations/search/forthcoming?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(8))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.self.href").value(containsString(id)));

        mockMvc.perform(patch("/api/reservations/{id}", id).content("{\"status\": \"REJECTED\"}")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8));

        mockMvc.perform(get("/api/reservations/search/forthcoming?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(8))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.self.href").value(containsString(id)));

        mockMvc.perform(patch("/api/reservations/{id}", id).content("{\"status\": \"CANCELED\"}")
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8));

        mockMvc.perform(get("/api/reservations/search/forthcoming?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(8))
                .andExpect(jsonPath("$._embedded.reservations[2]._links.self.href").value(containsString(id)));
    }

    @Test
    public void findNearestReservationsForLoggedinUserLocalized() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/reservations/search/forthcoming?projection=time")
                .header(AUTHORIZATION, token))
                .andExpect(jsonPath("$._embedded.reservations[1].restaurantAddress").value("адреса6"))
                .andExpect(jsonPath("$._embedded.reservations[1].restaurantName").value("Ім`я6"))
                .andExpect(jsonPath("$._embedded.reservations[2].restaurantAddress").value("адреса5"))
                .andExpect(jsonPath("$._embedded.reservations[2].restaurantName").value("Ім`я5"));
    }

    @Test
    public void findPassedReservationsForLoggedinUser() throws Exception {
        mockMvc.perform(get("/api/reservations/search/past?projection=reviewed")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations.length()").value(3))
                .andExpect(jsonPath("$._embedded.reservations[0].id").value(9))
                .andExpect(jsonPath("$._embedded.reservations[0].review").isNotEmpty())
                .andExpect(jsonPath("$._embedded.reservations[0].time").value("09:30"))
                .andExpect(jsonPath("$._embedded.reservations[0].comment").value("Some comment9"))
                .andExpect(jsonPath("$._embedded.reservations[0].date").value("2000-10-22"))
                .andExpect(jsonPath("$._embedded.reservations[0].status").value("CONFIRMED"))
                .andExpect(jsonPath("$._embedded.reservations[0].guestNumber").value(1))
                .andExpect(jsonPath("$._embedded.reservations[0].topPictureUrl").value("/files/4.jpg"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantAddress").value("address4"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantName").value("name4"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.length()").value(4))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.self.href").value("http://localhost/api/reservations/9"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.reservation.href").value("http://localhost/api/reservations/9{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.client.href").value("http://localhost/api/reservations/9/client{?projection}"))
                .andExpect(jsonPath("$._embedded.reservations[0]._links.restaurant.href").value("http://localhost/api/reservations/9/restaurant{?projection}"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size,
                                parameterWithName("projection").description("time")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.reservations[]").description("List of reservations"),
                                fieldWithPath("_embedded.reservations[].id").description("Reservation's ID"),
                                fieldWithPath("_embedded.reservations[].date").description("Date"),
                                fieldWithPath("_embedded.reservations[].time").description("Time"),
                                fieldWithPath("_embedded.reservations[].guestNumber").description("Number of guests"),
                                fieldWithPath("_embedded.reservations[].comment").description("Comment to the reservation"),
                                fieldWithPath("_embedded.reservations[].status").description("Status"),
                                fieldWithPath("_embedded.reservations[].restaurantName").description("Restaurant's name"),
                                fieldWithPath("_embedded.reservations[].restaurantAddress").description("Restaurant's address"),
                                fieldWithPath("_embedded.reservations[].topPictureUrl").description("Restaurant's top picture url"),
                                fieldWithPath("_embedded.reservations[].review").optional().description("Review for this reservation. Can be empty."),
                                fieldWithPath("_embedded.reservations[].review.rating").description("Review's rating for this reservation"),
                                fieldWithPath("_embedded.reservations[].review.comment").description("Review's comment for this reservation"),
                                fieldWithPath("_embedded.reservations[].review.creationDate").description("Review's creation date"),
                                fieldWithPath("_embedded.reservations[]._links.self.href").description("Reservation resource url"),
                                subsectionWithPath("_embedded.reservations[]._links.reservation").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.client").ignored(),
                                subsectionWithPath("_embedded.reservations[]._links.restaurant").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void findPassedReservationsForLoggedinUserLocalized() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/reservations/search/past?projection=reviewed")
                .header(AUTHORIZATION, token))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantAddress").value("адреса4"))
                .andExpect(jsonPath("$._embedded.reservations[0].restaurantName").value("Ім`я4"));
    }

    @Test
    public void singleReservationOverview() throws Exception {
        mockMvc.perform(get("/api/reservations/2?projection=restaurant")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(13))
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.topPictureUrl").value("/files/1.jpg"))
                .andExpect(jsonPath("$.restaurantPictures.length()").value(2))
                .andExpect(jsonPath("$.restaurantPictures.[0]").value("/files/14.jpg"))
                .andExpect(jsonPath("$.restaurantPictures.[1]").value("/files/15.jpg"))
                .andExpect(jsonPath("$.restaurantName").value("name1"))
                .andExpect(jsonPath("$.restaurantAddress").value("address1"))
                .andExpect(jsonPath("$.restaurantLatitude").value(1.33))
                .andExpect(jsonPath("$.restaurantLongitude").value(2.00))
                .andExpect(jsonPath("$.time").value("09:30"))
                .andExpect(jsonPath("$.date").value("2017-10-21"))
                .andExpect(jsonPath("$.status").value("NEW"))
                .andExpect(jsonPath("$.guestNumber").value(2))
                .andExpect(jsonPath("$.comment").value("Some comment2"))
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/2"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reservations/2{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reservations/2/restaurant{?projection}"))
                .andExpect(jsonPath("$._links.client.href").value("http://localhost/api/reservations/2/client{?projection}"))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                parameterWithName("projection").description("restaurant")
                        ),
                        responseFields(
                                fieldWithPath("id").description("Reservation's ID"),
                                fieldWithPath("topPictureUrl").description("Restaurant's top picture url"),
                                fieldWithPath("restaurantPictures[]").description("Restaurant's pictures"),
                                fieldWithPath("restaurantName").description("Restaurant's name"),
                                fieldWithPath("restaurantAddress").description("Restaurant's address"),
                                fieldWithPath("restaurantLatitude").description("Restaurant's latitude"),
                                fieldWithPath("restaurantLongitude").description("Restaurant's longitude"),
                                fieldWithPath("date").description("Reservation date"),
                                fieldWithPath("time").description("Reservation time"),
                                fieldWithPath("guestNumber").description("Number of guests"),
                                fieldWithPath("comment").description("Comment to the reservation"),
                                fieldWithPath("status").description("Reservation status"),
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void singleReservationOverviewLocalized() throws Exception {
        changeLanguageToUk();

        mockMvc.perform(get("/api/reservations/2?projection=restaurant")
                .header(AUTHORIZATION, token))
                .andExpect(jsonPath("$.restaurantName").value("Ім`я1"))
                .andExpect(jsonPath("$.restaurantAddress").value("адреса1"));
    }

    @Test
    public void userReviewForRestaurant() throws Exception {
        mockMvc.perform(get("/api/reviews/search/my?restaurant=http://localhost:8080/api/restaurants/1")
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$._embedded.reviews.length()").value(1))
                .andExpect(jsonPath("$._embedded.reviews[0].rating").value(4))
                .andExpect(jsonPath("$._embedded.reviews[0].comment").value("Comment6"))
                .andExpect(jsonPath("$._embedded.reviews[0].creationDate").value("2017-10-22T15:40:08"))
                .andExpect(jsonPath("$._embedded.reviews[0]._links.length()").value(5))
                .andExpect(jsonPath("$._embedded.reviews[0]._links.self.href").value("http://localhost/api/reviews/100"))
                .andExpect(jsonPath("$._embedded.reviews[0]._links.review.href").value("http://localhost/api/reviews/100{?projection}"))
                .andExpect(jsonPath("$._embedded.reviews[0]._links.user.href").value("http://localhost/api/reviews/100/user{?projection}"))
                .andExpect(jsonPath("$._embedded.reviews[0]._links.restaurant.href").value("http://localhost/api/reviews/100/restaurant{?projection}"))
                .andExpect(jsonPath("$._embedded.reviews[0]._links.reservation.href").value("http://localhost/api/reviews/100/reservation{?projection}"))
                .andExpect(jsonPath("$._links.length()").value(1))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reviews/search/my?restaurant=http://localhost:8080/api/restaurants/1&page=0&size=20"))
                .andExpect(jsonPath("$.page.length()").value(4))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(1))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0))

                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestParameters(
                                PagingRequestParameters.sort,
                                PagingRequestParameters.page,
                                PagingRequestParameters.size,
                                parameterWithName("restaurant").description("Restaurant resource url")
                        ),
                        responseFields(
                                fieldWithPath("_embedded.reviews[]").description("List of reviews"),
                                fieldWithPath("_embedded.reviews[].rating").description("Rating"),
                                fieldWithPath("_embedded.reviews[].comment").description("Client comment"),
                                fieldWithPath("_embedded.reviews[].creationDate").description("Creation date"),
                                subsectionWithPath("_embedded.reviews[]._links").ignored(),
                                PagingResponseFields.totalElements,
                                PagingResponseFields.totalPages,
                                PagingResponseFields.number,
                                PagingResponseFields.size,
                                subsectionWithPath("_links").ignored()
                        )));
    }

    @Test
    public void cancelMyReservation() throws Exception {
        String cancel = new ObjectMapper().writeValueAsString(Collections.singletonMap("status", CANCELED));

        mockMvc.perform(patch("/api/reservations/9").content(cancel)
                .header(AUTHORIZATION, token)
                .contentType(APPLICATION_JSON_UTF8)
                .accept(HAL_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(HAL_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(8))
                .andExpect(jsonPath("$.dateTime").value("2000-10-22T09:30:00"))
                .andExpect(jsonPath("$.guestNumber").value(1))
                .andExpect(jsonPath("$.comment").value("Some comment9"))
                .andExpect(jsonPath("$.notes").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.status").value(CANCELED.toString()))
                .andExpect(jsonPath("$.created").value("2000-10-19T14:42:08"))
                .andExpect(jsonPath("$.discount").isEmpty())
                .andExpect(jsonPath("$._links.length()").value(4))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/reservations/9"))
                .andExpect(jsonPath("$._links.reservation.href").value("http://localhost/api/reservations/9{?projection}"))
                .andExpect(jsonPath("$._links.client.href").value("http://localhost/api/reservations/9/client{?projection}"))
                .andExpect(jsonPath("$._links.restaurant.href").value("http://localhost/api/reservations/9/restaurant{?projection}"))
                .andDo(document("user-flow/{class-name}/{method-name}",
                        tokenHeadersSnippet,
                        requestFields(
                                fieldWithPath("status").description("CANCELED")
                        ),
                        responseFields(
                                fieldWithPath("dateTime").description("Reservation date and time"),
                                fieldWithPath("comment").description("Client comment"),
                                fieldWithPath("notes").ignored(),
                                fieldWithPath("guestNumber").description("Number of guests"),
                                fieldWithPath("status").description("Reservation status"),
                                fieldWithPath("discount").description("Discount for the client if any"),
                                fieldWithPath("created").description("Reservation creation date"),
                                subsectionWithPath("_links").ignored()
                        )
                ));
    }
}
