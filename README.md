**The Stol API**
===================
**Create an environment**
1) Create Ubuntu 18.10 x64 droplet
2) Setting up a Basic Firewall (if needed) https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04
3) Install Mysql https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04
3.1) For Workbench see https://stackoverflow.com/a/53487418/1437967
3) Run 'db/migration/create_schema.sql'
4) Install Docker https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04
5) Install nginx https://gitlab.com/stol_team/api/wikis/Development/Nginx-setup

-------------
